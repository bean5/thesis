#!/bin/bash

input=""
if [[ ${use_previous_model} == true ]]; then
	echo "Reuse previous model: true"
	input="--input-model /output/model.bin"
else
	echo "Reuse previous model: false"
	input="--input /input/index.mallet"
fi

if [[ ${use_ngrams} == true ]]; then
	echo "allow ngrams: true"
	$mallet_bin_path train-topics ${input} --use-ngrams --num-topics $num_topics --num-iterations $num_iterations --output-doc-topics /output/output-doc-topics.out --output-topic-keys /output/output-topic-keys.out --output-state /output/topic-state.gz --output-model /output/model.bin --xml-topic-report /output/topic-report.xml --optimize-interval $optimize_interval --optimize-burn-in $optimize_burn_in
else
	echo "allow ngrams: false"
	$mallet_bin_path train-topics ${input} --num-topics $num_topics --num-iterations $num_iterations --output-doc-topics /output/output-doc-topics.out --output-topic-keys /output/output-topic-keys.out --output-state /output/topic-state.gz --output-model /output/model.bin --xml-topic-report /output/topic-report.xml --optimize-interval $optimize_interval --optimize-burn-in $optimize_burn_in
fi
