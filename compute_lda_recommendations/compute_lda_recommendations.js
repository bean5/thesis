//Uses the HellingerDistance
{
	var fs = require('fs');
	var malletReader = require('/lib/node_utils/malletReader.js');
	var statistics = require('/lib/node_utils/statistics.js');
	var common = require('/lib/node_utils/common.js');

	var testing = process.env.testing === 'true' ? true : false;
	var number_of_recommendations = parseInt(process.env.number_of_recommendations, 10);
	var lda_doc_topics = process.env.lda_doc_topics;

	var distributions = [];
	var recommendations = {};

	var outputLocation = '/output/';

	// Paths to files
	var paths = {
		todo: outputLocation + 'todo.txt',
		status: outputLocation + 'status.txt',
		settings: outputLocation +'settings.json',
		debug: outputLocation +'debug.txt',
		recommendations: outputLocation +'recommendations.json',
		catalogCoverage: outputLocation +'catalogCoverage.json',
	};

	var streams = {
		todo: fs.createWriteStream(paths.todo),
		status: fs.createWriteStream(paths.status),
		settings: fs.createWriteStream(paths.settings),
		debug: fs.createWriteStream(paths.debug),
		recommendations: fs.createWriteStream(paths.recommendations),
		catalogCoverage: fs.createWriteStream(paths.catalogCoverage),
	};

	if(testing) fs.appendFileSync(paths.todo, 'TODO: Do not run in test mode!');
}

main();

/*
*	Functions
*/
function main() {
	//read in lines of input 1
	distributions = malletReader.readInDistributions(lda_doc_topics, testing ? 100 : undefined, false);

	// streams.debug.write("%j", distributions);

	computeRecommendations(computeHellingerDistance);
	// computeRecommendations(computeJSDivergence);

	streams.recommendations.write(JSON.stringify(recommendations, null, '\t'));

	common.each(streams, function(name, stream) {
		stream.end();
	});
}

function computeRecommendations(distanceMetric) {
	for(var i = 0; i < distributions.length; i++) {
		var leftDocumentID = distributions[i].fileName;
		recommendations[leftDocumentID] = {};

		if (i % 10 === 0) fs.appendFileSync(paths.status, JSON.stringify({'Now at ': i, 'of': distributions.length}, null, '\t'));

		var dist = 1;
		for(var j = 0; j < distributions.length; j++) {
			if(!testing && i === j) continue;//don't allow self recommandations.
			var rightDocumentID = distributions[j].fileName;

			// Compute distance and store
			recommendations[leftDocumentID][rightDocumentID] = (j !== i) ? distanceMetric(distributions[i].topicDistribution, distributions[j].topicDistribution) : 1;
		}

		// fs.appendFileSync(paths.todo, 'Make sure that passing in true is the correct thing to do.');
		recommendations[leftDocumentID] = common.keepBestK(recommendations[leftDocumentID], number_of_recommendations, true);
	}
}

function computeHellingerDistance(left, right) {
	//console.log('test HellingerDistance()');
	var total = 0;
	for(var i = 0; i < left.length; i++) {
		if(left[i] === right[i]) continue;
		total += Math.pow(Math.sqrt(left[i]) - Math.sqrt(right[i]), 2);
	}
	//console.log('HellingerDistance: ' + total);
	return Math.sqrt(total)/Math.sqrt(2);
}

function computeJSDivergence(left, right) {
	var midPoint = computeMidPoint(left, right);
	return (computeKLDivergence(left, midPoint) +
		computeKLDivergence(right, midPoint))/2;
}

function computeMidPoint(left, right) {
	//console.log('mid' + mid);
	return statistics.computeMidPoint(left, right);
}

//return KLDivergence of two points in the probability simplex
function computeKLDivergence(leftDistribution, rightDistribution) {
	//console.log('test KL()');
	return statistics.computeKLDivergence(leftDistribution, rightDistribution);
}
