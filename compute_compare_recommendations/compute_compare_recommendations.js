//globals
{
	var fs = require('fs');
	var common = require('/lib/node_utils/common.js');

	var testing = process.env.testing === 'true' ? true : false;

	var filePaths = [
		process.env.recommendation_set_1, // e.g. tf-idf
		process.env.recommendation_set_2, // e.g. lda
	];

	var outputLocation = '/output/';

	var results = [
		{
			fileName: filePaths[0],
			coverages: [],
			serendipity: null,
			nDCG: null,
			gain: null,
		},
		{
			fileName: filePaths[1],
			coverages: [],
			serendipity: null,
			nDCG: null,
			gain: null,
		}
	];
	var number_of_recommendations = parseInt(process.env.number_of_recommendations, 10);

	// Paths to files
	var paths = {
		todo: outputLocation + 'todo.txt',
		// status: outputLocation + 'status.txt',//not really needed since this runs so fast
		settings: outputLocation + 'settings.json',//no settings to really consider
		debug: outputLocation + 'debug.txt',
		results: outputLocation + 'results.json',
	};

	var streams = {
		todo: fs.createWriteStream(paths.todo),
		// status: fs.createWriteStream(paths.status),//not really needed since this runs so fast
		settings: fs.createWriteStream(paths.settings),//no settings to really consider
		debug: fs.createWriteStream(paths.debug),
		results: fs.createWriteStream(paths.results),
	};
	streams.settings.write(JSON.stringify({testing: testing, number_of_recommendations: number_of_recommendations, paths: paths, filePaths: filePaths}, null, '\t'));

	if(testing) console.warn('Warning: running in test mode');
	if(testing) fs.appendFileSync(paths.todo, 'TODO: Do not run in test mode!');
}
main();

/*
	Functions
*/
function main() {
	var sets = [];
	streams.todo.write('TODO: Write spec tests.\n');
	streams.todo.write('TODO: Do the following asyncronously for larger datasets.\n');

	sets[0] = fs.readFileSync(filePaths[0], 'utf8', function (err, data) {
		if (err) common.halt(err);
	});
	sets[1] = fs.readFileSync(filePaths[1], 'utf8', function (err, data) {
		if (err) common.halt(err);
	});
	sets[0] = JSON.parse(sets[0]);
	sets[1] = JSON.parse(sets[1]);
	// streams.debug.write(JSON.stringify(sets[0], null, '\t'));
	// console.log(sets[0]);

	// coverage at particular k
	streams.todo.write('TODO: Consider using used_keys and their counts to compute typical spread\n');

	// coverages
	results[0].coverages = compute_coverages(sets[0]);
	results[1].coverages = compute_coverages(sets[1]);

	streams.debug.write(JSON.stringify({compute_coverages: results[0].coverages}));
	streams.debug.write(JSON.stringify({compute_coverages: results[1].coverages}));

	streams.todo.write('TODO: Modify serendipity metric to account for baseline. Measuring off each other will just yield identical values.\n');
	results[0].serendipity = compute_serendipity(sets[0], sets[1], number_of_recommendations);
	results[1].serendipity = compute_serendipity(sets[1], sets[0], number_of_recommendations);

	streams.debug.write(JSON.stringify({compute_serendipity: results[0].serendipity}));
	streams.debug.write(JSON.stringify({compute_serendipity: results[1].serendipity}));

	streams.todo.write('TODO: Modify nDCG metric to account for baseline. Measuring off each other will just yield identical values.\n');
	streams.todo.write('TODO: Consider computing nDCG at [1,k] instead of just [k,k]\n');
	results[0].nDCG = compare_results_with_metric(sets[0], sets[1], number_of_recommendations, metric_nDCG);
	results[1].nDCG = compare_results_with_metric(sets[1], sets[0], number_of_recommendations, metric_nDCG);

	streams.debug.write(JSON.stringify({compare_results_with_metric: results[0].nDCG}), number_of_recommendations);
	streams.debug.write(JSON.stringify({compare_results_with_metric: results[1].nDCG}), number_of_recommendations);

	streams.todo.write('TODO: Modify gain metric to account for baseline. Measuring off each other will just yield identical values.\n');results[0].gain = compare_results_with_metric(sets[0], sets[1], number_of_recommendations, metric_gain);
	results[1].gain = compare_results_with_metric(sets[1], sets[0], number_of_recommendations, metric_gain);

	streams.debug.write(JSON.stringify({compare_results_with_metric: results[0].gain}), number_of_recommendations);
	streams.debug.write(JSON.stringify({compare_results_with_metric: results[1].gain}), number_of_recommendations);

	streams.results.write(JSON.stringify(results, null, '\t'));

	common.each(streams, function(name, stream) {
		stream.end();
	});
}

function compute_coverages(set) {
	var coverages = [];

	var documents = Object.keys(set);
	var recommended = Object.keys(set[documents[0]]);
	var total_possible = recommended.length;

	var used_keys = {};
	var k = 0;
	coverages[k++] = 0;
	for(var i = 0; i < total_possible; i++) {
		k = i + 1;
		var coverage_at_k = compute_coverage(set, k);
		coverages.push(coverage_at_k);
	}
	return coverages;
}

function compute_coverage(set, k) {
	var used_keys = {};

	var documents = Object.keys(set);
	var total_possible = documents.length;

	for(var i = 0; i < total_possible; i++) {
		var recommended = Object.keys(set[documents[i]]);
		for(var j = 0; j < recommended.length && j < k; j++) {
			if(used_keys[recommended[j]] === undefined)
				used_keys[recommended[j]] = 0;
			used_keys[recommended[j]]++;
		}
	}
	streams.debug.write(JSON.stringify({recommended_count: used_keys}, null, '\t'));
	// console.warn(used_keys);
	return Object.keys(used_keys).length/total_possible;
}

function compute_serendipity(set1, set2, k) {
	return 0;
}

// These aren't helpful in the scenarios of this thesis.
function compare_results_with_metric(set1, set2, k, metric) {
	var result = 0;

	var documents = Object.keys(set1);
	var total_possible = documents.length;

	for(i = 0; i < total_possible; i++) {
		var keys = [
			Object.keys(set1[documents[i]]).splice(0, k),
			Object.keys(set2[documents[i]]).splice(0, k),
		];
		result += metric(keys[0], keys[1], k);
	}
	return result;
}

function metric_nDCG(set1, set2, k) {
	return 1;
}

function metric_gain(set1, set2, k) {
	//make sure to only consider the top k of each to be fair
	var total = 0;
	var possible = set2.length;

	for(var i = 0; i < k; i++) {
		total += is_in_list(set1[i], set2, k) ? 1 : 0;
	}

	return total/possible;
}

function is_in_list(value, set, k) {
	for(var i = 0; i < k && i < set.length; i++) {
		if(set[i] == value) return true;
	}

	return false;
}
