TODO (Not required for thesis, but nice to have for future work)
---
* compute_tf_idf_recommendations:
	* Decrease docker VM memory allocation or remove it altogether--if possible (the `--max-old-space-size=` part)
	* Make TF-IDF more efficient--or replace it with another TF-IDF engine that is faster
* compute_clean_and_normalize:
	* Add option to remove all non-alphanumerics in compute_clean_and_normalize
	* Add option to remove all numerics in compute_clean_and_normalize
