#!/bin/bash

# Build index
echo "Building the mallet index."
if [[ ${use_bigrams} == true ]]; then
	echo "Maintain bigrams: true"
	sequence_type="--keep-sequence-bigrams"
else
	echo "Maintain bigrams: false"
	sequence_type="--keep-sequence"
fi

$mallet_bin_path import-dir \
	--input /input/ \
	--output /output/index.mallet \
	${sequence_type} \
	--stoplist-file ./stoplist-custom.txt

# Split data if requested
if [[ ${split_data} == "true" ]]; then
	echo "Splitting data into test and validation portions."
	$mallet_path split --input /input/index.mallet --training-file /input/index-train.mallet --testing-file /input/index-test.mallet --training-portion 1 #.90;# --validation-portion .05
fi
