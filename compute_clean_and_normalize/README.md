Clean + Normalize (Currently: remove extra HTML from LDS GC talks)
---
The purpose of this module is to remove boilerplate HTML code. In place of this, code for normalizing should be used. This has some historical problems because it depends on jQuery working on backend code. To do this at the time, you had to have jsdom. It looks like this is deprecated and the only way to make it work is to keep the versions of node_modules here rather than using the typical `npm install` route.

# TODO
* Consider using justText for a first run of filtering
* Combine `convert.js` and `convertRaw.js`
* Factor normalization out into another module (slower, but more maintainable+modular)
* Instead of hard-coding in pre-downloaded versions of node_modules, use `npm install`. Make sure to avoid running into the jquery code that emits errors, `swfobject.embedSWF("/resources/swf/ldsUniversalPlayer.swf", "ldsUniver`, although it does appear to be harmless (see `talk7205.html`):
	* remove node_modules/ volume
	* uncomment `#RUN npm install`
	* or: simply scrap this approach altogether and use perl
