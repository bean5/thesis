var overwrite = true || false;

var jsdom = require("jsdom");
$ = require("jquery")(jsdom.jsdom().parentWindow);
var fs = require("fs");

var htmlFilesLocation =  "/input/files/";
var convertedLocation = "/output/";

console.warn("The following replace may no longer be necessary");
convertFile(process.argv[2].replace(htmlFilesLocation, ""));

function convertFile(fileName) {
	console.log("filename: " + fileName);

	if(overwrite) {
		readIn(fileName);
	}
	else {
		fs.exists(convertedLocation + fileName, function(exists) {
			if(!exists) {
				readIn(fileName);
			}
			else {} // skip
		});
	}
}

function readIn(fileName) {
	fs.readFile(htmlFilesLocation + fileName, "utf8", function (err,data) {
		if (err) {
			console.log(err);
		}
		convertData(fileName, data);
	});
}

function convertData(fileName, data) {
	// load HTML
	// Start with a fairly empty document, and add things into it (using implied body doesn't work well)
	$("body").html("<div id='temp-element'>");

	$("#temp-element").html(data);

	// remove meta-data
	$("p.credit, p.gcbib, p.gcspeaker, gcspkpos, p.gcspkpos2, p.gctitle").remove();
	$("div.gchead").remove();

	htmlFindReplace(/(\u00a0|&nbsp;)/gi, " ");

	// remove images
	$("#temp-element").find("img").remove();

	htmlFindReplace(/Retrieved from LDS.org[a-z\-\:\d\.\s©&;]*/gi, " ");

	htmlFindReplace("; italics added", "");

	// remove word links
	anchors = $("a.nolink");
	for(var i = anchors.length - 1; i >= 0; i--) {
		$(anchors[i]).replaceWith($(anchors[i]).html());
	}

	anchors = $("a");

	// replace anchor tags with contents
	for(var i = anchors.length - 1; i >= 0; i--) {
		if($(anchors[i]).attr("href").match(/^getscrip.*/gi)) {
			// $(anchors[i]).replaceWith($(anchors[i]).html());
			$(anchors[i]).remove();
		}
		else if($(anchors[i]).attr("href").match(/^javascript.*/gi)) {
			// $(anchors[i]).replaceWith($(anchors[i]).html());
			$(anchors[i]).remove();
		}
		else if($(anchors[i]).attr("href").match(/^sync.*/gi)) {
			// $(anchors[i]).replaceWith($(anchors[i]).html());
			$(anchors[i]).remove();
		}
	}

	htmlFindReplace(/&nbsp;/i, " ");

	htmlFindReplace(/\(\s*(; italics added)?\s*\.\s*\)/gi, " ");

	htmlFindReplace(/\([\s\.]*\)/gi, " ");

	$("#details").replaceWith(" ");

	newHTML = $("#temp-element").html().toLowerCase();

	$("#temp-element").html(newHTML);

	$("#temp-element").find('#video-player').remove();
	$("#temp-element").find('head').remove();
	$("#temp-element").find('title').remove();
	$("#temp-element").find('meta').remove();
	$("#temp-element").find('link').remove();
	$("#temp-element").find('script').remove();
	$("#temp-element").find('span').remove();
	$("#temp-element").find(':hidden').remove();
	$("#temp-element").find(':hidden').remove();

	// remove comments
	$("#temp-element").find('*').contents().each(function() {
		if(this.nodeType == 8) {
			$(this).remove();
		}
	});

	// remove style="", etc.
	// htmlFindReplace(/style=".*?"/gi, "");
	htmlFindReplace(/[a-z]+\s*=\s*['"].*?['"]/gi, "");
	// htmlFindReplace(/[a-z]+\s*=\s*[a-z,0-9]/gi, "");

	// htmlFindReplace(/(<[\/]?[^p]([a-z0-9\-\s='"\/\.]*)>)/gi, " ");

	// References such as (<i>Times and Seasons</i>, Vol....)
	htmlFindReplace(/\(.*?<i>.*?<\/i>.*\)/gi, " ");
	htmlFindReplace(/\(.*(p\.|vol\.).*\)/gi, " ");

	// remove italics/strongs
	htmlFindReplace(/<\s*i.*?>(.*?)<\/\s*i\s*>/gi, " $1 ");
	$('br').remove();
	$('footer').remove();
	htmlFindReplace(/<em.*?>(.*?)<\/em>/gi, " $1 ");
	htmlFindReplace(/<i.*?>(.*?)<\/i>/gi, " $1 ");
	htmlFindReplace(/<\s*b.*?>(.*?)<\/b>/gi, " $1 ");
	htmlFindReplace(/<strong.*?>(.*?)<\/strong>/gi, " $1 ");
	htmlFindReplace(/<blockquote.*?>(.*?)<\/blockquote>/gi, " $1 ");
	htmlFindReplace(/<sup.*?>(.*?)<\/sup>/gi, " $1 ");
	htmlFindReplace(/<sub.*?>(.*?)<\/sub>/gi, " ");

	// empty references
	htmlFindReplace(/\(\s*see\s*;?\s*\)/gi, " ");
	htmlFindReplace(/\(\s*\)/gi, " ");

	// remove any tag, except p
	htmlFindReplace(/<\s*^[p][a-z]+\s*>(.*?)<\/\s*^[p][a-z]+\s*>/gi, " $1 ");
	htmlFindReplace(/<\s*^[p][a-z]+\s*>/gi, " ");
	htmlFindReplace(/<\/\s*^[p][a-z]+\s*>/gi, " ");
	htmlFindReplace(/<\/s*?^[p][a-z]+\s*\/>/gi, " ");

	// replace don't, we'll, he's, etc.
	htmlFindReplace(/'t /gi, "t ");
	htmlFindReplace(/'s /gi, "s ");
	htmlFindReplace(/'re /gi, "re ");
	htmlFindReplace(/'ll /gi, "ll ");

	htmlFindReplace(/\s+/gi, " ");
	htmlFindReplace(/(.*)/gi, "<blockquote>$1</blockquote>");
	// htmlFindReplace(/(<[\/]?p([a-z0-9\-\s='"\/\.]*)>)/gi, "<br />");
	// htmlFindReplace(/(<[\/]?([a-z0-9\-\s='"\/\.]*)>)/gi, "\r\n");
	// htmlFindReplace(/\s+/gi, " ");

	// put space between paragraphs
	// htmlFindReplace(/<\/p><p>/gi, "<\p>\n<p>");

	// get array of paragraphs
	paragraphs = $("p");

	// remove (most) punctuation
	contents = "";
	for(i = 0; i < paragraphs.length; i++) {
		// console.log($(paragraphs[i]));
		// console.log($(paragraphs[i]).html());
		// console.log(paragraphs[i]);
		innerds = $(paragraphs[i]).html();
		words = innerds.split(/\s/);
		for(j = 0; j < words.length; j++) {
			words[j] = words[j].replace(/[\.,-\/#!$%\^&\*;:{}=\-—\?_'"“”`~()]/g, " ");
			if(words[j].indexOf("<") > -1) {
				console.log("doc contains html: " + words[j]);
				console.log(innerds);
				var returnOnError = false;
				if(returnOnError)
					return;
				words[j] = words[j].replace(/<.*?>/gi, "");
				words[j] = words[j].replace(/<\/.*?>/gi, "");
				words[j] = words[j].replace(/<.*?\/>/gi, "");
				words[j] = words[j].replace(/</gi, "");
				words[j] = words[j].replace(/>/gi, "");
			}
			contents += " " + words[j];
		}
		contents = contents.replace(/\s+/g, " ");
		contents = contents.replace(/(\d)\s+(\d)/g, "$1$2");
		// console.log(words);
	}

	writeData(fileName, contents.trim());
}

function writeData(fileName, data) {
	writeLoc = convertedLocation + fileName;
	console.log("writing to: " + writeLoc);
	// console.log("data: " + data);
	fs.writeFile(writeLoc, new Buffer(data.toString("binary")), function(err) {
		if (err) {
			console.log(err);
			throw err;
		}
		else {
			console.log("The file was saved!");
		}
	});
}

function htmlFindReplace(re, replace) {
	newHTML = $("#temp-element").html().replace(re, replace);

	$("#temp-element").html(newHTML);
}

function sanityWriteCheck() {
	// sanity check for writing
	writeLoc = "test.txt";
	data = "Hey there!";
	fs.writeFile(writeLoc, new Buffer(data.toString("binary")), function(err) {
		if (err) {
			console.log(err);
			throw err;
		} else {
			console.log("The file was saved!");
		}
	});
}
