#!/bin/bash

run_chunk() {
	for arg in $*
	do
		for i in /input/files/talk$arg*.html
		do
			# Make sure file exists (sometimes $i ends up being talk1*.html or some such thing)
			if [[ -e $i ]]; then
				nodejs convert.js $i
			fi
		done

		for i in /input/files-remote/talk$arg*.html
		do
			# Make sure file exists (sometimes $i ends up being talk1*.html or some such thing)
			if [[ -e $i ]]; then
				nodejs convertRaw.js $i
			fi
		done
	done
}

# Allows cleaning to run somewhat in parallel (up to 9 at a time)
for i in {1..9}
do
	run_chunk $i &
done
wait
