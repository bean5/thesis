<?xml version="1.0" encoding="UTF-8"?>
<div class="gcera">
<div class="gchead">
<p class="gctitle">The Disease of Profanity</p>
<p class="gcspeaker">Elder Boyd K. Packer</p>
<p class="gcspkpos">Assistant to the Council of the Twelve</p>
<p class="gcbib">Boyd K. Packer, <i>Conference Report</i>, October 1967, pp. 126-129</p>
</div>
<div class="gcbody">
<p>
I ask, my brethren and sisters, for an interest in your faith and prayers as I continue a theme
introduced by President Joseph Fielding Smith in his remarks.
</p><p>
A number of years ago I went with a brother to tow in a wrecked car. It was a single car
accident, and the car was demolished; the driver, though unhurt, had been taken to the
hospital for treatment of shock and for examination.
</p><p>
<b>The havoc of profanity</b>
</p><p>
The next morning he came asking for his car, anxious to be on his way. When he was shown
the wreckage, his pent-up emotions and disappointment, sharpened perhaps by his misfortune,
exploded in a long stream of profanity. So obscene and biting were his words that they
exposed years of practice with profanity. His words were heard by other customers, among
them women, and must have touched their ears like acid.
</p><p>
One of my brothers crawled from beneath the car, where he had been working with a large
wrench. He too was upset, and with threatening gestures of the wrench (mechanics will know
that a 16-inch crescent wrench is a formidable weapon), he ordered the man off the premises.
"We don't have to listen to that kind of language here," he said. And the customer left,
cursing more obscenely than before.
</p><p>
Much later in the day he reappeared, subdued, penitent, and avoiding
everyone else; he found my brother.
</p><p>
"I have been in the hotel room all day," he said, "lying on the bed tormented. I can't tell you
how utterly ashamed I am for what happened this morning. My conduct was inexcusable. I
have been trying to think of some justification, and I can think of only one thing. In all my
life, never, not once, have I been told that my language was not acceptable. I have always
talked that way. You were the first one who ever told me that my language was out of order."
</p><p>
Isn't it interesting that a man could grow to maturity, the victim of such a vile habit, and
never meet a protest? How tolerant we have become, and how quickly we are moving. A
generation ago writers of newspapers, editors of magazines, and particularly the producers of
motion pictures, carefully censored profane and obscene words.
</p><p>
All that has now changed. It began with the novel. Writers, insisting that they must portray
life as it is, began to put into the mouths of their characters filthy, irreverent expressions.
These words on the pages of books came before the eyes of all ages and imprinted themselves
on the minds of our youth.
</p><p>
Carefully (we are always led carefully), profanity has inched and nudged and pushed its way
relentlessly into the motion picture and the magazine, and now even newspapers print
verbatim comments, the likes of which would have been considered intolerable a generation
ago.
</p><p>
"Why not show life as it is?" they ask. They even say it is hypocritical to do otherwise. "If it
is real," they say, "why hide it? You can't censor that which is real!"
</p><p>
Why hide it? Why protest against it? Many things that are <i>real</i> are not <i>right</i>. Disease germs
are real, but must we therefore spread them? A pestilent infection may be real, but ought we
to expose ourselves to it? Those who argue that so-called "real life" is license must remember
that where there's an <i>is</i>, there's an <i>ought</i>. Frequently, what is and what ought to be are far
apart. When <i>is</i> and <i>ought</i> come together, an ideal is formed. The reality of profanity does not
argue for the toleration of it.
</p><p>
Like the man in the shop, many of us may never have been told how serious an offense
profanity can be. Ere we know it we are victims of a vile habit&mdash;and the servant to our
tongue. The scriptures declare: 
</p><p>
<b>Controls for discipline</b>
</p><p>
"Behold, we put bits in the horses' mouths, that they may obey us; and we turn about their
whole body.
</p><p>
"Behold also the ships, which though they be so great, and are driven of fierce winds, yet are
they turned about with a very small helm, whithersoever the governor listeth.
</p><p>
"Even so the tongue is a little member, and boasteth great things . . .
</p><p>
"For every kind of beasts, and of birds, and of serpents, and of things in the sea, is tamed,
and hath been tamed of mankind: 
</p><p>
"But the tongue can no man tame; it is an unruly evil, full of deadly poison.
</p><p>
"Therewith bless we God, even the Father; and therewith curse we men, which are made after
the similitude of God.
</p><p>
"Out of the same mouth proceedeth blessing and cursing. My brethren, these things ought not
so to be" (<span class="citation" id="26208"><a href="javascript:void(0)" onclick="sx(this, 26208)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(26208)">James 3:3-5,7-10</a></span>).
</p><p>
<b>Habit patterns for discipline</b>
</p><p>
There is something on this subject I would tell young people who are forming the habit
patterns of their lives. Take, for example, the young athlete and his coach. I single out the
coach, for to him, as to few others, a boy will yield his character to be molded.
</p><p>
Young athlete, it is a great thing to aspire for a place on the team. A young man like you is
willing to give anything to belong. Your coach becomes an ideal to you; you want his
approval and to be like him. But remember, if that coach is in the habit of swearing, if he
directs the team with profane words or corrects and disciplines the athletes with obscenities,
that is a weakness in him, not a strength. That is nothing to be admired nor to be
copied. It is a flaw in his character. While it may not seem a big one,
through that flaw can seep contamination sufficient to weaken and destroy the finest of
characters, as a disease germ can lay low the well-framed, athletically strong, physical body.
</p><p>
Coach, there are men in the making on the practice field. Haven't you learned that when a
boy wants so much to succeed, if he hasn't pleased you, that silence is more powerful than
profanity?
</p><p>
While this counsel may apply to other professions, I single you out, coach, because of your
unparalleled power of example (and perhaps because the lesson is needed).
</p><p>
<b>Better than profanity</b>
</p><p>
There is no need for any of us to use profanity. Realize that you are more powerful in
expression without it. I give you two examples: 
</p><p>
Sir Winston Churchill, in his postwar account of the struggle with Nazism, introduced the
most revolting character in recent centuries without a profane adjective. I quote: 
</p><p>
"Thereafter mighty forces were adrift; the void was open, and into that void, after a pause
there strode a maniac of ferocious genius, the repository and expression of the most virulent
hatreds that have ever corroded the human breast&mdash;Corporal Hitler." (<i>Sir Winston Churchill</i>,
by Robert Lewis Taylor.)
</p><p>
Nobody needs to profane!
</p><p>
You may argue that we are not all Winston Churchills. Therefore, this next example is within
the reach of most everyone.
</p><p>
On one occasion, two of our children were at odds. A four-year-old boy, irritated beyond
restraint by an older brother but with no vocabulary of profanity to fall back upon, forced out
his lower lip and satisfied the moment with two words: "You ugly!"
</p><p>
Nobody needs to swear!
</p><p>
Because of little protest, like the man in the shop, any of us may have fallen victim to the
habit of profanity. If this has been your misfortune, I know a way that you can break the
habit quickly. This is what I suggest you do: Make an agreement with someone not in your
family but someone who works closest with you. Offer to pay him $1.00 or $2.00, even
$5.00, each time he hears you swear. For less than $50.00 you can break the habit. Smile if
you will, but you will find it is a very practical and powerful device.
</p><p>
<b>Control of emotions</b>
</p><p>
Now, keeping in mind the statement of President Smith, there is a compelling reason beyond
courtesy or propriety or culture for breaking such a habit. Profanity is more than just untidy
language, for when we profane we relate to low and vulgar words, the most sacred of all
names. I wince when I hear the name of the Lord so used, called upon in anger, in frustration,
in hatred.
</p><p>
This is more than just a name we deal with. This relates to spiritual authority and power and
lies at the very center of Christian doctrine.
</p><p>
The Lord said: "Therefore, whatsoever ye shall do, ye shall do it in my name"
(<span class="citation" id="5711"><a href="javascript:void(0)" onclick="sx(this, 5711)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(5711)">3 Ne. 27:7</a></span>).
</p><p>
In the Church that Jesus Christ established, all things are done in his name. Prayers are said,
children are blessed, testimonies borne, sermons preached, ordinances performed, sacrament
administered, the infirm anointed, graves dedicated.
</p><p>
What a mockery it then becomes when we use that sacred name profanely.
</p><p>
If you need some feeling for the seriousness of the offense, next time you hear such an
expression or you are tempted to use one yourself, substitute the name of your mother, or
your father, or your child, or your own name. Perhaps then the insulting and degrading
implications will be borne into you, to have a name you revere so used. Perhaps then you will
understand the third commandment.
</p><p>
"Thou shalt not take the name of the Lord thy God in vain; for the Lord will not hold him
guiltless that taketh his name in vain" (<span class="citation" id="22771"><a href="javascript:void(0)" onclick="sx(this, 22771)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(22771)">Ex. 20:7</a></span>).
</p><p>
<b>Reverence and worship in His name</b>
</p><p>
However common irreverence and profanity become, they are none the less
wrong. We teach our children so. In The Church of Jesus Christ of Latter-day Saints we
revere his name. We worship in his name; we love him.
</p><p>
He said: "Behold, verily, verily, I say unto you, ye must watch and pray always lest ye enter
into temptation; for Satan desireth to have you, that he may sift you as wheat.
</p><p>
"Therefore ye must always pray unto the Father <i>in my name</i>; 
</p><p>
"And whatsoever ye shall ask the Father <i>in my name</i>, which is right, believing that ye shall
receive, behold it shall be given unto you.
</p><p>
"Pray in your families unto the Father, always <i>in my name</i>, that your wives and your children
may be <i>blessed</i>" (<span class="citation" id="5710"><a href="javascript:void(0)" onclick="sx(this, 5710)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(5710)">3 Ne. 18:19-21</a></span>, italics added.)
</p><p>
The authority to use his name has been restored. The disease of profanity, now in epidemic
proportions, is spreading across the land, and so, in his name, we pray that a purity of heart
might descend upon us, for out of the abundance of the heart the mouth speaketh.
</p><p>
I bear to you my solemn witness that I know that Jesus is the Christ, that he lives, that this is
his Church, that there stands at the head of this Church a prophet of God, and I bear that
witness in the name of Jesus Christ. Amen.
</p>
</div>
</div>
