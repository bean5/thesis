<?xml version="1.0" encoding="UTF-8"?>
<div class="gcera">
<div class="gchead">
<p class="gctitle">The Birth of Truth</p>
<p class="gcspeaker">Elder Hugh B. Brown</p>
<p class="gcspkpos">Of the Council of the Twelve</p>
<p class="gcbib">Hugh B. Brown, <i>Conference Report</i>, April 1970, pp. 77-80</p>
</div>
<div class="gcbody">
<p>
My brethren and sisters, it is a joy to be home again. After a globe-encircling journey, which
results often in a better understanding of the peoples of other lands and cultures, I return with an
increased appreciation of our own beloved America, its freedoms, and its opportunities.
</p><p>
<b>Hunger for truth</b>
</p><p>
It is my impression also that people of all lands and cultures have an increasing hunger for truth
and an open-minded attitude toward new truths. Thinking men everywhere are seeking for light.
There is, in fact, a worldwide quest for truth.
</p><p>
Leaders in both religious and scientific fields are asking for a revival of learning and a
broad-minded attitude toward truth&mdash;wherever it may be found. Let me remind you, however,
that broad-mindedness too often is nothing but a flattening out of high-mindedness!
</p><p>
The good life is a life that is pursued intelligently, toward the cultivation of genuine spirituality
that is grounded in faith and knowledge, that is dedicated to truth.
</p><p>
<b>The glory of God</b>
</p><p>
Faith is the ground of all religion, but there is no special virtue in blind faith. Only faith that is
grounded in a courageous search for truth is worthy of the student. We should reject every
temptation to irrationality, overcome every inclination to disregard or distort the facts, avoid the
extremes of fanaticism, and above all else, demand the truth. Here is the firm foundation for our
religion&mdash;a religion that describes the glory of God as intelligence (<span class="citation" id="15617"><a href="javascript:void(0)" onclick="sx(this, 15617)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(15617)">D&amp;C 93:36</a></span>) and proclaims that man is
saved no faster than he gains knowledge.
</p><p>
Just as the truths of science must be tested and verified by reason and factual investigation, so
the moral and spiritual truths which the world is seeking from its prophets must be proved and
validated in the experience of men. In his search for truth, every man must be true to himself. He
must answer to his own reason and to his own moral conscience. Anything less than this would
betray his dignity as a human being and a child of God. True dignity is never won by place, and
it is never lost when honors are withdrawn.
</p><p>
<b>Endurance of truth</b>
</p><p>
Especially in the realms of spiritual and religious
endeavor where faith ventures into untried fields, truth must meet the test of unbelief and endure
the fires of persecution, opposition, rejection, and hatred. Truth crushed to earth shall rise again.
</p><p>
Perhaps it was this thought of the permanence and eternal endurance of truth which prompted
Oliver Wendell Holmes to write his illuminating poetic essay on "The Battle for Survival of
Newborn Truths." He said: 
</p><p>
"The time is wracked with birth pangs,<br />
     Every hour brings forth some gasping truth,<br />
     And truth, newborn, looks a misshapen, and untimely growth,<br />
     The terror of the household, and its shame,<br />
     A monster coiling in its nurse's lap<br />
     That some would strangle, some would only starve,<br />
     But still it breathes, and passed from hand to hand,<br />
     And suckled at a hundred half-clad breasts,<br />
     Comes slowly to its stature and its form,<br />
     Calms the rough ridges of its dragon scales,<br />
     Changes to shining locks its snaky hair, <br />
     And moves transfigured into angel guise, <br />
     Welcomed by all who cursed its hour of birth <br />
     And folded in the same encircling arms<br />
     That cast it like a serpent from their hold."
</p><p>
<b>Newly revealed truth</b>
</p><p>
Let us discuss some newly revealed truth&mdash;truth that has had just such a reception and
experience as the poet mentions, for it was thought to be "a misshapen and an untimely growth."
Yet it is coming to its stature and its form, and its truth will move transfigured yet into angel
guise.
</p><p>
The somewhat melancholy history of the past seems to have been a necessary forerunner to those
great events which we now proclaim. The passing of the sun of time beyond the meridian, after
the crucifixion of Christ, was followed by the twilight and the sunset, and then centuries of
darkness, after which the signs of dawn appear. The morning breaks, the shadows flee.
</p><p>
How gloriously the Lord has kept his promise that in the latter days he would pour out his Spirit
upon all flesh!
</p><p>
<b>A marvelous age</b>
</p><p>
What a marvelous age is this in which we live! What tremendous advancement has been made
within the last 150 years!
</p><p>
In the fields of communication and transportation alone, we have made such strides as would
cause our ancestors, if they could come and see us, to say that we were gods. They would be
stunned by radio and television and the marvelous achievements of science, the harnessing of
electricity and other powers by which we bring to servitude the great forces of nature which in
their day men feared and were wont to worship.
</p><p>
But lest we be given to boasting of these great events and achievements, we should be reminded
of how they are being used, and of what is happening in this world of ours by the very things
which our civilization has produced. Hunger and want, misery and woe seem to be spreading
through the world, threatening the very civilization that has made these things possible. It seems
that God's great plan included work for a wrecking crew, to tear down the old structure and
make room for that which is to come. But let not those who are responsible for these things be
comforted in this thought, for God has said: "It is impossible but that offenses will come: but woe
unto him, through whom they come!" (<span class="citation" id="31809"><a href="javascript:void(0)" onclick="sx(this, 31809)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(31809)">Luke 17:1</a></span>)
</p><p>
<b>Spiritual enlightenment</b>
</p><p>
But are we to look for great advancement in these fields of human thought and activity alone,
where material things seemingly are glorified and the spiritual things forgotten? Or may we
expect in fields of moral growth and spiritual enlightenment to find new truth and revelation
from God? When he said he would pour out his Spirit upon all flesh, (<span class="citation" id="26881"><a href="javascript:void(0)" onclick="sx(this, 26881)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(26881)">Joel 2:28</a></span>) I think he did not intend to
limit his inspiration to those who are working with material things alone, for in the spiritual
realm, too, there is need for something new.
</p><p>
<b>Restitution of all things</b>
</p><p>
You will remember when Peter and John went up to the temple at Jerusalem and came to the
gate called beautiful; the man who was lame, sitting there, asked alms of them; and Peter,
turning to him, said: "Silver and gold have I none; but such as I have, give I thee: In the name of
Jesus Christ of Nazareth rise up and walk." (<span class="citation" id="8102"><a href="javascript:void(0)" onclick="sx(this, 8102)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(8102)">Acts 3:6</a></span>)
</p><p>
The scriptures tell us he was healed, and he leaped and he shouted for joy at his deliverance.
Then a crowd gathered in wonder and amazement, and Peter told them that what was done was
not of their own power or holiness, but it was done in the name of Jesus Christ
(<span class="citation" id="8103"><a href="javascript:void(0)" onclick="sx(this, 8103)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(8103)">Acts 3:7-16</a></span>). Then he said to
the multitude: 
</p><p>
"Repent ye therefore, and be converted, that your sins may be blotted out, when the times of
refreshing shall come from the presence of the Lord; 
</p><p>
"And he shall send Jesus Christ, which before was preached unto you: 
</p><p>
"Whom the heaven must receive until the times of the restitution of all things, which God hath
spoken by the mouth of all his holy prophets since the world began." (<span class="citation" id="8104"><a href="javascript:void(0)" onclick="sx(this, 8104)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(8104)">Acts 3:19-21</a></span>)
</p><p>
<b>Dispensation of fulness of times</b>
</p><p>
The apostle Paul said that in the dispensation of the fulness of times he would gather together in
one all things in Christ, both which are in heaven and which are in the earth, even in him. (<span class="citation" id="21831"><a href="javascript:void(0)" onclick="sx(this, 21831)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(21831)">Eph. 1:10</a></span>)
</p><p>
You will remember, too, as the eleven stood with the Master out near Bethany where they saw a
cloud envelope him and take him into heaven, two angels stood by in white apparel and said to
those who were assembled: "Ye men of Galilee, why stand ye gazing up into heaven? this same
Jesus, which is taken up from you into heaven, shall so come in like manner as ye have seen him
go into heaven." (<span class="citation" id="8100"><a href="javascript:void(0)" onclick="sx(this, 8100)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(8100)">Acts 1:11</a></span>)
</p><p>
<b>Vision of John</b>
</p><p>
We refer again to that marvelous prediction of John, who, while banished on the Isle of Patmos,
had a vision and said: "I was in the Spirit on the Lord's day, and heard behind me a great voice,
as of a trumpet.
</p><p>
"Saying, I am Alpha and Omega, the first and the last: and, What thou seest, write in a book, and
send it unto the seven churches which are in Asia . . .
</p><p>
"And I turned to see the voice that spake with me. And being turned, I saw seven golden
candlesticks; 
</p><p>
"And in the midst of the seven candlesticks one like unto the Son of man, clothed with a garment
down to the foot, and girt about . . . with a golden girdle.
</p><p>
"His head and his hairs were white like wool, as white as snow; and his eyes were as a flame of
fire; 
</p><p>
"And his feet like unto fine brass, as if they burned in a furnace; and his voice as the sound of
many waters.
</p><p>
"And he had in his right hand seven stars: and out of his mouth went a sharp two-edged sword:
and his countenance was as the sun shineth in his strength.
</p><p>
"And when I saw him, I fell at his feet as dead. And he laid his right hand upon me, saying unto
me, Fear not; I am the first and the last: 
</p><p>
"I am he that liveth, and was dead; and, behold, I am alive forevermore. Amen . . .
</p><p>
"Write the things which thou hast seen, and the things which are, and the things which shall be
hereafter." (<span class="citation" id="42441"><a href="javascript:void(0)" onclick="sx(this, 42441)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(42441)">Rev. 1:10-19</a></span>)
</p><p>
Thus spake the Son of God to John the apostle.
</p><p>
<b>God revealed to man</b>
</p><p>
In the spring of 1820, concerning which you have heard something this morning already, just
150 years ago, God, our Father, revealed himself to man. He considered the occasion and the
message of such great importance that he came personally from the heavens and brought with
him his Only Begotten Son, and together they spoke to this young man and to all of us. (<span class="citation" id="30528"><a href="javascript:void(0)" onclick="sx(this, 30528)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(30528)">JS&mdash;H 1:17</a></span>) Since
that time others have come, other revelations have been given. The Angel Moroni and Moses
and Elias came. Peter, James, and John, John the Baptist, Elijah, and others. They have spoken
to men and commissioned them, and men are again communing with God.
</p><p>
Now, I am not unmindful of the fact that such a declaration as this is met not only with
incredulity and disbelief, but also with antagonism and anger. Men have employed against this
truth the self-same weapons as the adversary has always used in his battle against the truth.
</p><p>
<b>God speaks to man</b>
</p><p>
Here again was truth looked upon as a misshapen and untimely growth. And yet, I ask all
Christians who believe the Bible, do you doubt the words of Saul of Tarsus, who said that on his
way to Damascus to persecute the saints, he saw a light which blinded him, and he heard a
voice? He asked, "Who art thou, Lord?" And the voice replied: "I am Jesus Christ whom thou
persecutest." (<span class="citation" id="8113"><a href="javascript:void(0)" onclick="sx(this, 8113)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(8113)">Acts 9:5</a></span>)
</p><p>
I say Christians believe that record, and yet they say God cannot speak to men. They who believe
the Bible accept the record which tells us of the appearance of Moses and Elias on the Mount of
Transfiguration and that Peter, James, and John were there and saw them in the presence of the
Master. (<span class="citation" id="36018"><a href="javascript:void(0)" onclick="sx(this, 36018)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(36018)">Matt. 17:1-3</a></span>) Moses and Elias, mind you, had lived hundreds of years before that time, and yet men
say: "Yes, we believe the Bible where it tells of that. It was done once, but it can't be done
again."
</p><p>
I repeat: Why should men think it a thing incredible that God should speak to men? (<span class="citation" id="8121"><a href="javascript:void(0)" onclick="sx(this, 8121)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(8121)">Acts 26:8</a></span>) Has not that
been his method throughout the ages? Do we not need him? Have our civilization, our science,
and our boasted learning made us independent of him?
</p><p>
<b>Introduction to future events</b>
</p><p>
Our declaration to you today is but introductory, and though he came, and with him God the
Father, and following them these others whom I have briefly mentioned&mdash;all of this is but an
introduction to what is yet to come.
</p><p>
In the afterglow of Easter, listen to the Lord's promise: "For I will reveal myself from heaven
with power and glory, with all the hosts thereof, and dwell in righteousness with men on earth a
thousand years, and the wicked shall not stand." (<span class="citation" id="15524"><a href="javascript:void(0)" onclick="sx(this, 15524)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(15524)">D&amp;C 29:11</a></span>)
</p><p>
And again from Matthew: "For the Son of man shall come in the glory of his Father with his
angels; and then he shall reward every man according to his works." (<span class="citation" id="36017"><a href="javascript:void(0)" onclick="sx(this, 36017)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(36017)">Matt. 16:27</a></span>)
</p><p>
"For the Lord himself shall descend from heaven with a shout, with the voice of the archangel,
and with the trump of God: and the dead in Christ shall rise first." (<span class="citation" id="2749"><a href="javascript:void(0)" onclick="sx(this, 2749)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(2749)">1 Thes. 4:16</a></span>)
</p><p>
<b>A glorious promise</b>
</p><p>
This declaration that the Savior will come again is made to you, my brothers and sisters and
friends, in the spirit and by the power which gave these truths to man, and in his name I declare
to you that I know, as I know I live, that this is true. It is the most hopeful and the most glorious
announcement and promise that has been made in all the history of the world, save only that
which was made by the angels to the shepherds on the hills of Galilee when Christ was born. (<span class="citation" id="31788"><a href="javascript:void(0)" onclick="sx(this, 31788)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(31788)">Luke 2:9-15</a></span>)
</p><p>
Let us continue to search for truth in all fields of human interest and endeavor&mdash;"Till the war
drums throb no longer and the battle flags are furled in the parliament of man, the federation of
the world"; till the Prince of Peace (<span class="citation" id="25379"><a href="javascript:void(0)" onclick="sx(this, 25379)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(25379)">Isa. 9:6</a></span>) shall come and assume his rightful place as King of kings,
and there will be universal peace for 1,000 years. (<span class="citation" id="42450"><a href="javascript:void(0)" onclick="sx(this, 42450)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(42450)">Rev. 19:16</a></span>)
</p><p>
I pray that we may be individually preparing ourselves to meet him when he comes, for come he
will, and that much sooner than we think. Of this truth I testify to you in the name of Jesus
Christ. Amen.
</p>
</div>
</div>
