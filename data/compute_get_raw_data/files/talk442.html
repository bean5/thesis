<?xml version="1.0" encoding="UTF-8"?>
<div class="gcera">
<div class="gchead">
<p class="gctitle">The Basis of Christian Faith</p>
<p class="gcspeaker">Elder Albert E. Bowen</p>
<p class="gcspkpos">Of the Council of the Twelve Apostles</p>
<p class="gcbib">Albert E. Bowen, <i>Conference Report</i>, April 1950, pp. 49-55</p>
</div>
<div class="gcbody">
<p>
If I can manage it, I should like today to make a little comparison.
</p><p>
Something more than nineteen hundred years ago, twelve obscure men with conviction and a
message entered upon an undertaking which turned the world over and shaped the course of
history.
</p><p>
COMMISSION OF THE LORD
</p><p>
They were acting under a commission given them by the risen Lord as the final injunction of
his early ministry. Coming to them at an appointed place on the occasion of his last
appearance, he made this epoch marking announcement, "All power is given unto me in
heaven and in earth" (<span class="citation" id="34399"><a href="javascript:void(0)" onclick="sx(this, 34399)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(34399)">Matt. 28:18</a></span>).
That was a monumental assertion of authority. It was
the premise upon which he based his solemn charge: 
</p><p>
Go ye therefore, and teach all nations, baptizing them in the name of the Father, and
of the Son, and of the Holy Ghost: 
</p><p>
Teaching them to observe all things whatsoever I have commanded you
(<span class="citation" id="34401"><a href="javascript:void(0)" onclick="sx(this, 34401)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(34401)">Matt. 28:19-20</a></span>).
</p><p>
As Mark narrates the incident, the commission was accompanied by a promise of equally
positive and sobering import: "He that believeth . . . shall be saved"
(<span class="citation" id="33039"><a href="javascript:void(0)" onclick="sx(this, 33039)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(33039)">Mark 16:16</a></span>). To be
sure, certain requirements were enjoined upon believers as a condition to the fulfillment of the
promise, but they would follow as a consequence of genuine, sincere belief&mdash;the kind of
belief that Jesus was talking about. A tremendously arresting quality of this whole matter is
the tone of finality of it all. There is no uncertainty, no qualification, no
temporizing. It is the voice of complete assurance, supreme confidence, final authority such as
is not to be matched in the words of any other man who has lived in mortality. It is in
keeping with the character of one who had declared himself to be the Son of God, the
Redeemer of the world.
</p><p>
During the period of his mortal ministry, the crowd had perceived this quality in his
utterances and said in wonderment one to another that "he taught them as one having
authority" (<span class="citation" id="34353"><a href="javascript:void(0)" onclick="sx(this, 34353)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(34353)">Matt. 7:29</a></span>).
Neither does one get the sense that there is any bombast or vanity
or pretentious arrogation of power. Straight and clear in the calm authoritative tones of one
who had conquered death and thus redeemed the race from its power came the words, "All
power is given unto me in heaven and in earth"
(<span class="citation" id="34400"><a href="javascript:void(0)" onclick="sx(this, 34400)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(34400)">Matt. 28:18</a></span>). It is the bedrock upon which
the foundation of all his teachings rests. No one else in all the world has ever spoken like
that, and no one can disprove the assertion. On the contrary, there have been and are vast
multitudes who for nearly two thousand years have proclaimed and now proclaim assurance of
its truth.
</p><p>
AWESOME ASSIGNMENT
</p><p>
It would be difficult to conceive of an assignment more awesome than that one just referred
to as being given by the Lord to his chosen disciples. Consider for a moment their station.
They were humble men&mdash;fishermen and peasants&mdash;without wealth or social position or
high-placed friends. They had neither political prestige nor armed might. They lived in a
remote province of the haughtiest and mightiest empire of the earth, whose proud legions had
carried its banners to remote corners. Members of a turbulent, troublesome, and therefore
unpopular race, they were directed to carry an unknown and hitherto unheard-of message to
all the world, calling upon its inhabitants to observe all things whatsoever the crucified Lord
had commanded promising salvation to all who believed and complied. Before the magnitude
of that task, the stoutest heart might well have quailed.
</p><p>
What their personal feelings were we are not told. The record is silent. We are left to
inference from what they did about it. They seem not to have been overwhelmed or weighted
down with apprehension. Perhaps they were not too much startled because they had previously
been sent out as emissaries under the personal direction of the Master and had had personal
experience of his sustaining power. During the period of his mortal ministry they had been
under his personal tutelage and had heard him with unwavering assurance declare: 
</p><p>
. . . I am the resurrection, and the life: he that believeth in me, though he were dead,
yet shall he live (<span class="citation" id="27433"><a href="javascript:void(0)" onclick="sx(this, 27433)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(27433)">John 11:25</a></span>).
</p><p>
And again: 
</p><p>
For I came down from heaven, not to do mine own will, but the will of him that sent
me.
</p><p>
And this is the will of him that sent me that every one which seeth
the Son, and believeth on him, may have everlasting life: and I will raise him up at the
last day (<span class="citation" id="27411"><a href="javascript:void(0)" onclick="sx(this, 27411)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(27411)">John 6:38,40</a></span>).
</p><p>
With like definiteness they had heard him declare: 
</p><p>
I am the door: by me if any man enter in, he shall be saved, and shall go in and out,
and find pasture.
</p><p>
Therefore doth my Father love me, because I lay down my life, that I might take it
again.
</p><p>
. . . This commandment have I received of my Father
(<span class="citation" id="27425"><a href="javascript:void(0)" onclick="sx(this, 27425)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(27425)">John 10:9,17-18</a></span>).
</p><p>
They had seen him crucified and, in fulfillment of his words, rise from the dead. All this must
have given them an immensely fortifying trust in his promise. At any rate they went
unhesitatingly to their work.
</p><p>
FAITH PUT TO TEST
</p><p>
The quality of their faith and their courage was soon enough put to the test. When Peter and
John, going up to the temple, healed the crippled man
(<span class="citation" id="7630"><a href="javascript:void(0)" onclick="sx(this, 7630)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(7630)">Acts 3:1-11</a></span>),
they got themselves hailed before the
rulers who demanded of them by what power or by what name they had done this thing. Peter
boldly answered.
</p><p>
Be it known unto you all, and to all the people of Israel, that by the name of Jesus
Christ of Nazareth, whom ye crucified, whom God raised from the dead, even by him
doth this man stand here before you whole
(<span class="citation" id="7632"><a href="javascript:void(0)" onclick="sx(this, 7632)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(7632)">Acts 4:10</a></span>).
</p><p>
They were forbidden further to teach in that name, and ignoring the warning, were thrown
into prison. Being liberated, they continued their teachings and were beaten and enjoined from
teaching: but still they persisted
(<span class="citation" id="7637"><a href="javascript:void(0)" onclick="sx(this, 7637)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(7637)">Acts 4:17-18,21</a></span>), saying,
</p><p>
. . . Whether it be right in the sight of God to hearken unto you more than unto God,
judge ye.
</p><p>
For we cannot but speak the things which we have seen and heard
(<span class="citation" id="7638"><a href="javascript:void(0)" onclick="sx(this, 7638)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(7638)">Acts 4:19-20</a></span>).
</p><p>
Peter told his inquisitors to their teeth: 
</p><p>
The God of our fathers raised up Jesus, whom ye slew and hanged on a tree.
</p><p>
Him hath God exalted with his right hand to be a Prince and a Saviour, for to give
repentance to Israel, and forgiveness of sins
(<span class="citation" id="7639"><a href="javascript:void(0)" onclick="sx(this, 7639)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(7639)">Acts 5:30-31</a></span>).
</p><p>
That was the power of their message, and the basis of their strength.
</p><p>
PERSECUTIONS
</p><p>
Their numbers multiplied with amazing rapidity, and so did the persecution. They were
hunted down, driven into hiding, beaten, and stoned
(<span class="citation" id="7640"><a href="javascript:void(0)" onclick="sx(this, 7640)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(7640)">Acts 5:40</a></span>).
From the account of Saul of Tarsus
something of the bitterness of their suffering may be learned, but still they pressed on, their
multiplied number spreading throughout the empire and to the very capital itself. An edict of
extermination was decreed against the sect. They were driven into hiding,
cruelly tortured, thrown to the beasts in the circus for the amusement of the populace, where
they were torn limb from limb. But still the work spread, a triumph of fidelity to a cause and
sincerity in its advocacy.
</p><p>
These men believed. Men do not endure that kind of persecution without deep conviction.
Here was no lip-service or sham or apologetics or denaturing to suit the doctrines to the tastes
or practice of listeners. That is the kind of belief and these were the kind of men who
perpetuated the teachings of Jesus in the earth, rescued them from fading into forgetfulness,
and carried the Christian faith triumphantly to its establishment as the worship of the majority
of the people of the empire which once had proscribed it and decreed the extermination of its
adherents. That is the kind of belief of which Jesus spoke when he said, "He that believeth
shall be saved" (<span class="citation" id="33040"><a href="javascript:void(0)" onclick="sx(this, 33040)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(33040)">Mark 16:16</a></span>).
</p><p>
THE SON OF GOD
</p><p>
In the execution of their commission, the disciples clearly perceived that it was their first task
to get him accepted, to get men to believe that he was the Son of God, the resurrected Lord
who had redeemed the race from the bondage of death. All their teaching accordingly
revolved around that central theme. Without that, there could be no hope of inducing the
world to accept his moral and religious doctrines.
</p><p>
The pattern of their discourse was foreshadowed by Peter's bold declaration to the rulers: "Be
it known to you all and to all the people of Israel, that by the name of Jesus Christ of
Nazareth, whom ye crucified, whom God raised from the dead, even by him doth this man
stand here before you whole" (<span class="citation" id="7633"><a href="javascript:void(0)" onclick="sx(this, 7633)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(7633)">Acts 4:10</a></span>).
That is what gave authoritative sanction to their teachings about
the way of life which Jesus taught, and which so profoundly influenced the whole world.
</p><p>
INFLUENCE OF CHRISTIAN TEACHINGS
</p><p>
It was clearly the intent and purpose of the Savior that men and peoples and nations should
come to order their lives on the basis of principles he laid down, and it is something to
marvel at how deep those teachings finally penetrated into those nations that espoused the
Christian faith and furnished the standards of values and judgments to which they profess to
cling. The nations formed from the breakup of the Roman Empire, as well as new nations
which sprang or were peopled from them, in general fell under the influence of the Christian
teachings and were called the Christian nations. The very frameworks of their governments
were colored and shaped by these principles, and their Christian convictions have made these
nations dominant in the world for fifteen hundred years.
</p><p>
Various Greek philosophies, and Roman too, taught rules of life, but such of them as have
survived have done so by penetrating into the Christian faith which overshadowed them as the
dominant spiritual power of the western world. In the Christian nations when
men have judged conduct or the validity of principles, they have evaluated them as good or
bad according to their conformance or nonconformance to the standards laid down in the
Christian code. Before Mussolini and Hitler could corrupt the youth of their countries they
had to undermine and break down and destroy belief in the principles taught by Christ which
for centuries their countries had been taught to revere as ideals, howsoever short they may
have fallen in practice. While the Christian church compromised many of its vital principles
and in that degree weakened itself and subverted its initial purpose, it nevertheless still gave
lip-service to the Christ and a certain veneration for him and his precepts, which gave
controlling color to the institutions and practices of the Christian world.
</p><p>
DISQUIETING SIGNS
</p><p>
But something ominous is happening now. There are disquieting signs that all over
Christendom the underpinnings of the Christian faith are being knocked down. The crumbling
of the Christian pattern is of grave portent. Ancient supports may be torn away, but what shall
buttress us then? It begins to look as though the world is slipping back to the position it held
when the Lord commissioned his disciples to carry his message to all the nations. The task
again seems to be to get men to believe in him&mdash;to get him accepted. There is something
highly suggestive about the fact that the upheavals which are threatening the destruction of the
civilized world follow so closely on the heels of open denial by professed Christians of belief
in Christ as the Son of God and of the divinity of his teachings.
</p><p>
I suppose it has always been true that individual persons have disavowed belief in Jesus as the
Messiah, but generally this has not been true on a mass scale. Where such unbelief has been
declared, it has generally been professed that nevertheless the moral and ethical teachings of
Christ were still recognized as of the highest value and valid without acceptance of the claim
of his Messiahship or his resurrection. But repudiation cannot go halfway and stop. Denial of
his divinity is only the first step in the process of complete denial to which the logic of the
position inevitably drives. And now we seem to be experiencing on a scale never before
thought of the disavowal of any superior validity to the whole Christian creed. In other words,
we are now driven by the logic of events to recognize that you can't have Christianity without
Christ, and those who have decided to get along without him are driven to choose whether to
compromise on their principles, too. This is of almost worldwide consequence because
Christianity has penetrated into non-Christian nations far more than their creeds have
penetrated into the Christian world.
</p><p>
CRITICISM OF CHRISTIANITY
</p><p>
It is only two or three years ago that the president of a worshiping body in our country,
which formerly was assumed to call itself Christian, said: 
</p><p>
Christianity has been guilty of spiritual arrogance on a worldwide scale, labeling all
other religions false, and asserting that only when all mankind accepts the one true
religion will there be any hope for worldwide cooperation and peace. That amounts to
spiritual imperialism and is as out of date in our world today as any other form of
imperialism.
</p><p>
In a recent writing, an ordained minister who had held many pastorates proposes and argues
the necessity for a new religion for a new age. He begins by demolishing the Christian God,
and tearing to bits the Christian church, Protestant and Catholic alike. He tells us that
Christianity is a minority group even in America, and ridicules the idea that the great majority
are going to surrender their own beliefs to accept "the Christian God."
</p><p>
But that is precisely what Christ directed his disciples to teach the world to do. Moreover, that
is what the great body of the Roman world professedly did in its conversion from paganism to
Christianity.
</p><p>
The writer of the article dogmatically asserts, "God doesn't talk to you." All forms of religion
as known today, he says, and all ideas of God are man-made. They are not eternal; they grew
out of the feeling of man that he was impotent to achieve his ideals and turned the job of
carrying through to a supernatural power. He proposes as a religion for the modern age one
that is secular, presided over not by a ministry or a clergy or anyone acting under an
ordination but managed by professional and businessmen and workers and artists. He depicts
the organization of a brave new world where every man who has a talent will be expected to
use it. It will be noted on careful perusal that in this scheme of things there is no place for
God. The writer makes it abundantly clear that He does not exist, nor is there any future
estate for man. It would appear that man himself, and certainly religion, is to be but a tool of
the state.
</p><p>
ALTERNATE PROPOSALS
</p><p>
It is possible to agree with much of what the writer says about the failure of the Christian
Church to bring about the desired condition of peace and good order in the world and
harmonious living among men. He points to war and frustration and the disappointments of
human hopes through lapses in human behavior. But in all the principles and practices which
he sets forth as those to be embodied in his new religion, there is not one that is not already
laid down in the teachings of Jesus Christ. He proposes no new virtues. Just how the ideal of
the good life for all is to be implemented by supplanting the gospel taught by Jesus with a
secular-political-economic-sociological regime is not made clear, though there is some
indication that this is to be managed through the authoritarian powers of an omnipotent state,
which is a concept in direct variance from what Jesus taught. Right here I should like to
interpolate without using more words the stirring discourse we have just listened to from
President [David O.] McKay relating to human dignity and the right of all men to be free.
</p><p>
A REVELATION OF GOD
</p><p>
So we come at the end to the simple question whether religion is a revelation of God with
enduring validity in all times, and through the practice of which man may work his way up to
perfection, or is it a human creation with no higher sanction than the wisdom of man and
subject to change with the passing moods of changing times? The one gives stability and
constancy and purpose to life, the reason for being, with freedom to choose one's course: the
other sets man adrift with nothing enduring to hold on to and little hope to inspire noble
living.
</p><p>
In the brief time at my disposal, I have known that I could not make a complete portrayal of
the idea that is lying in the back of my mind. I had dared hope that I might advance
something suggestive enough to set your minds working on the idea sufficiently so that you
might fill in what of necessity I have left incomplete.
</p><p>
I have had more particularly in mind those who by their daily pursuits or association or
environmental influence might be confused or even disturbed by the godless humanism that is
so prevalent in the thought of the day. If you want to meet scholasticism with scholasticism to
bolster up your trust in the teachings and promises of the Master, you may take comfort in the
knowledge that many of the profoundest scholars of this and of other times still place trust in
God, who may be communed with through prayer.
</p><p>
I recall here the words of Henry George, the economist and political scientist, many of whose
political and economic and sociological views are in greater favor today than when he first
propounded them: "Political economy and social science," George said, "cannot teach any
lessons that are not embraced in the simple truths that were taught to poor fishermen and
Jewish peasants by one who 1800 years ago was crucified."
</p><p>
May all men learn to revere the teachings of the Lord and Savior. May they come to know
that in them is saving power and that outside of them within the realm of human wisdom
there is nothing that can save, I pray in the name of Jesus. Amen.
</p>
</div>
</div>
