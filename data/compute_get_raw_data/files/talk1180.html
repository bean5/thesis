<?xml version="1.0" encoding="UTF-8"?>
<div class="gcera">
<div class="gchead">
<p class="gctitle">The Divine Church</p>
<p class="gcspeaker">President David O. McKay</p>
<p class="gcbib">David O. McKay, <i>Conference Report</i>, April 1962, pp. 5-9</p>
</div>
<div class="gcbody">
<p>
One hundred and thirty-two years ago today a group of men and women, in obedience to a
commandment of God, were assembled in the house of Mr. Peter Whitmer, Sen., for the
purpose of organizing the Church.
</p><p>
It was just a group of friendly neighbors, unknown to anyone beyond the countryside in
which they followed their daily vocations. A good picture of the moral and economic
atmosphere of the neighborhood may be surmised from the following introduction of one of
the citizens: Joseph Knight, Sen. "'owned a farm, a grist mill and carding machine. He was
not rich, yet he possessed enough of this world's goods to secure to himself and family, not
only the necessaries, but also the comforts of life . . .' He 'was . . . a sober, honest man,
generally respected and beloved by his neighbors and acquaintances. He did not belong to any
religious sect, but was a believer in the Universalian doctrine.' The business in which Joseph
Knight, Sen., engaged, made it necessary at times for him to hire men, and the Prophet Joseph
was occasionally employed by him. To the Knight family . . . the young Prophet related
many of the things God had revealed respecting the Book of Mormon, then as yet, to come
forth." (<i>DHC</i> 1:47.)
</p><p>
Of such ordinary, rural men and women was the group composed who assembled in Peter
Whitmer's house in Fayette, Seneca County, New York, a century and thirty-two years ago
today.
</p><p>
Means of communication were primitive&mdash;seven years before the telegraph would be known.
The only light in the house after dark would be furnished by candle, perhaps by kerosene
lamp. The electric light globe would not be known for forty years. Sixty years&mdash;almost a
lifetime&mdash;before the automobile would be used! And the airplane existed only in the realm of
imagination. Yet one year before the organization of the Church, under the inspiration of the
Lord, Joseph Smith had written: 
</p><p>
". . . a marvelous work is about to come forth among the children of men"
(<span class="citation" id="14074"><a href="javascript:void(0)" onclick="sx(this, 14074)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(14074)">D&amp;C 4:1</a></span>).
</p><p>
There is no evidence that such a statement had ever before been made by an obscure lad, and
if it had, it would have passed into obscurity with the boastful pretensions or
imaginations of its author. Just as the anticipated, foolish aspirations of "Darius Green and his
flying machine,"&mdash;I am not sure whether I am right on that, but that is as I remember it as a
boy&mdash;who spoke disdainfully of the man who had made "wings of wax" that would not stand
"sunshine and hard whacks," and who boastfully said: "I shall make mine of leather, or
something or other."
</p><p>
I mention that merely to emphasize the fact that a Church to become a "marvelous work and a
wonder" must contain those elements of truth which find lodgment in the human mind, which
in honesty recognizes and loves truth wherever or whenever it is found.
</p><p>
It is true that over a century ago, when men heard that a young man claimed that God had
revealed himself, they mocked him, and in doubt turned away from him just as in the
beginning of the Christian Era wise and able men in Athens turned away from a lonely little
brown-eyed man who challenged much of their philosophy as false and their worship of
images as gross error, yet the fact remained that he was the only man in that great city of
intellectuals who knew by actual experience that a man may pass through the portals of death
and live&mdash;the only man in Athens who could clearly sense the difference between the
formality of idolatry and the heartfelt worship of the only true and living God. By the
Epicureans and Stoics with whom he had conversed and argued, Paul had been called a
"babbler," a "setter-forth of strange gods"
(<span class="citation" id="7900"><a href="javascript:void(0)" onclick="sx(this, 7900)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(7900)">Acts 17:18</a></span>);
</p><p>
"And they took him, and brought him unto Areopagus, saying, May we know what this new
doctrine, whereof thou speakest is?
</p><p>
"For thou bringest certain strange things to our ears: we would know therefore what these
things mean" (<span class="citation" id="7901"><a href="javascript:void(0)" onclick="sx(this, 7901)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(7901)">Acts 17:19-20</a></span>).
</p><p>
"Then Paul stood in the midst of Mars' Hill, and said, Ye men of Athens I perceive that in all
things ye are too superstitious.
</p><p>
"For as I passed by, and beheld your devotions, I found an altar with this inscription, TO
THE UNKNOWN GOD, Whom therefore ye ignorantly worship, him declare I unto you"
(<span class="citation" id="7902"><a href="javascript:void(0)" onclick="sx(this, 7902)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(7902)">Acts 17:22-23</a></span>).
</p><p>
Today, as then, too many men and women have other gods to which they give more thought
than to the resurrected Lord&mdash;the god of pleasure, the god of wealth, the god of indulgence,
the god of political power, the god of popularity, the god of race superiority&mdash;as varied and
numerous as were the gods in ancient Athens and Rome.
</p><p>
Thoughts that most frequently occupy the mind determine a man's course of action. It is
therefore a blessing to the world that there are occasions such as this, which, as warning
semaphores, say to mankind: <i>In your mad rush for pleasure, wealth, and fame, pause and</i>
<i>think what is of most value in life.</i>
</p><p>
What fundamental truths, what eternal principles, if any, were associated with that little group
which assembled one hundred and thirty-two years ago?
</p><p>
<i>The first</i> was <i>Man's Relationship to Deity.</i>
For the first time in eighteen hundred years, God had revealed himself as a Personal
Being. The relationship of Father and Son had been established by the divine introduction:
"This is My Beloved Son. Hear Him!" (<span class="citation" id="30423"><a href="javascript:void(0)" onclick="sx(this, 30423)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(30423)">JS&mdash;H 1:17</a></span>).
</p><p>
Those who were baptized into the Church that day in April 1830 <i>believed in the existence of</i>
<i>a Personal God; that his reality and that of his Son Jesus Christ constitute the eternal</i>
<i>foundation upon which this Church is built.</i>
</p><p>
Commenting upon this eternally existent, creative power of God, Dr. Charles A. Dinsmore of
Yale University, in <i>Christianity and Modern Thought</i>, aptly says: 
</p><p>
"Religion, standing on the known experience of the race, makes one bold and glorious
affirmation. She asserts that this power that makes for truth, for beauty, and for goodness is
not less personal than we. This leap of faith is justified because <i>God cannot be less than the</i>
<i>greatest of his works</i>, the <i>Cause</i> must be adequate to the effect. When, therefore, we call God
personal, we have interpreted him by the loftiest symbol we have. He may be infinitely more.
He cannot be less. When we call God a Spirit, we use the clearest lens we have to look at the
Everlasting. As Herbert Spencer has well said: '<i>The choice is not between a</i>
<i>personal God and something lower, but between a personal God and something higher.</i>'"
</p><p>
"My Lord and my God" (<span class="citation" id="28043"><a href="javascript:void(0)" onclick="sx(this, 28043)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(28043)">John 20:28</a></span>)
was not merely a spontaneous, meaningless exclamation of Thomas
when he beheld his Risen Lord. The Being before him was his God. Once we accept Christ as
divine, it is easy to visualize his Father as being just as personal as he; for Christ said, ". . .
he that hath seen me hath seen the Father"
(<span class="citation" id="28026"><a href="javascript:void(0)" onclick="sx(this, 28026)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(28026)">John 14:9</a></span>).
</p><p>
How boastful, how unfounded, is the brazen declaration of communism that "there is no
God," and that "Religion (the church) is but an opiate!"
</p><p>
Faith in the existence of an Intelligent Creator was the first element that contributed to the
perpetuity of the Church, <i>the everlasting foundation upon which the Church is built.</i>
</p><p>
<i>The second cornerstone is the Divine Sonship of Jesus Christ</i>. The gospel teaches that Christ
is the Son of God, the Redeemer of the world. No true follower is satisfied to accept him
merely as a great teacher, a great reformer, or even as the One Perfect Man. The Man of
Galilee is not figuratively, but <i>literally</i> the Son of the Living God.
</p><p>
<i>A third principle</i> which contributes to the stability of the Church and which impressed not
only that little group, but millions since, that a great and marvelous work was about to come
forth, <i>is the immortality of the human soul.</i>
</p><p>
Jesus passed through all the experiences of mortality just as you and I. He knew happiness.
He experienced pain. He rejoiced as well as sorrowed with others. He knew friendship. He
experienced also the sadness that comes through traitors and false accusers. He died a mortal
death even as every other mortal. As his spirit lived after death, so shall yours and mine.
</p><p>
<i>A fourth element</i> which contributed to the perpetuity of that little group was the
<i>Cherished Hope for the Brotherhood of Man</i>. One of the two great general principles to which all others
are subsidiary is this: ". . . love thy neighbour as thyself"
(<span class="citation" id="35192"><a href="javascript:void(0)" onclick="sx(this, 35192)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(35192)">Matt. 19:19</a></span>), and correlated with it,
the promise: "Inasmuch as ye have done it unto the least of these my brethren, ye have done
it unto me" (<span class="citation" id="35202"><a href="javascript:void(0)" onclick="sx(this, 35202)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(35202)">Matt. 25:40</a></span>).
</p><p>
The gospel bids the strong bear the burdens of the weak, and to use the advantages given
them by their larger opportunities in the interest of the common good that the whole level of
humanity may be lifted, and the path of spiritual attainment opened to the weakest and most
unlearned as well as to the strong and intelligent.
</p><p>
The Savior condemned hypocrisy and praised sincerity of purpose. He taught that if the heart
be pure, actions will be in accord therewith. Social sins&mdash;lying, stealing, dishonest dealings,
adultery, and the like&mdash;are first committed in thought.
</p><p>
"Sow a thought, reap an act,<br />
          Sow an act, reap a habit,<br />
          Sow a habit, reap a character,<br />
          Sow a character, reap an eternal destiny."<br /><br />
&nbsp; &nbsp;                                      &mdash;E. D. Boardman
</p><p>
Jesus taught that an unsullied character is the noblest aim in life. No man can sincerely
resolve to apply to his daily life the teachings of Jesus of Nazareth without sensing a change
in his own nature. The phrase, "born again" (<span class="citation" id="27997"><a href="javascript:void(0)" onclick="sx(this, 27997)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(27997)">John 3:3</a></span>)
has a deeper significance than many people
attach to it. This <i>changed feeling</i> may be indescribable, <i>but it is real</i>. Happy the person who
has truly sensed the uplifting, transforming power that comes from this nearness to the Savior,
this kinship to the Living Christ.
</p><p>
<i>Resistance</i> is necessary along with obtaining a sense of the real divinity. There should be
developed also the <i>power of self-mastery</i>. Someone has said that when God makes the prophet,
he does not unmake the man. I believe that, though being "born anew," and being entitled to
new life, new vigor, new blessings, yet the old weaknesses may still remain. The adversary
stands by, ever eager and ready to attack and strike us at our weakest point.
</p><p>
Take, for example, the incident of Jesus on the Mount of Temptation. After he had passed
through the ordinance of baptism to fulfil all righteousness, after he had received the
commendation of the Father and the testimony from on high that he is the Beloved Son in
whom the Father is well pleased, the tempter was there ready to thwart, if
possible, his divine mission. At his weakest moment, as Satan thought, when his body was
famished by long fasting, the Evil One presented himself, saying, ". . . If thou be the Son of
God, command that these stones be made bread"
(<span class="citation" id="35148"><a href="javascript:void(0)" onclick="sx(this, 35148)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(35148)">Matt. 4:3</a></span>). Though his body was weak, his
spirit was strong, as he answered: ". . . It is written, Man shall not live by bread alone, but by
every word that proceedeth out of the mouth of God"
(<span class="citation" id="35150"><a href="javascript:void(0)" onclick="sx(this, 35150)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(35150)">Matt. 4:4</a></span>).
</p><p>
With unwavering strength, Jesus withstood the tempter's taunts and promises that followed,
and triumphantly demanded, ". . . <i>Get thee hence, Satan: for it is written, Thou shalt worship</i>
<i>the Lord thy God, and him only shalt thou serve</i>"
(<span class="citation" id="35152"><a href="javascript:void(0)" onclick="sx(this, 35152)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(35152)">Matt. 4:10</a></span>).
</p><p>
So it is with each of us in our daily resisting of the tempter. He will make his appeal to what
may be our weakest point of resistance. His strongest strain will be on the weakest link in the
chain that binds our character. It may come in the form of yielding to habit, tendency, or
passion which we have indulged for years. It may be a desire for the old pipe or the cigarette
which we determined, if we were sincere, to put aside when we entered the waters of baptism.
And when that longing comes, after we are in the Church or kingdom, in that moment when
temptation comes, we may say to ourselves, "Though I intend to throw it aside, I will take it
only once more&mdash;this once will not count." That is the moment of resistance when we should
say, as Christ, "Get thee behind me"
(<span class="citation" id="35184"><a href="javascript:void(0)" onclick="sx(this, 35184)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(35184)">Matt. 16:23</a></span>).
</p><p>
This power of self-control in regard to our bodily longings, satisfying the passions, applies to
every member of the Church of Christ. In some way, the Evil One will attack us; some way
he can weaken us. In some way, he will bring before us that which will weaken our souls and
will tend to thwart the true development of the spirit within, the strengthening and growth of
the spirit, which time cannot kill, which is as enduring as the Eternal Father of the spirit. And
the things which will tend to dwarf this spirit or to hinder its growth are things which
members of the Church are called upon to resist.
</p><p>
One hundred and thirty-two years ago the Church was officially organized with six members.
It was unknown, and, I repeat, would be known only to the extent that it contained and
radiated those eternal principles which harmonize with the eternity of its Author, and only
thus could it become a great and marvelous work.
</p><p>
Today there are branches of the Church in many parts of the world. As the effulgent light of
a glorious sun gladdens the surface of the earth by day, so the Light of Truth is entering into
the hearts of many honest men and women throughout the world.
</p><p>
The marvelous progress that has been made in transportation and communication makes it
possible for the promulgation of the truths of the restored gospel to be made known to the
children of men everywhere on the face of the globe. It is possible for millions in America,
Europe, Asia, Africa, and the islands of the sea not only to hear, but in many instances to see
what you are doing as members for the gospel of truth.
</p><p>
To all members, and to our Father's children everywhere, we declare in all sincerity that God
lives! As sure as the light of the sun shines upon everything on the physical earth, so the
radiance that emanates from the Creator brightens every soul that comes into the world of
humanity, for it is in him that we "live and move and have our being"
(<span class="citation" id="7905"><a href="javascript:void(0)" onclick="sx(this, 7905)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(7905)">Acts 17:28</a></span>). All of us, therefore,
should make him the center of our lives.
</p><p>
Jesus Christ his Beloved Son also lives and stands at the head of the kingdom of God on
earth. Through him the eternal plan of the gospel has been given to man and restored in its
fulness to the Prophet Joseph Smith. Through obedience to the principles of the gospel, we
may become partakers of his divine Spirit, as Peter of old, after two and a half years of
association with the Redeemer, testified (see
<span class="citation" id="4865"><a href="javascript:void(0)" onclick="sx(this, 4865)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(4865)">2 Pet. 1:4</a></span>).
</p><p>
In the words of President John Taylor: 
</p><p>
"Go, ye messengers of glory; <br />
          Run, ye legates of the skies; <br />
          Go and tell the pleasing story<br />
          That a glorious angel flies; 
</p><p>
"Go, to all the gospel carry;<br />
          Let the joyful news abound; <br />
          Go till every nation hear you<br />
          Jew and Gentile greet the sound.<br />
          Let the gospel echo all the earth around."
</p><p>
I pray in the name of Jesus Christ. Amen.
</p>
</div>
</div>
