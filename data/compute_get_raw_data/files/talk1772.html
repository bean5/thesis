<?xml version="1.0" encoding="UTF-8"?>
<div class="gcera">
<div class="gchead">
<p class="gctitle">Only a Teacher</p>
<p class="gcspeaker">Elder Thomas S. Monson</p>
<p class="gcspkpos">Of the Council of the Twelve</p>
<p class="gcbib">Thomas S. Monson, <i>Conference Report</i>, April 1970, pp. 97-100</p>
</div>
<div class="gcbody">
<p>
Often we hear the expression, "Times have changed." And perhaps they have. Our generation has
witnessed enormous strides in the fields of medicine, transportation, communication, and
exploration, to name but a few. However, there are those isolated islands of constancy midst the
vast sea of change. For instance, boys are still boys. And they continue to make the same boyish
boasts.
</p><p>
<b>Only a teacher</b>
</p><p>
Some time ago I overheard what I am confident is an oft-repeated conversation. Three very
young boys were discussing the relative virtues of their fathers. One spoke out: "My dad is
bigger than your dad," to which another replied, "Well, my dad is smarter than your dad." The
third boy countered: "My dad is a doctor"; then turning to one boy, he taunted in derision, "and
your dad is only a teacher."
</p><p>
The call of a mother terminated the conversation, but the words continued to echo in my ears.
Only a teacher. Only a teacher. Only a teacher. One day, each of those small boys will come to
appreciate the true worth of inspired teachers and will acknowledge with sincere gratitude the
indelible imprint which such teachers will leave on their personal lives.
</p><p>
<b>"A teacher affects eternity"</b>
</p><p>
"A teacher," as Henry Brook Adam's observed, "affects eternity; he can never tell where his
influence stops." This truth pertains to each of our teachers: first, the teacher in the home; second,
the teacher in the school; third, the teacher in the Church.
</p><p>
Perhaps the teacher you and I remember best is the one who influenced us most. She may have
used no chalkboard nor possessed a college degree, but her lessons were everlasting and her
concern genuine. Yes, I speak of mother. And in the same breath, also include father. In reality,
every parent is a teacher.
</p><p>
The pupil in such a teacher's divinely commissioned classroom&mdash;indeed, the baby who comes to
your home or to mine&mdash;is a sweet new blossom of humanity, fresh fallen from God's own home
to flower on earth.
</p><p>
Such a thought may have prompted the poet to pen the words: 
</p><p>
"I took a piece of plastic clay<br />
     And idly fashioned it one day&mdash;<br />
     And as my fingers pressed it, still<br />
     It moved and yielded to my will.
</p><p>
"I came again when days were past; <br />
     The bit of clay was hard at last.<br />
     The form I gave it, still it bore,<br />
     And I could change that form no more!
</p><p>
"I took a piece of living clay,<br />
     And gently pressed it day by day,<br />
     And moulded with my power and art<br />
     A young child's soft and yielding heart.
</p><p>
"I came again when years were gone: <br />
     It was a man I looked upon.<br />
     He still that early impress bore,<br />
     And I could fashion it never more."<br /><br />
&nbsp; &nbsp;     &mdash;Author Unknown
</p><p>
<b>Time for teaching</b>
</p><p>
Prime time for teaching is fleeting. Opportunities are perishable. The parent who procrastinates
the pursuit of his responsibility as a teacher may in years to come gain bitter insight to Whittier's
expression: ". . . of all sad words of tongue or pen, The saddest are these: 'It might have been.'"
(John Greenleaf Whittier, "Maud Muller," stanza 53.)
</p><p>
Should a parent need added inspiration to commence his God-given teaching task, let him
remember that the most powerful combination of emotions in the world is not called out by any
grand cosmic event nor found in novels or history books&mdash;but merely by a parent gazing down
upon a sleeping child. "Created in the image of God," (<span class="citation" id="23809"><a href="javascript:void(0)" onclick="sx(this, 23809)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(23809)">Gen. 1:27</a></span>) that glorious biblical passage, acquires
new and vibrant meaning as a parent repeats this experience. Home becomes a haven called
heaven, and loving parents teach their children "to pray, and to walk uprightly before the Lord."
(<span class="citation" id="15571"><a href="javascript:void(0)" onclick="sx(this, 15571)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(15571)">D&amp;C 68:28</a></span>) Never does such an inspired parent fit the description, "only a teacher."
</p><p>
<b>The teacher in the school</b>
</p><p>
Next, let us consider the teacher in the school. Inevitably, there dawns that tearful morning when
home yields to the classroom part of its teaching time. Johnny and Nancy join the happy throng
which each day wends its way from the portals of home to the classrooms of school. There a new
world is discovered. Our children meet their teachers.
</p><p>
The teacher not only shapes the expectations and ambitions of her pupils, but she also influences
their attitudes toward their future and themselves. If she is unskilled, she leaves scars on the
lives of youth, cuts deeply into their self-esteem, and distorts their image of themselves as
human beings. But if she loves her students and has high expectations of them, their
self-confidence will grow, their capabilities will develop, and their future will be assured.
</p><p>
<b>The power to mislead</b>
</p><p>
Unfortunately, there are those few teachers who delight to destroy faith, rather than build bridges
to the good life. Ever must we remember that the power to lead is also the power to mislead, and
the power to mislead is the power to destroy.
</p><p>
In the words of President J. Reuben Clark, Jr.: "He wounds, maims, and cripples a soul who
raises doubts about or destroys faith in the ultimate truths. God will hold such a one strictly
accountable; and who can measure the depths to which one shall fall who willfully shatters in
another the opportunity for celestial glory?" (<i>Immortality and Eternal Life</i>, Vol. 2, p. 128.)
</p><p>
<b>A guide to truth</b>
</p><p>
Since we cannot control the classroom, we can at least prepare the pupil. You ask: "How?" I answer:
"Provide a guide to the glory of the celestial kingdom of God; even a barometer to distinguish
between the truths of God and the theories of men."
</p><p>
Several years ago I held in my hand much a guide. It was a volume of scripture we commonly
call the Triple Combination, containing the Book of Mormon, Doctrine and Covenants, and
Pearl of Great Price. The book was a gift from a loving father to a beautiful, blossoming
daughter who followed carefully his advice. On the flyleaf page her father had written these
inspired words: 
</p><p>
"April 9, 1944 "<br />
To My Dear Maurine: 
</p><p>
"That you may have a constant measure by which to judge between truth and the errors of man's
philosophies, and thus grow in spirituality as you increase in knowledge, I give you this sacred
book to read frequently and cherish throughout your life.
</p><p>
&nbsp; &nbsp; "Lovingly your father,<br />
&nbsp; &nbsp; &nbsp; Harold B. Lee"
</p><p>
<b>The teacher in the Church</b>
</p><p>
I ask the question: "Only a teacher?"
</p><p>
Finally, let us turn to the teacher we usually meet on
Sunday&mdash;the teacher in the Church. In such a setting, the history of the past, the hope of the
present, and the promise of the future all meet. Here especially, the teacher learns it is easy to be
a Pharisee, difficult to be a disciple. The teacher is judged by his students&mdash;not alone by what
and how he teaches, but also by how he lives.
</p><p>
The apostle Paul counseled the Romans: 
</p><p>
"Thou . . . which teachest another, teachest thou not thyself? thou that preachest a man should
not steal, dost thou steal? Thou that sayest a man should not commit adultery, dost thou commit
adultery?" (<span class="citation" id="42834"><a href="javascript:void(0)" onclick="sx(this, 42834)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(42834)">Rom. 2:21-22</a></span>)
</p><p>
Paul, that inspired and dynamic teacher, provides us a good example. Perhaps his success secret
is revealed through his experience in the dreary dungeon that held him prisoner. Paul knew the
tramp, tramp of the soldiers' feet and the clank, clank of the chains which bound him captive.
When the prison warden, who seemed to be favorably inclined toward Paul, asked him whether
he needed advice as to how to conduct himself before the emperor, Paul said he had an
adviser&mdash;the Holy Spirit.
</p><p>
This same Spirit guided Paul as he stood in the midst at Mars' hill, read the inscription "To The
Unknown God," and declared: ". . . Whom therefore ye ignorantly worship, him declare I unto
you.
</p><p>
"God that made the world and all things therein . . . dwelleth not in temples made with hands; 
</p><p>
". . . he giveth to all life, and breath, and all things; 
</p><p>
For in him we live, and move, and have our beings . . . For we are also his offspring." (<span class="citation" id="8118"><a href="javascript:void(0)" onclick="sx(this, 8118)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(8118)">Acts 17:23-24,25,28</a></span>)
</p><p>
Again the question, "Only a teacher?"
</p><p>
<b>The Master Teacher</b>
</p><p>
In the home, the school, or the house of God, there is one teacher whose life overshadows all
others. He taught of life and death, of duty and destiny. He lived not to be served, but to serve;
not to receive, but to give; not to save his life, but to sacrifice it for others. He described a love
more beautiful than lust, a poverty richer than treasure. It was said of this teacher that he taught
with authority and not as do the scribes. In today's world, when many men are greedy for gold
and for glory, and dominated by a teaching philosophy of "publish or perish," let us remember
that this teacher never wrote&mdash;once only he wrote on the sand, and the wind destroyed forever
his handwriting. His laws were not inscribed upon stone, but upon human hearts. I speak of the
master teacher, even Jesus Christ, the Son of God, the Savior and Redeemer of all mankind.
</p><p>
<b>Dedicated teachers</b>
</p><p>
When dedicated teachers respond to his gentle invitation, "Come learn of me," they learn, but
they also become partakers of his divine power. It was my experience as a small boy to come
under the influence of such a teacher. In our Sunday School class, she taught us concerning the
creation of the world, the fall of Adam, the atoning sacrifice of Jesus. She brought to her
classroom as honored guests Moses, Joshua, Peter, Thomas, Paul, and Jesus the Christ. Though
we did not see them, we learned to love, honor, and emulate them.
</p><p>
<b>Lesson on giving</b>
</p><p>
Never was her teaching so dynamic nor its impact more everlasting as one Sunday morning
when she sadly announced to us the passing of a classmate's mother. We had missed Billy that
morning, but knew not the reason for his absence. The lesson featured the theme, "It is more
blessed to give than to receive." (<span class="citation" id="8119"><a href="javascript:void(0)" onclick="sx(this, 8119)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(8119)">Acts 20:35</a></span>) Midway through the lesson, our teacher closed the manual and
opened our eyes and our ears and our hearts to the glory of God. She asked, "How much money
do we have in our class party fund?"
</p><p>
Depression days prompted a proud answer: "Four dollars and seventy-five cents."
</p><p>
Then ever so gently she suggested: "Billy's family is hard-pressed and grief-stricken. What would
you think of the possibility of visiting the family members this morning and giving to them your
fund?"
</p><p>
Ever shall I remember the tiny band walking those three city blocks, entering Billy's home,
greeting him, his brother, sisters, and father. Noticeably absent was his mother. Always I shall
treasure the tears which glistened in the eyes of all as the white envelope containing our precious
party fund passed from the delicate hand of our teacher to the needy hand of a heartbroken
father. We fairly skipped our way back to the chapel. Our hearts were lighter than they had ever
been; our joy more full; our understanding more profound. A God-inspired teacher had taught
her boys and girls an eternal lesson of divine truth. "It is more blessed to give than to receive." (<span class="citation" id="124438"><a href="javascript:void(0)" onclick="sx(this, 124438)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(124438)">Acts 20:35</a></span>)
</p><p>
Well could we have echoed the words of the disciples on the way to Emmaus: "Did not our hearts
burn within us . . . while [she] opened to us the scriptures?" (<span class="citation" id="31813"><a href="javascript:void(0)" onclick="sx(this, 31813)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(31813)">Luke 24:32</a></span>)
</p><p>
<b>A worthy compliment</b>
</p><p>
I return to the dialogue mentioned earlier. When the boy heard the taunts: "My dad is bigger than
yours," "My dad is smarter than yours," "My dad is a doctor," well could he have replied: "Your
dad may be bigger than mine; your dad may be smarter than mine; your dad may be a pilot, an
engineer or a doctor; but my dad, <i>my dad is a teacher.</i>"
</p><p>
May each of us ever merit such a sincere and worthy compliment, I pray humbly, in the name of
the master teacher, even the Son of God, Jesus Christ the Lord. Amen.
</p>
</div>
</div>
