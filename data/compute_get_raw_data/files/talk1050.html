<?xml version="1.0" encoding="UTF-8"?>
<div class="gcera">
<div class="gchead">
<p class="gctitle">The Ultimate Objective</p>
<p class="gcspeaker">Elder Richard L. Evans</p>
<p class="gcspkpos">Of the Council of the Twelve Apostles</p>
<p class="gcbib">Richard L. Evans, <i>Conference Report</i>, October 1959, pp. 126-128</p>
</div>
<div class="gcbody">
<p>
Each year on a day so designated, we recall the birth and accomplishment of Christopher
Columbus, a man no doubt inspired of God to do what he did, against all ignorance, against
all odds and obstacles. He is a symbol, one among many, of the difficulties men can endure if
they have sufficient faith in an ultimate objective.
</p><p>
The heroes of history, and the lives of those less known, have proved they could endure
working and waiting and great difficulty and discouragement, if there were some purpose,
some hope, some reasonable assurance of the ultimate objective.
</p><p>
The long hard journey is not too long if "home" is at the other end. But aimlessness would
give men little reason for lengthening out the effort, without some assurance, without some
real and solid incentive.
</p><p>
Remembered are the words of Robert Browning: 
</p><p>
"Ah, but a man's reach should exceed his grasp,<br />
Or what's a heaven for?"
</p><p>
But his reach should know that he is reaching for something real&mdash;or his reach will weary of
the reaching.
</p><p>
Everything has to have a reason, a purpose, an ultimate answer. And for such answers men
have searched and sought: Why do we live? What are the purposes of life? Why did the
Creator create? Why, indeed, were worlds brought into being?
</p><p>
For answer we would have to go back to the basic, literal facts of our relationship to God,
who gave us the opportunity of life, and who is in fact the Father of us all.
</p><p>
"In the beginning," we read in sacred writ, "God created the heaven and the earth"
(<span class="citation" id="23653"><a href="javascript:void(0)" onclick="sx(this, 23653)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(23653)">Gen. 1:1</a></span>).
</p><p>
But for answer we would have to go back before this beginning, with God's great plan and
purpose: the Gospel, we have come to call it, which we heard in the heavens before time
began, where we were with our Father, the Father of our spirits, and where we agreed to enter
mortality to prove ourselves and learn the lessons of life, and where we were assured our
Father would send his own beloved Firstborn Son to redeem us from death&mdash;that Son of
whom Paul said, "God . . . hath appointed heir of all things, by whom also he made the
worlds; Who being . . . the express image of his person . . . when he had by himself purged
our sins, sat down on the right hand of the Majesty on high"
(<span class="citation" id="24281"><a href="javascript:void(0)" onclick="sx(this, 24281)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(24281)">Heb. 1:1-3</a></span>).
</p><p>
The whole intent of scripture is one of establishing our relationship with God, our Father, and
with his Son, our Savior, and with the eternal plans and purposes for each and all of us, and
our relationships to life&mdash;and to one another also.
</p><p>
And what <i>are</i> these plans and purposes? What would a loving Father want for his children?
What would any father want for his children? Peace and health and happiness; learning and
progress and improvement; and everlasting life, and everlasting association with those we
love. What less could heaven be? What less would a Father plan or propose, for those he
loves, for those whom he made "in his own image"
(<span class="citation" id="23655"><a href="javascript:void(0)" onclick="sx(this, 23655)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(23655)">Gen. 1:27</a></span>)? He has declared his work
and his glory "to bring to pass the immortality and eternal life of man"
(<span class="citation" id="39120"><a href="javascript:void(0)" onclick="sx(this, 39120)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(39120)">Moses 1:39</a></span>).
This is the ultimate objective. This is the whole purpose of the Gospel he has given.
</p><p>
This makes life meaningful, everlastingly so. This is the assurance that gives incentive&mdash;that
gives faith in the face of all searching uncertainty. This makes life worth all the anguish, all
the effort, as we make our way through the world&mdash;learning that life is for learning, that our
Father sent us here for a period of proving, not to lose our way, but with a light within us to
lead us, if we will be led, to our highest possibilities, with freedom and faith and with a few
simple rules to keep, which we call commandments.
</p><p>
And as to keeping these commandments, we have our choice&mdash;our free agency, as it has
come to be called. How could it be otherwise? How could we grow without it? Who can learn
to make decisions if someone else always does the deciding? As we have to learn to let our
children learn much for themselves (after we have given them all the counsel we reasonably
can), so our Father in heaven has sent us here with freedom to decide for ourselves. And to
help us to decide, he has given us standards, advice, laws, rules. And they are not arbitrary,
unrealistic rules, but are simply counsel from a loving Father, who knows us, who knows our
nature. It is not his purpose that his children should be unhappy. No father intends to have his
children unhappy. And for this reason he has given us commandments for our health and
happiness, and peace and progress and quiet conscience.
</p><p>
In a remarkable commencement address, some months before he left this life, Mr. Cecil B.
DeMille made this moving observation concerning freedom, and the purpose of life, and the
keeping of the commandments: 
</p><p>
"We are too inclined to think of law as something merely restrictive," he said, "something
hemming us in. We sometimes think of law as the opposite of liberty. But that is a false
conception. That is not the way that God's inspired prophets and lawgivers looked upon the
law. Law has a twofold purpose. It is meant to govern. It is also meant to educate . . .
</p><p>
"God does not contradict himself. He did not create man and then, as an afterthought, impose
upon him a set of arbitrary, irritating, restrictive rules. He made man free&mdash;and then gave
him the commandments to keep him free.
</p><p>
"We cannot break the Ten Commandments. We can only break ourselves against them&mdash;or
else, by keeping them, rise through them to the fulness of freedom under God. God means us
to be free. With divine daring, he gave us the power of choice." (Excerpts from the
Commencement Address at Brigham Young University, May 31, 1957.)
</p><p>
In our own day and dispensation, the Lord has restated the law of cause and
effect, with these words: "There is a law, irrevocably decreed in heaven before the
foundations of this world, upon which all blessings are predicated&mdash;And when we obtain any
blessing from God, it is by obedience to that law upon which it is predicated"
(<span class="citation" id="13809"><a href="javascript:void(0)" onclick="sx(this, 13809)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(13809)">D&amp;C 130:20-21</a></span>).
</p><p>
The commandments are not old-fashioned, out-dated, or merely man-made. They apply to our
own as to other ages. And whenever we do anything basically against them, we pay a 
price&mdash;not because someone has said so, but because we are what we are, and because we are
irrevocably affected by the very laws of life. No matter what someone says, and no matter
who would set them aside, there are still heartaches and heartbreaks and inescapable
consequences for those who lie and cheat and bear false witness; for those who are immoral
and unfaithful to loved ones; for those who abuse themselves physically, who indulge
appetites, who acquire harmful habits; for those who set aside sure and safe standards, who
are coarse in conduct, and run contrary to the commandments, to the basic laws of life.
</p><p>
To find peace the peace within, the peace that passeth understanding&mdash;men must live in
honesty, honoring each other, honoring obligations working willingly, loving and cherishing
loved ones, serving and considering others, with patience, with virtue, with faith and
forbearance, with the assurance that life is for learning, for serving, for repenting, and
improving. And God be thanked for the blessed principle of repenting and improving, which
is a way that is open to us all.
</p><p>
There is a Kingdom, and there is a King. And there are requirements for citizenship in the
Kingdom&mdash;commandments, laws, ordinances, and obligations, and what is required of us for
peace in this world, and exaltation in the world to come, is to follow him and keep his
commandments.
</p><p>
We would witness this day that the Lord God lives, and that our Lord and Savior Jesus
Christ, his Divine and only Begotten Son, did redeem us from death, and even now is our
advocate with the Father, and sits by his Father's side, and that the fulness of the Gospel is
again on earth with power and authority to administer in its saving and exalting ordinances.
</p><p>
There is this certainty of assurance also: that he is willing to reveal his mind and will to us
today, to guide us, to hear and answer prayer, to open his arms to the prayerful and repentant,
even as he has done in other days.
</p><p>
And against the tension and trouble of our time&mdash;against injustice, threats, and force and
fear; want and worry; discouragement and despondency; unfaithfulness and duplicity; and
much of misunderstanding, and much of inhumanity from man to man&mdash;against all this there
is the blessed assurance of the glorious ultimate objective: of salvation for all, as offered by
our Savior, and of exaltation for those who will work at it and win it; of justice, of
compensation, of the ultimate defeat of evil; of peace and of progress and health and
happiness, of everlasting life with sweet reunion with loved ones.
</p><p>
And this day we would plead with all men, the searching and the sorrowing, the sick, the
discouraged, those burdened with sin and unquiet conscience; those who feel lost and lonely,
and those who have lost those they love&mdash;to all we would plead: take courage and faith and
assurance, according to the promises and purposes of Him who is the Father of us all, who is
mindful of us all.
</p><p>
By walking in his ways and keeping his commandments, God grant that all of us together
may move on to the glorious ultimate objective that is offered all of us&mdash;to the highest
opportunities of everlasting life, with our loved ones with us, always, and forever, in Jesus'
name. Amen.
</div>
</div>
