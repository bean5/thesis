<?xml version="1.0" encoding="UTF-8"?>
<div class="gcera">
<div class="gchead">
<p class="gctitle">The Virtues of Fathers</p>
<p class="gcspeaker">Elder Sterling W. Sill</p>
<p class="gcspkpos">Assistant to the Council of the Twelve Apostles</p>
<p class="gcbib">Sterling W. Sill, <i>Conference Report</i>, April 1960, pp. 67-70</p>
</div>
<div class="gcbody">
<p>
In the year 428 B.C. a play was being presented in the ancient city of Athens entitled
<i>Hippolytus</i>. This was a Greek tragedy written by Euripides. It was centered around Theseus,
the old king of Athens and his son Hippolytus. Theseus had received from his father,
Poseidon, the Greek god of the sea, three gifts in the form of three curses. These curses not
only had the power of temporal destruction, but they would also continue throughout eternity
to punish anyone against whom they were invoked.
</p><p>
The first of these curses was directed by Theseus against his own son, Hippolytus. Hippolytus
had done no wrong but Theseus had been deceived and did not discover his error until
Hippolytus was on his deathbed. And while Theseus had the power to invoke the curse he did
not have the power to set it aside once it was in operation. And so as the father sat by the
bedside of his dying son he said through his tears, "I weep for your good heart, your true and
upright mind. The gods have cheated me of my good sense." And as Hippolytus lay their
contemplating eternity, he said to his father, "'Twas a bitter gift your sire gave." And then
just before he died he pointed out that he could already see the gates of hell beyond which he
would suffer his own father's curse throughout eternity.
</p><p>
If we had been witnessing this tragic play in ancient Athens, we would probably have joined
our tears with the others not only in feeling sorry for Hippolytus, the victim of this dread
curse, but also more especially for his father who had set it in motion. But Theseus was not
the first to possess this power to curse, nor is he the only one who has turned it against his
own son.
</p><p>
Ten centuries before Theseus was born, God gave ancient Israel their law from the top of Mt.
Sinai, and out of the lightnings and thunders of that holy mountain came the divine warning
that ". . . the sins of the fathers shall be visited upon the children" (see
<span class="citation" id="22677"><a href="javascript:void(0)" onclick="sx(this, 22677)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(22677)">Ex. 20:5</a></span>). The
most effective way to set a curse in operation against one's own son is to develop the cause
of the curse in his own life. And then as our children play with us this interesting game of
"Follow the Leader," it will not be long before the curse will begin to appear in their
lives&mdash;that is, the power to lead, possessed by every parent, is also the power to mislead. The power
to mislead is the power to destroy; it is the power to cause eternal suffering.
</p><p>
It is a little bit startling to realize that this father and son tragedy is being enacted in real life
in many of our own homes. Let me give you a more up-to-date Theseus and Hippolytus story.
</p><p>
A friend of mine recently called me on the telephone and told me that his young son had the
habit of coming home from Sunday School each week and discussing his Sunday School
lesson with his father. Sometimes the father was unable to handle the situation adequately,
and it became necessary for him to get outside help. And on this particular occasion he asked
me if I would help him with the right information. We discussed the idea at some length and
noted the scriptural references that were applicable.
</p><p>
But I suggested to my friend that he could not solve this problem with just one answer. It
would be impossible to keep his son content for very long with the answers that the father got
from someone else. The son would want the father to know the answers for himself. Before
the son was very much older he would also discover that his father did not go to Sunday
School, and he would want to know why. At Sunday School they would
teach the son that some of the things that the father was doing were contrary to the
commandments of God. Then this fine young son would be forced to make some decisions of
his own. Should he follow his father or should he follow the Church? The father is the one
who provides him with his food and his clothing and his love. He is the one who takes him
on picnics and provides for his general welfare. It would be pretty difficult for the Church to
win against that kind of competition. And it is pretty difficult to get the curse stopped once it
is set in motion. If this splendid young son could see the end of his life from its beginning, he
might say to his father as did Hippolytus, that he could already see the gates of hell beyond
which he would suffer eternally for his father's bad example. This situation furnishes us with
a little different setting for the statement of Jesus that ". . . a man's foes shall be they of his
own household" (<span class="citation" id="35035"><a href="javascript:void(0)" onclick="sx(this, 35035)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(35035)">Matt. 10:36</a></span>).
</p><p>
We are greatly disturbed whenever evil is brought upon one person by someone else; for
example we were upset when Russia closed her church doors by governmental decree. Russian
leaders are presently trying to terminate any personal relationship which otherwise might exist
between God and the people of Russia. But what Russia has done officially, many of us are
doing individually. That is, what good does it do if our churches are open if we are not in
them? Or, how much better off are we than the Russians if we do not manifest our faith by
our works.
</p><p>
The chief representative of the great communist state which is disputing our way of life was
recently invited to be our guest in this country. And as he went about among us, he talked of
"burying" us and our way of life. He talked about competing with us in the manufacture of
guided missiles, intercontinental rockets, and other instruments of destruction. He said nothing
about competing with us in freedom or human dignity. He said nothing about competing with
us in the individual welfare of people. And I thought what a stimulating thing it would be if
the great nations were vigorously competing with one another for leadership in faith in God
and the individual righteousness of people.
</p><p>
In 1958 <i>The U.S. News &amp; World Report</i> carried an interesting headline: "What 22 Years of
U.S.-Soviet Talks Have Produced." The article pointed out that during this period 3400
meetings had been held between high diplomatic representatives of the United States and the
Soviet Union. During this time they had made fifty-two major agreements, fifty of which had
already been broken by the Russians.
</p><p>
Fortunately for us our eternal exaltation does not depend upon whether Russia keeps or breaks
her international agreements. But we might ask ourselves if our upsurge in juvenile crime and
delinquency is a satisfactory result of what twenty-two years of dealing with our own children
and with God have produced. During the past twenty-two years we have also attended many
meetings. We have made many major agreements with each other and with God. Some of
these agreements have been made at the waters of baptism; others have been made as we have
received and been advanced in the priesthood. We have made some important agreements at
the marriage altar. And each week we meet before the Sacrament table and witness unto our
Heavenly Father that we will always keep his commandments. Wouldn't it be interesting if
some impartial statistician could determine how many of these important agreements we have
made and how our personal performance percentage compared with the Russians?
</p><p>
We should remember that any disobedience to God or any other offenses that we pick up in
our own lives are soon transmitted to others, particularly our children. That is, the power of
example is the greatest power in the world. That is the way we learn to walk. That is the way
we learn to talk. That is why we speak with the accent we do. That is how we learn to dress
ourselves. That is why we have our hair cut and our clothing tailored the way we do.
</p><p>
I suppose that if I had seen you eat your breakfast this morning I would have discovered that
most of you ate with a fork in your right hand. But I discovered the other day that in certain
parts of Canada the people eat with the fork in their left hand. I suppose the
reason is that they have seen somebody else do it that way. Probably if we had been born in
China we may not have eaten with a fork at all.
</p><p>
The other day I attended a meeting during which someone on the platform yawned. Then I
watched that yawn go all over the audience. The people who were yawning in the audience
were not even aware of why they were yawning. Unconsciously they were following the
example of someone else. That is also the way we get many of our manners, our morals, and
our attitudes.
</p><p>
Thomas Carlyle said, "We reform others when we walk uprightly." And it is just as true that
we destroy others when we walk unrighteously. Even Jesus said, "The son can do nothing of
himself but what he seeth the Father do"
(<span class="citation" id="27902"><a href="javascript:void(0)" onclick="sx(this, 27902)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(27902)">John 5:19</a></span>). <i>Our</i> children will also do what they see
us do. They may not follow our advice, but they will follow us.
</p><p>
One of the important functions in the life of Jesus was to serve as a pattern for us. He gave us
the greatest of all the success formulas when he said simply, "Come follow me"
(<span class="citation" id="31530"><a href="javascript:void(0)" onclick="sx(this, 31530)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(31530)">Luke 18:22</a></span>). And every
life must eventually be judged by how well we follow that one instruction. We also reach our
highest rank while serving as an example for others, particularly our children. It has been said
that the first question that God will ask every parent is, "Where are your children?" Our
responsibility is not just to be mothers and fathers of bodies. We are also appointed to be
mothers and fathers of blessings.
</p><p>
When Alexander the Great was twelve years old his father, Philip, arranged to have Aristotle,
the great Macedonian orator and philosopher, become his companion and tutor. Later
Alexander said that Aristotle was his father. What be meant was that while he had received
his body from Philip, Aristotle was the father of his mind. If you would like to take back to
your work one of the most challenging thoughts that I know anything about, that is it. That is,
physical paternity by itself is an ordinary office, that is something that is participated in by all
of creation from the top to the bottom. But what about mental paternity and spiritual
paternity? Who are the fathers of our ideals, and what kind of fathers do we have for our
children's spirituality?
</p><p>
Some of those being taught by Jesus kept saying, "We have Abraham to our father." Jesus
said to them, "God is able of these stones to raise up children unto Abraham"
(<span class="citation" id="34999"><a href="javascript:void(0)" onclick="sx(this, 34999)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(34999)">Matt. 3:9</a></span>).
Jesus said, "Ye are of your father the devil, and the lusts of your father ye will do"
(<span class="citation" id="27909"><a href="javascript:void(0)" onclick="sx(this, 27909)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(27909)">John 8:44</a></span>).
We ought to exercise the greatest care about our own spiritual paternity.
</p><p>
Fortunately the lesson from Sinai did not end with the decree that ". . . the sins of the fathers
shall be visited upon the children"
(<span class="citation" id="22678"><a href="javascript:void(0)" onclick="sx(this, 22678)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(22678)">Ex. 20:5</a></span>).
It is also true that the virtues of the fathers are
visited upon the children. Theseus received from his father, Poseidon, three great curses. You
have received from your Father in heaven some great blessings which you may direct as you
choose.
</p><p>
Nancy Hanks directed one of her blessings toward her son Abraham Lincoln. And later in his
life he said, "All that I am or ever hope to be I owe to my angel mother." Jesus conferred one
of his blessings upon Simon Peter and raised the life of this humble fisherman to one of great
spiritual power.
</p><p>
We may confer as many blessings as we like, on whomever we like, by the inspiration of our
own lives. We speak a great deal in the Church about our right to <i>receive</i> inspiration from
God, and that is a tremendous blessing. But the thing we don't always understand is our right
to <i>give</i> inspiration. Yet if there should be subtracted from each of us the good that we have
received from someone else, there might not be very much of any of us left.
</p><p>
Some time ago I listened to a great Sunday School teacher recount the thrilling story of
creation. "So God created man in his own image"
(<span class="citation" id="23658"><a href="javascript:void(0)" onclick="sx(this, 23658)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(23658)">Gen. 1:27</a></span>). And as I listened to this story
unfold, I closed my eyes and wished that I could have been there to have seen this great event
take place. Then I remembered something that I have tried not to forget, and that is that the
creation of man is not something that was finished and done with in the Garden of Eden
6,000 years ago. The creation of man is still going on, and we are the creators; that is, we are
creating the faith and the enthusiasm and the attitudes which will determine
what men and women will be throughout all of eternity.
</p><p>
As parents we have helped to create bodies, but that is not the end of our responsibility. We
must also create individual righteousness. Dr. Alan Stockdale has called our attention to an
interesting challenge by saying, "God left a world unfinished for man to work his skill upon.
He left the electricity still in the cloud, the oil still in the earth. He left the rivers unbridged
and the forests unfelled and the cities unbuilt. God gave to man the challenge of raw
materials, not the ease of finished things. He left the problems unsolved and the pictures
unpainted and the music unsung that man might know the joys and glories of creation. God
created the quarries, but he carves the statues only by the hand of man."
</p><p>
God has also left the world of men unfinished. He has left the character unformed, the lessons
unlearned, the testimonies unacquired, and the determination undeveloped. Then as a means of
our accomplishment he has given us this basic, fundamental universal law which says, "We
reap as we sow" (<span class="citation" id="23260"><a href="javascript:void(0)" onclick="sx(this, 23260)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(23260)">Gal. 6:7</a></span>).
But that is only a part of the fact. Mostly we reap as others have sown for
us. We reap as our parents have sown. We reap as our teachers have sown. And one of the
most thrilling ideas in the world is that our children will reap as we sow. This is a part of the
divine law "that the virtues of the fathers shall be visited upon the children."
</p><p>
Each of us has been given a set of the most wonderful blessings, which we may confer upon
whomever we choose. May God help us to use this great eternal power effectively which he
has placed in our hands, I pray in the name of Jesus Christ. Amen.
</p>
</div>
</div>
