<?xml version="1.0" encoding="UTF-8"?>
<div class="gcera">
<div class="gchead">
<p class="gctitle">Time Is on Your Side</p>
<p class="gcspeaker">Elder Loren C. Dunn</p>
<p class="gcspkpos">Of the First Council of the Seventy</p>
<p class="gcbib">Loren C. Dunn, <i>Conference Report</i>, October 1969, pp. 12-14</p>
</div>
<div class="gcbody">
<p>
I knew an athlete some years ago who had tremendous talent. He had almost perfect physical
coordination. In fact, he was so good he would not train, yet his talents still exceeded the
talents and abilities of those around him.
</p><p>
It was demoralizing sometimes for those who had to follow every training rule in order to
bring themselves to speak of physical performance, only to have him exceed them because of
his natural abilities.
</p><p>
But I happened to be at the stadium one afternoon a few years later when this athlete, who
had progressed rather rapidly in a very promising sports career, had what some might call his
moment of truth. He was playing with people who had talents as great as he did, and as the
pace of the game picked up, the pressures began to mount.
</p><p>
He reached inside himself for that great second effort that he always had, but it became
obvious that this time he could not marshal all that he needed. That afternoon marked the
beginning of a gradual decline, which finally found him retiring from the game years before
he should have retired. His original decision to disregard the rules of preparation had, in the
end, cost him many years of performance.
</p><p>
<b>Challenges to standards</b>
</p><p>
Many times we see people around us who violate the patterns of living and the rules
that we have been taught to live by, and they seem to do it without any ill effects. On the
surface it would seem that it may not make any difference whether we live these rules or not,
because those who violate them appear to suffer no consequences. In all ages, it seems, there
have been challenges to those who believe in virtue, honesty, and high moral
standards&mdash;challenges to those who accept these standards as God-given and that they ultimately will
carry their own reward.
</p><p>
"We are always in the forge, or on the anvil," said Beecher; "by trials God is shaping us for
higher things."
</p><p>
These challenges come from many different directions. For instance, there are those who
expound the so-called new morality and say that it matters not if a person participates in free
love, nor does the marriage contract mean that husband and wife should be faithful to each
other. But those who believe this are wrong, and time, which is running out on them if they
don't change, will prove them wrong.
</p><p>
<b>Self-mastery not indulgence</b>
</p><p>
"There are some things which never grow old-fashioned," says President McKay. "The
sweetness of a baby is one. The virtue and chastity of manhood is another. Youth is the time
to lay the foundation for our homes. I know there are those who tell you that suppression is
wrong," he continues, "but I assure you that self-mastery, not indulgence, is the virtue that
contributes to the virility of manhood and to the beauty of womanhood"
(<i>Man May Know for Himself</i>, p. 250).
</p><p>
There are also those who sanction the use of drugs, using such reasoning as the fact that the
use of marijuana is so widespread that it should be accepted and even condoned, for, they say,
it creates no more problems than does alcohol. Those who use this reasoning fail to point out,
however, that alcohol disables over six and one-half million people each year and that
one-half of the fatal traffic accidents in the United States alone are related to excessive
drinking. To recommend the use of marijuana by linking it to alcohol is like approving of a
hepatitis epidemic on the basis that it probably won't be any more damaging than
tuberculosis.
</p><p>
There has been sharp divergence of opinion in the United States over the use of marijuana, so
much so that it prompted the organization of a presidential task force to try to ferret out the
facts. This task force has recently made its initial report, which states, according to an
international newspaper, that the widespread use of marijuana represents a significant mental
health problem. Depending on the dose, it may have substantial detrimental effects on both
the mental and the physical well-being of the user.
</p><p>
Because of these perils, it continues, every effort should be made by the federal government
to curb the import and distribution of marijuana as well as of strong drugs
(<i>Christian Science Monitor</i>, September 15, 1969).
</p><p>
<b>Time on side of integrity</b>
</p><p>
To you who are challenged by others because you believe in the law of chastity,
because you believe that drugs are not the answer, because you believe in such God-given
axioms as "Thou shalt not steal" (<span class="citation" id="22801"><a href="javascript:void(0)" onclick="sx(this, 22801)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(22801)">Ex. 20:15</a></span>)
and "Thou shalt not lie" (<span class="citation" id="22802"><a href="javascript:void(0)" onclick="sx(this, 22802)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(22802)">Ex. 20:16</a></span>),
or because you have a simple and
basic faith in God the Father and in his Son Jesus Christ and in your own eternal worth, just
remember that time is on your side. Be patient, and the same people who challenge you, if
they do not change, will ultimately prove to you, by their lives, that they don't have the
answers&mdash;either for you or for themselves.
</p><p>
This is not to say that it will be easy. Sometimes the desire to be accepted by an individual or
a group causes a person to do things that he really doesn't want to do; but if you can maintain
your integrity, you'll come to understand what Lehi meant when he taught that men are that
they might have joy (<span class="citation" id="3803"><a href="javascript:void(0)" onclick="sx(this, 3803)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(3803)">2 Ne. 2:25</a></span>)&mdash;not
fleeting pleasure but real joy.
</p><p>
<b>Remorse for wrong-doing</b>
</p><p>
Beware also of the temptation to violate the laws of God with the thought in mind that
one can always repent but not really anticipate any remorse as a result of the wrongdoing.
Repentance is a great principle, probably the greatest in the gospel of Jesus Christ; and thank
heaven the Lord holds the opportunity of repentance out to all.
</p><p>
Yet perhaps it would do no damage to occasionally dwell on the awful nature of sin rather
than relying continually on the redeeming qualities of repentance. We have a three-year-old
daughter whom we love very dearly. Not long ago I was doing some studying at my desk at
home, and she was in the room playing with a glass of water that was on the desk. As she
picked up that large glass with her little fingers, I repeatedly warned her that she must be
careful or she would drop the glass, which, of course, she finally did. It shattered as it hit the
floor, and splinters went in every direction.
</p><p>
Showing the patience of a wise parent, I immediately spanked her, explaining to her that the
spanking was the consequence of her insisting on not listening to me by picking up the glass
until it dropped and was broken. She shed some tears and gave me a hug, which she usually
does when she knows she is in trouble, and the event was quickly forgotten.
</p><p>
Since she often plays in her bare feet, I took her out of the room and made every effort to
sweep up all the glass particles. But the thought came to me that perhaps I hadn't gotten all
the splinters of glass, and at some future time when she is playing in that
room, those little feet might find the splinters which went undetected, and she would have to
suffer anew for that which she did.
</p><p>
<b>Repentance brings forgiveness</b>
</p><p>
For a young person to violate the law of chastity or some other commandment and
then to later put his or her life in order, such action, I am sure, will mean the forgiveness of
an understanding and loving God. Yet as that person progresses in life and reaches a point
where he or she enters into a marriage contract and as they have children of their own, it just
might be that a splinter of a previous wrongdoing somewhere on the floor of his or her life
might prick the conscience.
</p><p>
This is not to say that the Lord hasn't forgiven them, but as they begin to understand the full
meaning, the full significance of that which they once did, they may find it unfortunately
difficult to forgive themselves. And perhaps this is ultimately the hardest part of repentance,
being able to forgive one's self in light of the seriousness of the transgression. Certainly in
this, as in all other things, we need the help of the Lord.
</p><p>
<b>"Wickedness never was happiness"</b>
</p><p>
Alma told the truth when he taught his son, Corianton, that "wickedness never was
happiness" (<span class="citation" id="9116"><a href="javascript:void(0)" onclick="sx(this, 9116)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(9116)">Alma 41:10</a></span>).
</p><p>
It is my testimony that the teachings of the Church of Jesus Christ are for the purpose of
saving all mankind from the remorse of wrongdoing; that time is on the side of those who
hold to these principles and is working against those who do otherwise.
</p><p>
May the Lord help us to appreciate the eternal nature of the laws which he has given us and
their purpose, which is to bring joy, happiness, and peace of mind to man. And to this may I
add my witness of the truthfulness of the gospel of Jesus Christ. I know that God lives and
that Jesus Christ is his Son. I know this. I know that this is the Church of Jesus Christ and
that it is led by revelation, in the name of Jesus Christ. Amen. 
</p>
</div>
</div>
