<?xml version="1.0" encoding="UTF-8"?>
<div class="gcera">
<div class="gchead">
<p class="gctitle">The Significance of Belief</p>
<p class="gcspeaker">Elder Albert E. Bowen</p>
<p class="gcspkpos">Of the Council of the Twelve Apostles</p>
<p class="gcbib">Albert E. Bowen, <i>Conference Report</i>, October 1945, pp. 64-69</p>
</div>
<div class="gcbody">
<p>
There is perhaps nothing so important to the individual as what he believes nor nothing so
important to communities and nations as what their peoples in the aggregate believe. I mean
really believe. And there is often a wide distinction between what men say they believe&mdash;the
professions they make&mdash;and the reality of their convictions. I want to talk a little today about
the significance of belief.
</p><p>
THE SIGNIFICANCE OF BELIEF
</p><p>
Jesus seems to have attached supreme importance to it. His concern was that men should
believe him, accept his message. Conscious that this shaping of thought and establishment of
conviction would be a slow process, extending far out beyond the span of his earth life, he
spent a good portion of the years of his ministry in training a few disciples, whom he had
gathered about him, to carry on after he himself should be gone.
</p><p>
He had no temporary or ephemeral communication to impart; it was a world-shaking doctrine
and was intended for perpetuity. Neither was it provincial in its scope. Though his whole life
had been spent in a small subject province, apparently more tempestuous and troublesome
than important, his vision ranged out over the whole earth wherever men are. Accordingly, he
gave those disciples he had taught, a commission accompanied by a promise. The commission
was to go into all the world and teach his message to every creature
(<span class="citation" id="32988"><a href="javascript:void(0)" onclick="sx(this, 32988)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(32988)">Mark 16:15</a></span>).
The promise is: "He that
believeth . . . shall be saved" (<span class="citation" id="32989"><a href="javascript:void(0)" onclick="sx(this, 32989)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(32989)">Mark 16:16</a></span>).
Thus is belief made the starting point of all
progress. It is true that certain other things were required to be done as a condition to the
fulfilment of the promise, but these of necessity must come as a consequence of belief.
Without that there is no chance that compliance with requirements would follow. There is no
promise except to him who believes. Belief here signifies a complete acceptance which in its
turn compels conformance to the teaching espoused. The condition is not satisfied by a mere
lip service. Professions of belief, no matter how vehemently protested, amount to nothing
unless they eventuate in conforming deeds.
</p><p>
Jesus had ample demonstration of this during his own ministry. The multitudes followed so
long as they were recipients of his material benefactions. It is said that his fame went
throughout all Syria, and they brought their sick, and he healed them. They came from
Galilee, and from Decapolis, and from Jerusalem, and from beyond Jordan. They sat at his
feet on the mountainside where he fed them because whereof they would forcibly have
crowned him king (<span class="citation" id="27187"><a href="javascript:void(0)" onclick="sx(this, 27187)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(27187)">John 6:15</a></span>),
but he escaped from them and went by night to the other side of the lake
where the crowds next day clamorously sought him out. Then when he began to unfold to
them the personal requirements devolving upon recipients of his teaching they melted rapidly
away, and the record says, "They walked no more with him"
(<span class="citation" id="27191"><a href="javascript:void(0)" onclick="sx(this, 27191)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(27191)">John 6:66</a></span>). They had no belief. They were
seekers after personal gain without appetite for reformation from practices which centered in
themselves.
</p><p>
Contrast with that his belief in his own message. It cost him his life, but he carried through.
And what about those disciples he had picked and taught? The message was just as
unwelcome coming from them as it had been coming from their master. They too were
hounded and scourged and hunted down. As believers multiplied, persecution intensified until
finally extermination was decreed. Then came the supreme test of their sincerity of belief.
Believers were ferreted out, confined to dungeons, and condemned to die. They were thrown
to wild beasts for the entertainment of the populace at the arena. They suffered themselves to
be torn limb from limb because their belief had settled into convictions so deep seated that
they would die rather than recant. They did not have to die. They could have saved
themselves by a very simple act. They had only to renounce and they could have gone free
but that was not their kind of belief. It was more precious to them than life itself. It was the
kind of which Jesus spoke when he promised that "He that believeth . . . shall be saved"
(<span class="citation" id="32990"><a href="javascript:void(0)" onclick="sx(this, 32990)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(32990)">Mark 16:16</a></span>). That
is to say, a saving belief must be one that persists in all extremities. Where is it to be found
in the earth today? It does not deal in expediencies. It does not inquire about the trend of
popular favor. With principle-sacrificing compromises it has no commerce.
</p><p>
LIVING FAITH INVINCIBLE
</p><p>
It may be thought or said that this quality of faith is exemplified in the armed conflict just
closed. Our soldiers and sailors and airmen sacrificed their lives for a cause. That, however,
was in resistance to a physical assault. It was meeting physical force with physical force. We
believed that we were in danger of physical subjugation with consequent penalties. Our
countrymen went out to repel that danger and keep us free from physical domination with a
consequent train of other ills. The test comes now. Have we a set of principles believed by us
to be right which we are willing to preserve at any sacrifice and at any cost&mdash;principles
affecting our internal integrity founded in righteous law and justice? That question is not
answered yet. So far as the signs indicate, the outlook is not promising. The indications seem
to point to the rule of expediency and battering with evil for the best bargain we can get. But
I do not now want to talk about that.
</p><p>
Was the course taken by the early Christians justified?
Might there not have been some other way, some expedient, some give-and-take
accommodation to reconcile antagonisms without going to the extremity of death? Was it
prudent to flout the power of the great Roman empire and persist in a course which it
condemned? It would seem indeed presumptuous that a little handful of despised people,
destitute of wealth or influence, should resist the edicts of the greatest secular power of the
world. But such is the force of doctrine, the power of unwavering belief, the strength of
sincere men with resolute conviction that their teachings flourished in the face of the direst
persecution. The might of the empire could not crush that kind of faith living in the human
heart. It did not extinguish Christianity. And to that circumstance is owed the perpetuation of
Christian teaching in the world. If we want to know how great that debt is, we have only to
ask what kind of void would be left if the effect of nearly two thousand years of that teaching
were blotted out. We should lose substantially all that distinguishes the Christian nations from
the non-Christian nations. We should lose the sense of distinction which has led us in this day
so heartily to condemn barbarities which have shocked humanity. The very freedom of which
the western world boasts, reaching its greatest perfection in the United States of America,
owes its existence to the Christian teaching about human brotherhood and the worth and
dignity of the human soul. These are fruits of that message which Jesus commissioned his
disciples to bear to all the world. The very progress of invention and scientific discovery
itself, which has done so much for the material emancipation of man, is born of that freedom,
as comparison with the backward nations of the world will disclose. It is to Christ's
message&mdash;not to scholastic research&mdash;that we turn for what we know about the meaning and
purpose of life and the controlling power of spiritual and moral law. No one may conceive the
degree in which the world would be impoverished if that little band of disciples had not
devoutly believed and through their belief perpetuated the teaching.
</p><p>
THE TEACHINGS OF CHRIST A STANDARD FOR ALL AGES
</p><p>
Its doctrine has been so thoroughly absorbed into the life of Christian nations, particularly our
own, that quite unconsciously men resort to it as furnishing the standard for measuring the
validity of the acts of their fellows. When we call some things good and others bad, some
courses right and others wrong, we are evaluating them by comparison with the standards set
in the teachings of the Master. When the politician condemns the practices of his adversary in
matters of human behavior, he resorts to Christ's teachings, perhaps unknowingly, for the
ideal by comparison with which the criticized acts are revealed as wrong. His own promises
of betterment likewise are in the pattern of ideals drawn from the same source. All that we
have that is best in our individual lives and in our national life we draw from what was
preserved to the world by those sacrificing early Christians who through suffering and death
perpetuated the teachings that had been committed to them. It is fresh in the memory of all of
us, growing out of recent and earlier example, that tyrants seeking to impose their evil
despotisms have to begin by a crusade intended to root out and destroy the doctrines ingrained
in their peoples through centuries of absorption of Christian thought. In these considerations
lies the answer to the question whether the sacrifices made were justified and whether by
compromise, accommodation, and the practice of expediency those early sufferers might have
avoided persecution and conflict with the empire. Over and beyond all these, those doctrines
taught the achievement of immortality through the Lord's death and the plan for achieving
eternal life, which is exaltation in the celestial kingdom of God.
</p><p>
CONVICTION MAKES A PEOPLE STRONG UNDER TRIAL
</p><p>
Let us take another example out of our own history. Our people began the building of their
city at Nauvoo stripped bare. They had been pillaged, despoiled, and driven. In a scant six
years they had established a flourishing city. Many of their houses still stand, respectable
dwellings in this modern day. They were driven out in winter. From the Iowa shores of the
river they could see the lights in their comfortable homes while babes were born in wind and
sleet with no other protection than that afforded by their canvas-covered wagons. From there
they made their weary journey across prairies, through rivers, and over mountains to these
desolate valleys. The line of their march was marked by the graves of their dead. Here they
endured privation, hardship, hunger. They didn't have to do any of that. They would have had
only to renounce their belief to be let alone where they were. This is demonstrated by the fact
that some did just that and remained unmolested. They chose rather to endure the want and
misery and suffering that became their portion because they believed. That is the quality of
belief that saves. There is no lip service in that. It transcends the bounds of pretense and sham
and self-seeking, and anchors itself in unyielding conviction.
</p><p>
It is possible to say that they were wrong&mdash;mistaken in their belief. It is possible to say that
they were imprudent and unwise. But it is not possible to deny the depth of their conviction
nor the integrity of their manhood. Neither may the power of their belief be gainsaid. Their
achievement stands revealed to the world. Men do not gather grapes from thorns nor figs from
thistles (<span class="citation" id="33965"><a href="javascript:void(0)" onclick="sx(this, 33965)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(33965)">Matt. 7:16</a></span>).
Those who do not accept their beliefs may feel themselves justified. But we who
profess their faith may not escape its high demands. Are we willing to endure what they
endured for our beliefs? Do those beliefs mean so much to us? Are they that important in our
appraisals? Either those beliefs are founded in truth or they are not. If not, then the system
reared upon them should in the nature of things have fallen apart long ago. If they are so
founded, then those who profess them may not temporize. We today may not meet the same
tests they did, but we shall have to face tests just as searching and perhaps harder to be borne.
</p><p>
We still have to carry the banner. The doctrines by which they lived still require to be
perpetuated and spread. Their perpetuation exacts of us the same unwavering steadfastness of
conviction as actuated those who established us here. We may not be dispossessed, or driven
out, or find a wilderness to reclaim. But we shall be engaged by counter influences perhaps
more insidious and therefore more difficult to discern, less easy to understand or to sense the
danger of, for we live in a world seething in a welter of confusion.
</p><p>
BELIEF IN AND PRACTICE OF TRUE RELIGION THE SAFETY OF THE WORLD
</p><p>
Whether we recognize it or not, it is beliefs&mdash;the beliefs that get themselves accepted&mdash;that rule
the world. Those beliefs may exalt a nation or drag it down to degeneracy and degradation
depending upon their inherent quality. Ships and tanks and airplanes and guns, while
necessary implements for waging physical warfare, are not the real source of a nation's
strength. Its strength lies in the basic integrity of its people and that depends upon the beliefs
they cherish which fashion their lives. The shooting war is over, but peace in its accurate
sense is not here. It will not be until it is set up in the hearts of men. The war of ideas is still
raging in the world. Opposing beliefs are contending for supremacy. All are clamorously
recruiting converts. The business of shaping thought, establishing beliefs, getting ideas
accepted, is the most important as well as the most active and flourishing business in the
world today.
</p><p>
It is with ideas, beliefs, that we are concerned, for the very safety of the world and of
mankind depends upon the nature of the beliefs that get themselves adopted. Our message is
the same message Jesus gave to his disciples, namely that men should believe in him, a belief
with a conviction that eventuates in living his doctrines. The commission to spread it in the
world still stands. The means remains the same, teaching by those who believe. It cannot be
done by unbelievers. The doctrines still possess saving power. "He that believeth . . . shall be
saved" (<span class="citation" id="32991"><a href="javascript:void(0)" onclick="sx(this, 32991)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(32991)">Mark 16:16</a></span>).
But there can be no compromise of principles.
</p><p>
There will be scoffers and deriders. Can we stand derision and still stand unmoved? There
will be those in and out of our own membership who will deplore as trivial the differences of
belief which set us apart by ourselves, who will recommend that for the sake of easy
fraternization we relax in our distinguishing doctrines enough to extinguish apparent
differences. It is so much easier and more comfortable to conform to the customs and ideas
about us. A little dilution of our beliefs, it will be said, can do no harm.
</p><p>
Probably the disciples of Jesus could have escaped persecution if they had been willing to
yield a little and had contended themselves with proclaiming him as a great teacher. If they
had just refrained from declaring that he was the Son of God, they probably would have had
little difficulty. That would have made their teaching palatable and improved the social and
fraternal relations between them and their neighbors. But his Messiahship was the essence of
his message. It is the thing that gave it authority. It was that which gave it saving power.
Delete that, for purposes of courting favor and being agreeable, and you have robbed it of its
whole value. It is to his doctrines, including his achieving for us immortality, that we must
turn for understanding of the meaning and purpose of life out of which understanding must
ultimately come the peace for which the world longs. Those doctrines embrace a whole course
of living as a preparation for eternity of life in God's kingdom. And if political persuasions or
economic prejudices or social theories or fraternal felicity collide with those teachings, the
teachings must still stand. Unfortunate indeed is any man who has exalted any of these above
or on a plane of equality with the teachings of his religious faith.
</p><p>
If we really believe in our souls in the doctrines of our faith, then they must take precedence
over all other philosophies or enticements. They cannot take second place to any persuasion.
</p><p>
I am sure that no one will suppose that I am advocating aloofness or presuming to suggest
that we regard ourselves as being above or better than others. I do not wish to incite any
antagonisms nor to invite unfriendliness of intercourse. I am only trying to say that having as
we profess God-given, saving principles, we must hold them sacred, for on them the hope of
eternal happiness as well as earthly peace hangs. We should poorly serve humanity if for
convenience we gave them a stone when their crying need is for bread
(<span class="citation" id="33963"><a href="javascript:void(0)" onclick="sx(this, 33963)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(33963)">Matt. 7:9</a></span>).
</p><p>
May God give us the sincerity of conviction to meet every test, I pray, in the name of Jesus.
Amen.
</p>
</div>
</div>
