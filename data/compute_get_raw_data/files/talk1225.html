<?xml version="1.0" encoding="UTF-8"?>
<div class="gcera">
<div class="gchead">
<p class="gctitle">The Errand of Youth</p>
<p class="gcspeaker">Elder Boyd K. Packer</p>
<p class="gcspkpos">Assistant to the Council of the Twelve Apostles</p>
<p class="gcbib">Boyd K. Packer, <i>Conference Report</i>, October 1962, pp. 47-50</p>
</div>
<div class="gcbody">
<p>
I wish to speak to young people everywhere. I confess to being partial to those of you who
are in your teens. The very qualities that cause some of us who are a bit older to worry about
you&mdash;youthful exuberance, resistance to restraint and domination&mdash;when matured a little
will be your great strength.
</p><p>
When we hear the question, and we often do, "What is wrong with our teenagers?" I want to
thunder out, "The only thing wrong with teenagers is that there aren't enough of them." I
wish, earnestly wish, that this could be a private conversation, for I am prompted to talk to
you about a very personal and sacred matter. But I have such faith in you to be willing to talk
to you about this subject when your fathers and mothers are present. In fact, I think you will
come to know how important it is to have them present.
</p><p>
I take my text from the Book of Mormon. Jacob, a great Book of Mormon prophet, was
teaching his people in the temple, and we find this descriptive verse: "Wherefore I, Jacob,
gave unto them these words as I taught them in the temple, having first obtained mine errand
from the Lord" (<span class="citation" id="25843"><a href="javascript:void(0)" onclick="sx(this, 25843)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(25843)">Jacob 1:17</a></span>).
I repeat, "having first obtained mine errand from the Lord." It is
about this errand, your errand, that I would speak.
</p><p>
Not too long ago I rode for several hundred miles with a group which included a boy named
Henry. Though Henry was just in his early teens I was impressed with his inquisitive nature,
with his searching, intelligent questions, and I thought, "Here is a young man with whom I
can talk man to man about things spiritual." Henry has already obtained part of his errand. He
is planning years ahead for service in the mission field. In The Church of Jesus Christ of
Latter-day Saints there is not only room for young men and women, but you are needed here.
The majority of nearly 12,000 full time missionaries serving throughout the
world&mdash;in Yokohama and Hong Kong, in Melbourne and Auckland, in Santiago and
Hermosillo, in Hamburg and Vienna&mdash;the great majority are young men just past nineteen
years of age. In this Church you are not only given full opportunity and full responsibility,
but also full ecclesiastical authority. It is when I contemplate this that I repeat, here teenagers
are not just tolerated, here they are needed. And it is when I contemplate this that I want to
repeat again, the only thing wrong with you teenagers is that there aren't enough of you.
</p><p>
When I speak, I include in this errand all of you, not just those of you who have already
distinguished yourselves&mdash;the captain of the football team, the valedictorian, the college or
high school beauty queen. You are included, but I am speaking at least as much to you who
consider yourselves nobody or at best just anybody. Some of you have been involved in
serious trouble and difficulty that is only partly of your own making. Some of you I am sure,
feel your parents don't love you. In this I am sure you are mistaken. Some of you feel that
because of these mistakes that what I say shall not apply to you. You may even feel that no
one has a regard for you, that even the Lord doesn't love you. In this you are most certainly
in error.
</p><p>
If you obtain your errand in life from the Lord, there is a special spiritual preparation
necessary. It is something you must do alone, each of you, individually, by yourselves. It is
intimate and personal and sacred. It relates to the most delicate and sensitive of your feelings,
and it is only in the spirit of reverence that I approach this subject with you.
</p><p>
To achieve this spiritual preparation you must set out on a quest. The quest has all of the
aspects of high adventure. It will require the gallantry of knighthood, all of the virtues of the
storybook princess. It will take the resourcefulness of the pioneer, the courage of the
astronaut, and the humility of a true saint. It will require some unteenage-like maturity. I say
this because right now as teenagers you are trying to assert yourselves, trying to say to the
world, mostly to yourselves, "I am <i>somebody</i>." But, this preparation will require some
different attributes, some that perhaps have not matured in you as yet. It is almost out of
keeping with your teenage personalities for you to be submissive and humble, isn't it?
</p><p>
Recently I was tucking one of our little boys in bed. He was just five. There had been a
difference of opinion as to whether it was bedtime or not. He had been guided gently to bed
with something less than democracy. He looked up at me from under the covers and gritted
his little teeth and said, "You not in charge of me." Wise beyond his years he spoke just like
one of you teenagers. And, it is against this natural expression of youth that you will find
your greatest contest.
</p><p>
The errand, the quest is the search for a testimony&mdash;an individual conviction, a certain
knowledge that Jesus is the Christ, that God lives. Although much of religious expression is in
group activity, this matter of testimony is not. It is individual&mdash;on your own, by yourself. It
is because I have such confidence in you that I approach this sacred subject. I have
confidence in all of the Henry's and the Bob's and Diane's and Beverly's and Allen's, and so
I speak pointedly to you.
</p><p>
The Prophet Joseph Smith was about your age, in his fifteenth year,
when he wanted to know for himself, for sure, what his errand in life should be. And, after
reading James, chapter 1, verse 5: "If any of you lack wisdom, let him ask of God, that giveth
to all men liberally, and upbraideth not; and it shall be given him"
(<span class="citation" id="26156"><a href="javascript:void(0)" onclick="sx(this, 26156)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(26156)">James 1:5</a></span>), he came to the conclusion,
". . . I must either remain in darkness and confusion, or else I must do as James directs, that
is ask of God. I at length came to the determination to 'ask of God' concluding that if he
gave wisdom to them that lacked wisdom, and would give liberally, and not upbraid, I would
venture" (<span class="citation" id="30417"><a href="javascript:void(0)" onclick="sx(this, 30417)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(30417)">JS&mdash;H 1:13</a></span>).
</p><p>
Do you know how to pray, teenager? Have you ever tried it&mdash;by yourself, alone? Have you
ever knelt down and poured out your soul to your Father in heaven, asking for help, asking
him to guide you as you seek for your errand in life?
</p><p>
Joseph Smith sought seclusion, by himself, alone, as a teenage individual to attempt to pray.
He asked the Lord two questions; first, which of all the churches is true, and next, which he
should join (<span class="citation" id="30427"><a href="javascript:void(0)" onclick="sx(this, 30427)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(30427)">JS&mdash;H 1:18</a></span>).
These two questions are appropriate for every teenager to ask, those of you who
are in the Church and those of you who are seeking after truth. Now if you have the
inclination or the desire to find out for yourselves, you are entering in by the way. Again
from the Book of Mormon I quote the Prophet Nephi, who had been speaking to his people
about this matter of testimony, and near the conclusion of his sermon he said: 
</p><p>
"Wherefore, now after I have spoken these words if ye cannot understand them it will be
because ye ask not, neither do ye knock; wherefore, ye are not brought into the light, but must
perish in the dark.
</p><p>
"For behold, again I say unto you that if ye will enter in by the way, and receive the Holy
Ghost, it will show unto you all things what ye should do"
(<span class="citation" id="3686"><a href="javascript:void(0)" onclick="sx(this, 3686)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(3686)">2 Ne. 32:4-5</a></span>).
</p><p>
There is a difference, you know, between saying prayers and praying. Don't expect it all to
come at once. It is worth earning. Your efforts may seem in vain, but pray unceasingly,
unyielding. The Prophet Moroni said: 
</p><p>
". . . dispute not because ye see not, for ye receive no witness until after the trial of your
faith" (<span class="citation" id="22241"><a href="javascript:void(0)" onclick="sx(this, 22241)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(22241)">Ether 12:6</a></span>).
</p><p>
Once you have a testimony of your own, some things won't seem to change a great deal. You
will still have to work for what you get. You won't be immune to illness or death. You will
still have problems to solve, but you will have great strength, and you will be prompted by
the Spirit of the Lord in the solution of these problems. As you accept membership in the
Church, you have the gift of the Holy Ghost conferred upon you. Some of you who are young
members of the Church and some of us who are older have made very little use of this gift. It
is a quiet gift. It is a still small voice. May I illustrate?
</p><p>
Many years ago my parents lived on a modest little farm. They were ordinary people of
humble circumstances. They had prayerfully asked the Lord to bless them with all of the
necessities of life and some of the comforts and conveniences. One Monday morning Father
came in from the field. He had broken the plow. "I must go into Brigham City," he said, "and
get some welding done. Would you like to go?" Mother was washing, but she hastily set
things aside and prepared the youngsters for a trip to town. The big copper boiler was lifted
from the range, the buckets of hot water were set off the stove into the bedroom. Mother took
the youngsters to the front gate where Father soon appeared with the white-topped buggy. As
she put her foot onto the step, she paused and said, "Dad, somehow I think I shouldn't go
with you today." You can imagine the conversation. "But why not? Hurry, time is wasting.
You know you have shopping to do." Mother finally said, "I just <i>feel</i> like I shouldn't go."
Thank goodness Father didn't tease her out of it. "If you feel that way, Mother," he said,
"perhaps you should stay home."
</p><p>
She lifted the youngsters out of the buggy, and you can well guess what they started to do.
Dad shook the reins, the buggy pulled down across the bridge, up the opposite bank and out
of sight, and she has told me many times that she stood there and said to herself, "Now
wasn't that silly of me." She busied herself with her washing again and in a moment or two
she smelled smoke. Everything they owned, much of what they had prayed for, was in that
modest little home. She didn't find the fire until the ceiling of the bedroom burst into flame, a
ceiling made of muslin, sized with glue and wallpapered. A rusted stove pipe had permitted a
spark to fall and settle in the dust atop the ceiling. A bucket brigade from the back pump, and
the fire was soon out, and the incident closes without significance, unless you ask the
question, "Why didn't she go to town that day?"
</p><p>
There is a sentence that has been tremendously important to me in the Book of Mormon.
Nephi in speaking to Laman and Lemuel said: 
</p><p>
". . . Ye have seen an angel, and he spake unto you; yea, ye have heard his
voice from time to time; and he hath spoken unto you in a still small voice but ye were past
feeling, that ye could not feel his words"
(<span class="citation" id="1580"><a href="javascript:void(0)" onclick="sx(this, 1580)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(1580)">1 Ne. 17:45</a></span>).
</p><p>
Again, I say, teenagers, that you are needed in this Church. There is a great mission, a great
errand for you to perform. Young Henry will hardly be prepared in time for his mission call.
Some of us, in our youthfulness, may unwisely want to say to our Father in heaven that
which my little son said to me. We may be tempted to grit our teeth and say to him, "You're
not in charge of me." This spirit is present in the poem "Invictus" which concludes: 
</p><p>
"It matters not how straight the gate&mdash;<br />
     How charged with punishment the scroll.<br />
     <i>I</i> am the master of my fate,<br />
     <i>I</i> am the captain of my soul."<br /><br />
&nbsp; &nbsp;               William Ernest Henley
</p><p>
It takes a spirit different from that if you, teenagers, will find your testimony. The late Orson
F. Whitney of the Council of the Twelve Apostles wrote a poem entitled "The Soul's
Captain." In answer to the declaration "<i>I</i> am the captain of my soul!" Brother Whitney said: 
</p><p>
"Art thou in truth?<br />
     Then what of him who bought thee with his blood?<br />
     Who plunged into devouring seas<br />
     And snatched thee from the flood,
</p><p>
"Who bore for all our fallen race<br />
     What none but him could bear&mdash;<br />
     The God who died that man might live<br />
     And endless glory share.
</p><p>
"Of what avail thy vaunted strength<br />
     Apart from his vast might?<br />
     Pray that his light may pierce the gloom<br />
     That thou mayest see aright.
</p><p>
"Men are as bubbles on the wave,<br />
     As leaves upon the tree,<br />
     Thou, captain of thy soul! Forsooth,<br />
     Who gave that place to thee?
</p><p>
"Free will is thine&mdash;free agency,<br />
     To wield for right or wrong; <br />
     But thou must answer unto him<br />
     To whom all souls belong.
</p><p>
"Bend to the dust that 'head unbowed,'<br />
     Small part of life's great whole,<br />
     And see in him and him alone,<br />
     The captain of thy soul."
</p><p>
Humbly, my teenage friends, I tell you that I as well as all of these brethren here, have made
that quest. Though less qualified perhaps than you, it became my blessing to <i>know</i> for sure
which of all the churches is true, and it is because of experience that I hold out to you, not
just the possibility that God will answer your prayer, but the very certainty of it. We tell you
that in this Church there is love for you. In this Church you are needed. We love you because
the Lord loves you. I bear humble witness that I know that God lives. I know that Jesus is the
Christ, and that he loves all of us, including the youth. And I bear that witness in the name of
Jesus Christ. Amen.
</div>
</div>
