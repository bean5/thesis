<?xml version="1.0" encoding="UTF-8"?>
<div class="gcera">
<div class="gchead">
<p class="gctitle">The Struggle of Life</p>
<p class="gcspeaker">Elder Albert E. Bowen</p>
<p class="gcspkpos">Of the Council of the Twelve Apostles</p>
<p class="gcbib">Albert E. Bowen, <i>Conference Report</i> October 1949, pp. 138-143</p>
</div>
<div class="gcbody">
<p>
As I have listened to the various speakers during this conference, I have been impressed with
the persistence of one theme. Every speaker has urged us all to greater fidelity to principles
and a closer conformance in practice to the teachings of our belief. Now I come to think of it,
I can't remember any time when this was not so. I have no remembrance of sermons in our
religious services which did not exhort the congregation to live in closer harmony with gospel
teachings. Always the admonition is to do better.
</p><p>
ADMONITION TO DO BETTER
</p><p>
So characteristic is this feature that I am led to wonder if listeners might not sometimes be
tempted to ask, "Aren't you ever satisfied?" Can you not tell us for once that we are doing
well enough?" I cannot remember ever having heard such complacency expressed. I have
heard plenty of commendation for the good done and encouragement for the advancement
made. I have heard recitals of incidents evidencing individual deeds of great sublimity
wherein men have risen to lofty heights of spiritual and moral grandeur. These have been
acknowledged as benefactors of mankind and extolled as exemplars of what is praiseworthy.
</p><p>
But always such men and deeds are held forth as exhibitions of the inherent human capacity
to rise above baser instincts and climb to higher standards of goodness. Their attainments, it
will be noted, are rehearsed for their admonitory value&mdash;as a basis for enticing others, in
emulation, to improve themselves by struggling upward to the high plane achieved by their
exemplars. So always the same exhortation, whether expressed in direct terms or by manifest
implication, is there, urging us on to do better, to conform to the standards of our high ideal.
</p><p>
Moreover, I am persuaded on reflection that such will and should always be the case. There
can be no end to importunings for improvement because improvement, growth, progress,
self-betterment is a concept basic to our creed. It is a cardinal principle going to the depths
and bottomed on the meaning and purpose of life.
</p><p>
THE GOSPEL PLAN
</p><p>
The gospel is the revelation of God for the salvation of man. Coming from God it is perfect,
the authentic plan for right living.
</p><p>
If observed in its completeness, it will make men perfect, and ultimate individual perfection,
according to gospel teachings is the goal of life, its real purpose. When men attain it they will
be saved, which is the ultimate of all hopes and aspirations, the inspiration for all
striving. In that matchless sermon delivered from the mountainside,
Jesus admonished his listeners:
</p><p>
Be ye therefore perfect, even as your Father which is in heaven is perfect
(<span class="citation" id="34256"><a href="javascript:void(0)" onclick="sx(this, 34256)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(34256)">Matt. 5:48</a></span>).
</p><p>
And Paul and Timothy, writing to the Philippians said of the Savior that he,
</p><p>
. . . being in the form of God, thought it not robbery to be equal with God
(<span class="citation" id="41235"><a href="javascript:void(0)" onclick="sx(this, 41235)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(41235)">Philip. 2:6</a></span>).
</p><p>
HUMAN FRAILTIES
</p><p>
But men are mortal and beset by human frailties. They are enticed by the pressures of
immediate carnal desire to depart from the high standards of the perfect law. When they are
under the influence of an exalted occasion, they make high resolves. They firmly determine to
avoid past mistakes and to do better. But gone out from under the spell of that influence and
absorbed in the complicated pursuits of life, they find difficulty in holding fast to their noble
purposes. In competition with their fellows they are influenced by the natural instinct to play
a winning game. An opportunity presents itself to turn a good deal, to outsmart a fellow man,
or profit at another's expense by suppressing some facts or misrepresenting others, or
practicing some other form of deception. Or it may be that they see a chance to gain
advantage by evil speaking about a rival or to gratify a debasing appetite or a lustful passion,
and under the pressure of the immediate impulse the high resolve is dimmed, the noble
determination submerged, and they slip below the standard of their ideal. So it is essential that
they come again, and frequently, under the influence which kindles anew the warmth of spirit
in which good resolutions are begotten, that they may go out fortified to withstand the
pressures of temptation which lure them into false ways. Happily, if they refresh themselves
frequently enough under ennobling influences, the spirit of repentance will be at work with
them, and they will make conquest of some temptations&mdash;rise above them&mdash;and advance thus
far toward their final goal.
</p><p>
RESOLUTION TO DO GOOD
</p><p>
That is one reason why, when we congregate together, we must always and forever be
admonished and urged and inspired to renew and strengthen our good determinations, by
degrees to correct our imperfections and advance in the scale of goodness. So long as men are
subject to be lured by ignoble desires from the perfect law of life, they need constant
reminders to bring them back and fortify them against repeated departures. So long as that
condition obtains, which is throughout mortality, just so long will it be needful that
religious services be devoted to admonition and persuasion and, if
may be, to inspiring with the resolution to withstand evil and cleave to the good&mdash;to conquer
even the desire to yield to debasing appetites or passions or to lower themselves to the level
of ignoble deeds.
</p><p>
I trust, therefore, that none of us shall feel that admonitions and exhortations and even
reprovings are offered in the spirit of complaining or of chastisement, but rather as reminders
of the necessity in our own self-interest of moving forward to higher planes. It is one of the
prime offices of religion and of worshiping assemblies that interest should be centered on the
grandeur of purity and perfection of life. It has ever been so, and is not something peculiar to
our day. It is a practice as old as history and must endure to the end of time.
</p><p>
EARLY CHRISTIAN EXHORTATIONS
</p><p>
If you go back to the early history of the Christian Church, you will find it there. The epistles
of Paul, for example, are full of chidings for transgressions, pleadings to forsake evil ways
and exhortations to live righteously.
</p><p>
Know ye not,
</p><p>
he wrote to the Corinthians,
</p><p>
that the unrighteous shall not inherit the kingdom of God? Be not deceived:
neither fornicators, nor idolaters, nor adulterers . . .
</p><p>
Nor thieves, nor covetous, nor drunkards, nor revilers, nor extortioners, shall
inherit the kingdom of God
(<span class="citation" id="175"><a href="javascript:void(0)" onclick="sx(this, 175)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(175)">1 Cor. 6:9-10</a></span>).
</p><p>
He also pleaded with them to put away envyings and strife and dissensions, which he
denounced as carnal and not compatible with the spirit which belonged to those who had
accepted the Christ (<span class="citation" id="3019"><a href="javascript:void(0)" onclick="sx(this, 3019)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(3019)">2 Cor. 12:20</a></span>;
<span class="citation" id="21637"><a href="javascript:void(0)" onclick="sx(this, 21637)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(21637)">Eph. 4:31</a></span>).
The things he warned against are such as reveal blemishes in human
behavior and make manifest its imperfections.
</p><p>
So Peter in his epistle addressed to the saints in Pontus, Galatia, Cappadocia, Asia, and
Bithynia urges:
</p><p>
. . . laying aside all malice, and all guile, hypocrisies, and envies, and all evil
speakings (<span class="citation" id="2237"><a href="javascript:void(0)" onclick="sx(this, 2237)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(2237)">1 Pet. 2:1</a></span>).
</p><p>
He reminded them that in times past, before Christ had been preached to them, they had
walked in lasciviousness, lusts, excess of wine, revelings, banquetings, and abominable
idolatries and admonished them that they must now make an end of these things
(<span class="citation" id="2241"><a href="javascript:void(0)" onclick="sx(this, 2241)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(2241)">1 Pet. 4:1-3</a></span>). He exhorted
them to patience in persecution, long-suffering, endurance of scorn, if need be, because of
forsaking former ways to humility, charity, and steadfastness in the faith, husbands and wives
respecting and fortifying each other (see
<span class="citation" id="2238"><a href="javascript:void(0)" onclick="sx(this, 2238)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(2238)">1 Pet. 3:1-17</a></span>).
</p><p>
These expounders of the early Christian faith, it is to be observed, were not content to
deal in abstractions or to gloss over evil doings lest some might take offense. They
particularized to the degree that no one could be left in doubt as to what they meant. They
neither compromised principles nor softened their censure of wrong. Thus, Paul, after the
sweeping generalization that the "unrighteous shall not inherit the kingdom of God," proceeds
to tell specifically some of the things which make men unrighteous and unfit for the kingdom.
The unrighteous include thieves, the covetous, drunkards, revilers, and extortioners, as well as
those whose hearts are so eaten out with envy that they become breeders of strife and
dissensions. Peter expands the list of things that belong to the qualities of unrighteousness to
include malice, guile, hypocrisies, evil speaking, lasciviousness, lusts, revelings, and
abominable idolatries. These no doubt were practices indulged by the particular congregations
to whom Paul and Peter wrote.
</p><p>
If you will take the trouble to go through the gospels and the letters and epistles and
narratives of the men whom Jesus commissioned to carry his message and perpetuate it in the
world, you cannot help noting the striking sameness of evil things they exhorted against with
the deeds and habits which fall under censure today. The catalogue of vices seems to have
been fairly complete way back in that remote period. There hasn't been very much added, and
there isn't much to subtract from the list. After all the intervening centuries of teaching, we
still need the same admonitions against the same vices. Neither has there been any virtue
added to Christ's teaching. These facts perhaps ought not to prove so startling as they may
seem when recognition of them first bursts upon our consciousness.
</p><p>
STRUGGLE FOR PERFECTION
</p><p>
The persistence of these human frailties from the beginning of the race till now is but an
indication of the heritage of mortality rooted down deep in it. The age-old urging to conquer
them attests that mortal imperfections are antagonistic to other instincts native to the human
family. There is then set up in the individual a conflict between the opposing forces of good
and evil. We should accordingly expect the vices and the virtues respectively, to be essentially
of the same nature till the conflict is over, though there may be differences of degree and of
manifestation. The conquest of evil by the good is the struggle of life. It is the struggle for
perfection and the attainment of salvation which is supremacy over evil. We must not be too
discouraged because progress is slow, for it involves working changes in human desires and
inclinations. Perfection has to be achieved; salvation has to be won. They do
not come as free bestowals. The process seems to be through winning the struggle for
supremacy between human imperfections and the mandates of the God-given perfect law. It is
by meeting adversities, battling down obstacles, rising triumphant over opposing forces that
man builds muscle and moral and intellectual fiber and spiritual stamina. It is the process by
which he has built up his amazing mastery in the physical world and the forces that operate in
it reducing them to servitude and ordering them to his bidding. There is no such thing in this
world as getting something for nothing. Everything has its price. Every step forward in the
realm of human progress, in the amazing advance of man in his mastery in the physical world
has come out of grueling toil and sweat, heartbreaking disappointments and failures and, after
failure returning again to the struggle.
</p><p>
The heights by great men reached and kept<br />
Were not attained by sudden flight,<br />
But they, while their companions slept,<br />
Were toiling upward in the night.<br /><br />
&nbsp; &nbsp; &nbsp; &nbsp;               <i>The Ladder of St. Augustine</i><br />
&nbsp; &nbsp; &nbsp; &nbsp;               Henry W. Longfellow
</p><p>
PRACTICE OF VIRTUES
</p><p>
That inexorable law is operative in the spiritual realm as well as in the temporal domain. It is
the law of life operative in all its aspects that progress, growth, advancement are the result of
struggle and conquest. In the spiritual realm the struggle is between good and evil, a struggle
for the supremacy of righteousness. There is only one way to win in that struggle, and that is
to practice the virtues and cease to practice evil. The formula is simple. It consists in adopting
as habitual behavior that set of principles and teachings which collectively we call the gospel.
There is no other way. Our lives are patterned, our natures formed, our characters established
by the things we do and not by theoretical professions of principles or abstract
contemplations. If you want to overcome envy, you have to practice rejoicing in the good
fortune and successes and attainments of your fellows; if you want to purge yourself of
covetousness, you have to practice generosity and contentment in seeing others prosper as you
would like yourself to prosper; if you want to be rid of reviling, you must practice reverence
and respect for worthy things; if you want to avoid drunkenness, you have to practice
sobriety; if you want to be cleansed of lasciviousness, you have to practice continence and
purity of thought; if you want to conquer thieving, you must practice honesty; if you want to
be free of the vice of extortion, you must practice benevolence and fairness toward others, and
so on we might go till we have enumerated every vice and its opposing virtue throughout the
whole catalogue of gospel precepts. Obey them in practice, make them the governing feature
in your lives and you will win perfection, and hence salvation.
</p><p>
FIDELITY TO GOSPEL LAW
</p><p>
It is easy to conceive that greater progress might have been made if those entrusted with the
teaching of the gospel law had maintained a greater fidelity to its principles. I have already
called attention to the practice during apostolic times of naming the evil practices which must
be done away and recommending conformance to the saving principles of the Christian
teaching. But in the interest of winning converts and spreading power this practice was
relaxed to suit the temper of the world. As Macaulay observed, the surest and easiest way to
win converts is to lower standards. In an early century a great deal of effort was expended in
an attempt to reconcile Christian teaching with pagan philosophy. This was an impossible
task, but an apparent harmony was achieved by bending Christian doctrines into conformity
which resulted in its adulteration and the consequent weakening or destruction of its saving
power. It did win a more universal favor, facilitate the drawing in of greater numbers, but at a
devitalizing cost which always flows from compromising principles of right. It was even
brazenly taught by men in places of power, entrusted with guidance, in the interest of
perpetuating and extending their sway, that certain Christian principles were to be suppressed
because not congenial to people given over to contrary indulgences, so that, as Macaulay
declared: "... instead of toiling to elevate human nature to the noble standard fixed by divine
precept and example," the standard was lowered "till it was beneath the average of human
nature."
</p><p>
Thus was sacrificed the true office of divine worship and guidance, instead of holding up
before men the ideal of the God-given and perfect gospel law and fortifying them for the
struggle incident to the conquest of evil, they were seduced into deadening compromises with
sin, and progress toward the ultimate triumph of righteousness was immeasurably retarded. In
this contemplation it ought to be clear to us that in all our worshiping assemblies it should be
accepted as established usage to be received without resentment, but gratefully, that the law of
God should be reiterated and emphasized and exhortation given for conformance of life
thereto. You leaders cannot discharge your duties as such unless you see that this is done.
Only thus can we be regenerated by the gospel's saving power and through obedience to it
rise triumphant above our mortal imperfections, which may God grant us power to do, I pray
in the name of Jesus. Amen.
</p>
</div>
</div>
