<?xml version="1.0" encoding="UTF-8"?>
<div class="gcera">
<div class="gchead">
<p class="gctitle">The Power of Faith</p>
<p class="gcspeaker">President Hugh B. Brown</p>
<p class="gcspkpos">First Counselor in the First Presidency</p>
<p class="gcbib">Hugh B. Brown, <i>Conference Report</i>, October 1969, pp. 104-107</p>
</div>
<div class="gcbody">
<p>
My dear brethren and sisters, it is a real honor to be included as one of the speakers of
this great conference, an honor, however, that I would gladly exchange with anyone at this
moment; an honor that carries with it some responsibilities. I should like to be in harmony
with what has been said or may be said, and to that end seek divine guidance. 
</p><p>
<b>The power of faith</b>
</p><p>
I should like to briefly discuss with members of the Church, as well as with
nonmembers, a subject of universal interest and import, a subject that is the
moving cause of action&mdash;the power of faith.
</p><p>
We understand that the worlds were framed by the word of God through this principle, "so
that things which are seen were not made of things which do appear"
(<span class="citation" id="24387"><a href="javascript:void(0)" onclick="sx(this, 24387)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(24387)">Heb. 11:3</a></span>).
</p><p>
The predominating sense in which this subject is used throughout the scriptures is that of full
confidence and trust in the being, purposes, and words of God. Such trust, if implicit, will
remove all doubt concerning the things accomplished or promised of God, even though such
things be not apparent to or explicable by the ordinary senses.
</p><p>
Some think religious people are impractical and live in the clouds of unjustified hope. The
notion that science is all fact and religion all faith is fiction. Science, as well as religion, is
based upon faith, for faith is ever "the evidence of things not seen"
(<span class="citation" id="24385"><a href="javascript:void(0)" onclick="sx(this, 24385)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(24385)">Heb. 11:1</a></span>).
</p><p>
<b>Practical value of faith</b>
</p><p>
We do not teach the principle of faith merely for what it will do for one in the next
world. We believe that there is real practical value in mental concepts which increase one's
self-respect and effectiveness here and now. To believe that there is an all-wise Father in
charge of the universe and that we are related to him, that we are in fact children of God with
the "hallmark" of divinity upon us, is to live in a different world from those who believe that
man is a mere animal concerned only with requirements for creature existence, which must
end at death. Because of low aim, the lives of such people lack trajectory and vision and fall
short of their spiritual capacity.
</p><p>
If you convince a young man to think of life, here and hereafter, as being of one piece,
continuing through from premortal to postmortal without any break in the endless chain, if he
can realize that each of the various stages of his development helps to condition him for the
next, if you convince him that he can take nothing but himself into the next world&mdash;his
intelligence, his experience, his character&mdash;if this conviction becomes really dynamic faith, it
will have definite and lasting effect on the quality of his life, both here and hereafter.
</p><p>
<b>Meaning of eternal life</b>
</p><p>
Eternal life means more than merely continuing to exist. Its qualitative value will be
determined by what we believe and do while in mortality and by our conformity to eternal
law in the life to come. Eternal existence would be most undesirable if that existence became
fixed and static upon arrival there. "It is hope and expectation and desire and something ever
more about to be" that gives lilt and verve to mortal life. We cannot imagine nor would we
desire an eternity without opportunity for growth and development. We believe in eternal
progression.
</p><p>
Faith in God and in the ultimate triumph of right contributes to mental and spiritual poise in
the face of difficulties. It is a sustaining power when a confining or antagonistic environment
challenges one's courage.
</p><p>
And so we recommend faith as a present, living power for good here and now as well as for
what it will do for us in achieving salvation hereafter.
</p><p>
If one has a vivid sense of his own divinity, he will not easily be persuaded to deprave his
mind, debauch his body, or sell his freedom for temporary gain. Goethe is right when he
makes Mephistopheles, his devil, say, "I am the spirit of negation." Negation always bedevils
life.
</p><p>
<b>Faith appropriates spiritual values</b>
</p><p>
Wherever in life great spiritual values await man's appropriation, only faith can
appropriate them. Man cannot live without faith, because in life's adventure the central
problem is character-building&mdash;which is not a product of logic, but of faith in ideals and
sacrificial devotion to them. The writer of the Epistle to the Hebrews
(<span class="citation" id="24386"><a href="javascript:void(0)" onclick="sx(this, 24386)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(24386)">Heb. 11:1-40</a></span>) saw the intimate
relationship between the quality of faith and the quality of life and called upon his readers to
judge the Christian life by its consequences in character.
</p><p>
We cannot avoid looking ahead and to some degree basing our activities
upon things we cannot see. But bit by bit we gain assurance. We have some knowledge of
what is and of what has been. But it is necessary that we have faith in what is yet to come.
</p><p>
In this universal venture of life, its full meaning can be understood only by the application of
faith, wherein the best treasures of the spirit are obtainable only through courageous
open-heartedness and the kind of character which is possible to all men of deep conviction.
</p><p>
<b>What faith is not</b>
</p><p>
Every discussion of faith must distinguish it from its caricatures. Faith is not credulity.
It is not believing things you know aren't so. It is not a formula to get the universe to do
your bidding. It is not a set of beliefs to be swallowed in one gulp. Faith is not knowledge; it
is mixed with uncertainty or it would not be faith. Faith does not dwindle as wisdom grows.
</p><p>
<b>Confidence in life</b>
</p><p>
Above all, faith is to be contrasted with pessimism and cynicism. Those who say they
have become disillusioned with life are lost without faith. Faith is confidence in the
worthwhileness of life. It is assurance and trust. Perhaps the greatest contrast to faith is fear.
Jesus often said to his followers, "Be not afraid"
(<span class="citation" id="35929"><a href="javascript:void(0)" onclick="sx(this, 35929)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(35929)">Matt. 14:27</a></span>).
</p><p>
The stern, appealing love of God behind life, his good purposes through it, his victory ahead
of it, and man, a fellow worker, called into an unfinished world to bear a hand with God in
its completion&mdash;here is a game to challenge all stouthearted men.
</p><p>
To believe that we do not stand alone, that we are fellow laborers with God, our <i>human</i>
purposes comprehended in his purpose&mdash;God behind us, within us, ahead of us: this is the
solid rock upon which all rational religion rests.
</p><p>
Man tears his spiritual heritage to shreds in licentiousness and drink. He wallows in vice, wins
by cruelty, violates love, is treacherous to trust. His sins clothe the world in lamentation. Yet
within him is a trust that he cannot stifle. He is the only creature we know of whose nature is
divided against himself. Man hates his sill even while he commits it. He repents, tries again,
falls, rises, stumbles on&mdash;and in all his best hours man cries out for help.
</p><p>
<b>Faith makes the difference</b>
</p><p>
No message short of religion has ever met man's need in this estate. Faith that God
himself is pledged to the victory of righteousness in men in the world, that he cares, forgives,
enters into man's struggle with transforming power, and crowns the long endeavor with
triumphant character&mdash;such faith alone has been great enough to meet the needs of men.
</p><p>
When faith in God goes, man loses his securest refuge and must suffer. Strong men, broken in
health, or men who have lost the fortunes of a life-time, families with long illness, mothers
who have wept at children's graves&mdash;these and other staggering blows test the faith of good
and bad alike. Nothing but religious faith has been able to save men from despair. As Jesus
said, the rains descend, and the floods come, and the winds, whether man's house be built on
rock or sand (<span class="citation" id="35914"><a href="javascript:void(0)" onclick="sx(this, 35914)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(35914)">Matt. 7:24-27</a></span>).
It is faith that makes the difference.
</p><p>
<b>Story of woman of faith</b>
</p><p>
And here I should like to introduce a story coming out of the first world war. I had a
companion, a fellow officer, who was a very rich man, highly educated. He was a lawyer, had
great power, was self-sufficient, and he said to me as we often talked of religion (because he
knew who I was), "There is nothing in life that I would like to have that I cannot buy with
my money."
</p><p>
Shortly thereafter he and I with two other officers were assigned to go to the city of Arras,
France, which was under siege. It had been evacuated, and upon arrival there we thought
there was no one in the city. We noted that the fire of the enemy was concentrated on the
cathedral. We made our way to that cathedral and went in. There we found a little woman
kneeling at an altar. We paused, respecting her devotion. Then shortly she
arose, wrapped her little shawl around her frail shoulders, and came tottering down the aisle.
The man among us who could speak better French said, "Are you in trouble?"
</p><p>
She straightened her shoulders, pulled in her chin, and said, "No, I'm not in trouble. I was in
trouble when I came here, but I've left it there at the altar."
</p><p>
"And what was your trouble?"
</p><p>
She said, "I received word this morning that my fifth son has given his life for France. Their
father went first, and then one by one all of them have gone. But," straightening again, "I
have no trouble; I've left it there because I believe in the immortality of the soul. I believe
that men will live after death. I know that I shall meet my loved ones again."
</p><p>
When the little soul went out, there were tears in the eyes of the men who were there, and the
one who had said to me that he could purchase anything with money turned to me and
said, "You and I have seen men in battle display courage and valor that is admirable, but in all my
life I have never seen anything to compare with the faith, the fortitude, and the courage of that
little woman."
</p><p>
Then he said, "I would give all the money I have if I could have something of what she has."
</p><p>
<b>Faith in hereafter</b>
</p><p>
I tell that story for two reasons. One is that today many parents are getting word that
their sons have been lost. We ourselves went through that experience. I tell it for the reason
that I hope every parent who has a son in danger in Vietnam will have faith in the hereafter,
faith in God, faith in themselves, faith in the immortality of the soul. I tell it secondly
because my own beloved companion, and you will excuse this personal reference, is lying
after long illness listening to this service. I greet her, for she had exactly that kind of courage
and faith when our son was taken from us.
</p><p>
God help us to arise to a point where we can retain faith in the future, whatever it may hold.
We need most of all, when suffering, to remember there <i>is</i> an explanation, though we may not
know exactly what it is.
</p><p>
Religious faith gives confidence that human tragedy is not a meaningless sport of physical
forces. Life is not what Voltaire called it, "a bad joke"; it is really a school of discipline
whose author and teacher is God.
</p><p>
<b>Faith a road to truth</b>
</p><p>
Faith is a road to truth, without which some truths can never be reached at all. The
reason for its inevitableness in life is not our lack of knowledge, but rather that faith is as
indispensable as logical demonstration in any real knowing in the world. Faith is not a
substitute for truth, but a pathway to truth.
</p><p>
However undecided men may appear, they cannot altogether avoid decision on the main
matter of religion. Life will not let them. For a while the mind may hold itself suspended
between alternatives. The adventure of life goes on, and men inevitably tend to live either as
though the Christian God were real or as though he were not. This, then, is the summary of
the matter. Life is a great adventure in which faith is indispensable. In this adventure, faith in
God presents the issues of transcendent import. And on these issues life itself continually
compels decisions.
</p><p>
<b>Faith to endure to end</b>
</p><p>
My brethren and sisters, my friends, humbly I bear witness to you that there is a God
in heaven and that he knows that we are his. He knows who and where we are, and he stands
ready to help us at any time.
</p><p>
God help us that we may live in such a manner that he <i>can</i> help us. May we have the faith to
endure to the end, as we are told that only they who endure to the end can be saved
(<span class="citation" id="35949"><a href="javascript:void(0)" onclick="sx(this, 35949)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(35949)">Matt. 24:13</a></span>). I leave
with you this testimony and my blessing on this occasion, that whatever the vicissitudes of
life may be, you may have the faith, the fortitude, and the courage to meet them triumphantly,
I humbly pray in the name of Jesus Christ. Amen.
</p>
</div>
</div>
