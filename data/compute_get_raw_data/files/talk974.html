<?xml version="1.0" encoding="UTF-8"?>
<div class="gcera">
<div class="gchead">
<p class="gctitle">The Big Three</p>
<p class="gcspeaker">Elder Sterling W. Sill</p>
<p class="gcspkpos">Assistant to the Council of the Twelve Apostles</p>
<p class="gcbib">Sterling W. Sill, <i>Conference Report</i>, October 1958, pp. 102-105</p>
</div>
<div class="gcbody">
<p>
One of the most important businesses in the world is the business of holding conventions. 
This week in every important center in this and other countries men and women will be
assembling in groups to discuss their problems, exchange ideas, and develop techniques for
accomplishment.  I have had an interesting experience during these past few months of
meeting with several occupational groups and listening to them discuss their interests.  After
each experience I have thought how much more interesting, and how tremendously more
important, are the things that we discuss in the Church, where we meet and talk about God
and eternal life and how to build character and godliness into our own lives.
</p><p>
All education is primarily about ourselves.  We study medicine to learn how to keep ourselves
well physically. Through the studies of the mind&mdash;psychology and psychiatry&mdash;we learn
how to keep ourselves well mentally. Agriculture is how we feed ourselves. The social studies
teach us to live together, successfully.  We study law to try to keep ourselves out of trouble.
Then we have this important field of religion by use of which we look out for our spiritual
welfare.
</p><p>
The biggest problems involved in any of these fields center in us.  Probably the thing that we
know less about than anything else in the world is our own individual selves.  You can ask a
man many questions about science, invention, or history, and he will answer you. But if you
ask him to write out an analysis of himself, to tell you about his mind and soul qualities, or if
you ask him how he became the kind of man he is&mdash;you may not get very good answers.  Or
suppose that you ask him where he came from, why he is here, or where he is going.  What
kind of answer do you think you would get? How long do you think it would take someone
to get to a given destination if he didn't know where he was going or why the journey was
being made? "The Big Three" among life's questions are: Whence? Why? Whither?
</p><p>
The old Persian philosopher Omar Khayyam wrestled long and hard with these questions
without getting any very satisfactory answers.  He summarizes his conclusions as follows: 
</p><p>
I came like Water, and like Wind I go.
</p><p>
Into this Universe, and why not knowing<br />
Nor whence, like Water willy-nilly flowing:<br />
And out of it, as Wind along the Waste,<br />
I know not whither, willy-nilly blowing.
</p><p>
Up from Earth's Center through the seventh Gate
</p><p>
I rose, and on the Throne of Saturn sate,<br />
And many a Knot unravel'd by the Road;<br />
But not the Master Knot of Human Fate.<br />
There was a Door to which I found no Key:<br />
There was a veil past which I could not see.
             (<i>Rubaiyat</i>, Stanza 28-29, 31-32.)
</p><p>
Shakespeare's Macbeth gave his opinion of the importance and purpose of existence by
saying,
</p><p>
"It [Life] is a tale told by an idiot, full of sound and fury, signifying nothing" (<i>Macbeth</i>, Act
V, Sc. 5). And Hamlet added,
</p><p>
"How weary, stale, flat and unprofitable, seem to me all the uses of this world! . . . 'Tis like
an unweeded garden, that goes to seed; things rank and gross in nature possess it merely"
(<i>Hamlet</i>, Act I, Sc. 2).
</p><p>
What I would like to say this morning is that some of the most stimulating ideas ever known
in the world are the thrilling answers to the big three given in the revelations of the Lord.
</p><p>
Our lives have been divided into three general periods. First there was a long pre-mortal
existence when we lived as the spirit children of God.  This is followed by a brief mortality. 
Then comes an everlasting immortality. There is a definite purpose to be accomplished in
each of these periods, and our success in each depends upon what we did in those periods
preceding.  In this respect we might compare life with a three-act play.  If you came into the
theater after the first act had been finished and left before the third act began, you might not
understand the play.  For about the same reasons this life, taken by itself, simply did not
make sense to Hamlet, Macbeth, or Omar Khayyam.  Yet each period has great significance.
</p><p>
The Lord has said, "And they who keep their first estate shall be added upon; and they who
keep not their first estate shall not have glory in the same kingdom with those who keep their
first estate; and they who keep their second estate shall have glory added, upon their heads for
ever and ever" (<span class="citation" id="7211"><a href="javascript:void(0)" onclick="sx(this, 7211)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(7211)">Abr. 3:26</a></span>).
</p><p>
In order to make an intelligent road map for the accomplishment of our lives we need to
know what happened in the first act.  We also need to understand the tremendous importance
of those purposes to be achieved in the second act. And we need to know many things about
the third act&mdash;and we need to know them before the third act begins.  I have a relative who
when she reads a book always reads the last chapter first she wants to know where she is
going before she gets started. And that is a pretty good idea to apply to our own future. An
intelligent "preview" of the third act can be all-important to the final outcome. But first,
suppose that we go back and review briefly the first act.
</p><p>
In the pre-existence, as in the two other periods, Jesus is our example. Nothing could be
plainer from the scriptures than that the life of Christ did not begin at Bethlehem, nor did it
end on Calvary. It is equally true that our lives do not begin or end within the narrow
boundaries of mortality. The first things we knew about ourselves were in the grand council
in heaven where our own future was being discussed.  You were there; God was there; all the
spirit children of God were there.  Then we walked by sight. We have all seen God; he is our
father; he was helping to prepare us for the great experiences of our second estate.
</p><p>
All life is primarily a preparation. We prepare for school; we prepare for marriage; we
prepare for our life's work; we prepare for death.  Our preexistence was also a preparation.  It
was the childhood of our immortality. We had come to a place in our preparation where all
young people always come, where it is desirable for them to move away from the homes of
their parents where they can be by themselves.  Even though their newly-established homes
may lack some of the advantages of the homes of their parents, it is still important for them
to learn to stand on their own feet, to be tested, and proven and tried. In our own case, God
wanted us to see good and evil side by side and learn to make the right choices on our
initiative.  We would have far more freedom in this if we were living by
ourselves than in the more immediate presence of God.
</p><p>
In the grand council our second estate was explained to us.  An earth was to be created to
serve as our new home. We were to be given wonderful, beautiful bodies of flesh and bones
without which we could not have a fulness of joy.  For the first time in our existence we were
to be endowed with the powers of procreation.  We were to have the privilege of organizing a
family to last through time and eternity.  This should be bound together by the authority of
the priesthood and sealed and sanctified in the temple of the Lord. We were to have the
opportunity to gain experience in exercising our free agency to help us to become sovereign
souls.  At this grand council the Savior was selected and ordained to come to the earth and
redeem us from our sins.
</p><p>
Abraham, in telling of a vision that he was given of the pre-existence said,
</p><p>
"Now the Lord had shown unto me, Abraham, the intelligences that were organized before the
world was; and among all these there were many of the noble and great ones.<br />
"And God saw these souls that they were good, and he stood in the midst of them and he
said, These I will make my rulers; for he stood among those that were spirits, and he saw that
they were good, and he said unto me: Abraham, thou art one of them; thou wast chosen
before thou was born" (<span class="citation" id="7210"><a href="javascript:void(0)" onclick="sx(this, 7210)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(7210)">Abr. 3:22-23</a></span>).
</p><p>
Adding to Abraham's statement that there were many noble and great who were ordained to
positions of responsibility, Joseph Smith indicates that we were also ordained.  He said,
"Every man who has a calling to minister to the inhabitants of this earth was ordained to that
very purpose in the grand council in heaven before the world was" (<i>DHC</i>, 6:364).
</p><p>
After this part of our preparation had been completed, we are told that "all the sons of God
shouted for joy" (<span class="citation" id="26689"><a href="javascript:void(0)" onclick="sx(this, 26689)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(26689)">Job 38:7</a></span>).
I feel certain that if we knew now what we understood perfectly
then, we would be willing to go on our hands and knees through life for the opportunity of
proving ourselves faithful and deserving of our magnificent opportunities.
</p><p>
Then we came into our second estate through the miracle of birth.  There are some who claim
to have difficulty in believing in the possibility of a literal physical bodily resurrection. It
seems to me that no one should have any problem believing in the eternal life of the body
who can believe in its creation through birth to begin with&mdash;that two microscopic cells can
unite and by a spontaneous process of subdivision create this great masterpiece which is a
human being, including body, mind, and personality.
</p><p>
Referring to the Savior's birth, Matthew said:
</p><p>
"Now when Jesus was born in Bethlehem of
Judea in the days of Herod the king, behold there came wise men from the east to Jerusalem,<br />
"Saying, Where is he that is born King of the Jews? for we have seen his star in the east, and
are come to worship him" (<span class="citation" id="34837"><a href="javascript:void(0)" onclick="sx(this, 34837)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(34837)">Matt. 2:1-2</a></span>).
That is the question that wise men have been asking
ever since.  Ever since that day nearly two thousand years ago, wise men have been inquiring,
"Where can we find Jesus?  How can we know the Savior?" For "there is none other name
given whereby man can be saved"
(<span class="citation" id="13612"><a href="javascript:void(0)" onclick="sx(this, 13612)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(13612)">D&amp;C 18:23</a></span>).
The journey of the wise men was over when they had found the king; and so is ours.
</p><p>
Then we enter the third act.  Most of the rewards come in the last act. There is where we find
"the happy endings." That is also where we discover the tragedies, depending upon the kind
of life we have lived in our second estate.
</p><p>
There is an old Greek play written around the fall of Athens.  It tells of a Roman general who
had captured an Athenian philosopher. The Roman had told the Athenian that he was to be
put to death, but the philosopher did not seem greatly disturbed and the Roman thought that
probably he didn't understand.  And he said to the Athenian that maybe he did not know what
it meant to die. The Athenian expressed himself that he understood but he felt the Roman did
not understand. He said to his captor: 
</p><p>
"Thou dost not know what it is to die, for thou dost not know what it is to
live.  To die is to begin to live.  It is to end all stale and weary work and to begin a nobler
and a better. It is to leave the company of deceitful knaves for the society of gods and
goodness."
</p><p>
That is our proper objective for the last act.  Death is the gateway to immortality.  The most
important part of life is death. James M. Barrie's little character, Peter Pan, in an extremity
cried out bravely, "To die will be an awful big adventure." Who can doubt that it will be so?
We live to die, and then we die to live.
</p><p>
Yesterday the singing mothers inspired us with John Howard Payne's immortal verse, "Home,
Sweet Home." When this song was written in 1822, John Howard Payne was living in Paris,
far away from the old homestead which he knew and loved so well. But he was in the process
of preparing to go home for a much-anticipated holiday. He knew, as we know that the
happiest holidays are those we go home for. To go home is to go back where you grew up;
home is where mother and father are; and John Howard Payne was going home. But it will
not be very long before every one of us will also be going home. We will also be going back
to where we grew up; we will be going back to where God is, to where our mothers, fathers,
and families are.
</p><p>
After the resurrection we will have these wonderful bodies, celestialized and glorified, with
quickened senses, amplified powers of perception and vastly increased capacity for
understanding, love, and happiness. Not only will our bodies be immortal and celestial but our
personalities will be immortal and celestial also. If we have properly prepared during our
second estate, then with what enthusiasm we will sing with John Howard Payne, "There is no
place like home."
</p><p>
I would like to leave with you my testimony that the gospel of Jesus Christ has been restored
to the earth with the authority to administer in all of the ordinances having to do with the
celestial kingdom.  A great flood of new knowledge has recently come into the world,
including three great volumes of new scripture outlining in every detail the answers to the
most important questions of our lives.  May God help us to understand and live those
answers, I pray in the name of Jesus Christ. Amen.
</p>
</div>
</div>
