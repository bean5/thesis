<?xml version="1.0" encoding="UTF-8"?>
<div class="gcera">
<div class="gchead">
<p class="gctitle">We Are Not Alone in Life</p>
<p class="gcspeaker">Elder Richard L. Evans</p>
<p class="gcspkpos">Of the Council of the Twelve Apostles</p>
<p class="gcbib">Richard L. Evans, <i>Conference Report</i>, October 1954, pp. 86-89</p>
</div>
<div class="gcbody">
<p>
My Beloved Friends: 
</p><p>
As we see and talk to other people (and even sometimes as we look into our own hearts), it is
apparent that there is much of loneliness in life&mdash;not only the loneliness that comes from
lack of companionship with people&mdash;but also the loneliness that comes with lack of purpose,
with lack of understanding of the reasons why we live.
</p><p>
No doubt, some loneliness comes because we are always inseparably ourselves.  Some
thoughts, some experiences, some intuitions, some of the awareness we have within us we
cannot fully share with anyone else.  We come into the world alone.  We leave it alone.  We
are always and eternally our own separate selves.
</p><p>
But loneliness is more than simply solitude.  (A person can be very lonely in a crowded busy
place.)  And there is a kind of loneliness that comes from a sense of not belonging, of not
fitting in, of not knowing our part in the picture&mdash;of not knowing what we are, or who we
are, or where we came from, or where we are going, or why we are here, or what life is
basically all about.
</p><p>
The mortal years of life pass swiftly and soon. And except for some glorious, eternal
certainties there could well be a universal feeling of frustration.  We labor long for things that
sustain life and for things that afford a little passing pleasure&mdash;but there is nothing of these
tangibles that we can take with us. These things we call our own are ours only for a short
time.  The farmer's fields not long ago belonged to someone else, and soon again will belong
to someone else.  The stocks, the bonds, the Buildings, the houses we have, whatever we have
title to, we all shall leave in yet a little while&mdash;and our going will make a mockery of all the
titles of our earthly tenancy.
</p><p>
About all we can take with us after all, are the knowledge and character we have acquired,
the intelligence we have developed or improved upon, the service we have given, the lessons
we have learned, and the blessed assurance that we may have our life and loved ones, always
and forever&mdash;as assured us by a wise and kindly Father whose children we all are.  And
knowing Him, and what He is to us, (and what we are to one another), what His purpose is in
sending us here from His presence, is one of the surest safeguards against loneliness and
feelings of frustration.
</p><p>
Some few evenings ago, I sat at dinner by the side of a distinguished, successful industrialist,
who told me simply and in a few sentences how he faced the heavy problems of his life, and
met the decisions of each day: 
</p><p>
"When I get up in the morning, he said, "I often feel that I can't face it but as I get down on
my knees and say simply 'God help me to do what I have to do this day,' strength comes and
I feel that I am equal to it.  And I think of Him as my Father, and talk to Him as simply and
directly as I used to talk to my father when he was here."
</p><p>
And then he added: "Sometimes I do things I know I shouldn't do.  But when I do, I don't lie
to God about my motives.  I know it's no use.  I know He knows my heart, my thoughts.  I
know what I have done, and He knows what I have done.  And I don't try to deceive Him or
myself."
</p><p>
I was mellowed and humbled by the direct and simple spirit of this friend with whom I sat
the other evening.  He was not of my faith, but in my own earnest belief, he could not have
talked to God with so much satisfaction or assurance if he had thought of Him merely as a
force, or as an ineffable essence, the nature and purpose of which he knew nothing&mdash;or at
least nothing that would bring to him the assured feeling that he was in fact talking to his
Father.
</p><p>
It is urgently important in life to draw nearer to a knowledge of the nature of God, and of our
relationship to Him and to one another.  And what better place to begin than with the first
book of the Bible&mdash;what better place to turn than to literal scriptural language? 
</p><p>
"In the beginning God created the heaven and the earth . . . And God said, Let us make man
in our own image, after our likeness . . . So God created man in his own image, in the image
of God created he him . . . And God saw everything that he had made, and, behold, it was
very good" (<span class="citation" id="23604"><a href="javascript:void(0)" onclick="sx(this, 23604)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(23604)">Gen. 1:1,26-27,31</a></span>).
</p><p>
It was a good world; it is a good world&mdash;despite the foolishness and perversities of men.  It
is good because of its beauties and bounties, and because of the glorious purpose and limitless
possibilities that a loving Father has given His children&mdash;a Father whom the scriptures testify
is personal and approachable, even as Paul proclaimed in his Epistle to the Hebrews that Jesus
the Christ was in "the express image" of his Father's person
(<span class="citation" id="24258"><a href="javascript:void(0)" onclick="sx(this, 24258)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(24258)">Heb. 1:3</a></span>).
</p><p>
Scripture records that many men have seen God, among them Moses and Aaron and the
seventy elders of Israel
(<span class="citation" id="22645"><a href="javascript:void(0)" onclick="sx(this, 22645)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(22645)">Ex. 24:9-11</a></span>)&mdash;even
as John recorded in Revelation that "his
servants shall serve him: And they shall see his face"
(<span class="citation" id="42246"><a href="javascript:void(0)" onclick="sx(this, 42246)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(42246)">Rev. 22:3-4</a></span>).
</p><p>
And Stephen the Martyr, "being full of the Holy Ghost, looked up stedfastly into heaven, and
saw . . . Jesus standing on the right hand of God"
(<span class="citation" id="7717"><a href="javascript:void(0)" onclick="sx(this, 7717)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(7717)">Acts 7:55</a></span>).
</p><p>
And Jesus frequently addressed His Father.  In Gethsemane: "O my Father, if it be possible,
let this cup pass from me" (<span class="citation" id="34662"><a href="javascript:void(0)" onclick="sx(this, 34662)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(34662)">Matt. 26:39</a></span>).
</p><p>
On Calvary: "Father, forgive them; for they know not what they do"
(<span class="citation" id="31386"><a href="javascript:void(0)" onclick="sx(this, 31386)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(31386)">Luke 23:34</a></span>).
</p><p>
And earlier with the Twelve: "These words spake Jesus, and lifted up his eyes to heaven, and
said, Father, the hour is come . . .
</p><p>
"And now, O Father, glorify thou me . . . with the glory which I had with thee before the
world was . . .
</p><p>
"Holy Father, keep through thine own name those whom thou hast given me, that they may
be one, as we are . . .
</p><p>
["And this is life eternal, that they might know thee the only true God, and Jesus Christ,
whom thou hast sent"] (<span class="citation" id="27614"><a href="javascript:void(0)" onclick="sx(this, 27614)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(27614)">John 17:1,5,11,33</a></span>).
</p><p>
There is much more of scripture that affirms the oneness of purpose of the Father and of His
beloved Son&mdash;and that also affirms their separateness of person as a literal, physical fact. 
And as Jesus approached Him, so also we approach the Father, in all our needs.  In every
problem, in sorrow and success, in all the things we struggle with from day to day, we can
reach out to Him with the assurance that He is there.  He lives.  He speaks.  His voice is not
unto the ancients only, but even unto our own day there is witness of His personal presence. 
He is a God of continuous revelation, of continuous mindfulness for all of us, and He does
not shut Himself in the Heavens if we will let Him come into our lives.
</p><p>
He has sent us here, from where we were with Him before birth, for a brief period of mortal
experience, with our free agency, our right of choice, with principles and commandments, and
with His Spirit to light us through life, and has assured us everlasting life with the glorious
promise of limitless and eternal progress and possibilities, with all the sweetness of
association of family and friends in the peace and protection of His presence&mdash;if we will. 
He has assured us that "men are that they might have joy" (Book of
Mormon, <span class="citation" id="3560"><a href="javascript:void(0)" onclick="sx(this, 3560)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(3560)">2 Ne. 2:25</a></span>),
and has declared it to be His purpose "to bring to pass the
immortality and eternal life of man"
(<span class="citation" id="39056"><a href="javascript:void(0)" onclick="sx(this, 39056)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(39056)">Moses 1:39</a></span>).
</p><p>
Knowing our feelings for our own loved ones, for our own children, we can take confidence
in the merry and love and understanding and in the helpfulness of our Father in heaven, who
will not leave the humblest child or the most lonely among us, alone in life.
</p><p>
You who are sick&mdash;you who are wracked with pain, you who are confined with physical
infirmity&mdash;you are not alone in life.  There is faith, there is hope, there is mercy, there is
help from Him.  "He that keepeth thee will not slumber"
(<span class="citation" id="41788"><a href="javascript:void(0)" onclick="sx(this, 41788)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(41788)">Ps. 121:3</a></span>).
</p><p>
You who are discouraged, whose obligations are heavy, whose best efforts somehow seem to
fall short of success; you who have been falsely dealt with; you who have met reverses and
disappointments, you who have lost heart: There is a kind and jug and merciful Father in
heaven to whom you can turn, and who will see that you lose nothing that should have been
yours. He can bring peace to your hearts, and restore faith and purpose.  You are not alone.
</p><p>
And you who are tried and tempted, by appetites, by evil in its subtle shapes; you who have
been careless in your conduct, who have lived the kind of lives that fall short of what you
know you should have lived&mdash;and are contending with conscience and are torn inside
yourselves: You also are not alone in life, for the Lord God who gave you life has also given
the glorious principle of repentance, which, upon sincere turning away from false ways, can
restore again the blessed peace that comes with quiet conscience.
</p><p>
You who have been hurt&mdash;hurt in your hearts, hurt in spirit, you who have been offended
and have withdrawn yourselves and become a little aloof&mdash;you need not be alone.  The door
is open.
</p><p>
You who have unanswered questions (which all of us have); you who are torn between the
teachings of contending teachers, who are confused by conflicting theories: Keep faith.
Reserve judgment.  Be patient.  God lives.  He is the source of all truth, and where there
seem to be discrepancies it is simply because we do not know enough. The theories of men
change swiftly, but "the glory of God is intelligence"
(<span class="citation" id="13292"><a href="javascript:void(0)" onclick="sx(this, 13292)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(13292)">D&amp;C 93:36</a></span>), and
there is no truth in all the universe that the Father of us all would not wish you to seek and to
accept&mdash;for man cannot be "saved in ignorance"
(<span class="citation" id="13332"><a href="javascript:void(0)" onclick="sx(this, 13332)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(13332)">D&amp;C 131:6</a></span>). Keep an
open mind and an open heart and a teachable spirit.  "Seek learning, even by study and also
by faith" (<span class="citation" id="13282"><a href="javascript:void(0)" onclick="sx(this, 13282)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(13282)">D&amp;C 88:118</a></span>).
</p><p>
And you who are young, who have ambitions for the future, but who face serious
uncertainties: Go forward and live your lives with faith.  Look far ahead; decide on some
good goal. Study, work, and prepare yourselves.  Make solid plans and pursue solid purposes
and don't place undue emphasis on the passing, trivial pleasures.  When the proper time
comes, make your homes and have your families, and face your problems with faith.  Your
Father in heaven knows and understands you, and will help and lead you to happiness and
usefulness here, and to your high destiny hereafter, if you will keep close to Him and take
Him into your confidence
</p><p>
And you who have lost your loved ones: You are not alone.  God, who is the Father of the
spirits of all men, has sent us here from His presence until he calls us to return.  And our
loved ones who have left us will always be themselves, and we may see and know and be
with them again, always and forever&mdash;if we will but take the steps that lead to eternal family
reunion.  They are nearer to us than we know.
</p><p>
We are none of us alone in life.  We belong to an eternal family. We belong also to one
another&mdash;and God, who made us in His image
(<span class="citation" id="23606"><a href="javascript:void(0)" onclick="sx(this, 23606)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(23606)">Gen. 1:26-27</a></span>), is the Father of us all.  And there is justice
and mercy and fair and adequate opportunity for all of us from Him who is and has been
mindful of us all, from birth and before&mdash;through death and beyond.
</p><p>
He is there and within our reach. He will guide and enlighten and lift. He is the source of
truth, of comfort, of protection, and of the peace that passeth understanding,
and the source of the sweet and satisfying assurance that life and truth are limitless and
everlasting, and despite all problems and all perplexities we are not left alone in life.
</p><p>
We would testify to all who hear this day of the living reality of Him who did make us in His
own image&mdash;that He lives, that He has spoken, that He does speak; that He sent His Son into
the world, who is our Savior and of whose divinity this day we testify; and that the heavens
have been opened in this day and dispensation.
</p><p>
We are none of us alone in life, but in the hands of Him to whom His Son, our Savior and
Redeemer, offered this sublime prayer: 
</p><p>
"Our Father which art in heaven, Hallowed be thy name. Thy kingdom come. Thy will be
done in earth, as it is in heaven. Give us this day our daily bread.  And forgive us our debts,
as we forgive our debtors.  And lead us not into temptation, but deliver us from evil: For
thine is the kingdom, and the power, and the glory, for ever.  Amen"
(<span class="citation" id="34619"><a href="javascript:void(0)" onclick="sx(this, 34619)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(34619)">Matt. 6:9-13</a></span>).
</p>
</div>
</div>
