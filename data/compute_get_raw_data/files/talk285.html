<?xml version="1.0" encoding="UTF-8"?>
<div class="gcera">
<div class="gchead">
<p class="gctitle">What Did the Pioneers Bring?</p>
<p class="gcspeaker">Elder Stephen L Richards</p>
<p class="gcspkpos">Of the Council of the Twelve Apostles</p>
<p class="gcbib">Stephen L Richards, <i>Conference Report</i>, April 1947, pp. 85-91</p>
</div>
<div class="gcbody">
<p>
I think, my brethren and sisters and friends, that no apology for repetition is necessary during
this conference. The pioneer theme so dominates the occasion and our thinking that we can
scarcely be expected to do other than speak about it. So I propose to address myself to the
subject, "What did the pioneers bring?"
</p><p>
PURPOSE OF MORMON PIONEERS IN SETTLING THE WEST
</p><p>
Pioneer movements for reclamation of new territory were not uncommon in America a
hundred years ago. Land was the most commonly accepted form of wealth, and the
availability of new lands made their quest a dominant pursuit of the people.
Colonization had built America, and the extension of her frontiers was a general enterprise.
</p><p>
There were, it is true, many unusual circumstances attending the pioneer settlement which we
now commemorate. The distance from established communities covered by the migration and
the penetration into unexplored and forbidding country were much greater than those of
average advances. The number of people moved and colonized was exceptionally large; the
territory sought to be included in the project was vast; the expulsion of the people from their
homes and their cruel and intolerant treatment in a free democratic country would serve to
give character to this migration. The continued persecution of the people after their settlement
here and the adverse attitude of their government were unusual items. All these circumstances
might well serve to focus attention upon the pioneer movement of 1847 as being unusual and
distinctive among comparable undertakings of frontier peoples of our country; but, in my
opinion, these conditions, of themselves, do not adequately account for the historical
placement of the Mormon colonization of the West in the number one position among all
pioneer movements and conquests in America, certainly from the standpoint of resources
available and results achieved.
</p><p>
To understand the pioneers and their accomplishments, we must examine their motives.
Herein we shall find the difference between them and other pioneers and frontiersmen of our
country. They came for freedom and peace as others have done. They came to make homes
for themselves as others have done. They came to worship God and practice their religion to
the satisfaction of their consciences, as others have done; but here is one thing they came for
which, so far as I know, has no counterpart in any other pioneer movement: They came with
the avowed purpose of establishing a society so that they would be able to take back to the
civilization from which they had fled, yes, even to their persecutors, the principles of life and
conduct which were the source of their own inspiration, cohesion, success, and happiness. I do
not mean to say that missionary efforts have not been undertaken by other groups, but for
pure Christlike altruism in purpose and deed, I place the founders of this commonwealth on
the very summit of all Christian endeavor.
</p><p>
It was ingrained in their very beings that their greatest blessings would come in blessing
others. They knew they had a message that was a boon to mankind; they knew they were
under obligation to propagate that message among the peoples of the world; and they never
for one moment lost sight of that obligation and their endeavor to fulfil it. In the processes of
subduing a most stubborn country, with all its discouragements, disappointments, and
exactions of time, energy, patience, and courage, they never ceased to give liberally of their
hard-earned substance and their limited man power in carrying abroad the
sacred principles which dominated their lives. The early companies of immigrants in their
long marches across the prairies met countermarches of missionaries toiling back over the
same hard road they had so recently trod with the same determination, equal expectancy and
hope, and ofttimes comparable sacrifice as when they undertook the long trek to the West.
Thus the pioneers came and went back as no other people have ever done, and their
descendants have kept up the process for a century of time.
</p><p>
BELIEF IN ANCIENT PROPHECY
</p><p>
What was the compelling force which drove them to such superhuman exertion and such
widespread sacrifices? Strange as it may seem, it was their literal acceptance of an ancient
prophecy revivified by modern revelation:
</p><p>
. . . It shall come to pass in the last days, that the mountain of the Lord's house
shall be established in the top of the mountains, and shall be exalted above the
hills; and all nations shall flow unto it. And many people shall go and say, Come
ye, and let us go up to the mountain of the Lord, to the house of the God of
Jacob; and he will teach us of his ways, and we will walk in his paths; for out of
Zion shall go forth the law, and the word of the Lord from Jerusalem
(<span class="citation" id="25048"><a href="javascript:void(0)" onclick="sx(this, 25048)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(25048)">Isa. 2:2-3</a></span>).
</p><p>
Every pioneer believed that prophecy with his whole heart. He saw the vision of its fulfilment
in all his labors, trials, and privations. He wanted a home with comfort for his family, of
course. He wanted a good society and prosperity, but all these were subordinate to the
fulfilment of this prophecy&mdash;the establishment of Zion.
</p><p>
We all rejoice in the general high esteem accorded Brigham Young as a master colonizer,
statesman, and empire builder. He is fully entitled to this acclaim from his fellow men, but
not many outside his own followers have understood the real secret of his success. It is true
that he was practical, far-sighted, and adept at organization, but those who know the inner
forces behind his accomplishments will tell you that his power was spiritual, rather than
temporal. The unity so essential to the cooperative effort of the people was a spiritual unity,
arising out of a universal conviction of the sacred nature of the cause they espoused and a
common acceptance of the responsibilities it entailed. In all of Brigham Young's work and
ministry there was another in spirit always at his side, always supporting him and inspiring
him, whose guidance and direction he ever acknowledged. That was his predecessor, Joseph
Smith, the earthly founder of the cause he represented, the inspirer of the people through
whom their destiny had been revealed. Brigham never forgot and never ignored Joseph;
neither did the people. They fought with all their strength to carry out the mission he had put
upon them. That mission was both temporal and spiritual but predominantly spiritual.
</p><p>
INDUSTRY, EDUCATION, LOYALTY BROUGHT BY THE PIONEERS
</p><p>
What then did the pioneers bring? They brought industry in a measure that has seldom been
equaled. They taught and practiced the gospel of work as the foundation for success and
happiness. That gospel was perhaps more widely accepted in their day than it is today,
unfortunately. They demonstrated its efficacy, and their demonstration stands today as an
example and incentive to the world.
</p><p>
They brought education and a love for the artistic and beautiful. Not many of them were
scholarly. Their opportunities for learning had been very meager, but they had within them an
innate yearning for truth, which, after all, is the real basis for education. It was an integral
part of their conception of the purpose of life to develop intelligence and acquire knowledge.
Intelligence was invested with the highest possible attributes, proclaimed to be the very glory
of God (<span class="citation" id="12384"><a href="javascript:void(0)" onclick="sx(this, 12384)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(12384)">D&amp;C 93:36</a></span>).
It was but natural, therefore, that education and its cultural, refining influences should
receive their ardent support. The education which they fostered was not narrow and restricted
as some education is. It was directed toward the acquisition of knowledge in all phases of life
and the universe; and it did one thing which, unfortunately, modern education does not always
do&mdash;it did not subordinate the quality of intelligence essential to comprehend the things of the
spirit to the order of intelligence necessary for the acquisition of other facts. With this lofty
concept of intelligence came a deep-seated love of the beautiful which is the foundation for
creative art, as well as for artistic appreciation. This love of beauty did not always find
tangible expression, but it prompted many worth-while and sometimes outstanding endeavors
in architecture, music, drama, and other cultural projects. It was undoubtedly this deep love
for learning and truth which has been responsible in succeeding generations for the high
position our state has attained in the field of literacy and education and in the percentage of
its population who have won recognition in scientific and other fields of learning. I believe
that Utah has been among the foremost, if not first, of all the states in the Union in these
respects.
</p><p>
They brought with them a high order of loyalty and a great capacity for firm devotion to the
cause they espoused. We can scarcely estimate what this meant to the success of their
enterprises. In the main they were rugged individuals, free men, many of whose immediate
ancestors had fought for liberty; yet they were willing and eager to consecrate themselves and
all they had to the cause which brought them here&mdash;the cause they loved. Theirs was the type
of unselfish devotion which makes for the success of great causes in the world. Without that
devotion no leadership, however competent, could have succeeded.
</p><p>
WISDOM AN OUTSTANDING QUALITY OF THE PIONEERS
</p><p>
I come now to the greatest thing of all which the pioneers brought with them, and that I
characterize as wisdom, wisdom about the important things in life. The really vital and
fundamental aspects of our lives and living may be classified under very few headings. I think
about four would be sufficient&mdash;the body, character, the family, and the social order. If
everything were all right with these four items, the world would be in good order, and
wisdom about these things is and always has been the greatest need of mankind. The pioneers
brought with them this much required wisdom. It was not of their own making. It was given
to them before they came here. In fact, it was not of any man's making, for it was the
wisdom of the ages bequeathed to them by Divine Providence.
</p><p>
First, consider the body of man. Everyone wants a sound body. Not all are willing to take the
steps to secure it. The pioneers brought a new concept of the body which invested it with
sacred significance. They taught that the body is the earthly tabernacle wherein the spirit of
man, the literal child of God, is housed and that the body cannot be defiled or polluted or
otherwise abused by taking into it poisons and deleterious substances without offering affront
to God whose spirit dwells therein. In this concept infractions of the laws of health are
attended not only with physical penalties, but with spiritual consequences as well. There is a
double duty to preserve the wholesomeness of the body; and, for guidance in this duty, they
brought with them a code of health rules, which, although given more than one hundred years
ago, have had the sanction and the corroboration of scientific researches never even thought
of at the time of their origin. Here was wisdom about the body, and the contributions coming
to the people from that wisdom are immeasurable.
</p><p>
Second&mdash;character or personality, if you will. I see but little difference. I define character as
the sum total of all the attributes incorporated into the structure of a man's life, and the
complexion of his character is determined by the preponderance of good or bad qualities.
Now the wisdom which came about character was not new. It was very old, but it had a new
and very special emphasis. It taught not only that man is the child of God, of the most noble
lineage but that he is destined also, if he lives for it, to be associated with his Heavenly
Father in carrying forth his eternal works in all time to come. Could there be a higher
incentive for worthy living and character, with no uncertainty as to the criteria upon which all
elections and choices should be made? I know of nothing more stimulating to the attainment
of high character in men and women than a clear concept of their divine origin and eternal
destiny.
</p><p>
Next&mdash;the family. What a world of joy and sorrow and tragedy and bliss that word spells for
us! It fills the pages of countless books. It is the subject of articles, orations, debates, and
controversies of legislation and judicial decision, and right today I notice a
magazine writer who questions the necessity of the institution and mildly predicts its
extinction in the not-too-distant future. What was the wisdom the pioneers brought about the
family? Why, they invested it with the noblest and most exalted attributes which have ever
come to it in all the history of the world. They taught that it is not only a basic unit for happy
life and progress here on this earth but that it constitutes also the very foundation of our hope
for supreme exaltation in the celestial kingdom of our God. Indeed, the heaven we seek is
little more than the projection of our homes into eternity. How at variance with these lofty
concepts of home and family are the tragic evils in domestic life today&mdash;divorce, broken
homes, neglected, wayward children more to be pitied than abused because of the
disintegration of family life. In my thinking this very disintegration has been responsible in no
small measure for the growth of the disorders and "isms" in government and society which
have so plagued the world and which today constitute our greatest menace. Oh, if the wisdom
which these humble pioneers brought could only find application in the families of the world
what a boon it would be to the comfort and the happiness and the progress of humanity.
</p><p>
Lastly, the social order by which I mean to include the art of men's living together
comfortably and in peace. The wise contribution which the pioneers brought on this altogether
important aspect of life can be told in a single word&mdash;brotherhood. They taught, in the most
realistic way, the concept of all nations, kindreds, tongues, and peoples belonging to the
family of God. They taught fraternity but not without paternity. The whole doctrine of
Christian relationship, altruism, and service may be summed up in the designation, "my
brother," "my sister." They believed a hundred years ago that the only substantial hope for
universal peace lay in the extension of this doctrine of brotherhood throughout the world.
Many others in times gone by and at the present have proclaimed this doctrine. I am grateful
that it is so. I hope their proclamation will help, but I confess to some skepticism when I see
the reception this doctrine gets. Some months ago I heard an address over the radio from an
eminent divine, the Archbishop of Canterbury, speaking from Philadelphia. Brotherhood and
peace was his subject. I was pleased to hear him make the declaration that there was little
chance for the establishment of brotherhood without recognition of the Fatherhood of God. I
read an account of his speech in the public press the next day after it was given, and a few
weeks later I read another account of it in a magazine. In neither account was there any
mention whatever of this declaration which I regarded as the most important and vital thing in
his speech. What the world needs for composition of its difficulties and the establishment of a
lasting peace is not merely a so-called spiritual brotherhood which makes a fine sounding
phrase, but also a brotherhood of the sons of God in this earth translated in terms of mutual,
practical helpfulness.
</p><p>
That was the wisdom about the social order and peace which the pioneers brought and
demonstrated when they came to this land.
</p><p>
All of my fellow members of the Church will readily understand that these wise contributions
of which I speak and many more were but principles of the gospel of the Lord Jesus Christ
which had been restored through the Prophet Joseph Smith but a short time prior to the event
which we commemorate this year. It was because of the pioneers' implicit faith in this
transcendent message of life and truth that they established the Lord's house in "the top of the
mountains" (<span class="citation" id="25045"><a href="javascript:void(0)" onclick="sx(this, 25045)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(25045)">Isa. 2:2</a></span>).
It was a great thing to set up a commonwealth and transform a desert into cities,
towns, and villages with the homes, schools, and facilities we now enjoy. It was a vastly
greater accomplishment to establish the kingdom of God and send forth from Zion that
salutary message of hope and faith and divine, eternal wisdom to all mankind. This was the
real heritage our noble pioneers brought with them and left to us and our friends who have
come to join us in this lovely land which we call the Zion of our Lord. It is the most precious
gift in life. God help us to prize it, to live it and to spread it, I humbly pray, in the name of
Jesus. Amen.
</p>
</div>
</div>
