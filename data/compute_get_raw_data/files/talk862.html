<?xml version="1.0" encoding="UTF-8"?>
<div class="gcera">
<div class="gchead">
<p class="gctitle">The Dimensions of Life</p>
<p class="gcspeaker">Elder Sterling W. Sill</p>
<p class="gcspkpos">Assistant to the Council of the Twelve Apostles</p>
<p class="gcbib">Sterling W. Sill, <i>Conference Report</i>, October 1956, pp. 63-66</p>
</div>
<div class="gcbody">
<p>
A great American philosopher once said that we should thank God every day of our
lives for the privilege of having been born. And then he went on to speculate on the unique
question of how unfortunate it would have been if we had not been born, and he pointed out
some of the wonderful things that we would have missed.
</p><p>
Really to understand the tremendous value of life as revealed by the gospel multiplies by
many times the importance of this thought. Life is our most valuable possession. Just to live
is a marvelous blessing, especially to live in these days of wonder and enlightenment known
as the Dispensation of the Fulness of Times.
</p><p>
In the days of Job it was said, "All that a man hath will he give for his life"
(<span class="citation" id="26672"><a href="javascript:void(0)" onclick="sx(this, 26672)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(26672)">Job 2:4</a></span>). For a
very wise purpose, God has implanted in every human heart a great natural desire for
continued existence. We cling to life with every ounce of our strength. Even in severe
sickness or oppressive trouble, we will still go to almost any length to prolong life even for a
week or a month, though the period gained may be one of pain or hopelessness. But we will
suffer almost any inconvenience or endure almost any hardship just to live.
</p><p>
Now if mortal life is worth so much, how much is eternal life worth? And what would it
mean to us if it were lost? God himself placed a value on eternal life when he said it was his
greatest gift to man. It therefore automatically becomes our most important opportunity to
give every co-operation to help bring it about. And a good place to start is the place
suggested by the philosopher&mdash;that is, to live our appreciation every day. What a wonderful
way to begin this quest for eternal life, if we could always live the sentiment of the song that
says&mdash;
</p><p>
I love life, and I want to live,<br />
     To drink of life's fulness, take all it can give;<br />
     I love life, every moment must count,<br />
     To glory in its sunshine and revel in its fount.
</p><p>
Even if we gave "everything" to secure eternal life, we still have made the most wonderful
bargain in the world. William James said, "The greatest use of life is to spend it for
something that outlasts it." Eternal exaltation lasts forever and is the greatest possible good.
</p><p>
But the benefits of eternal life are not limited to its dimension of length. It has been pointed
out that life has four dimensions: 
</p><p>
First, there is the length of life&mdash;or how long we live.
</p><p>
Second, there is the breadth of life&mdash; or how interestingly we live.
</p><p>
Third, there is the depth of life&mdash;or how much we live, represented by those great qualities of
love, worship, devotion, service, etc.
</p><p>
Then there is a fourth dimension of life, which might be compared to that more or less
mysterious fourth dimension of space, the purpose of life&mdash;or why we live.
</p><p>
In the ordinary situations we multiply the dimensions to get the total volume. Suppose
therefore that we could multiply the dimensions of life.
</p><p>
First there is the length of life.
</p><p>
We have made some progress in the last few centuries in increasing life's length. You may be
interested to know that if you had lived two thousand years ago in Jerusalem, your life
expectancy at birth would have been approximately nineteen years. In George Washington's
day in America it was thirty-five years. In the America of our day, it is seventy years. We
have not only tripled life's length, but it is also now possible for us to have clearer minds and
stronger bodies and live in a world from which physical pain has largely been eliminated.
</p><p>
But no one is satisfied with this accomplishment. The only life we seek is eternal life.
It has been wisely said that&mdash;"If the death of the body should forever end human life and
personality, then the universe would be throwing away with utter heedlessness its most
precious possession. A reasonable person does not build a violin with infinite care, gathering
the materials and shaping the body of it, so that it can play the composition of the masters,
and then by some whim of chance caprice, smash it to bits. Neither does God create in his
own image the great masterpiece of a human life, and then when it has just begun to live,
throw it utterly away."
</p><p>
God holds firmly in his hands the keys of eternal life.
</p><p>
Now suppose that we could multiply the length by the breadth of life.
</p><p>
Life at its best, even in mortality, is filled with interest and wonders. After the creation, God
looked upon the earth and called it good. It is an earth of boundless beauty and endless
fascination, where we may continually grow in knowledge and appreciation. When in our
pre-mortal existence we beheld the foundations of the earth being laid and knew that we were
going to have the privilege of living upon it, we are told that "all the sons of God shouted for
joy" (<span class="citation" id="26676"><a href="javascript:void(0)" onclick="sx(this, 26676)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(26676)">Job 38:7</a></span>).
And I am sure that if we fully remembered now what we knew for sure then,
we would be willing to crawl on our hands and knees through life for the privilege of being
born and having the opportunity of proving ourselves faithful during the experiences of
mortality.
</p><p>
Then our first parents were placed upon the earth and were asked to decide whether or not
they would eat the fruit from the tree of knowledge, and after they had eaten, God said, "the
man is become as one of us, to know good and evil"
(<span class="citation" id="23637"><a href="javascript:void(0)" onclick="sx(this, 23637)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(23637)">Gen. 3:22</a></span>).
And I would like to point
out in passing, that the right kind of knowledge still tends to have that effect upon people. It
still tends to make them become as gods. And the most important classification of that
knowledge is to know God and his plans for our betterment. When at the beginning of that
long, awful night of betrayal and trial Jesus offered the great prayer to his Father, he said,
"And this is life eternal, that they might know thee the only true God, and Jesus Christ, whom
thou hast sent" (<span class="citation" id="27703"><a href="javascript:void(0)" onclick="sx(this, 27703)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(27703)">John 17:3</a></span>).
</p><p>
We live in a day when the gospel has been restored to the earth in a fulness never known
before. In addition to the things that other dispensations have had, we now have the three
great volumes of new scripture, outlining in every detail the simple principles of the gospel.
The pathway to eternal life has now been perfectly marked and brilliantly lighted, and no one
now needs to get off the straight and narrow way, except by his own choice. We live in a day
when we may eat the fruit from the tree of knowledge of good and evil to our heart's content.
There is no flaming sword guarding the tree of knowledge, and some of the greatest joys of
life are the joys of understanding, born in our own minds. Edward Dyer said&mdash;
</p><p>
     My mind to me a kingdom is;<br />
     Such pleasant joys therein I find<br />
     That it excels all other bliss<br />
     That earth affords or grows by kind.
</p><p>
The gift of eternal exaltation includes not only a celestial body, but also a celestial mind. We
will have quickened senses, amplified powers of perception, and vastly increased capacity for
happiness and understanding.
</p><p>
We know from firsthand experience some of the traits and characteristics of glorified,
immortal beings, from those who have visited the earth. In describing the Angel Moroni, the
Prophet Joseph Smith said, "His whole person was glorious beyond description, and his
countenance truly like lightning . . ." Not only was his person glorious, but also the Prophet said
his clothing was brilliant "beyond any earthly thing I had ever seen; nor do I believe that any
earthly thing could be made to appear, so exceedingly white and brilliant"
(<span class="citation" id="30363"><a href="javascript:void(0)" onclick="sx(this, 30363)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(30363)">JS&mdash;H 1:32,31</a></span>).
</p><p>
We are all familiar with the wonderful lift it gives us to be appropriately dressed in beautiful
clothing. We adorn our bodies and keep them clean and attractive and in other ways go to
great lengths to make them pleasant places to live. If attractive clothing gives us pleasure,
what must be the joy of living forever, dressed in a glorified, celestialized body&mdash;to live with a
celestial family and friends on a celestial earth&mdash;but with the great added fascination of having
a celestial mind, one that thinks like God!
</p><p>
Then suppose we multiply the total of the length and breadth by the depth of life.
</p><p>
The objective of life is not only to live long, but also to live well. It is not only to acquire but
also to become; it is not only to receive benefits but also to render service. Wealth consists
not so much in what we have as in what we are and do. I suppose that the eight most
important words ever spoken are these: "So God created man in his own image"
(<span class="citation" id="23632"><a href="javascript:void(0)" onclick="sx(this, 23632)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(23632)">Gen. 1:27</a></span>).
But not only has each of you been created in the image of God, but each has also been
endowed with a set of attributes of divinity, the development of which is one of the purposes
for which we live. As Jesus admonished us, "Be ye therefore perfect, even as your Father
which is in heaven is perfect"
(<span class="citation" id="34760"><a href="javascript:void(0)" onclick="sx(this, 34760)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(34760)">Matt. 5:48</a></span>).
The plan of eternal progression contemplates that
the offspring may ultimately become like the parent, and therefore fulfills the scripture which
says that "men are that they might have joy"
(<span class="citation" id="3590"><a href="javascript:void(0)" onclick="sx(this, 3590)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(3590)">2 Ne. 2:25</a></span>),
as the greatest joys of life are the joys of being.
</p><p>
Then there is the purpose of life, that which gives life its significance.
</p><p>
For a wise and glorious purpose<br />
Thou [God] hast placed us here on earth<br />
And withheld the recollection<br />
Of our former friends and birth.
</p><p>
 &nbsp; &nbsp;         (Eliza R. Snow.)
</p><p>
Some day that recollection and those friendships will be given back to us, but in the
meantime, what a wonderful stimulation to know that life is not an accident or an afterthought
or a result of blind chance! The great plan of salvation was designed by God our Father for
our benefit. We have been working toward the goal of eternal exaltation through a long
period of premortal existence. Then we walked by sight. We knew God. He is our Father. We
lived with him. We saw his glorious, resurrected celestial body. We felt the wonder of his
celestial mind and the delight of his wonderful personality. We wanted to be like him. We
knew we must follow his example. We must learn obedience. We must learn to walk a little
way by faith. We must pass the final test of mortality where we are free to choose for
ourselves. We must be educated and proven and sanctified and redeemed.
</p><p>
And when we have finally proved ourselves worthy of exaltation, then eternity will be the
measure of life's length; celestial glory will be the measure of its breadth; to be like God will
be the measure of its depth.
</p><p>
Our salvation is made up of so many individual thoughts and acts and hours of effort,
certainly it would be the height of foolishness so much to dread to throw mortal life away all
at once, but then deliberately to throw away eternal life a little at a time. It has been said that
few, if any, will ever lose their salvation by a blowout. Mostly salvation is lost by a series of
slow leaks&mdash;a little indecision, a little indifference, a little procrastination, a little
slothfulness.
</p><p>
Disobedience may cut down life's length by producing spiritual death. Lethargy may reduce
its breadth and intensity. Sin may destroy its depth, its godliness, its joy. Ignorance may
thwart its purpose.
</p><p>
Brothers and sisters, the gospel has been given to help us increase the dimensions of our lives.
That was also the mission of the Savior of the world who said, "I am come that they might
have life, and that they might have it more abundantly"
(<span class="citation" id="27690"><a href="javascript:void(0)" onclick="sx(this, 27690)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(27690)">John 10:10</a></span>). "...all that a man hath
he will give for eternal life" (see
<span class="citation" id="26673"><a href="javascript:void(0)" onclick="sx(this, 26673)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(26673)">Job 2:4</a></span>)
is still the greatest bargain in the world. May God
help us to spend our lives effectively to that end, I pray in the name of Jesus Christ. Amen.
</p>
</div>
</div>
