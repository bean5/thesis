<?xml version="1.0" encoding="UTF-8"?>
<div class="gcera">
<div class="gchead">
<p class="gctitle">The Importance of Stability</p>
<p class="gcspeaker">Elder Albert E. Bowen</p>
<p class="gcspkpos">Of the Council of the Twelve Apostles</p>
<p class="gcbib">Albert E. Bowen, <i>Conference Report</i>, October 1948, pp. 85-90</p>
</div>
<div class="gcbody">
<p>
Constancy is a virtue of such high degree that James, in his epistle to the tribes of Israel,
declared it to be one of the distinguishing characteristics of Deity. He wrote.
</p><p>
Every good gift and every perfect gift . . . cometh down from the Father of lights, with whom
is no variableness, neither shadow of turning
(<span class="citation" id="26069"><a href="javascript:void(0)" onclick="sx(this, 26069)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(26069)">James 1:17</a></span>).
</p><p>
In like vein, Paul writing to the Hebrews, says of the Lord:
</p><p>
Jesus Christ the same yesterday, and to day, and for ever
(<span class="citation" id="24228"><a href="javascript:void(0)" onclick="sx(this, 24228)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(24228)">Heb. 13:8</a></span>).
</p><p>
GOD UNCHANGEABLE
</p><p>
In the very nature of things, the Almighty must possess this steadfastness. To be worshiped he
must command the implicit confidence of the worshiper. Unless there is an abiding faith in
the integrity of Deity, there could be no trust. But there could be neither faith nor trust if he
were inconstant, changeable, capricious, or unstable. One must know that he is to be counted
on. His promises must be infallibly sure of performance, and the same approbation or
condemnation must flow unerringly from the same acts regardless of by whom, or where, or
in what age of the world committed, with due allowance for the knowledge and enlightenment
available to the actors. The ultimate law by which man's conduct is to be judged must be the
same law tomorrow as today and so on down through all the tomorrows. Such is our concept
of the immutability of the course of God. There is something immensely solid about that
concept. It speaks of perpetuity and gives a sense of something enduring to stand on.
</p><p>
THE SOUNDNESS OF PRINCIPLES
</p><p>
But it is not a popular idea in this day when principles and practices and institutions and
beliefs, grown venerable with age, are cast aside with contemptuous abandon, often for no
other reason than that they are old. We have even invented some names for those who refuse
to throw overboard the principles by which they have lived and flourished. In the language of
the day, no doubt, James and Paul would be called reactionaries, anti-liberals,
non-progressives. It would be easily demonstrable that most of the supposedly new and
progressive offerings of the hour are in fact age-old and have been tried and found delusive
and been thrown into the discard in the far-distant past. Their advocates so far from pointing
the way to progress, are the real reactionaries, leading back to discredited failures of long ago.
I have a notion that the reactionary or progressive quality of a doctrine should be determined
by the soundness or lack of soundness of the principles it embodies and not by its age. Take
for instance the Ten Commandments.
</p><p>
THE TEN COMMANDMENTS
</p><p>
They are fairly old. But which one of them would you eliminate? In what degree have the
principles they lay down found their place in the laws enacted by the legislative bodies of
modern nations? Would any one in all the world be the worse off for observing them? Can
they be violated without injurious consequences to the violator? These are fair tests of their
eternal nature. It is safe to say that the observance of them never brought to the individual
remorse, nor caused injury or suffering to another. On the contrary, remorse, self-accusation,
sorrow, and injury to others are the inescapable consequences of the violation of most of
them.
</p><p>
"Thou shalt not bear false witness against thy neighbor"
(<span class="citation" id="22593"><a href="javascript:void(0)" onclick="sx(this, 22593)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(22593)">Ex. 20:16</a></span>) is particularly to be
commended to our attention in these electioneering times. If all the falsity and calculated
deception were squeezed out of many of the speeches we listen to, they could be reduced to
about one minute's duration instead of thirty.
</p><p>
"Thou shalt not covet . . . any thing that is thy neighbour's"
(<span class="citation" id="22594"><a href="javascript:void(0)" onclick="sx(this, 22594)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(22594)">Ex. 20:17</a></span>). The observance
of this law would rid the world of most of its strife. Out of a fairly long experience in dealing
with the disputations of men, and the causes, I am persuaded that most of them arise out of a
covetous desire to obtain some material thing or to reap some advantage to which the
contender is not entitled. If everybody wanted to do what he knew was right&mdash;deal justly, man
to man, and would be content to have what he justly could claim&mdash;there wouldn't be much
litigation or strife. If applied to the conduct of nations, there would be no war. War results
when one nation covets what another nation has or seeks dominion over it. The victim does
not want to give up either its possession or its independence. The designing one says, "I am
bigger than you," or "I have a bigger or better equipped army so I shall take what I want by
force." The other resists, and we have war.
</p><p>
The tenth commandment has to be obeyed before war and contention can cease. It states a
universal principle, true for all time; hence, it is subject neither to change nor compromise.
The same may be said of all the commandments of God. I can think of none, the keeping of
which is harmful to the observer or any one else. On the contrary, it brings inward peace.
Great stability results to individuals and nations from steadfast adherence.
</p><p>
THE QUALITY OF STABILITY
</p><p>
John Ruskin, in that unique book, the <i>Seven Lamps of Architecture</i>, speaks of stability as an
essential quality of the very buildings which we rear and which are the expression of our
culture:
</p><p>
The greatest glory of a building is not in its stones, or in its gold.
</p><p>
Its glory is in its Age, and in that deep sense of voicefulness . . . it is in that golden stain of
time, that we are to look for the real light, and color, and preciousness of architecture; and it
is not until a building has assumed this character, till it has been entrusted with the frame, and
hallowed by the deeds of men, till its walls have been witnesses of suffering, and its pillars
rise out of the shadows of death, that its existence, more lasting as it is than that of the
natural objects of the world around it, can be gifted with even so much as these possess of
language and of life.
</p><p>
Today, however, men are not building for durability either in their structures, their lives, their
religious faiths, or their institutions. The result is a troubled world. Everywhere is anxiety and
the dread arising from uncertainty which halts or stays all the normal processes of life. It all
arises out of one cause&mdash;lack of fidelity to right principles&mdash;principles which are known and
are not mysteriously hidden. Men have failed in allegiance to their religious principles and
nations have not been true to their political principles. The two infidelities go together. When
there is a breakdown of religious constancy, there inescapably follows deterioration in the
political morality. Both have the same root cause, namely, the breaking away from or the
compromising of sound principles. It amounts to a running away from reality and giving way
to the urge for avoiding the hard and rigorous disciplines incident to meeting the issues of
life, trying to reach goals without traveling the thorny road that leads to them. We want to
avoid all the disagreeable things. We are trying to live under a pleasure economy in a pleasure
world. So we live, really, in nothing: for no God, for no piety towards the past, for no pride
of race or personality. Once we lived for freedom, pledging "our lives, our fortunes and our
sacred honor." The very expression implies sacrifice and suffering, discipline of the soul to
meet reality. Now we want to be spared suffering of any kind&mdash;physical, emotional, or mental.
We seek security, a six-hour day, a car, and a pension. But all the time life eludes us, peace
of mind eludes us, and we have dissatisfaction, turmoil, uncertainty, and dread.
</p><p>
CONTINUITY NEEDED IN CHANGES
</p><p>
True, human institutions, bearing in themselves the imperfections incident to the limited
wisdom and capacities of those who fashion and operate them, fall short of the permanence
characteristic of things divine and may be subject to change with the progress of unfolding
wisdom and experience. But change should be toward the ideal and should not run ahead of
readiness to receive and assimilate it without violent disruptions lest mischief instead of
betterment result. Change so brought about will be accomplished without destroying
continuity, a factor vital to orderly progress.
</p><p>
Let me point this up with an illustration. When the American colonies revolted against the
mother country, it was not out of a purpose to discard the principles by which they had lived.
As Englishmen they merely claimed the recognized rights of Englishmen, which rights, they
contended, they, as residents of the colonies, were being denied. If Britain had acceded to
their requests, they would have been content to remain as subjects of the kingdom and there
would have been no revolution. Britain did not yield to their demands. So they declared a
political separation and fought a war to make their declaration good. Having achieved that,
they had to set up a new framework of government to carry on. But they did not throw their
old principles into the scrap heap. They perpetuated them under their new government. The
principle they had contended for was freedom&mdash;the rights of free men. That continued to be
the purpose of their new government and was the core of continuity binding the old to the
new. They were still, as their English forebears had been, a God-fearing Christian nation
standing on their right to be free. The government they fashioned was concerned primarily
with making that ancient right secure. They tried by every art and device they knew to
provide against another infringement of it. It took a little floundering, a period of travail,
before they got their government going; but when they did, it constituted no rupture with the
past, and that continuity imparted a wonderful stability to the new nation; it began life free of
blood purges, reprisals and excesses. For one hundred sixty years now it has run its unbroken
course. So begun and so perpetuated, the United States of America has achieved unparalleled
transcendence among the nations. It stands today the last hope of free men, the one steadying
support to this reeling world.
</p><p>
FURY OF FRENCH REVOLUTION
</p><p>
Contrast that with the story of the French Revolution which came along shortly after. This
was inaugurated by a wild fury of murder, rapine, and blind vengeance, with monsters
contending for supremacy and the victor sending the vanquished to the guillotine. Excesses
spiraled, with frenzied zealots pushing ever to further extremes. At the bottom was the utter
abandonment of principles. It was sought to wipe out the past. God was dethroned and
mocked in derision; reason deified and a new cult proclaimed. With the thread of continuity
completely severed, there could be no settling down to a stabilized order, and terror reigned
until Napoleon trained on them his guns and established his personal rule. Since then,
governments have risen and fallen in France, republic passing into totalitarian empire, and
empire back into republic, and republic into republic with the very existence of the present
one hanging in the balance. The attendant uncertainty and, confusion and lack of steadfastness
is its plague.
</p><p>
SOCIALISTIC EXPERIMENTATION
</p><p>
And yet with this and multitudinous other examples of history before us, we have today in
this land those who would destroy our solid foundations by importing here from the Old
World what our forebears ran away from to come and plant deep in the virgin soil of this new
land the roots of the tree of liberty. The news is leaking out that there is now forming, to be
publicly launched as soon as the elections are over, a new party, spearheaded by men who,
having enjoyed a brief hour of power, but now out of favor in the party that elevated them,
are loath to see authority slipping out of their hands. Their purpose is to bring to pass here the
socialistic experimentation that is destroying the strength of England and shaking that nation
to its foundation. That experiment has been aptly described as "the half-way house on the
road to totalitarianism.
</p><p>
The land is also fecund of pseudo religious cults spawning like pestilence. We have already
lost some of our own stability and shall not recover it if the people surrender to the seductive
lure of specious phantasms, religious or political, forswearing the solid principles on which
we are foundationed and on which the Church and the nation have grown to greatness and
power.
</p><p>
UNREST AND TURMOIL ABROAD
</p><p>
Look out over the world today and you see seething unrest, turmoil, confusion, dread,
suspicion, envy, distrust, and preparation for devastating war. What has happened?
</p><p>
Britain and France went to war to guarantee the territorial integrity of Poland. We made that
cause our cause. The principle was that no nation should be suffered to be trampled underfoot
by a ruthless invader of its land. The enemy was subdued, but the principle of protection of a
people against the incursions and oppression of a foreign tyrant, for which the war was
ostensibly fought, was relinquished and a large part of the territory of Poland was suffered to
be seized and its entire people brought under the dominion of a despot quite as ruthless and
cruel as the first invader. The invasion of Finland was denounced as an act of wanton
brutality and the perpetrator of the invasion as a despot as tyrannical as any in the world.
Then by a turn of the wheel of fortune that despot got over on to our side, or more properly
speaking, we got on to his and winked our eyes at his dismemberment of that unhappy land
and his impositions upon its people of unconscionable indemnities.
</p><p>
Without so much as consulting our ancient friend, China, we gave consent to the seizure of a
vast chunk of its territory and the control over its vital communications.
</p><p>
ONE TRUTH AND ONE MORALITY
</p><p>
The world is reaping the fruit of this abandonment of principles. There is only one truth and
one morality. When discovered, it matters not whether they find application to religious
observances or to political systems. They bear the hallmark of eternity and may not with
impunity be abandoned or compromised.
</p><p>
Stability will come when men once more live by the promises they make and in their public
morality as in their private conduct, in their religious as in their political life, they develop
integrity of purpose and steadfastness to principle and adherence to known laws foundationed
in the wisdom of the eternal.
</p><p>
As we struggle forward toward that goal, let us hold steadfastly before our eyes the shining
beacon of Christ's perfect order as stated for us by Alma:
</p><p>
. . . he cannot walk in crooked paths; neither doth he vary from that which he hath said;
neither hath he a shadow of turning from the fight to the left, or from that which is right to
that which is wrong; therefore, his course is one eternal round
(<span class="citation" id="8858"><a href="javascript:void(0)" onclick="sx(this, 8858)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(8858)">Alma 7:20</a></span>).
</p><p>
May God grant us the wisdom and the strength to achieve this stability, I pray, in the name of
Jesus. Amen.
</p>
</div>
</div>
