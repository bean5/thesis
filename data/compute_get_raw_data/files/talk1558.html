<?xml version="1.0" encoding="UTF-8"?>
<div class="gcera">
<div class="gchead">
<p class="gctitle">Be Comforted</p>
<p class="gcspeaker">Elder Marion D. Hanks</p>
<p class="gcspkpos">Of the First Council of the Seventy</p>
<p class="gcbib">Marion D. Hanks, <i>Conference Report</i>, April 1967, pp. 123-126</p>
</div>
<div class="gcbody">
<p>
A few days ago as I pondered this responsibility a letter arrived from an anguished parent
whose child had taken a wrong turn in the road and had become enmeshed in serious trouble.
About the same time a telephone call brought a request for help from a mother, under similar
circumstances. That weekend the "Church Section" quoted in an article a letter from another
heartbroken mother whose promising son had been destroyed by drugs. Experience recalls many such matters.
</p><p>
Each of these instances involved parents who had earnestly tried to do their duty, had
themselves lived honorable, devoted lives, had reared choice families, had loved and found
much to praise in the child who had made the wrong decisions. Nevertheless, the child had
made the wrong decision.
</p><p>
Beyond the anguish caused by the errant child in such cases is often the added burden of
censure from neighbors, and self-condemnation as scriptures are read or alluded to in classes
and meetings and discussions.
</p><p>
What has the Church to say to a sincere parent who, like all parents, has made mistakes, but
who has really tried &mdash;only to have a child disregard his teachings and example and choose
another way?
</p><p>
Possibly no subject is more frequently and earnestly treated in Church instruction and
admonition and programming these days than that of the responsibility of parents to their
children. All who have association with youth and families know the importance of this
emphasis, and none could question the validity of the effort.
</p><p>
Outside the Church, people of honest interest and goodwill feel the same and seek the same
objectives.
</p><p>
<b>The charge to parents to teach children</b>
</p><p>
Few scriptural admonitions are more clearly or strongly given than those relating to the
responsibility of parents and adults to their children and the younger generation. If we were to
select a scripture that is perhaps as frequently referred to as any other by speakers and
teachers, we would be reasonably safe in choosing the 68th Section of the Doctrine and
Covenants, where a well-known instruction from the Lord to his children is recorded. Certain
of the brethren received counsel, and instructions of general application were included.
Among the choice directions from the Lord is this verse: 
</p><p>
"And again, inasmuch as parents have children in Zion, or in any of her stakes which are
organized, that teach them not to understand the doctrine of repentance, faith in Christ the
Son of the living God, and of baptism and the gift of the Holy Ghost by the laying on of the
hands, when eight years old, the sin be upon the heads of the parents" (<span class="citation" id="14968"><a href="javascript:void(0)" onclick="sx(this, 14968)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(14968)">D&amp;C 68:25</a></span>).
</p><p>
Parenthood, the revelation teaches, involves the responsibility to "teach their children to pray,
and to walk uprightly before the Lord" (<span class="citation" id="14971"><a href="javascript:void(0)" onclick="sx(this, 14971)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(14971)">D&amp;C 68:28</a></span>).
</p><p>
My purpose today is to express my own deep appreciation of the validity and relevance of
these instructions, and to say that I believe and accept them as the word of the Lord. But
there is another side of this story that deserves attention and compassionate consideration.
</p><p>
<b>Potent influences upon children</b>
</p><p>
We are all aware that home and parental and adult influence are of greatly persuasive
importance in the lives of children. In commenting on the relationship of adult to child over
this pulpit some years ago, I noted that among other things the following are true: 
</p><p>
1. Children are inclined to be like their parents and the homes from which they come.
</p><p>
2. Children are also influenced by associates who come from other homes and therefore are
influenced by the nature of the other homes and the parents who live in them.
</p><p>
3. Other adults and environments have great influence on young people.
</p><p>
4. Young men and women soon discover the truth about parents or other adults whose lives
are not consistent with their expressed convictions in the way they live.
</p><p>
It is true that there are a number of examples of fine young people who rise above their home
and family environments and their training and the example of the adult generation. They
somehow find the way themselves and, setting high goals, manifest the determination and
courage and capacity to achieve them. There are exceptions on the other side of the coin also,
and it is of this that I would like to speak for a moment.
</p><p>
<b>Solace for the heart-broken</b>
</p><p>
What of the earnest, sincere parents who do their best to rear their family with integrity and
devotion, only to see a child (or several children) choose paths leading to destinations that
break the hearts of the parents? Like other parents, this father and mother, aware of their
vulnerabilities and limitations, have earnestly tried to rear their children in the nurture and
admonition of the Lord. As they hear the sermons and testimonies and observe the good
fortune of neighbors whose children follow the established way, their hearts break and their
spirits sag. They are sickened by questions they cannot answer, critical of
themselves, assisted in their own self-criticism by the sometimes unthinking judging of others.
</p><p>
What shall be said to such heart broken parents? Is there any encouragement for them in the
scriptures? What have the prophets said?
</p><p>
Ezekiel was a prophet during the captivity of Israel. He preached to a people to whom it was
comforting to attribute their current problems to the sins of former generations. They were
habituated to quoting a prophecy: "The fathers have eaten sour grapes, and the children's teeth
are set on edge" (<span class="citation" id="23132"><a href="javascript:void(0)" onclick="sx(this, 23132)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(23132)">Ezek. 18:2</a></span>)
</p><p>
There is, of course, a measure of truth in this proverb, as every parent or close observer of
the human experience knows. Our children do suffer in many ways from our defections or
derelictions, just as they prosper from our proper instruction and our love and good example.
</p><p>
As Ezekiel admonished Israel he spoke these words, recorded in the 18th chapter of Ezekiel: 
</p><p>
"The word of the Lord came unto me again, saying,
</p><p>
"What mean ye, that ye use this proverb concerning the land of Israel, saying, The fathers
have eaten sour grapes, and the children's teeth are set on edge?
</p><p>
"As I live, saith the Lord God, ye shall not have occasion any more to use this proverb in
Israel" (<span class="citation" id="23131"><a href="javascript:void(0)" onclick="sx(this, 23131)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(23131)">Ezek. 18:1-3</a></span>).
</p><p>
<b>Individual responsibility</b>
</p><p>
As I read the record, Ezekiel was not minimizing the sorrowful imposition of trouble in the
life of a child who is deprived of the truth or misled by the faithlessness of a parent. Ezekiel
was reemphasizing for Israel the great importance of individual responsibility before God and
of God's impartiality in dealing with every man according to his own character. Hear these
words of the Lord through the Prophet, immediately following his instruction that they no
more use (or misuse) the proverb in Israel: 
</p><p>
"Behold, all souls are mine, as the soul of the father, so also the soul of the son is mine: the
soul that sinneth, it shall die" (<span class="citation" id="23133"><a href="javascript:void(0)" onclick="sx(this, 23133)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(23133)">Ezek. 18:4</a></span>).
</p><p>
Repeating those last words, "the soul that sinneth, it shall die," the Lord added: 
</p><p>
<b>Compensation for righteousness</b>
</p><p>
"The son shall not bear the iniquity of the father, neither shall the father bear the iniquity of
the son: the righteousness of the righteous shall be upon him, and the wickedness of the
wicked shall be upon him" (<span class="citation" id="23134"><a href="javascript:void(0)" onclick="sx(this, 23134)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(23134)">Ezek. 18:20</a></span>).
</p><p>
Ezekiel then encouraged repentance and obedience, noting that the repentant sinner may avoid
the eternal consequence of his deed through the forgiveness of the Lord. A wicked man who
repents and becomes righteous will live. A righteous man who becomes wicked will die.
Every man must stand before God and answer for his own choices and for his own character.
</p><p>
What Ezekiel said to ancient Israel, I believe we must understand and apply to modern Israel.
Where homes and hearts are sundered by the resentful or rebellious bad choices of a child
who is accountable and has made his own stubborn decisions, which cross in their willfulness
the purposes of the parents, God understands and does not condemn the honest parents.
</p><p>
Jeremiah quoted and refuted the same proverb: 
</p><p>
"In those days they shall say no more, The fathers have eaten a sour grape, and the children's
teeth are set on edge.
</p><p>
"But every one shall die for his own iniquity: every man that eateth the sour grape, his teeth
shall be set on edge" (<span class="citation" id="26517"><a href="javascript:void(0)" onclick="sx(this, 26517)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(26517)">Jer. 31:29-30</a></span>).
</p><p>
There is some solace for the sorrowing heart in recalling the first family of the Bible. Seeking
earnestly to live obediently in response to their knowledge of good and evil, faithful parents
tried to teach their children. One son understood and offered unto God an acceptable sacrifice.
Another son did not or would not understand. To him and his offering God had no respect. So
serious was his misunderstanding and his response that he rose up against his brother and slew
him (<span class="citation" id="23756"><a href="javascript:void(0)" onclick="sx(this, 23756)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(23756)">Gen. 4:3-8</a></span>).
</p><p>
What of the first family of the Book of Mormon? Reared by the same mother and father in
the same household, some of the brothers loved God and followed the
counsels of their parents. They were loyal to their heritage and possibilities. Other sons took
an opposite course. They were willful and rebellious and unresponsive to the instructions and
example and entreaties of their father and mother. Repeatedly they followed their own
wayward wills, to the heartbreak of their parents and to their own ultimate disaster.
</p><p>
<b>The Father's compassion</b>
</p><p>
If there needs be more evidence of the widespread nature of the problem and the deep
compassion of the Father for those who suffer through it, consider another family in which
one choice son humbly accepted the counsel and plan of his father and followed that plan
according to his father's will, while another son, also an authority in the kingdom of his
father, followed his own wayward will and base arrogance, rebelled against his father and his
instructions, and, not content with this, induced a third of his brothers and sisters to rebel
against their father and to follow him, to their own heartbreak and sorrow.
</p><p>
Whatever application is to be made of Ezekiel's instructions, surely there is this invitation to
those in whose households there is peace and joy and rejoicing because of their posterity: be
humble. Be compassionate and considerate and prayerful in behalf of those who have suffered
the misfortune of a wayward child. Thank God, watch and pray, and be humble.
</p><p>
To those to whom the sorrow of a child unresponsive to parental instruction and example has
come, be comforted. God understands. He knows what it means to have a rebellious son and
wayward children. Many others understand.
</p><p>
<b>Freedom and its consequences</b>
</p><p>
Again let it be said that there is no disposition to minimize the importance of our doing all
we can to lead and direct and inspire obedience in our children. We can tragically impose
upon their lives by our failures. But let there be concern and consideration also in recognizing
the principle of agency in accountable people and the responsibility to answer for the choices
that are made.
</p><p>
God requires that we accept responsibility for our individual decisions; he deals with every
man according to his own character. He has taught us through Ezekiel not alone that every
man must stand on his own and answer for his own decisions, but that God desires that all
shall turn to him and live, he having no pleasure in the suffering of his children for their sins
(<span class="citation" id="23136"><a href="javascript:void(0)" onclick="sx(this, 23136)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(23136)">Ezek. 33:11</a></span>).
</p><p>
In a recent magazine it is quoted: "On the last six days of Passover, Jews say a special prayer
&mdash;the half Hallel. Tradition has it that when the Egyptians, in pursuing the Jews, were
drowning in the Red Sea, the Lord kept his angels from singing his praises, admonishing
them, 'How can you sing hymns while my creatures are drowning in the sea?' "
</p><p>
God help us to be humble if we are blessed with children who follow the way they have been
shown. God help us to be gracious and compassionate with others whose experience has not
been so favorable. God help choice parents who have truly tried, but have had heartbreak, to
know his love and his warmth and the gracious encouragement of his understanding heart. In
the name of Jesus Christ. Amen.
</p>
</div>
</div>
