<?xml version="1.0" encoding="UTF-8"?>
<div class="gcera">
<div class="gchead">
<p class="gctitle">The Pangs of Unlearning</p>
<p class="gcspeaker">Elder Delbert L. Stapley</p>
<p class="gcspkpos">Of the Council of the Twelve</p>
<p class="gcbib">Delbert L. Stapley, <i>Conference Report</i>, April 1967, pp. 30-34</p>
</div>
<div class="gcbody">
<p>
Recently, I read a talk, given by a doctor to a convention of medical men, entitled "The Pangs
of Unlearning." He called attention to the discoveries through research of new drugs and
improved treatments that require much unlearning by physicians and surgeons, because many
former practices and medicines do not best serve the interests of today's patients. A doctor
friend of mine significantly stated that the majority of medical services practiced ten years ago
are obsolete today. This talk challenged my interest, as I envisioned updated learning,
reorienting, and retraining as applying to the pattern of our own lives.
</p><p>
I should like to share with you some of my thoughts on this subject of unlearning and the
possible personal improvement each individual can attain by living strictly within the
framework and spirit of the gospel of Jesus Christ.
</p><p>
<b>Learning anew from research and revelation</b>
</p><p>
The word <i>unlearn</i> as here used does not mean a casting aside of eternal truths and everlasting
knowledge. Rather, it means altering our habits of behavior so as to live more in harmony
with God's will. It reflects a desire and a willingness to keep pace with  up-to-date knowledge
gained through research (as well as inspiration and revelation), which provides advanced
learning, new methods, and techniques to replace the less effective or obsolete. Are we
faithful and obedient enough in the Church to accept authorized changes that represent
improvement and growth?
</p><p>
The wonderful, complex instrument for registering our experiences that we call the mind
gathers a maze of data, to be sifted, analyzed, and appraised. In such a process some data will
be found useful and some without value. In the latter case, what shall be done with such
material?
</p><p>
It would be well, of course, if it could be thrown away, like refuse, and forgotten altogether.
That, however, is quite out of the question, as the Master so clearly illustrated in his parable
of the wheat and the tares (<span class="citation" id="35725"><a href="javascript:void(0)" onclick="sx(this, 35725)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(35725)">Matt. 13:24-30</a></span>). And no way has, as yet, been discovered by which we may press a
button or turn a switch and have that which is of no value to our learning cast aside
automatically. That which is learned, the useful and the seemingly unuseful alike, memory
retains.
</p><p>
What, then, can be done as our learning process grows and expands and memory retains the
good and the not good that have been accumulated there? The simplest answer to such a
question would be "unlearn the not good." We are then faced with the next question, "How
can that be done?"
</p><p>
<b>Exercise free-agency</b>
</p><p>
The answer is not so simple nor so easy. It involves many matters and requires earnest,
prayerful consideration. In the first place, the primary law of intelligent life, free agency, or
the personal power to exercise judgment should be made operative. With that power set in
motion, the good and the not good may be determined. Yet even here man on his own may
not be perfect in judgment. One needs to be humble in spirit, contrite of heart,
ready in prayer, as was the Master, even though he was perfect. By such
means one is entitled to the prompting and guidance of the Holy Ghost, so necessary when
one is on the road perfectness, yet needing to "unlearn" that which is not good.
</p><p>
<b>Creative attitudes</b>
</p><p>
Another factor important in the process of learning and unlearning is that of attitude. Some of
us need to unlearn personal attitudes that are contrary and resistant to gospel teachings and
requirements. Certain attitudes are destructive to true character. They inhibit growth. If
allowed to develop, they may produce disastrous consequences. Negative, cynical, and other
kindred attitudes are dangerous to faith, hope, humility, righteous desires, and high purpose,
which virtues are essential to the discovery and retention of that which is best in the learning
process and of "unlearning" the undesirable in life. One should, therefore, be well aware of
the many types of attitudes present in daily living.
</p><p>
We sometimes wonder why people behave as they do. Perhaps it is because they are unwilling
to unlearn the reasons for their unwarranted behavior. Now, I do not want you to think I am
advocating the unlearning of eternal truths, principles, standards, ideals, and ordinances,
because these gospel verities never change. God's laws are immutable and endure forever. By
increasing our learning, however, we become acquainted with additional truths and higher
laws referred to in scripture as truth, light, spirit, and the mysteries of godliness. A scientist
frequently forsakes theory he has learned because research uncovers advanced knowledge that
changes or makes obsolete some former concepts but does not eliminate basic principles. The
sciences are subject to constant change. This is true also in technological advances, where we
forsake the old and accept the new improved methods of performance that have advanced our
civilization tremendously.
</p><p>
<b>Cherish the word of God</b>
</p><p>
While all this advancement takes place in our modern world, we cannot afford to forsake or
discard the teachings and revelations of God. People brought up in a religious faith that does
not teach the true doctrines of Christ, regardless of how sincere they may be, must unlearn
much of what they were taught and accept the new light and way to obtain salvation and
glory. Because the children of Israel had gone astray and were so steeped in the faith and
tradition of their fathers, they were unable to unlearn the law given for their temporal benefit,
for the higher spiritual law brought to them personally by the Christ. They thus failed to
recognize the Christ when he was sent of God, the Father, to live among them. It was Christ
who came to fulfill the lesser law and to reveal to them the higher law of his gospel. Jesus
was put to death because his own people of the house of Israel could not unlearn and prepare
themselves to receive him, their Jehovah, Savior, and God.
</p><p>
The peoples of the world must unlearn the idea that all churches are acceptable unto God.
Some teach that it doesn't matter which road one takes (meaning which church one belongs
to), since all roads, it is claimed, lead back to the presence of God. This premise does not
accord with the teachings of the scriptures.
</p><p>
<b>No "new wine in old bottles"</b>
</p><p>
Christ did not accept any of the churches of his day to supply the framework for his earthly
kingdom. He taught, "Neither do men put new wine into old bottles: else the bottles break . . .
but they put new wine into new bottles, and both are preserved" (<span class="citation" id="35715"><a href="javascript:void(0)" onclick="sx(this, 35715)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(35715)">Matt. 9:17</a></span>). For the same
reason the organization of his Church and the gospel of his kingdom could not fit into the
framework of the existing churches. They were false and unsuited to Christ's needs and
purpose. The identical condition was also true in this dispensation when God restored the
gospel and his kingdom through his divinely called servant, Joseph Smith.
</p><p>
<b>"One Lord, one faith, one baptism"</b>
</p><p>
Man must unlearn the idea that any and all baptisms are acceptable unto
God. There is only one true mode of baptism, and that is immersion. Only men who hold the
appropriate priesthood office and are divinely called and ordained can efficaciously perform
this holy ordinance in the gospel and know that it is acceptable to God and that a record will
be made of it on earth as well as in heaven.
</p><p>
I sincerely testify that as all members of Christ's Church progress toward perfection they will
enjoy increased knowledge and clearer vision of God's plans and purposes. They will also
have some unlearning to do, not because basic truths, standards, and principles change, but
because new methods and techniques are employed to achieve greater and more widespread
improved performance and spiritual results.
</p><p>
Now, to support this thought, I quote from the teachings of the Prophet Joseph Smith: "We
consider that God has created man with a mind capable of instruction, and a faculty which
may be enlarged in proportion to the heed and diligence given to the light communicated
from heaven to the intellect; and that the nearer man approaches perfection, the clearer are his
views, and the greater his enjoyments, till he has overcome the evils of his life and lost every
desire for sin; and like the ancients, arrives at that point of faith where he is wrapped in the
power and glory of his Maker, and is caught up to dwell with him. But we consider that this
is a station to which no man ever arrived in a moment; he must have been instructed in the
government and laws of that kingdom by proper degrees, until his mind is capable in some
measure of comprehending the propriety, justice, equality, and consistency of the same . . .
[and] that it is necessary for men to receive an understanding concerning the laws of the
heavenly kingdom, before they are permitted to enter it: we mean the celestial glory."
(<i>Documentary History of the Church</i>, Vol. 2, page 8.)
</p><p>
<b>Vigorous renewal for "the perfecting of the Saints"</b>
</p><p>
Nevertheless, people become too complacent and satisfied with what they have. It is most
difficult for them to unlearn and accept the better way. It is also difficult for some members
of the Church to unlearn and give up less effective methods of doing things for greatly
improved programs planned to build increased spirituality, faith, and testimony to perfect the
Saints of God. The Church programs are constantly being strengthened and perfected to meet
the challenge of the growing, progressive needs of its members.
</p><p>
<b>Church correlation</b>
</p><p>
We hear much these days about Church correlation, which is an important step forward in
promoting a rounded-out educational understanding of all that pertains to God's latter-day
kingdom. The lesson outlines are prepared by the Church correlation committee and are
adapted to meet the needs of the members of each Church organization. This prevents any
overlapping in study courses, which produce well-informed doctrinal and Church history
students who can intelligently give an answer and reason for the hope within them.
Correlation of Church organizations, lesson material, and coordination of activities will
increase effectiveness and strength in the lives of members, both young and old.
</p><p>
<b>Rededication of parents</b>
</p><p>
Parents must unlearn the leaving of all gospel teaching to the organizations of the Church,
when the prime responsibility for the teaching of children rests upon the home. If home
evenings are not held or are poorly planned, children and parents are denied the wholesome
association and companionship of one another. This condition requires a change of attitude
and an up-dated learning to enjoy the blessings of this choice family experience. The Church
program planned for these occasions is ideal and can, with some imagination, be adapted to
every family need. Parents should unlearn the ineffective methods of dealing with their
children, finding more effectual ways; then children will feel free to discuss and counsel with
parents about the intimate, delicate, and confidential matters that concern them.
</p><p>
<b>Renewal of loyalty to God's laws</b>
</p><p>
Do some of us need to reevaluate what constitutes proper observance of the Word of
Wisdom? Are we becoming too liberal in our personal interpretation and application of this
law? The Apostle Paul counseled: "Abstain from all appearance of evil" (<span class="citation" id="2745"><a href="javascript:void(0)" onclick="sx(this, 2745)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(2745)">1 Thes. 5:22</a></span>). Here
again, we can unlearn and resolve to stay strictly on the Lord's side of this law and be safe
and at peace with ourselves.
</p><p>
Can we justify a partial payment to the tithing fund as an honest accounting with the Lord on
his law of the tithe? Shouldn't we be honest with him and unlearn any wrongful practices to
fully meet the obligation and conditions of this law?
</p><p>
Man must unlearn his changing liberal attitude toward sex that minimizes the sacredness of
sex behavior and opens the way for licentious living. I proclaim with all the power of my
being that God's seventh commandment to the children of Israel through Moses, "Thou shalt
not commit adultery," is a law as binding upon man today as then. Adultery is one of the
most abominable sins in the sight of the Lord (<span class="citation" id="9083"><a href="javascript:void(0)" onclick="sx(this, 9083)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(9083)">Alma 39:5</a></span>), and forbidden by our God (see
<span class="citation" id="14933"><a href="javascript:void(0)" onclick="sx(this, 14933)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(14933)">D&amp;C 42:24</a></span>;
(<span class="citation" id="22773"><a href="javascript:void(0)" onclick="sx(this, 22773)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(22773)">Ex. 20:14</a></span>).
Those who willfully violate this law must pay God's penalty,
which is denial to the celestial kingdom (<span class="citation" id="463"><a href="javascript:void(0)" onclick="sx(this, 463)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(463)">1 Cor. 6:9-10</a></span>).
</p><p>
If prayers are not a regular practice in the home and personal prayers uttered daily, isn't it
wise for us to be more faithful in keeping in contact with our God? Wouldn't it be wise to
unlearn some of our feelings, habits, and doings that prevent us from enjoying the sweet
companionship of the Holy Ghost to guide and direct us in our personal lives? Many brethren
endowed with the Holy Priesthood should unlearn a complacent approach to the duties and
responsibilities attendant to this holy power. The Lord has counseled that every priesthood
bearer is to "learn his duty, and to act in the office in which he is appointed, in all diligence"
(<span class="citation" id="15040"><a href="javascript:void(0)" onclick="sx(this, 15040)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(15040)">D&amp;C 107:99</a></span>).
Slothfulness in one's duty isn't acceptable to the Lord. He further requires
that men must "do many things of their own freewill, and bring to pass much righteous"
(<span class="citation" id="14944"><a href="javascript:void(0)" onclick="sx(this, 14944)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(14944)">D&amp;C 58:27</a></span>).
</p><p>
There is unlearning to do to adjust from the previous ward teaching program to the present
home teaching plan to Church families. The new plan is much superior to the older method
and has far greater potential for effective results.
</p><p>
This can also be said of new programs in other fields of church service, such as genealogy,
temple work, stake missions, education of youth.
</p><p>
Failure to accept and follow wholeheartedly the counsel and example of our leader in moral,
ethical, and spiritual matters does not produce harmony but disharmony. It also places one in
the position of pitting one's knowledge and learning against that which God has inspired or
revealed through his anointed servant. Some question the right of the Church through its
leader to speak up and let the world know and understand the position of the Church on
ethical, moral, and political principles or standards that have to do with the rights and welfare
of man. Who is able to speak more clearly and authoritatively on such matters? If man loses
his God-given right of agency, freedom, and ethical practices by unrighteous interference and
unjust controls, his religious life will suffer, because the climate in which religion should
flourish becomes restricted and untenable.
</p><p>
<b>Re-education for spiritual growth</b>
</p><p>
The Savior taught many truths during his ministry upon the earth; but men were offended by
his teachings and reviled against him, for their hearts were not right, and their spirits were not
attuned to his. Although many stood against him, he was right and they were wrong. Was not
his atoning sacrifice for the purpose of helping man to <i>unlearn</i> his sinful ways, which grow
up within us like the tares among the wheat? We must unlearn all dross, that we might be
more like our Redeemer and our Eternal Father.
</p><p>
In this present day many need to unlearn unorthodox teachings and  improper standards and to
humble themselves, as it were, in sackcloth and ashes. All of us should make
a personal evaluation and determine where we can profitably unlearn false opinions and
erroneous teachings. Our duty is to condition ourselves to be more valuable in promoting the
work of God's kingdom. Freedom does not license contention nor approve nonconformists to
supplant God's ways with their own. The Lord proclaimed to Isaiah: "For my thoughts are not
your thoughts, neither are your ways my ways, saith the Lord.
</p><p>
"For as the heavens are higher than the earth, so are my ways higher than your ways, and my
thoughts than your thoughts" (<span class="citation" id="25339"><a href="javascript:void(0)" onclick="sx(this, 25339)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(25339)">Isa. 55:8-9</a></span>).
</p><p>
Perhaps this statement will cause us to remember how small we are in comparison to our
God, who is all-knowing and all-powerful. The Lord taught Moses a great lesson in this
regard. After he had shown Moses by vision the workmanship of his hands, he withdrew from
Moses, and his glory was not upon him. Moses was left unto himself, and he fell unto the
earth exhausted (<span class="citation" id="39274"><a href="javascript:void(0)" onclick="sx(this, 39274)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(39274)">Moses 1:4-9</a></span>). It was many hours before he again received his natural strength, and when he
did, he humbly said: "Now for this cause I know that man is nothing, which thing I never had
supposed" (<span class="citation" id="39275"><a href="javascript:void(0)" onclick="sx(this, 39275)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(39275)">Moses 1:10</a></span>). This counsel should remind all of us to be meek and contrite of
spirit.
</p><p>
<b>Repentance, the way to perfection</b>
</p><p>
As we advance toward perfection, there will be higher laws revealed to our understanding and
benefit that will replace those of a lower order. This truth was first taught to Adam and Eve
in the Garden of Eden, when the Lord gave them two choices: (1) not to partake of the
forbidden fruit (<span class="citation" id="23748"><a href="javascript:void(0)" onclick="sx(this, 23748)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(23748)">Gen. 2:16-17</a></span>); and (2) to multiply and replenish the earth
(<span class="citation" id="23743"><a href="javascript:void(0)" onclick="sx(this, 23743)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(23743)">Gen. 1:28</a></span>), which choices call for obedience
to a lesser law or a higher one. They chose to fulfill the higher law. Again, when the Savior
sojourned among men, he replaced a lesser law, which Moses, his servant, had given to the
children of Israel, with the higher law of the gospel, his plan of life and salvation. Therefore,
as we progress in righteousness and truth, we will come in contact with higher laws
previously unknown that, when revealed, all of us must accept and obey to perfect ourselves
and become more like our God and his Son, Jesus Christ. When that goal is achieved, we will
again be in their presence and glory. God bless us with the Holy Ghost to help us choose
wisely and with faithful assurance, that we, without question, are always on the Lord's side of
every question.
</p><p>
I leave you my witness and my testimony, brothers and sisters, to the truthfulness of this
work. I know this is God's restored kingdom and that it is here for the blessing and for the
salvation of his children. God bless us to so live, I pray in the name of Jesus Christ. Amen.
</p>
</div>
</div>
