<?xml version="1.0" encoding="UTF-8"?>
<div class="gcera">
<div class="gchead">
<p class="gctitle">Working Out Our Own Salvation</p>
<p class="gcspeaker">Elder Albert E. Bowen</p>
<p class="gcspkpos">Of the Council of the Twelve Apostles</p>
<p class="gcbib">Albert E. Bowen, <i>Conference Report</i>, April 1948, pp. 89-94</p>
</div>
<div class="gcbody">
<p>
One cannot read the record of Paul's missionary service without sensing his deep solicitude
for those who through his ministry had been converted to the faith. As he moved from place
to place, he contrived by one means or another to keep himself informed of the progress and
failures of the bodies of the Church which he had previously established. To him they
appeared to be as children who had not yet learned to walk alone, and he stood over them
with outstretched hand to pick them up and steady them again when their childish unsteady
feet stumbled and they were threatened with too disastrous a fall. Out of the gleanings of such
information as filtered through to him, he constructed a picture of their doings and of the
particular nature of the dangers which threatened their steadfastness.
</p><p>
LETTERS OF PAUL
</p><p>
Then he wrote them letters of instruction and of admonition and of promise, designed to
fortify them in their beliefs, and to warn them of the disastrous consequences of unwholesome
practices and to revive in their hearts the stirrings of a reborn hope&mdash;a pattern incidentally
which might profitably be studied by all having responsibilities of leadership,
</p><p>
Among the objects of his special concern were the Philippian saints, He wrote them some
letters, one of which contained this pregnant exhortation:
</p><p>
Wherefore, my beloved, as ye have always obeyed, not as in my presence only, but now
much more in my absence, work out your own salvation with fear and trembling
(<span class="citation" id="41230"><a href="javascript:void(0)" onclick="sx(this, 41230)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(41230)">Philip. 2:12</a></span>).
</p><p>
So great was Paul's yearning for them that it is easy to picture him as willing, even eager, if
he could have done it, to have made their struggle for them that they might have been spared
the disappointments and discouragements and heartaches incident to their climb upward to the
high plane which his teaching had set for them. But that could not be done. They had to fight
their own fight; they had to rise through their own endeavor. In short, they had to work out
their own salvation, and he so told them.
</p><p>
That is the statement of a principle of such primary importance that it ought to be received as
axiomatic.
</p><p>
WORK ESSENTIAL TO PROGRESS
</p><p>
Work is a condition precedent to progress and accomplishment in every realm of life. It is a
commonplace, accepted by all without question, that the way to develop strength of muscle in
the physical body is to exercise the muscles, to put them to work performing the function for
which they were intended. By inaction, nonuse, they would grow flabby and finally lose the
power to fill the duties of their natural offices. It is readily admitted, too, that mental vigor
comes only when the powers of the mind are extended to the mastery of difficult tasks. We
all have had the disappointing experience of seeing young men of apparently scintillating
brilliance fail utterly of achieving the distinction which their natural endowment gave promise
of, just as we have been happily surprised at the success gained by some of slower mentality.
The difference lies in their differing degrees of industry and endurance. The tenacious,
plodding mind has often outrun the quick and apparently more alert one simply because the
possessor of the former has been willing to submit himself to the rigid discipline of weary
hours of toil which the latter would not endure. Almost we might, it seems to me, lay it down
as a working rule that achievement is in proportion to the amount of intelligent effort one is
willing to put into an enterprise.
</p><p>
SPIRITUALITY DEVELOPS THROUGH PRACTICE
</p><p>
Strangely enough, while the truth of these observations is readily admitted in relation to
physical and mental development, there seem to be relatively few who recognize their equal
applicability to what is spoken of as the spiritual realm. Taking people by and large, it would
seem that an overwhelmingly preponderant number of them, while recognizing fully that
intellectual growth can come only through unremitting toil, and that the development of
physical skill can come only through persistent effort, yet somehow, when their religious or
spiritual lives are concerned, they act as if a different rule governs. The attitude seems to be
that religion is a placid, quiescent thing imposing no dynamic demand, and whose details can
be put into the hands of some chosen class to take care of, who relay to the masses the
essentials in the way of formulas, admonitions, or exhortations. What is spoken of as
spirituality seems not to be thought of as being susceptible of development through practice.
By some occult process it is supposed to bloom without cultivation and bear fruit without
tending.
</p><p>
I find no warrant anywhere for such an assumption. If one aspires to fashion wood, or metal,
or stone into houses, or cathedrals, or temples, or into some more delicate form of beauty, he
must develop the skill for his craft through painstaking toil and endeavor. If he would paint a
sunset, or coax music out of the harp or lyre, he must cultivate the artistry to do it by
interminable practice with color and brush or instrument. It makes no difference how richly
he may be endowed by nature with talents of craftsmanship or artistry if he lets those talents
lie unused or uncultivated, he will never arrive at the state of excellence in craftsmanship or
art. Howsoever lavishly one may be gifted with the qualities of mind that would fit him to be
a great mathematician, or chemist, or physicist, or biologist, or historian, he never can become
either except at the price of grueling struggle directed with intelligence to the desired end.
That is the inexorable law of life. It may neither be escaped nor circumvented. The old
copybook maxim was: "There is no excellence without labor." That must remain forever true.
It is the uncompromising law of this world.
</p><p>
EVERYTHING HAS ITS PRICE
</p><p>
Everything has its price, and, if obtained, the price must be paid. No one ever gets anything
for nothing. People sometimes flatter themselves that they do, but they are deceiving
themselves. They always pay in one coin or another. It may be in the coin of the realm, or
may be in the forfeiture of a degree of self-respect, or of honor, or of liberty, or the free
exercise of the right of choice. We see abundant examples of this all about us today,
individuals and whole nations, too, beguiled by the seductive promises of plenty without the
trouble and anxiety of care for their own concern, surrendering themselves to the fatuous
allurements of deceptive demagogues or to the blighting tyrannies of ruthless despots. Like
Esau, they are selling their birthright for nothing better than a mess of pottage
(<span class="citation" id="23544"><a href="javascript:void(0)" onclick="sx(this, 23544)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(23544)">Gen. 25:34</a></span>). And, saddest
of all, they are dwarfing their own powers for progress and blighting their own prospects for
achievement.
</p><p>
The struggle must be one's own. Nobody may do the learning act for another. One cannot
obtain wisdom, or learning, or beauty of character as a gift or an inheritance. One's father and
a long line of ancestry may have achieved distinction in one or more of the fields of notable
endeavor, but the individual may not ride in on the accomplishments of either or all of them.
Here, as elsewhere, he must perfect himself through his own striving; and he cannot rise
above the level of his own accomplishment wrought through his own labor. There is just one
way by which an inspiring son may rise to the same eminence as an illustrious father, and that
is through the same process by which that father rose to the high plane he occupies, by the
perfecting of his own powers, through mastering obstacles, overcoming discouragements,
cultivating virtues, and pressing unremittingly towards his goal. There is no other way.
</p><p>
THE GOAL OF RELIGION
</p><p>
The same law governs in respect to religious or spiritual life. We have no warrant for
supposing that we can build, or piece together, or fashion, a beautiful or perfect life without
working painstakingly at it. And the goal of all religion is the perfection of life. It can be
attained only through practice of the deeds which lead to perfection. It was the Master himself
who, in the course of that inimitable Sermon on the Mount, invited his listeners to this high
aspiration:
</p><p>
Be ye therefore perfect, even as your Father which is in heaven is perfect
(<span class="citation" id="34193"><a href="javascript:void(0)" onclick="sx(this, 34193)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(34193)">Matt. 5:48</a></span>).
</p><p>
And before the eyes of the rich young man who came inquiring what good thing he should do
that he might have eternal life, Jesus dangled the concept of perfection as the ultimate goal of
life:
</p><p>
. . . If thou wilt be perfect, go and sell that thou hast, and give to the poor, and thou shalt
have treasure in heaven: and come and follow me
(<span class="citation" id="34219"><a href="javascript:void(0)" onclick="sx(this, 34219)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(34219)">Matt. 19:21</a></span>).
</p><p>
Here was no promise of a sheltered, placid life, but rather a bidding to heroic action. If the
young man had been able to do it, he would have achieved a spiritual grandeur beyond
anything all his wealth could buy.
</p><p>
Paul says of the Master himself:
</p><p>
Though he were a Son, yet learned he obedience by the things which he suffered
(<span class="citation" id="24225"><a href="javascript:void(0)" onclick="sx(this, 24225)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(24225)">Heb. 5:8</a></span>).
</p><p>
And to the Corinthian saints he wrote:
</p><p>
. . . and every man shall receive his own reward according to his own labour
(<span class="citation" id="160"><a href="javascript:void(0)" onclick="sx(this, 160)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(160)">1 Cor. 3:8</a></span>).
</p><p>
To the same body he further declared:
</p><p>
Every man's work shall be made manifest: for the day shall declare it, because it shall be
revealed by fire; and the fire shall try every man's work of what sort it is.
</p><p>
If any man's work abide which he hath built thereupon [that is upon Jesus Christ as the
foundation], he shall receive a reward
(<span class="citation" id="161"><a href="javascript:void(0)" onclick="sx(this, 161)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(161)">1 Cor. 3:13-14</a></span>).
</p><p>
JUDGMENT ACCORDING TO WORKS
</p><p>
The Revelator declares concerning those who in vision he saw come forth from the dead:
</p><p>
. . . and they were judged every man according to their works
(<span class="citation" id="42198"><a href="javascript:void(0)" onclick="sx(this, 42198)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(42198)">Rev. 20:13</a></span>).
</p><p>
In that beautiful parable of the builders, Jesus likened that man who heard his sayings and <i>did</i>
them, to a wise man who built his house upon a rock, and when it was caught in rain and
flood and tempest, it fell not, for it was founded upon a rock; but he who heard his sayings
and did them <i>not</i>, he likened to a foolish man who built his house upon the sand, and when
the rain descended and the floods came and the wind blew and beat upon that house, it fell,
and great was the fall of it (<span class="citation" id="34200"><a href="javascript:void(0)" onclick="sx(this, 34200)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(34200)">Matt. 7:24-27</a></span>).
</p><p>
Modern-day revelation is replete with the same teaching, declaring with directness that at the
final reckoning, every man shall be judged:
</p><p>
. . . according to his works and the deeds which he hath done
(<span class="citation" id="12428"><a href="javascript:void(0)" onclick="sx(this, 12428)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(12428)">D&amp;C 19:3</a></span>).
</p><p>
It goes much further than that. It declares that:
</p><p>
Whatever principle of intelligence we attain unto in this life, it will rise with us in the
resurrection.
</p><p>
And if a person gains more knowledge and intelligence in this life through his diligence and
obedience than another, he will have so much the advantage in the world to come
(<span class="citation" id="12557"><a href="javascript:void(0)" onclick="sx(this, 12557)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(12557)">D&amp;C 130:18-19</a></span>).
</p><p>
That is only another way of saying that ennobling qualities woven into a life are eternal things.
They never cease to be of value or to bring their reward. It is to be noted, too, that the only
way to get them is by diligence and obedience. They do not come as gratuities. In fact, it is
the growing process induced by diligent effort that eventuates in the final result. There is no
need to multiply evidences. Scriptural teachings as well as the teachings of experience and
reason are all one way.
</p><p>
That I may not leave any chance of being misunderstood, I want to say now I have no
intention of getting involved in the old controversy as to whether salvation is by works or by
grace. With a proper definition of terms there is no basis for controversy.
</p><p>
GROWTH DEPENDS ON INDIVIDUAL EXERTIONS
</p><p>
Neither do I wish to be understood as saying that technical intellectual training is essential to
spiritual progress or understanding. There are too many evidences to the contrary. There need,
however, be no enmity between the two, though their methods may be dissimilar. Perhaps the
assumed distinction between the spiritual and the temporal grows out of the limitations of our
understanding. We may find when we come to see far enough down the perspective that they
unite in one. In fact God has said that to him all things are spiritual and not at any time has
he made anything that is temporal (<span class="citation" id="12439"><a href="javascript:void(0)" onclick="sx(this, 12439)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(12439)">D&amp;C 29:34</a></span>).
Perhaps, for safety, I ought to say, too, that I do not intend
to discount the reality or the value of divine interposition in aid of struggling mortals,
providing redemption and the plan of living which they could not provide for themselves The
thing that I do want to make clear is that the principle, that progress and growth depend upon
our own exertion and compliance with the governing laws, is a universal principle applicable
in the spiritual realm no less than in the realm of the temporal. Otherwise there is no meaning
to the great parable of the talents spoken of by Jesus. He likened the kingdom of heaven to a
man about to journey to a far country who called his servants to him and delivered to them
his goods <i>in proportion to their several abilities</i>. To one he gave five talents, to another two,
and to another one. He who received the five talents forthwith traded with them and doubled
them. So likewise did he who received the two talents. But he of the one talent hid it. When
the master returned, each gave his accounting. They who had multiplied their talents were
commended, but he who returned but the one talent which had been given him was
denounced as a slothful servant, and it was taken from him and given to the one who had
converted the five into ten (<span class="citation" id="34234"><a href="javascript:void(0)" onclick="sx(this, 34234)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(34234)">Matt. 25:14-30</a></span>).
</p><p>
CHURCH PROVIDES OPPORTUNITIES TO WORK
</p><p>
This brings me to the lesson I want to draw from all that has been said before and furnishes
the justification for saying it. This Church is so organized that it provides something for every
member to do. And the doing of those things offers the only means by which members grow
toward their promised destiny. Teachings are, in themselves, mere abstractions, powerless to
save unless they are translated into deeds. It is the doing of the deeds that leads to growth
through development of latent powers and the evolving of desired qualities of character. And
deeds are the individual acts of persons. One might sit passively for an eternity hearing the
best of instruction, or the exposition of the loftiest of principles without being much improved
unless those teachings and principles were made fruitful through conversion into practices of
living. It is what we make of our lives that counts. It is at once a marvel and a beauty of
Christ's gospel that for every requirement it lays down in the way of admonition to
righteousness, it provides a practical means for bringing it about. And that way is always to
provide things for the individual to do which result in the development of the desired qualities
of mind and spirit. This it does through the organized Church.
</p><p>
Thus does the Church become the medium for giving practical effect to the teachings of the
gospel. It introduces order where ineffectiveness and frustration would otherwise be. It
provides the means by which its members may step by step build into themselves that
aggregate of desired qualities which make men good and crowd out the qualities that are bad.
Thus do they grow towards perfection and do their part towards working out their own
salvation (<span class="citation" id="41231"><a href="javascript:void(0)" onclick="sx(this, 41231)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(41231)">Philip. 2:12</a></span>).
</p>
</div>
</div>
