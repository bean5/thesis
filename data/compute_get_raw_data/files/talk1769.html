<?xml version="1.0" encoding="UTF-8"?>
<div class="gcera">
<div class="gchead">
<p class="gctitle">Honesty is the Best Policy</p>
<p class="gcspeaker">Elder Theodore M. Burton</p>
<p class="gcspkpos">Assistant to the Council of the Twelve</p>
<p class="gcbib">Theodore M. Burton, <i>Conference Report</i>, April 1970, pp. 90-92</p>
</div>
<div class="gcbody">
<p>
It used to be said of a Mormon that his word was as good as his bond. Once a Mormon gave
his word, you could rely on it. Even if it meant a personal sacrifice of money, time, or effort,
once he gave his word you could depend on him to do as he promised. Is the same thing true
today?
</p><p>
<b>Honesty takes many forms</b>
</p><p>
Honesty can take many forms, such as giving a full day's work for a full day's pay. Can one
be considered honest who loafs on his job; who does not take pains with his work; who
wastes time in the rest room, around the water fountain, or who stretches his lunch hour an
extra 15 minutes? It is easy to compile a list of dishonest business practices that take money
from an employer. We can name such things as making unnecessary personal telephone calls,
coming late to work, taking home paper, pencils, postage stamps, or mailing personal letters
through the company postage meter. Such practices once frowned on are almost universally
accepted today with the excuse that "everyone does it." The fact of the matter is that everyone
<i>doesn't</i> do it. There are still many honest people in this world.
</p><p>
<b>Honesty within the family</b>
</p><p>
Honesty includes more than material things. There must be honesty within the family. A man
must be honest with his wife and a wife honest with her husband. Children must be honest
with parents and parents honest with their children. Honesty involves loyalty to family,
friends, neighbors, the community, and the nation. Honesty is a host of little things that make
a person trustworthy. Honesty is a fundamental principle in the true worship of a kind, loving
Father in heaven. One of the reasons the Father loved the Son so much was because he was
trustworthy. Jesus said: 
</p><p>
"Therefore doth my Father love me, because I lay down my life, that I might take it again.
</p><p>
"No man taketh it from me, but I lay it down of myself. I have power to lay it down, and I
have power to take it up again. This commandment have I received of my Father"
(<span class="citation" id="28586"><a href="javascript:void(0)" onclick="sx(this, 28586)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(28586)">John 10:17-18</a></span>).
</p><p>
Jesus used the power God gave him to help others. How wonderful if that same testimony
could apply to us in our dealings with others. How wonderful if we could likewise say:
Therefore doth my Father love me, because I do what he asks me to do.
</p><p>
<b>Sharpness in business dealings</b>
</p><p>
I devoutly wish that <i>all</i> members of The Church of Jesus Christ of Latter-day Saints could be
numbered among the honest, trustworthy people of this world. Some members of the Church
succumb to the world in which they live. They wear their religion on Sunday, but forget it
when they enter the business world. There they become as sharp and untrustworthy in
business dealings as some of their associates. You can be both successful and honest&mdash;in
fact, you can be more successful as an honest man than you ever can as a self-seeking,
dishonest person.
</p><p>
No one is born honest. No one is born dishonest. We have to be taught to be honest. We have
to experience the pain, worry, and discomfort of dishonesty to know that truly, "honesty <i>is</i> the
best policy." The prophet Alma told his son Corianton, "Behold, I say unto you, wickedness
<i>never</i> was happiness" (<span class="citation" id="9133"><a href="javascript:void(0)" onclick="sx(this, 9133)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(9133)">Alma 41:10</a></span>,
italics added). Dishonest persons soon
discover this. Not only does dishonesty ruin their lives, but it also brings shame and dishonor
to their families. Dishonesty also brings shame and suffering to the people of the church to
which they belong. Like it or not, we are all soon tarred with the same brush.
</p><p>
<b>Dishonesty in little things</b>
</p><p>
It is easy to be dishonest in little things. Few people think it dishonest to take a pencil home.
The Church has about 3,500 employees. Figuring five cents per pencil, if each person were to
take one pencil, such a loss would amount to $175.00. A person who would think twice about
stealing $175.00 may never lose a moment's sleep about taking a pencil. Yet what limits dare
one set for dishonesty? Little things soon amount to big things. Before we are aware, we
graduate to greater sin.
</p><p>
The telling of a lie may appear to be a little thing, but one lie leads to another until a person's
reputation is lost. Once a person is branded a liar, a cheat, or a thief, it takes a long period of
repentance and restoration to bring back a reputation so easily tarnished by a careless and
thoughtless act. As long as we have to learn to be honest or dishonest, why not learn to be
honest?
</p><p>
I wish all boys could have had a mother such as I had. One day I came home eating an apple.
Mother asked me where I got it. I told her I found it. She soon discovered that I had found it
in Mr. Goddard's grocery store, and mother insisted I take it back. I protested that it was
partly eaten, but at her urging I took the partly eaten apple back to Mr. Goddard and
shamefully told him I had robbed his store. He phoned mother to tell her I had brought it
back and said he had seen me take it, but it was such a little thing he hadn't bothered to say
anything about it. It wasn't a little thing to mother. She loved us too much to have a thief in
the family.
</p><p>
<b>Justification through faultfinding</b>
</p><p>
There is a phenomenon that accompanies dishonest persons. Before long they become very
critical and tend to find fault with leaders who call their attention to their unrighteousness.
Instead of repenting and changing their lives for the better, they tend to justify their own
misdeeds by finding fault with their leaders. The Prophet Joseph Smith said: 
</p><p>
"I will give you one of the Keys of the mysteries of the Kingdom. It is an eternal principle,
that has existed with God from all eternity: That man who rises up to condemn others, finding
fault with the Church, saying that they are out of the way, while he himself is righteous, then
know assuredly, that that man is in the high road to apostasy; and if he does not repent, will
apostatize, as God lives" (<i>Teachings of the Prophet Joseph Smith</i>, Joseph Fielding Smith,
comp., pp. 156-57).
</p><p>
<b>In prosperity we forget God</b>
</p><p>
Another truism is that God does bless the righteous. Often in that moment when God showers
blessings upon us most abundantly, we forget him because we no longer need his sustaining
hand. I hope with our present prosperity we are not forgetting our Maker nor forgetting those
practices of honesty and integrity that have made us what we are today. Helaman, a great
Book of Mormon prophet, wrote: 
</p><p>
". . . behold how false, and also the unsteadiness of the hearts of the children of men; yea, we
can see that the Lord in his great infinite goodness doth bless and prosper those who put their
trust in him.
</p><p>
"Yea, and we may see at the very time when he doth prosper his people . . . then is the time
that they do harden their hearts, and do forget the Lord their God, and do trample under their
feet the Holy One&mdash;yea, and this because of their ease, and their exceedingly great
prosperity.
</p><p>
"Yea, how quick to be lifted up in pride . . . and how slow are they to remember the Lord
their God, and to give ear unto his counsels, yea, how slow to walk in wisdom's paths!"
(<span class="citation" id="24708"><a href="javascript:void(0)" onclick="sx(this, 24708)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(24708)">Hel. 12:1-2,5</a></span>).
</p><p>
Would it not be wise to examine our practices to see which road we are walking? Is <i>our</i> word
as good as our bond? Are we honest in our dealings with others, even in little things?
</p><p>
<b>Leaders in righteousness</b>
</p><p>
Just because we Latter-day Saints live in a current world characterized by sharp and dishonest
practices is no excuse for us to be untrustworthy. Because others lie is no excuse for us to be
dishonest. On the contrary, we must be leaders of righteousness so that others may know the
paths of honesty and righteousness that lead back into the presence of God the Eternal Father.
The power of the priesthood is given us to lead. Those who bear that priesthood must be
pillars of honesty and virtue in every sense of the word. The apostle Peter spoke to priesthood
leaders in this way: 
</p><p>
"But ye are a chosen generation, a royal priesthood, an holy nation, a peculiar people; that ye
should shed forth the praises of him who hath called you out of darkness into his marvelous
light: 
</p><p>
"Dearly beloved, I beseech you as strangers and pilgrims, abstain from fleshly lusts, which
war against the soul; 
</p><p>
"Having your conversation honest among the Gentiles: that, whereas they speak against you as
evildoers, they may by your good works, which they shall behold, glorify God in the day of
visitation" (<span class="citation" id="2327"><a href="javascript:void(0)" onclick="sx(this, 2327)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(2327)">1 Pet. 2:9,11-12</a></span>).
</p><p>
<b>Honoring the royal name we bear</b>
</p><p>
As the children of God, our lives must be filled with good works, honest practices, and honest
virtues that are characteristic of the children of God. When we take upon ourselves the name
of Jesus Christ, we bear the responsibility of guarding that great name with our very lives.
</p><p>
To fail to honor that royal name we bear as Christians is to hold the very God we espouse to
open ridicule and shame. In effect, we crucify him anew before the world.
</p><p>
Now we know these things are true. Like Amulek of old we know better, but often we won't
listen. As Amulek said: 
</p><p>
". . . I did harden my heart, for I was called many times and I would not hear; therefore I
knew concerning these things, yet I would not know; therefore I went on rebelling against
God, in the wickedness of my heart" (<span class="citation" id="9123"><a href="javascript:void(0)" onclick="sx(this, 9123)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(9123)">Alma 10:6</a></span>,
italics added).
</p><p>
You know as I know that the ways of God will not fail. His purposes will be accomplished
and he will save us from our sins if only we will not harden our hearts.
</p><p>
This is God's work. We are God's children and must not fail him. Let us all then obey those
teachings which we know deep down in our hearts are true. It is time to remember that Jesus
truly is the Christ, the living Son of the living God. He is the Redeemer and Savior of this
world. Of the divinity of his teachings and the rightness of his cause I bear my personal
witness in the name of Jesus Christ. Amen.
</p>
</div>
</div>
