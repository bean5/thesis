<?xml version="1.0" encoding="UTF-8"?>
<div class="gcera">
<div class="gchead">
<p class="gctitle">Can I Really Know?</p>
<p class="gcspeaker">Elder Boyd K. Packer</p>
<p class="gcspkpos">Assistant to the Council of the Twelve Apostles</p>
<p class="gcbib">Boyd K. Packer, <i>Conference Report</i>, October 1964, pp. 126-129</p>
</div>
<div class="gcbody">
<p>
Some time ago a representative of the Church on a plane bound for a large west coast city
was drawn into conversation with a young attorney. Their conversation centered on the front
page of a newspaper, a large city tabloid with the sordid, the ugly, the tragic openly
displayed.
</p><p>
The attorney said the newspaper was typical of humanity and typical of
life&mdash;miserable, meaningless, and in all ways useless and futile. The elder protested, holding that
life was purposeful, and that there lives a God who loves his children, and that life is good
indeed.
</p><p>
When the attorney learned that he was speaking to a minister of the gospel, he said with some
emphasis, "All right! We have one hour and twenty-eight minutes left on this flight, and I
want you to tell me what business you or anyone else has traipsing about the earth saying that
there is a God or that life has any substantial meaning."
</p><p>
<b>Jesus Is the Christ</b>
</p><p>
He then confessed himself to be an atheist and pressed his disbelief so urgently that finally he
was told, "You are wrong, my friend. There is a God. He lives. I <i>know</i> he lives." And he
heard the elder proclaim with fervor his witness that Jesus is the Christ.
</p><p>
But the testimony fell on doubtful ears. "You don't <i>know</i>," he said. "Nobody <i>knows</i> that! You
can't <i>know</i> it."
</p><p>
The elder would not yield and the attorney finally said condescendingly, "All right. You say
you know. Then [inferring, 'if you are so smart'] tell me how you know."
</p><p>
The elder had been faced with questions before, in written and oral examinations attendant to
receiving advanced degrees, but never had a question come which seemed to be so
monumentally significant.
</p><p>
I mention this incident, for it illustrates the challenge that members of the Church face&mdash;all
of them. This challenge particularly becomes a stumbling block to our youth. They face a
dilemma when the cynic and the skeptic treat them with academic contempt because they hold
to a simple child-like faith. Before such a challenge many of them turn away, embarrassed
and ashamed that they cannot answer the question.
</p><p>
Essential to Knowledge of Spiritual Truth
</p><p>
As our friend attempted to answer this question, he found himself helpless to communicate
with the attorney, for when he said, "The Holy Ghost has borne witness to my soul," the
attorney said, "I don't know what you are talking about."
</p><p>
The words "prayer" and "discernment" and "faith" were meaningless to the attorney, for they
were outside the realm of his experience.
</p><p>
"You see," said the attorney, "you don't really know. If you did, you would be able to tell me
how you know." The implication was that anything we know we readily can explain in words
alone.
</p><p>
But Paul said:
</p><p>
"Now we have received, not the spirit of the world, but the spirit which is of God; that we
might know the things that are freely given to us of God.
</p><p>
"Which things also we speak, not in the words which man's wisdom teacheth, but which the
Holy Ghost teacheth; comparing spiritual things with spiritual.
</p><p>
"But the natural man receiveth not the things of the Spirit of God: for they are foolishness
unto him: neither can he know them, because they are spiritually discerned"
(<span class="citation" id="385"><a href="javascript:void(0)" onclick="sx(this, 385)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(385)">1 Cor. 2:12-14</a></span>).
</p><p>
The elder felt that he might have borne his testimony unwisely and prayed in his heart that if
the young attorney could not understand the words, he could at least feel the sincerity of the
declaration.
</p><p>
"All knowledge is not conveyed in words alone," he said. And then he asked the attorney,
"Do you know what salt tastes like?"
</p><p>
"Of course I do," was the reply.
</p><p>
"When did you taste salt last?"
</p><p>
"Why, just as we had dinner on the plane."
</p><p>
"You just <i>think</i> you know what salt tastes like," said the elder.
</p><p>
"I know what salt tastes like as well as I know anything," said the attorney.
</p><p>
"If I gave you a cup of salt and a cup of sugar and let you taste them both, could you tell the
salt from the sugar?"
</p><p>
"Now you are getting juvenile," was his reply. "Of course I could tell the difference. I know
what salt tastes like. It is an everyday experience; I know it as well as I know anything."
</p><p>
"Then," said the elder, "may I ask you one further question? Assuming that I
had never tasted salt, could you explain to me, in words, just what it tastes like?"
</p><p>
After some thought the attorney ventured, "Well . . . I . . . it is not sweet, and it is not sour."
</p><p>
"You have told me what it isn't," was the answer, "not what it is."
</p><p>
<b>Limitations of Words</b>
</p><p>
After several attempts he admitted failure in the little exercise of conveying in words
knowledge so commonplace as that. He found himself quite as helpless as the elder had been
to answer his question.
</p><p>
As they parted in the terminal the elder bore testimony once again, saying, "I claim to know
there is a God. You ridiculed that testimony and said that if I did know I would be able to tell
you exactly how I know.
</p><p>
"My friend, spiritually speaking, I have tasted salt. I am no more able to convey to you in
words how this knowledge has come than you are to perform the simple exercise of telling
me what salt tastes like. But I say to you again, there is a God. He does live. And just
because you don't know, don't try to tell me that I don't know, for I do."
</p><p>
Young people, do not apologize or be ashamed because you cannot frame into words that
which you know in your heart to be true. Do not repudiate your testimony merely because
you have no marvelous manifestations to discuss.
</p><p>
Lehi saw in his dream those who "tasted the fruit," and "were ashamed, because of those that
were scoffing at them; and they fell away into forbidden paths and were lost"
(<span class="citation" id="1594"><a href="javascript:void(0)" onclick="sx(this, 1594)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(1594)">1 Ne. 8:28</a></span>).
</p><p>
<b>Learning by Other Means</b>
</p><p>
We sympathize with you and know how difficult it is to hold to the truth, particularly when
professors of worldly knowledge&mdash;some of them counterfeit Christians&mdash;debunk and scoff.
We know from personal experience that you may have some doubts. You may wonder at
times, "Can I ever really know for sure?" You may even wonder, "Does anyone really know
for sure?"
</p><p>
President David O. McKay once told of his search for a testimony as a youth. "I realized in
youth," he said, "that the most precious thing that a man could obtain in this life was a
testimony of the divinity of this work. I hungered for it."
</p><p>
He indicated that he had somehow received the impression that the testimony would come as
a great spiritual manifestation.
</p><p>
"I remember," he said, "riding over the hills one afternoon thinking of these things and
concluded that there in the silence of the hills was the best place to get that testimony.
</p><p>
"I stopped my horse and threw the reins over his head . . . I knelt down and with all of the
fervor of my heart poured out my soul to God and asked him for a testimony of this gospel. I
had in mind that there would be some manifestation, that I should receive some
transformation that would leave me without doubt.
</p><p>
"I arose, mounted my horse, and as I started over the trail I remember rather introspectively
searching myself, and involuntarily shaking my head, saying to myself, 'No, sir, there is no
change; I am just the same boy I was before I knelt down.'"
</p><p>
<b>Qualifications of Witnesses</b>
</p><p>
President McKay continues, "The anticipated manifestation had not come. Nor was that the
only occasion. However, it did come, but not in the way that I had anticipated. Even the
manifestation of God's power and the presence of his angels came, but when it did come, it
was simply a confirmation; it was not the testimony."
</p><p>
In answer to your question, "Can I ever really know for sure?" we answer, just as certainly as
you fill the requirements, that testimony will come. The Lord has never said, nor was it ever
pretended, that this testimony yields itself to scientific investigation, to mere curiosity, or to
academic inquiry.
</p><p>
In answer to your question, "Does anybody really know?" Yes, tens of thousands know. The
brethren know. Your parents know.
</p><p>
I have respect for the truth. It is wrong to fabricate, to invent, to mislead.
</p><p>
There is another dimension also. When one has received that witness, and is called to testify,
for him to dilute, to minimize, to withhold would be grossly wrong. It is in the face of this
that I feel the urgency to bear witness. And I bear my solemn witness that Jesus is the Christ.
I say that I know Jesus is the Christ, that the gospel of Jesus Christ was restored to Joseph
Smith, a prophet of God, that David O. McKay who presides over this Church is a prophet of
God. In the name of Jesus Christ. Amen.
</div>
</div>
