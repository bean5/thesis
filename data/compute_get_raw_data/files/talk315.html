<?xml version="1.0" encoding="UTF-8"?>
<div class="gcera">
<div class="gchead">
<p class="gctitle">Practical Usage of Religion</p>
<p class="gcspeaker">Elder Stephen L Richards</p>
<p class="gcspkpos">Of the Council of the Twelve Apostles</p>
<p class="gcbib">Stephen L Richards, <i>Conference Report</i>, October 1947, pp. 131-137</p>
</div>
<div class="gcbody">
<p>
We gather periodically in the conferences of the Church, or listen over the radio to the
proceedings as a part of our worship, in order that we may be refreshed in our faith and
edified in our concepts and duties. I am sure that the sessions of the present conference have
served us well in these respects. It is my earnest desire that I may contribute a little to the
stimulation and encouragement we are receiving.
</p><p>
BEARERS OF THE GOSPEL MESSAGE
</p><p>
I know of few things more stirring to our faith and devotion in this noble cause with which
we have the honor to be identified than a clear realization of its lofty purposes among the
children of men. Perhaps it is not given to many of us to see the over-all picture in its
perfection. We can only try with the equipment at our command to make the world
understand the importance and vitality of the message we bear. After we
have done our best in thought, word, and action, we can but pray that the Lord will add his
blessing.
</p><p>
We want the world to understand the position of this Church. We are propagandists for its
doctrines and principles. Unfortunately, that word has come to have a rather unsavory
connotation, for, in its correct meaning, it describes us. We are converted bearers of a
message which we are charged to give to the world. We cannot shrink from that obligation
and be loyal to the cause. We bear this message to our fellow men, not only because we are
commanded so to do but also because we have in our hearts a deep regard for the welfare of
men and a Christian desire to help them. We are fully convinced that the message we have for
them is the greatest boon which can come into their lives.
</p><p>
Now, what is this message which this Church carries to mankind? Naturally, within the
limitations of these brief remarks, I cannot attempt to present more than aspects of it and that
merely in outline. If I can do that in the common language we speak today with any measure
of clarity, I shall be very grateful.
</p><p>
MESSAGE DEFINES RELIGION
</p><p>
This message defines religion. It interprets all phases of a man's existence in terms of
religion. There is no part of living not influenced by it. Our thoughts, our environment, our
education, our companionships and associations, our health, our concepts of wealth,
government, and society in the scope of this message are all religious considerations. Religion
therefore becomes not a philosophy apart from life to be held up for scrutiny, criticism, and
debate. Rather, it is an integrated way of life, a system and program of individual and
community living under eternal law which man did not make and cannot change.
</p><p>
Elder Albert E. Bowen the other day told us so impressively how it cannot be compromised.
When a man comes to know and feel such an interpretation of religion, it completely
dominates his appraisals, his choices, and his judgment. Many would characterize such a man
as a fanatic, and he would be a fanatic if his religion were not true and the divine source of
wisdom.
</p><p>
RELIGION A MOTIVATING FORCE
</p><p>
Have the critics of such an exact religion ever set out the merits of religion in moderation? I
thank Brother Richard L. Evans for defining that word for me in his remarks of the broadcast
this morning. Sunday religion, it is sometimes called, the kind men carry in their wives'
names. How do they justify a partial acceptance of divine law and principle? Surely
consistency would demand that if the source of religion is accredited, the application must be
universal and unvaried. I can understand how many men have not accepted interpretations of
divine law and religion, how many have rebelled against the practices of
religionists, but if religion is accepted at all, I cannot see how logical, clear-thinking minds
can take it for any less than what it really is&mdash;the motivating, all-controlling force in the life
of man and the organization of the universe.
</p><p>
I wonder if we are able to appreciate what such an interpretation of religion if widely
accepted would do for the world today? Here are a few of the things I feel it would
accomplish. It would remove uncertainty and doubt as to the principles and standards which
should be observed in reaching decisions on all matters, personal, social, national, and
international. What a tremendous gain it would be if the answer to the age-old question,
"What is right?" could be found by most men to be in the acceptance of the divine source of
right. Here again appears the inconsistency and futility of the position of the so-called partial
religionists. Pretty generally throughout the world recognition is given to the divine laws
against killing, adultery, stealing, and lying, and little question is raised regarding the source
and perpetuity of these regulations of human conduct. Where is the justification for
discrimination against the other laws exacting worship and obedience to the God of the
universe and proscribing idolatry, blasphemy, and desecration of the Lord's holy day? The
laws commanding worship and reverence for our divine and earthly parents
(<span class="citation" id="22575"><a href="javascript:void(0)" onclick="sx(this, 22575)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(22575)">Ex. 20:12</a></span>) constitute one-half
of the Decalogue, and remember they come first. In the interpretation of religion we give to
the world they are first, in practice as well as in theology.
</p><p>
RELIGION NEEDED IN WORLD AFFAIRS
</p><p>
Do you think that if such an interpretation of religion were widespread, even among the
Christian nations of the earth we could have any such spectacle of discord and intrigue as the
nations of the world have recently presented to disgusted and discouraged people all over the
earth? Have you ever heard of a voice being raised in any of the sessions of the United
Nations since its inception more than two years ago protesting the infractions of God's laws
or importuning his help in achieving the purposes of that organization? I think you have not,
unless perhaps in some innocuous way, because I suspect that it is tacitly agreed that God and
religion shall be shut out of the proceedings. Well, my friends, it is a part of the message that
the Church of Jesus Christ of Latter-day Saints bears to the world that God and religion
cannot be shut out from the consideration of world affairs without mortal hazard to the cause
of goodness and peace.
</p><p>
INALIENABLE RIGHTS
</p><p>
I have read and heard a good many statements by eminent writers and speakers to the effect
that our liberty of which we are justly proud is an achievement and not a gift. In the sense
that it had to be worked for, fought for, and preserved with vigilance these statements
are true. But let it never be forgotten that our concept of liberty is a gift. No
human is the author of that concept. Many great men have so recognized it as did Thomas
Jefferson when he wrote the Declaration of Independence and declared that "men are endowed
with certain inalienable rights." Why are these rights inalienable? Because men did not create
the right to liberty! In the exercise of his free agency he may surrender his privileges, and his
property, and he may become the slave of others or of the state, but his free agency is as
native to him as the air he breathes. It is part and parcel of his eternal constitution, and
Jefferson was "righter than I think he himself knew when he declared it an endowment which
cannot be alienated.
</p><p>
The message which we bear affirms that God is the Author of our inalienable liberty; that
men, all men are of noble lineage, sons and daughters of the Eternal Father; and that liberty is
their birthright. I think that prior to the advent of the restored gospel a little over a century
ago there was no such understanding of the precious gift of liberty as there is today. In his
latter-day revelations the Lord has set forth much concerning it, but I thank God that before
these revelations came, noble men were blessed with this lofty concept of man's inherent right
to liberty and that they were prompted to incorporate these divine principles in the organic
law and history of our favored land
(<span class="citation" id="12396"><a href="javascript:void(0)" onclick="sx(this, 12396)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(12396)">D&amp;C 101:80</a></span>).
</p><p>
NEED FOR DIVINE AID
</p><p>
Now I know that there are many in Christian nations and many in prominent places who
accede to this taboo on religion in the consideration of national and world affairs. They seem
to think that they can fight aggressive, atheistic communism without uttering a word in
defense and exposition of divinely-given concepts, and without even seeking divine aid in the
preservation of divine principles for the race. I do not pretend to qualify or speak as an expert
on international affairs, but I am sure that I voice the sentiment and feeling of millions of
God-loving people over the world when I assert that the sooner the issues now confronting the
nations are recognized as a moral conflict between right and wrong, between truth and error,
between Christ and anti-Christ, the sooner will come the solution and peace. I know that this
is and has always been the position of this Church. There are prophecies, ancient and modern,
statements and declarations, and experience to support this position.
</p><p>
PROGRAM TO MEET WORLD CRISIS
</p><p>
I am aware of what some may say, even though they may not be unsympathetic with the
views I have expressed. They ask: Even though the time has come for a "showdown" between
the forces of good and the forces of evil, what justifies your Church with its relatively small
numbers, short history, and inconspicuous place in the world of religions in
assuming to prescribe a program of religious concepts and action to meet the world crisis
today? Why not let the great religions of the earth take the burden of the battle and you trail
along? Well, I hesitate a little to give my answer to that question. Not because I am not sure
of the answer, but because I am fearful about hurting feelings. I give my answer in three
parts:
</p><p>
FAILURE OF WORLD RELIGIONS
</p><p>
First, in the centuries during which the numerically great religious denominations of the world
have dominated the religious concepts and actions of their peoples there has been failure,
signal failure to uphold divine standards of righteousness, brotherly love, and peace. These
great religions, however good the intentions, have proved impotent to forestall wars and to
prevent barbarism, brutality, and atrocities such as the world has seldom known even in its
darkest ages and this, too among peoples where substantially the whole populace professed
one creed. Of course, it is idle to conjecture what the world might have been without these
creeds. No one with historical knowledge will fail to acknowledge their contribution to the
enlightenment and culture of the race, but as a source of motivation in the control of men's
greeds and passions the known results are certainly not encouraging.
</p><p>
ANSWERS TO LIFE'S QUESTIONS
</p><p>
Second, the ever-growing quest for knowledge among the enlightened people of the earth
demands answers, reliable information on questions vital to life and its meaning. In many
cases science has accentuated the importance of these questions and has done much to clarify
thinking about them, but science has seldom given the answers, especially in the fields of
human behavior and relationships. It seems indelicate, I know, to many not of my faith for
me to assert that we have the answers. I mean, of course, about the source, the meaning, and
purpose of life and the government of man. We do not know and cannot say just why the
Lord did not commit these answers which are the essence of the holy gospel to the keeping of
many men in many nations, but we do know that he committed them to the custody of this
Church through those whom he chose to initiate his work in the latter-days. For this
conviction we have substantial proof, evidence, admissible and competent evidence, not
gathered from the debatable sources of antiquity but from witnesses and experiences within
recent generations of men, attested and recorded to meet all reasonable demands. Here is
God's truth and his latest word for the direction of the human family. In application it has
been successful and not a failure.
</p><p>
DIVINE COMMISSION TO ACT
</p><p>
Third, I think that nearly everyone will agree that to sustain effectively a cause one must have
the authority to represent it. He must be a part of it, on the inside and not on
the outside. He must be authorized to speak for it. I know that I broach a controversial
question in speaking of the authority to represent God and Christ in the earth. Must we not
discuss the great issues of the world because they are controversial, out of fear of treading on
someone's sentimental toes? I believe that thinking, honest, inquisitive people the world over
want the truth about authority, the divine commission to set up the Lord's work in the earth
to administer the ordinances of the holy gospel and to interpret his word and will for the
guidance of mankind. Now, there is much incontrovertible evidence that I might review here
today in support of the claims to divine authority which this Church makes. I am not going to
present this evidence; time will not permit; and many of you who listen know of it. I will
confine my comment to one phase of this subject of authority of which I think our friends of
the world have a very inadequate conception.
</p><p>
Is it not logical to assume that God, who is the Father of all men and who has endowed all
men with liberty and equality of opportunity, does not "play favorites"? If it be regarded as a
blessing and an honor to represent him, is there any good reason why that blessing and
dignity should not be conferred on all of his sons who are worthy? Do you know of any
warrant, scriptural or otherwise, for the creation and maintenance of special groups of spiritual
experts to hold the authority of the Lord to the exclusion of other good men? What
justification is there for differentiation among loyal and true sons of God by the clothes they
wear? Does the apparel bring authority or is it essential to the recognition of a good life
devoted to the Lord? And does it not seem reasonable that if the worthy sons of God are to
enjoy his companionship in the world to come as a reward for their faithful labors here in this
life that they should all bear a portion of his power and be endowed with a part of his
spiritual intelligence which constitute the essence of divine authority and priesthood? What a
boon it would be to the world if all men understood the nature and permanence of the Holy
Priesthood and its indispensable part in the exaltation of a man and his family. Well,
unfortunately, but few understand it, and the world goes along century after century following
in the pattern of form and tradition, and influenced more by the display of pomp and
pageantry than by a sound theology manifest in practical, living religion. That constitutes my
answer to those who ask why we cannot trail along in the wake of the so-called great
religions in defense of the Christ and his way of life in the crucial contest of the present day.
</p><p>
CHURCH TO TAKE THE LEAD
</p><p>
It then follows, as I see it, that we are to take the lead. There is no one to follow except him
who is and always has been at our head. With his approval leaders have been selected for us.
They hold no more or any different kind of priesthood from the humblest
good man in the kingdom, but they have a special commission to guide and direct our efforts
in establishing the kingdom and prosecuting its work in the world. And to one is given the
keys of the kingdom (<span class="citation" id="34159"><a href="javascript:void(0)" onclick="sx(this, 34159)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(34159)">Matt. 16:19</a></span>).
We will follow their leadership, and they will not lead us astray, for
they are unselfishly devoted to our interests and their wisdom is inspired.
</p><p>
There is a hungry world about us, my dear brethren and sisters, a challenging, hungry world.
People need food for the body, food for the soul. We haven't the millions necessary to build
and clothe their bodies. If we had, I think we would do it so that no one would suffer. We do
what we can. We do have in our possession, however, what stricken humanity needs to satisfy
its hungry soul and revive hope and confidence for peace and security. The message we bear
is one of enlightenment to the statesman as well as the pauper. It teaches that enduring
happiness is to be found only in goodness and that the highest tribute to the Christ is the
tribute of a good life. It teaches that strength, the strength of the nation as so well portrayed
by Brother Mark E. Petersen in his Church of the Air address this morning, is the product of
that goodness. This message defines God, not as an intangible principle in the universe, but a
personality with such inexpressibly beautiful, admirable, and potent attributes as to claim the
everlasting love, confidence, reverence, and adulation of every living soul who comes to know
the truth about him. It sets forth a brotherhood which in spirit and application transcends any
definition of that relationship the world has known, at least in modern times. It brings comfort
to the sorrowing. It rebukes avarice and places the world of business on the high plane of
stewardship and trusteeship in the acquisition and distribution of the wealth of the world. It
brings contentment and peace of mind. It gives unfailing purpose to life. It accentuates
personality in every, man, woman, and child. Indeed, it makes all things&mdash;government and
even the Church it&mdash;self-subservient to the eternal welfare of man.
</p><p>
If I could have my wish today, it would be that every one of God's children could hear this
glorious message and earnestly give consideration to it. I am sure that thereby there would
come into countless hearts through the whisperings of the spirit that joy and happiness which
attend our own testimonies of the truth. Through our missionary system we have made a
tremendous contribution to this end, but it is not enough. We must find new ways to inform
and persuade the world, and if we are faithful and truly devoted, God will open the way. That
is my conviction.
</p><p>
I know as I know that I live that this is his kingdom. He will never desert it. God help all of
us never to desert him, I humbly pray in the name of Jesus Christ. Amen.
</p>
</div>
</div>
