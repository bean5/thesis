<?xml version="1.0" encoding="UTF-8"?>
<div class="gcera">
<div class="gchead">
<p class="gctitle">A Charter for Youth</p>
<p class="gcspeaker">Elder Gordon B. Hinckley</p>
<p class="gcspkpos">Of the Council of the Twelve Apostles</p>
<p class="gcbib">Gordon B. Hinckley, <i>Conference Report</i>, October 1965, pp. 50-54</p>
</div>
<div class="gcbody">
<p>
I am aware that I speak to many times more outside this historic building than are here
assembled. I seek the inspiration of the Lord that my words may find reception in your hearts.
</p><p>
One of the fascinating and challenging scenes of this season is the procession of millions of
young men and women returning to universities. One senses not only their great expectations,
but also their fears and frustrations. Others of their age are depressed by the fact that they are
being drafted into the armed services to form a vast military reserve while their associates on
active duty are involved in an undeclared but nonetheless real and bloody war in a distant and
strange land.
</p><p>
<b>Frustrating Time for Youth</b>
</p><p>
No one need be reminded that this is a frustrating time for youth. Many find themselves in
rebellion against the practices and institutions of our day. They are sincere in their discontent.
They hunger for something better.
</p><p>
They have come to realize that there are values which money cannot buy. They miss the
stability of old-fashioned home life; they hunger for a more personal relationship with
teachers who might challenge their inquisitive minds. Many are disillusioned over old
standards of patriotism and loyalty. Even in the churches too many have found themselves
worshiping a dead ritual rather than the Living God. They have hungered for bread and have
been given a stone (<span class="citation" id="35442"><a href="javascript:void(0)" onclick="sx(this, 35442)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(35442)">Matt. 7:9</a></span>).
</p><p>
Those of you who witnessed or read of the Berkeley riots last spring, and lesser riots at other
schools, cannot minimize the seriousness of the plight in which thousands of our young
people find themselves.
</p><p>
I cannot agree with much of what they have done to voice their complaints, but I can agree
that many of them deserve something better than they are getting. They are being cheated&mdash;by
themselves in part&mdash;but more so by us their parents, their teachers, their leaders. They are
entitled to more, and ours is the obligation to offer it. And so, I should like to speak to those
of my own generation and propose in great earnestness a charter for youth based on the
gospel which we espouse.
</p><p>
It is a four-point charter. It is a bill of entitlement, setting forth briefly some of those
priceless values we owe every young American, and the youth of the world. They are&mdash;
</p><p>
     1. A home to grow in.<br />
     2. An education worth striving for.<br />
     3. A land to be proud of.<br />
     4. A faith to live by.
</p><p>
<b>A Home to Grow In</b>
</p><p>
I mention first <i>a home to grow in</i>. I recently read an article written by a young man who
roamed the Berkeley campus and its environs. His descriptions were clever, but his
illustrations were tragic. He told of a girl, a student from an affluent home. Her father was a
man of means, an executive of a large corporation, loyal to the company, loyal to his club,
loyal to his party, but unwittingly a traitor to his family. Her mother had saved the civic
opera, but had lost her children. The daughter, a child of promise, had become entangled in a
student revolt, and without an anchor, had quit school, and had drifted to the beatnik crowd,
her will-o'-the-wisp satisfactions coming only from nights of reveling and days of rebellion.
</p><p>
Of course, her father mourned and her mother wept. They blamed her, evidently unaware of
their own miserable example of parenthood which had done much to bring her to the tragic
circumstances in which she found herself.
</p><p>
As I read that account there passed through my mind the classic statement uttered at this
pulpit by President McKay&mdash;"No other success can compensate for failure in the home."
</p><p>
It is the rightful heritage of every child to be part of a home in which to grow&mdash;to grow in
love in the family relationship, to grow in appreciation one for another, to grow in
understanding of the things of the world, to grow in knowledge of the things of God.
</p><p>
I was recently handed these statistics taken from the county records of one of our Southwest
communities. In 1964 in this county of which I speak, there were 5807 marriages and 5419
divorces, almost one divorce for every marriage. Can we expect stability out of instability? Is
it any wonder that many of our youth wander in rebellion when they come from homes where
there is no evidence of love, where there is a lack of respect one for another, where there is
no expression of faith? We hear much these days of the Great Society, and I do not disparage
the motives of those who espouse it, but we shall have a great society only as we develop
good people, and the source of good people is good homes.
</p><p>
It was said of old, "Except the Lord build the house, they labour in vain that build it"
(<span class="citation" id="41855"><a href="javascript:void(0)" onclick="sx(this, 41855)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(41855)">Ps. 127:1</a></span>).
</p><p>
Our children deserve such a home in which to grow. I am not speaking of the architecture or
the furnishings. I am speaking of the quality of our family life. I am grateful that we as a
Church have as a basic part of our program the practice of a weekly family home evening. It
is a significant thing that in these busy days thousands of families across the world are
making an earnest effort to consecrate one evening a week to sing together, to instruct one
another in the ways of the Lord, to kneel together in prayer, there to thank the Lord for his
mercies and to invoke his blessings upon our lives, our homes, our labors, our land.
</p><p>
I think we little estimate the vast good that will come of this program. I commend it to our
people, and I commend it to every parent in the land and say that we stand ready to assist you
who may not be of our faith. We shall be happy to send you suggestions and materials on
how to conduct a weekly family home evening, and I do not hesitate to promise you that both
you and your children will become increasingly grateful for the observance of this practice. It
was John who declared: "I have no greater joy than to hear that my children walk in truth"
(<span class="citation" id="5322"><a href="javascript:void(0)" onclick="sx(this, 5322)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(5322)">3 Jn. 1:4</a></span>). This will be your blessing.
</p><p>
And it was Isaiah who said: ". . . all thy children shall be taught of the Lord; and great shall
be the peace of thy children" (<span class="citation" id="25291"><a href="javascript:void(0)" onclick="sx(this, 25291)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(25291)">Isa. 54:13</a></span>).
</p><p>
We cannot afford to disregard the sacred mandate laid upon us to teach our children, first by
the example of our own living, and secondly, by those precepts which, if followed, will bring
peace to their lives. Every child is entitled to the blessing of a good home.
</p><p>
<b>An Education Worth Striving For</b>
</p><p>
I move to the second premise of this charter for youth&mdash;<i>an education worth striving for</i>.
Time will permit little more than a brief mention of a few observations.
</p><p>
Education has become our largest business. On the basis of economics alone, it is larger than
steel, or automobiles, or chemicals. On the basis of its influence upon our society, its impact
is incalculable. Its very size, particularly in our universities, has brought into relief its most
serious problem&mdash;a lack of communication between teacher and student, and a consequent
lack of motivation of those who come to be taught.
</p><p>
A recent article in one of our national magazines contained this statement from a college
teacher: ". . . there has hardly been a time, in my experience, when students needed more
attention and patient listening to by experienced professors than today. The pity is that so
many of us retreat into research, government contracts, and sabbatical travel, leaving counsel
and instruction to junior colleagues and graduate assistants . . . What is needed are fewer
books and articles by college professors and more cooperative search by teacher and taught
for an authority upon which to base freedom and individuality." (J. Glenn Gray, <i>Harper's Magazine</i>, May 1965; p. 59.)
</p><p>
I am aware of the "publish or perish" pressure under which teachers work in some of our
universities, but I should like to say to these teachers that your learned monographs will yield
little satisfaction as the years pass if you discover that while you published, your students
perished.
</p><p>
The great thoughts, the great expressions, the great acts of all time deserve more than cursory
criticism. They deserve a sympathetic and an enthusiastic presentation to youth, who in their
hearts hunger for ideals and long to look at the stars. Nor is it our responsibility as teachers to
destroy the faith of those who come to us, it is our opportunity to recognize and build on that
faith. If God be the author of all truth, as we believe, then there can be no conflict between
true science, true philosophy, and true religion. And, further, as George Santayana has said,
</p><p>
"It is not wisdom to be only wise,<br />
     And on the inward vision close the eyes,<br />
     But it is wisdom to believe the heart."<br /><br />
&nbsp; &nbsp;                    ("O World.")
</p><p>
Your students deserve more than your knowledge. They deserve and hunger for your
inspiration. They want the warm glow of personal relationships. This always has been the
hallmark of a great teacher "who is the student's accomplice in learning rather than his
adversary." This is the education worth striving for and the education worth providing.
</p><p>
<b>A Land To Be Proud of</b>
</p><p>
I move to the next&mdash;<i>a land to be proud of</i>. Congress recently passed a law inflicting heavy
penalties for the willful destruction of draft cards. That destruction was essentially an act of
defiance, but it was most serious as a symptom of a malady that is not likely to be cured by
legislation. Patriotism evidently is gone from the hearts of many of our youth.
</p><p>
Perhaps this condition comes of lack of knowledge, a provincialism that knows nothing else
and scoffs at what little it knows. Perhaps it comes of ingratitude. This attitude is not new.
Joshua, speaking for the Lord, doubtless had in mind this same indifference when he said to a
new generation that had not known the trials of the old: ". . . I have given you a land for
which ye did not labour, and cities which ye built not, and ye dwell in them; of the vineyards
and oliveyards which ye planted not do ye eat" (<span class="citation" id="30065"><a href="javascript:void(0)" onclick="sx(this, 30065)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(30065)">Josh. 24:13</a></span>).
</p><p>
Those who have paid in toil and tears for their inheritance have loved the land on which they
lived. The forebears of many of those assembled in this Tabernacle today walked the long
trail over the prairie and the mountains. In these valleys they grubbed and toiled to wrest a
living from the desert. They came to love that for which they labored, and a great patriotism
filled their souls.
</p><p>
We shall not build love of country by taking away from our youth the principles which made
us strong&mdash;thrift, initiative, self-reliance, and an overriding sense of duty to God and to man.
</p><p>
A terrible price has been paid by those who have gone before us, this that we might have the
blessings of liberty and peace. I stood not long ago at Valley Forge, where George
Washington and his ragged army spent the winter of 1776. As I did so, I thought of a scene
from Maxwell Anderson's play in which Washington looks on a little group of his soldiers,
shoveling the cold earth over a dead comrade, and says grimly, "This liberty will look easy by
and by when nobody dies to get it."
</p><p>
How we need to kindle in the hearts of youth an old-fashioned love of country and a
reverence for the land of their birth. But we shall not do it with tawdry political maneuvering
and enormous handouts for which nothing is given in return.
</p><p>
Love of country is born of nobler stuff&mdash;of the challenge of struggle that makes precious the
prize that's earned.
</p><p>
This is a good land, declared by the Lord in the scripture in which we believe to be ". . . a
land . . . choice above all other lands" (<span class="citation" id="1600"><a href="javascript:void(0)" onclick="sx(this, 1600)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(1600)">1 Ne. 2:20</a></span>),
governed under a constitution framed under the inspiration of the Almighty.
</p><p>
"Breathes there the man, with soul so dead,<br />
     Who never to himself hath said,<br />
     This is my own, my native land!"<br /><br />
&nbsp; &nbsp;     (Sir Walter Scott, from "The Lay of the Last Minstrel," Canto VI, st. 1.)
</p><p>
This is what youth needs&mdash;pride of birth, pride of inheritance, pride in the land of which
each is a part.
</p><p>
<b>A Faith To Live By</b>
</p><p>
And now the fourth premise of my charter&mdash;<i>a faith to live by</i>.
</p><p>
It was said of old that "where there is no vision, the people perish"
(<span class="citation" id="41472"><a href="javascript:void(0)" onclick="sx(this, 41472)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(41472)">Prov. 29:18</a></span>). Vision
of what? Vision concerning the things of God, and a stem and unbending adherence to
divinely pronounced standards. There is evidence aplenty that young people will respond to
the clear call of divine truth, but they are quick to detect and abandon that which has only a
form of godliness but denies the power thereof (<span class="citation" id="5157"><a href="javascript:void(0)" onclick="sx(this, 5157)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(5157)">2 Tim. 3:5</a></span>),
"teaching for doctrines the commandments of men"
(<span class="citation" id="35464"><a href="javascript:void(0)" onclick="sx(this, 35464)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(35464)">Matt. 15:9</a></span>; see
<span class="citation" id="30475"><a href="javascript:void(0)" onclick="sx(this, 30475)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(30475)">JS&mdash;H 1:19</a></span>).
</p><p>
I have sincere respect for my brethren of other faiths, and I know that they are aware of the
great problem they face in a dilution of their teachings as some try to make their doctrine
more generally acceptable. Dr. Robert McAffee Brown, professor of religion at Stanford, was
recently quoted as saying: 
</p><p>
"Much of what is going on at present on the Protestant scene gives the impression of being
willing to jettison whatever is necessary in order to appeal to the modern mentality . . .
</p><p>
"It is not the task of Christians to whittle away their heritage until it is finally palatable to
all." (<i>The Daily Herald</i>, [Provo, Utah], August 12, 1965, p. 13-A.)
</p><p>
To this we might add that what is palatable to all is not likely to be satisfying to any, and
particularly to a generation of searching, questioning, seeking, probing young men and
women.
</p><p>
In all the change about them, they need a constancy of faith in unchanging verities. They need
the testimony of their parents and their teachers, of their preachers and their leaders that God
our Eternal Father lives and rules over the universe; that Jesus is the Christ, his Only Begotten
in the flesh, the Savior of the world, that the heavens are not sealed; that revelation comes to
those appointed of God to receive it; that divine authority is upon the earth.
</p><p>
I know that young men and women will respond to this faith and this challenge. We have
nearly twelve thousand of them today serving across the world as missionaries. Their strength
is a certain faith. Their cause is the cause of Christ, the Prince of peace. Their declaration is a
testimony that God has again spoken from the heavens. Their ministry is in the service of
their fellowmen. Their joy, like that of the Master, is in the soul that repenteth
(<span class="citation" id="14558"><a href="javascript:void(0)" onclick="sx(this, 14558)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(14558)">D&amp;C 18:13</a></span>).
</p><p>
I have been with them in the muddy back streets of Korea and in the crowded roads of Hong
Kong. I have been with them in the towns and cities of America. I have been with them in
the great capitals and the quiet villages of Europe. They are the same everywhere, serving for
two or more years at their own expense in the cause of the Master and of mankind.
</p><p>
I earnestly hope that if there be any among those who are listening this day at whose door a
Mormon missionary may knock, you will welcome him and listen. You will find him to be a
young man with a faith to live by and a conviction to share. You will find him to be a happy
young man, alert and lively, unashamed of the gospel of Jesus Christ (<span class="citation" id="42786"><a href="javascript:void(0)" onclick="sx(this, 42786)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(42786)">Rom. 1:16</a></span>)
and with a capacity to explain the reason for the faith that is within him
(<span class="citation" id="2303"><a href="javascript:void(0)" onclick="sx(this, 2303)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(2303)">1 Pet. 3:15</a></span>).
</p><p>
And as you learn to know him better, you will discover that he likely grew up in a home
where there was love and virtue, patience and prayer; that he was attending school when he
left for his mission and hopes to return to sit at the feet of good counselors and able teachers
and partake of wisdom and knowledge mixed with faith; that with a great inheritance from
forebears who pioneered the wilderness for conscience' sake, he loves the land of which he is
a part, and that he carries in his heart a certain quiet conviction of the living reality of God
and the Lord Jesus Christ and of the assurance that life is eternal and purposeful.
</p><p>
Would that every young man and woman in the land might be blessed to develop and live
under such a charter for youth&mdash;<i>that each might have a home in which to grow, an education</i>
<i>worth striving for, a land to be proud of, a faith to live by</i>.
</p><p>
We their parents, their teachers, their leaders, can help them. God help us so to do that we
may bless their lives and in so doing bless our own, I humbly pray in the name of Jesus
Christ. Amen.
</p>
</div>
</div>
