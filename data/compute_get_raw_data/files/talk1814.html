<?xml version="1.0" encoding="UTF-8"?>
<div class="gcera">
<div class="gchead">
<p class="gctitle">Families and Fences</p>
<p class="gcspeaker">Elder Boyd K. Packer</p>
<p class="gcspkpos">Of the Council of the Twelve</p>
<p class="gcbib">Boyd K. Packer, <i>Conference Report</i>, October 1970, pp. 118-122</p>
</div>
<div class="gcbody">
I come to this pulpit this Sabbath morning with a new obligation, anxious perhaps as never before for the sustaining influence of the Spirit of the Lord, for an interest in your faith and prayers for us here and for those who shall be listening, as I speak to the parents of wayward and lost children.
</p><p>
<b>Missing children</b>
</p><p>
Sometime ago, a father, worried about a serious problem with his son, was heard to remark, "When he leaves and we don't know where he is, there's pain in our hearts, but when he's here there are times when he's a pain in the neck." It's about that pain in the heart that I want to speak. I speak to a very large audience, I fear.
</p><p>
Hardly is there a neighborhood without at least one mother whose last waking, anxious thoughts and prayers are for a son or a daughter wandering who knows where. Nor is there much distance between homes where an anxious father can hardly put in a day's work without being drawn within himself time after time, to wonder, "What have we done wrong? What can we do to get our child back?"
</p><p>
<b>Home under attack</b>
</p><p>
Even parents with the best intentions&mdash;some who have really tried&mdash;now know that heartache. Many parents have tried in every way to protect their children&mdash;only now to find they are losing one. For the home and the family are under attack. Ponder these words, if you will:
</p><p>
<blockquote class="poem">Profanity<br />
Nudity<br />
Immorality<br />
Divorce<br />
Pornography<br />
Addiction<br />
Violence<br />
Perversion</blockquote>
</p><p>
These words have taken on a new meaning in the last few years, haven't they?
</p><p>
You are within walking distance, at least within a few minutes' drive, of a theater in your own neighborhood. There will be shown, within the week, a film open to young and old alike that as recently as ten years ago would have been banned, the film confiscated, and the theater owner placed under indictment. But now it's there, and soon it will be seen at home on your television screens.
</p><p>
<b>Disobedient to parents</b>
</p><p>
The apostle Paul prophesied to Timothy:
</p><p>
"This know also, that in the last days perilous times shall come.
</p><p>
"For men shall be lovers of their own selves, covetous, boasters, proud, blasphemers, disobedient to parents." (<span class="citation" id="5181"><a href="javascript:void(0)" onclick="sx(this, 5181)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(5181)">2 Tim. 3:1-2</a></span>)
</p><p>
There is more to that scripture, but we stop on that phrase "disobedient to parents."
</p><p>
We have no desire to touch the subject that causes you so much pain, nor to condemn you as a failure. But you are failing, and that's what makes it hurt. If failure is to end, one must face squarely problems like this, however much it hurts.
</p><p>
<b>Facing the painful truth</b>
</p><p>
A few years ago I was called in the wee hours of the morning to the side of my ailing mother, who was hospitalized for a series of tests.
</p><p>
"I'm going home," she said. "I'll not continue with these tests. I want you to take me home right now. I won't go through another day of this."
</p><p>
"But mother," I said, "you must go through with this. They have reason to believe that you have cancer, and if it is as they suppose, you have the worst kind."
</p><p>
There! It had been said. After all the evading, all the whispered conversations. After all the care never to say that word when she was around. It was out!
</p><p>
She sat quietly on her bed for a long time and then said, "Well, if that's what it is, that's what it is, and I'll fight it." Her Danish dander was up. And fight it she did, and winner she was.
</p><p>
Some may suppose she lost her battle to that disease, but she came away a glorious, successful winner. Her victory was assured when she faced the painful truth. Her courage began then.
</p><p>
<b>Changes begin with parents</b>
</p><p>
Parents, can we first consider the most painful part of your problem? If you want to reclaim your son or daughter, why don't you leave off trying to alter your child just for a little while and concentrate on yourself. The changes must begin with you, not with your children.
</p><p>
You can't continue to do what you have been doing (even though you thought it was right) and expect to unproduce some behavior in your child, when your conduct was one of the things that produced it.
</p><p>
There! It's been said! After all the evading, all the concern for wayward children. After all the blaming of others, the care to be gentle with parents. It's out!
</p><p>
It's you, not the child, that needs immediate attention.
</p><p>
Now parents, there is substantial help for you if you will accept it. I add with emphasis that the help we propose is not easy, for the measures are equal to the seriousness of your problem. There is no patent medicine to effect an immediate cure.
</p><p>
<b>Cure involves religion</b>
</p><p>
And parents, if you seek for a cure that ignores faith and religious doctrine, you look for a cure where it never will be found. When we talk of religious principles and doctrines and quote scripture, interesting, isn't it, how many don't feel comfortable with talk like that. But when we talk about your problems with your family and offer a solution, then your interest is intense.
</p><p>
Know that you can't talk about one without talking about the other, and expect to solve your problems. Once parents know that there is a God and that we are his children, they can face problems like this and win.
</p><p>
If you are helpless, he is not. If you are lost, he is not.
</p><p>
If you don't know what to do next, he knows.
</p><p>
It would take a miracle, you say? Well, if it takes a miracle, why not.
</p><p>
<b>A course of prevention</b>
</p><p>
We urge you to move first on a course of prevention.
</p><p>
There is a poem entitled "The Fence or the Ambulance." It tells of efforts to provide an ambulance at the bottom of a cliff and concludes with these two verses:
</p><p>
<blockquote class="poem">"Then an old sage remarked: It's a marvel to me<br />
That people give far more attention<br />
To repairing results than to stopping the cause<br />
When they'd much better aim at prevention.<br />
Let us stop at its source all this mischief, cried he,<br />
Come neighbors and friends, let us rally;<br />
If the cliff we will fence, we might almost dispense<br />
With the ambulance down in the valley.</blockquote>
</p><p>
<blockquote class="poem2">"Better guide well the young than reclaim them when old,<br />
For the voice of true wisdom is calling:<br />
'To rescue the fallen is good, but 'tis best<br />
To prevent other people from falling.'<br />
Better close up the source of temptation and crime,<br />
Than deliver from dungeon or galley;<br />
Better put a strong fence round the top of the cliff,<br />
Than an ambulance down in the valley."</blockquote>
		<p class="credit">&nbsp; &nbsp; &mdash;Joseph Malins</p>
</p><p>
We prevent physical disease by immunization. This heart pain you are suffering perhaps might likewise have been prevented with very simple measures at one time. Fortunately the very steps necessary for prevention are the ones that will produce the healing. In other words, prevention is the best cure, even in advanced cases.
</p><p>
Now I would like to show you a very practical and a very powerful place to begin, both to protect your children and, in the case of one you are losing, to redeem him.
</p><p>
<b>"Family Home Evenings"</b>
</p><p>
I have in my hands the publication <i>Family Home Evenings</i>. It is the seventh in a series and is available across the world in 17 languages. If you would go through it with me, you would find that this one is based on the New Testament. The theme is free agency. While it draws lessons from New Testament days, it does not content itself with them back then and there. It leaps across the centuries and concerns itself with you, and here and now.
</p><p>
It is well illustrated, much of it in full color, and has many meaningful activities for families with children of any age.
</p><p>
Here (page 35), for instance, is a crossword puzzle. And here (page 20) on this colorful page is a game. Cut it out and make a spinner of cardboard, and the whole family can play. You'll find yourselves, depending on the moves you make, somewhere between "Heavenly Treasures" and "Earthly Pleasures."
</p><p>
Here is a lesson entitled "How Our Family Came to Be" (page 51). ". . . tell your children," it suggests, "how you met, fell in love, and married. Be sure both parents participate, and illustrate your story with pictures and mementoes you have saved&mdash;the wedding dress, the announcements; wedding pictures. It might be a good idea to tape your narrative and keep it for your children to play to their children some day."
</p><p>
Let me list some of the other titles: "Our Family Government," "Learning to Worship," "Speaking Words of Purity," "Family Finances," "Parenthood, a Sacred Opportunity," "Respect for Authority," "The Value of Humor," "So You're Going to Move," "When the Unexpected Happens," "The Birth and Infancy of the Savior."
</p><p>
<b>"A Call to Be Free"</b>
</p><p>
Here is one entitled "A Call to Be Free." That's the siren call your child is following, you know. This lesson includes a page of very official-looking colored certificates with instructions to "choose for each family member some activity he has not learned to do; then give each member a certificate . . . signed by the father: 'This certificate gives the owner permission to play a tune on the piano as a part of family home evening.'" (Of course, the child has never had piano lessons.)
</p><p>
Other certificates may include (depending on the age of the child) "walking on one's hands, speaking in a foreign language, or painting an oil portrait." Then as each member says he cannot do the thing permitted, talk about why he is not free to do the thing he is permitted to do. The discussion will reveal that "each person must learn the laws that govern the development of an ability and then learn to obey those laws. Thus obedience leads to freedom."
</p><p>
Here, under special helps for families with small children, it suggests they put toy cars on the table top and feel free to run them anywhere they want and in any manner they like. Even little minds can see the results of this.
</p><p>
There is much more to this lesson and to all of these special lessons&mdash;subtle, powerful magnets that help to draw your child closer to the family circle.
</p><p>
<b>Uniform home evening</b>
</p><p>
This program is designed for a family meeting to be held once a week. In the Church, Monday night has been designated and set aside, Churchwide, for families to be at home together. Instruction has recently gone out, from which I quote:
</p><p>
"Those responsible for priesthood and auxiliary programs, including temple activities, youth athletic activities, student activities, etc., should take notice of this decision in order that Monday night will be uniformly observed throughout the Church and the families be left free from Church activities so that they can meet together in the family home evening." (<i>Priesthood Bulletin</i>, September 1970.)
</p><p>
With this program comes the promise from the prophets, the living prophets, that if parents will gather their children about them once a week and teach the gospel, those children in such families will not go astray.
</p><p>
<b>Gospel essential to program</b>
</p><p>
Some of you outside the Church, and unfortunately many within, hope that you could take a manual like this without accepting fully the gospel of Jesus Christ, the responsibilities of Church membership, and the scriptures upon which it is based. You are permitted to do that. (We could even give you a "certificate" to permit you to raise an ideal family.) You still would not be free to do so without obeying the laws. To take a program like this without the gospel would have you act as one who obtained a needle to immunize a child against a fatal disease but rejected the serum to go in it that could save him.
</p><p>
<b>Leadership of family</b>
</p><p>
Parents, it is past time for you to assume spiritual leadership of your family. If there is no substance to your present belief, then have the courage to seek the truth.
</p><p>
There is, living now, the finest generation of youth that ever walked the earth. You have seen some of them serving on missions. Perhaps you have turned them away. You ought to seek them out. If they are nothing else, they are adequate evidence that youth can live in honor. And there are tens of thousands of them who are literal saints&mdash;Latter-day Saints.
</p><p>
<b>Never give up</b>
</p><p>
Now parents, I desire to inspire you with hope. You who have heartache, you must never give up. No matter how dark it gets or no matter how far away or how far down your son or daughter has fallen, you must never give up. Never, never, never.
</p><p>
I desire to inspire you with hope.
</p><p>
<blockquote class="poem">"Soft as the voice of an angel, whispering a message unheard,<br />
Hope with a gentle persuasion whispers her comforting word.<br />
Wait till the darkness is over, wait till the coming of dawn,<br />
Hope for the sunshine tomorrow, after the shower is gone.<br />
Whispering hope, Oh how welcome thy voice . . ."</blockquote>
</p><p>
God bless you heartbroken parents. There is no pain so piercing as that caused by the loss of a child, nor joy so exquisite as the joy at his redemption.
</p><p>
<b>God lives</b>
</p><p>
I come to you now as one of the Twelve, each ordained as a special witness. I affirm to you that I have that witness. I know that God lives, that Jesus is the Christ. I know that though the world "seeth him not, neither knoweth him," that he lives.
</p><p>
Heartbroken parents, lay claim upon his promise: "I will not leave you comfortless; I will come to you." (<span class="citation" id="28605"><a href="javascript:void(0)" onclick="sx(this, 28605)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(28605)">John 14:17-18</a></span>) In the name of Jesus Christ. Amen.
</p>
</div>
</div>
