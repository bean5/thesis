<?xml version="1.0" encoding="UTF-8"?>
<div class="gcera">
<div class="gchead">
<p class="gctitle">To Have Dominion</p>
<p class="gcspeaker">Elder Sterling W. Sill</p>
<p class="gcspkpos">Assistant to the Council of the Twelve Apostles</p>
<p class="gcbib">Sterling W. Sill, <i>Conference Report</i>, October 1963, pp. 77-83</p>
</div>
<div class="gcbody">
<p>
My brethren, I appreciate this privilege of having a part with you in the general priesthood
conference of the Church. I have been greatly stimulated as I am sure you have by the
messages of these fine young men who have talked to us so interestingly about the importance
of controlling our own lives.
</p><p>
One of the most inspiring messages in all sacred scripture is the story of the sixth day of
creation when God made man in his own image
(<span class="citation" id="23686"><a href="javascript:void(0)" onclick="sx(this, 23686)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(23686)">Gen. 1:26-27</a></span>). He also endowed him with a set of his own
attributes. Then, as the very climax of creation, God gave man dominion over everything
upon the earth (<span class="citation" id="23687"><a href="javascript:void(0)" onclick="sx(this, 23687)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(23687)">Gen. 1:28</a></span>),
including himself. The dictionary says that "dominion" means
control or the power to govern. The most important part of the dominion given to man was
self-dominion. In all of creation, it was only to man that God said, ". . . thou mayest choose
for thyself" (<span class="citation" id="39177"><a href="javascript:void(0)" onclick="sx(this, 39177)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(39177)">Moses 3:17</a></span>).
</p><p>
On one occasion Joseph Smith was asked to explain the unusual harmony existing among his
large group of church members, though they differed so greatly in background, nationality,
and experience. The Prophet replied, "I teach the people correct principles and they govern
themselves." (Cited by John Taylor, <i>JD</i> 10:57-58.)
</p><p>
One of the most important parts of real religion is to qualify ourselves to govern our own
lives effectively and righteously. Someone has said, "He that would move the world, must
first move himself." We talk a great deal about the fact that we have been given the
priesthood. The priesthood is the authority to act in the name of the Lord. But by itself that is
not enough. We must also develop the "ability" to act in the name of the Lord. The authority
can never be of very great consequence without the ability. That is, how much benefit would
be derived from having the authority to make converts without the ability to make converts.
</p><p>
The most inspiring thing about the life of Jesus was not his ability to quiet the storm or
control the tempest, but his absolute control of himself. The Master did not need to make a
single mistake in order to find out that it was wrong. We have developed a fairly good control
over some of our body members; for example, I have great authority over my finger. If I tell
it to bend, it bends. If I tell it to unbend, it unbends. If I give my feet an order, they obey
immediately, and we will have succeeded in our religious responsibility when we get that
same kind of control over our thoughts, our emotions, our tongues, our industry, our faith,
and our desire to serve God. Some of us have mistrained our appetites to a point where we
tend to "think" with our stomachs; that is, our appetites frequently have more influence in
directing our lives than our reason or even the commandments of God. This same misuse of
our powers frequently gives our fears, our doubts, our prejudices, our hates, and our sex
impulses the control of our lives. Before we can be successful in our God-given dominion,
our emotions must be brought under the direction of the spirit.
</p><p>
St. Augustine said, "Wouldst thou have thy flesh obey thy spirit? Then have thy spirit obey
thy God. Thou must be governed, if thou wouldst govern." And only when we properly
govern ourselves according to what is right, can we escape the destructive rule of our moods
and appetites.
</p><p>
Sir Walter Raleigh said, "A man must first govern himself, ere he be fit to govern a family;
and his family, ere he is fit to bear the government in the commonwealth."
</p><p>
Each of us has been given a magnificent instrument called a brain, which was intended to
play a much more prominent part in our religious life than it sometimes does. The brain, not
the feelings or the passions, was designated by God to be the presiding officer of the
personality. And when we honor the authority of the mind, we become masters instead of
slaves.
</p><p>
A. Bertha Kleinman has written the following verse about self-mastery.
</p><p>
&nbsp; &nbsp; &nbsp; &nbsp;         SELF-MASTERY
</p><p>
"What tho I conquer my enemies,<br />
       And lay up store and pelf,<br />
    I am a conqueror poor indeed,<br />
       Till I subdue myself.
</p><p>
"What tho I read and learn by heart<br />
       Whole books while I am young,<br />
    I am a linguist in disgrace,<br />
       Who cannot guard my tongue.
</p><p>
"What tho on campus I excel<br />
       A champ in meet and fight<br />
    If trained efficient still I can't<br />
       Control an appetite.
</p><p>
"What tho exemptions write my name<br />
       High on the honor roll<br />
    Electives, solids fail me if<br />
       I learn no self-control.
</p><p>
"And tho I graduate and soar<br />
       And life is good to me,<br />
    My heart shall write me failure till<br />
       I learn self-mastery."
</p><p>
Our human nature is made up of an interesting duality, which Jesus referred to as the spirit
and the flesh, and most of us permit a constant conflict to rage between the two. Plato refers
to this duality as an upper soul and a lower soul. He describes the lower soul as the dwelling
place of weakness, sin, and appetite, whereas the upper soul is the residence of the intellect; it
is the headquarters of reason and the operational base of judgment and righteousness. On this
battle ground the fate of each of us is being decided daily. Each individual is tending toward
his natural status of king or slave. As we overcome the unworthy elements within ourselves,
we become masters, capable of ruling our lives in wisdom with righteous power. As we
surrender to our appetites, we become slaves. The alcoholic, the immoral, the dishonest, the
profane, and the idle are losing the battle to the lower soul by allowing themselves too many
lower soul experiences.
</p><p>
The one business of life is to succeed, and one of our greatest Christian duties is to organize
and supervise ourselves for righteous accomplishment. We must be more successful in
disciplining the mind and training the will. Someone has pointed out that "planning" is the
place where man shows himself most like God. Who could be more Godlike than one who
intelligently plans his own life? He is the one who blueprints accomplishment and builds the
roadway of success. The highest paid man in the army is the general. He is the one who
"thinks" and "plans" for the army. But each of us is the general of his own life, and each is
also his own soldier. As generals, our job is to work out a better program for ourselves as
soldiers, and the more skilful we are as generals, the more successful we will be as soldiers.
</p><p>
Sometime ago I spent a few hours with a group of missionaries. We were discussing
missionary work under the two great headings of the "message" and the "messenger." We are
halfway to success when we understand the tremendous importance of the message that the
gospel of Jesus Christ has again been restored to the earth with the authority to officiate in all
of the principles and ordinances of the gospel having to do with the celestial kingdom. But no
great message is ever delivered without a great messenger. Inasmuch as the professional
approach to any accomplishment is first to isolate the problem, I said to the missionaries,
"Before I can be of much help to you, I need to know what your problems are. Will each of
you tell me in one word why you're not ten times as effective as you are?"
</p><p>
As the answers were given, we wrote them on the blackboard. However, when we analyzed
them we found that every single one of them had to do with the "messenger," none of them
was about the message. I said to them, "I'm going back to church headquarters in the morning,
and I would like to be able to report what's wrong with the message." But no one had any
complaint with the message. Their only problems involved changing the messenger.
</p><p>
One missionary said, "I can't be a good missionary because I am not friendly."
</p><p>
I said, "What do you mean?"
</p><p>
He said, "Well, my companion loves everyone, and everyone loves him. Our contacts all
gather around him, but because I am not that kind of a person I am left by myself."
</p><p>
I said, "Would you show me what you mean by going down this aisle and shaking hands with
these people the way you ordinarily do it?"
</p><p>
In complying he did his usual unimpressive job. Then I said to him, "Now, will you go down
this other aisle and shake hands with these other people the way your companion does it?"
</p><p>
Then he squared his shoulders, got a little different look in his eye and a little different
tension in his muscles as he tried to demonstrate to me how his companion did it. He seemed
to be an immediate success while following the example of his companion. I told him about
the famous "<i>As If</i>" principle of William James. Mr. James said if you want to have a quality
act "<i>As If</i>" you already had it. If you want to be friendly, act "<i>As If</i>" you are already friendly.
How long does it take one to learn to be friendly? It takes just one-quarter of
a second, just long enough to make up your mind to practice the "<i>As If</i>" principle. If you
want to be brave, act "<i>As If</i>" you were already brave, don't go around telling everyone how
scared and weak you are. It is the axiom of the theater that each actor should live his part.
</p><p>
On one occasion Theodore Roosevelt was decorating one of his generals for bravery. He said,
"This is the bravest man that I have ever seen." He said, "He walked right behind me all the
way up San Juan Hill." Theodore Roosevelt was a sickly child. He began life as a weakling,
not expected to live; but he trained himself to think courage, strength, health, and vitality, and
that is what he got. One of the things that frightens me most as I go about a little bit is to
hear so many people talking weakness, failure, and sin. The most widespread disease in the
world is the inferiority complex. And when we think inferiority, that is what we get. Another
missionary described his problem by saying, "I can't concentrate." I said, "What do you plan
doing about it?" He said, "There's nothing I can do; I just can't concentrate." One of our most
unfortunate weaknesses is that we sometimes think we are under sentence to remain forever as
we presently are. Yet one of the most exciting ideas in life is the possibility of changing
ourselves for the better.
</p><p>
William James said, "The greatest discovery of my generation is that we can change our
circumstances by changing our attitudes of mind." A lot of people want to change their
circumstances, but few are willing to change themselves. It is very interesting, however, that
the problem that these missionaries seemed to have the most difficulty with, was that of
getting the beds off their backs in the morning. I brought away a mental picture timed at 6:00
am showing the missionaries pulling and struggling trying to get up, with the mattress being
successful in holding them down. Isn't it ridiculous that we sometimes live through an entire
lifetime and never learn to get up in the morning? The Church is now 133 years old, and
some of us have barely made a start in living the "message" because we have exhausted our
strength struggling with the messenger. So far as I know almost every problem that holds us
back involves a misuse of this God-given dominion. Certainly we need a better mastery of the
message, but we also have a lot more work to do on the messenger.
</p><p>
Solomon said, "With all thy getting get wisdom" (see
(<span class="citation" id="41446"><a href="javascript:void(0)" onclick="sx(this, 41446)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(41446)">Prov. 4:7</a></span>). And then someone that
must have been much wiser than Solomon said, "With all thy getting, get going." George
Bernard Shaw touched our problem when he said that the primary occupation of life is taking
a mob of appetites, and organizing them into an army of purposes and ambitions.
</p><p>
It is a very significant point of view that every human being has been given two creators. One
is God, and the other is himself. That is, the creation of man is not something that was
finished and done with in the Garden of Eden. The creation of man is still going on. It is
taking place today, and it took place last week, and it will take place next month, and you are
the creator. That is, you are currently creating the enthusiasms and the industry and the
courage and the faith that will determine what your lives will be throughout eternity.
Someone has asked this interesting question: "How would you like to create your own mind?"
But isn't that exactly what everyone does?
</p><p>
William James said, "The mind is made up by what it feeds upon." The mind becomes what
God intended it should be, only when it is fed on enough upper soul experiences. It has been
said that "the mind like the dyer's hand, is colored by what it holds." That is, if I hold in my
hand a sponge full of purple dye, my hand becomes purple, and if I hold in my mind and
heart great ideas of honor, righteousness, industry, and the love of truth, my whole personality
is colored accordingly. And our self-dominion is made more effective when we make love to
the right kind of ideas, and refuse all lower soul experiences. While Cain was training himself
to ". . . love Satan more than God"
(<span class="citation" id="39181"><a href="javascript:void(0)" onclick="sx(this, 39181)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(39181)">Moses 5:18</a></span>),
he was giving dominion to his lower
soul. This is a process that many frequently follow. Sometime ago a young
man discussed with me an improper marriage which he was contemplating. I asked him why.
He said he was in love. But love alone is an insufficient basis for marriage. Anyone can fall
in love with anything. Many people have fallen in love with idleness, profanity, adultery, and
drunkenness. Cain fell in love with Satan.
</p><p>
A chain smoker was recently ordered by his doctor to give up smoking. He had fallen in love
with cigarettes, and he felt very sorry for himself that he was now being forced to give up his
bad habit. He said, "What good could it possibly do me to quit smoking when I have to stand
over myself like a policeman with a club, ordering myself to do something that I don't want
to do?" It is pretty difficult to force ourselves to be decent or successful while we are in love
with sin and failure.
</p><p>
There is a lower soul psychology that says that the way to develop the personality is to give
expression to our desires. It says that parents should not say "no" to their children because of
the possibility of dwarfing their personalities. If a child feels like slamming the door, he
should slam it. If he feels like sowing some wild oats, he should sow them. It says that
desires should be expressed, otherwise the child's growth may be inhibited and his personality
distorted. This philosophy has made a great contribution to our upsurge in juvenile and adult
delinquency. We might bring some upper soul philosophy to bear on this point by a review of
the Ten Commandments. Yet, a prominent minister recently said that the Ten Commandments
should no longer be used as the basis for religious training. He said that the Ten
Commandments gave young people the idea that the church was a wet blanket. He said the
dictatorial "Thou shalt nots" were not in good taste any more. He said, "In my church I no
longer refer to the Ten Commandments." He didn't say whether or not he thought chastity,
honesty, and worship should be done away with, or whether God had changed his mind about
these values, but only that he had convinced himself that the Ten Commandments were
outmoded and no longer useful.
</p><p>
Another religious leader said that the stern command "Thou shalt not" was much too harsh for
our present-day sensitivity, and he suggested that the form of the commandments should be
modified and some softer word such as "advise" or "suggest" or "recommend" be used. We
make one of our most serious mistakes when we become too soft to accept truth unless it is
highly sugar coated. We settle too many of our problems by compromise, or how we feel,
rather than by what is right. Frequently we would rather be ruined by praise than sawed by
criticism. It is pretty serious business when we turn our backs on good merely because we
don't like someone's tone of voice or because what is said doesn't quite suit our fancy.
</p><p>
The story is told of a father and a son riding down the highway. The son was explaining to
the father what he didn't like about the Ten Commandments. He said they were negative and
besides that he didn't like anyone telling him what not to do. Soon they came to an
intersection in the highway. There was one signboard telling where the left-hand road led, and
another signboard telling where the right-hand road led. The father took the wrong road. This
greatly disturbed the son. He couldn't understand how the father could make such a ridiculous
mistake. The father admitted that he had read the signboard, but he said, "I just don't want
any signboard telling me where to go."
</p><p>
For our benefit God has erected some signboards of right and wrong, and we when are
headed toward destruction the sign is flashed saying, "Thou shalt not." What we do from there
on, however, is strictly up to us. Sometime ago I read one of Lincoln's anti-slavery debates.
Lincoln's opponent had said, "You can't afford to free the southern slaves, because there are
some four million of them. Each has a value to his owner of approximately $1,000. That is, if
you free the slaves you will upset the economy of this little group of slave owners by some
four billion dollars which they can't afford, but in addition who will take care of the
corn, the cotton, and the tobacco crops."
</p><p>
When Lincoln came to the platform, he brushed all of these considerations aside as
immaterial. He said, "There is only one question that we need to answer about slavery, and
that is this: Is slavery right or is it wrong? Is it right for some men to hold other men in
bondage?" Now I hope that sometime when you have a problem that is causing you difficulty,
you will remember Lincoln's formula of right and wrong.
</p><p>
Sometime ago it was reported that an engineer was discharged from his employment. He
asked his employer for the reason. The employer said, "You allowed us to make a wrong
decision which cost us considerable money." The engineer said, "But certainly you remember
that I advised you against making that decision." The employer said, "Yes, I know that you
did, but you didn't pound the table when you did it."
</p><p>
The Lord didn't make that mistake when he gave the Ten Commandments. He pounded the
table and tried to make the occasion as memorable as possible, and he expects us to be
equally forceful in carrying out his instructions. I would like to read to you a description of
the setting in which the Ten Commandments were given. It gives us an atmosphere for
shaping our own dominion.
</p><p>
The scripture says, "And it came to pass on the third day in the morning, that there were
thunders and lightnings, and a thick cloud upon the mount, and the voice of a trumpet
exceedingly loud; so that all the people that was in the camp trembled.
</p><p>
"And Moses brought forth the people out of the camp to meet with God; and they stood at the
nether part of the mount.
</p><p>
"And mount Sinai was altogether on a smoke, because the Lord descended upon it in fire: and
the smoke thereof ascended as the smoke of a furnace, and the whole mount quaked greatly"
(<span class="citation" id="22710"><a href="javascript:void(0)" onclick="sx(this, 22710)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(22710)">Ex. 19:16-18</a></span>).
I seriously doubt that the Lord has changed his mind since then.
</p><p>
God himself cannot look upon sin with the least degree of allowance
(<span class="citation" id="14237"><a href="javascript:void(0)" onclick="sx(this, 14237)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(14237)">D&amp;C 1:31</a></span>). He permits none of it in
his presence. But he has said, ". . . nevertheless thou mayest choose for thyself"
(<span class="citation" id="39178"><a href="javascript:void(0)" onclick="sx(this, 39178)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(39178)">Moses 3:17</a></span>).
He has given us dominion that we might develop our own lives. Aristotle once told
Alexander the Great that the most dangerous enemy that ever confronted an army was never
in the ranks of the foe, but always in your own camp. And that is a good thing for us to
remember. Suppose we ask ourselves who is the greatest enemy of America? It isn't Russia or
China or Cuba; that is ridiculous. Who causes our strikes and brings about our racial strife?
Who robs our banks and causes our many kinds of delinquency? Who is it that makes our
political blunders, gives us a bad name abroad, and causes our weaknesses at home? Or who
is responsible for our individual sins and keeps us ignorant, lethargic, and unsuccessful?
</p><p>
The Lord suggested the answer, when on September 22, 1832, he gave a great revelation in
which he said in part, "And now I give unto you a commandment to beware concerning
yourselves" (<span class="citation" id="14304"><a href="javascript:void(0)" onclick="sx(this, 14304)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(14304)">D&amp;C 84:43</a></span>).
Our own signs say, "Beware of the dog" or "Beware of the
train" or "Beware of the communists," but the Lord gets nearer to our problem when he says
". . . beware concerning yourselves. . . ." The chief characteristic of sin, and the chief
characteristic of lack of success is our failure to manage our thoughts, our attitudes, and our
ambitions. Pythagoras said, "No man is free who cannot command himself." And we might
add that no man is capable of making the most and the best of his life who cannot command
himself. We will have happiness in our homes, success in our work, righteousness in our
personal lives, and eternal life in God's presence, only as we learn self-mastery and develop
the will-power to put it in force. It is the responsibility of the priesthood to prepare the way
before the glorious second coming of Christ. It is our personal responsibility to prepare our
families and our individual lives for celestial glory, and we will fail or succeed in exact
proportion as we get dominion over our own lives. The Lord has said, ". . . let virtue garnish
thy thoughts unceasingly; . . .
</p><p>
"The Holy Ghost shall he thy constant companion, . . . and thy dominion
shall be an everlasting dominion, and without compulsory means it shall flow unto thee
forever and ever" (<span class="citation" id="14344"><a href="javascript:void(0)" onclick="sx(this, 14344)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(14344)">D&amp;C 121:45-46</a></span>).
</p><p>
Carl Erskine, the great former Dodger baseball pitcher, once said, "I never pray to win, I just
pray to be in my best form." What a thrilling accomplishment if every bearer of the
priesthood was always in his best form; for even one man can, if he will, change the morale
of a whole community. Edward Everett Hale once said,
</p><p>
"I am only one,<br />
    But still I am one.<br />
    I cannot do everything,<br />
    But still I can do something;<br />
    And because I cannot do everything<br />
    I will not refuse to do the something that I can do."
</p><p>
My brethren in the priesthood, that the Lord will help us to get dominion over our lives, is
my prayer which I ask in the name of Jesus Christ. Amen.
</p>
</div>
</div>
