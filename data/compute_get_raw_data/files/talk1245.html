<?xml version="1.0" encoding="UTF-8"?>
<div class="gcera">
<div class="gchead">
<p class="gctitle">"...To Forgive Is Divine"</p>
<p class="gcspeaker">Elder Henry D. Taylor</p>
<p class="gcspkpos">Assistant to the Council of the Twelve Apostles</p>
<p class="gcbib">Henry D. Taylor, <i>Conference Report</i>, October 1962, pp. 109-111</p>
</div>
<div class="gcbody">
<p>
One of the most beautiful principles of the gospel is that of repentance. It holds out hope and
encouragement to each of us, the descendants of Adam and Eve. Because we are mortal and
live in a world where temptations abound, it is not difficult to make mistakes and commit
errors. Through the atonement effected by Jesus Christ, our Savior, we are assured that our
errors and mistakes may be rectified by displaying Godly sorrow and
abandonment of unrighteous ways.
</p><p>
One of the most vital qualities of the principle of repentance is forgiveness. Unless each of us
can learn to forgive others for real or imagined trespasses against us, we cannot properly
repent. Someone has said, "Humanity is never so beautiful as when praying for forgiveness, or
else forgiving another." The Savior's teachings to us are replete with admonitions always to
be ready and willing to forgive. In instructing his disciples to pray, he suggested they petition
the Father: "Forgive us our debts, as we forgive our debtors."
</p><p>
Then he counseled: "For if ye forgive men their trespasses, your heavenly Father will also
forgive you: . . ." And he added a word of caution, "But if ye forgive not men their
trespasses, neither will your Father forgive your trespasses"
(<span class="citation" id="35164"><a href="javascript:void(0)" onclick="sx(this, 35164)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(35164)">Matt. 6:12,14-15</a></span>).
</p><p>
At another time the Lord instructed: "Wherefore, I say unto you, that ye ought to forgive one
another; for he that forgiveth not his brother his trespasses standeth condemned before the
Lord; for there remaineth in him the greater sin.
</p><p>
"I, the Lord, will forgive whom I will forgive but of you it is required to forgive all men"
(<span class="citation" id="14143"><a href="javascript:void(0)" onclick="sx(this, 14143)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(14143)">D&amp;C 64:9-10</a></span>).
</p><p>
One cannot hold grudges and unkind feelings without harming himself. He becomes bitter; his
vision is distorted; and his soul becomes cankered. Harsh and sharp words can leave a sting
behind, a pang of unhappiness and regret in the heart and conscience of the offender. "Little,
vicious minds abound with anger and revenge and are incapable of feeling the pleasure of
forgiving their enemies," said a wise man.
</p><p>
A person holding grudges hurts himself more than he does his enemy. A prophet has wisely
counseled: ". . . let not the sun go down on your wrath"
(<span class="citation" id="21741"><a href="javascript:void(0)" onclick="sx(this, 21741)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(21741)">Eph. 4:26</a></span>).
</p><p>
To forgive a person once or twice may not be too difficult, but to continue to forgive many
times when one has been wronged may become a real test of character. Upon one occasion
when Jesus was teaching his disciples, Peter approached him and posed this question: ". . .
Lord, how oft shall my brother sin against me, and I forgive him? till seven times?
</p><p>
"Jesus saith unto him; I say not unto thee, Until seven times: but, Until seventy times seven"
(<span class="citation" id="35188"><a href="javascript:void(0)" onclick="sx(this, 35188)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(35188)">Matt. 18:21-22</a></span>).
By this declaration we may be assured that the Savior meant that we should
forgive without number or limit.
</p><p>
The poet, Alexander Pope, has written: "Good nature and good sense must ever join; to err is
human; to forgive divine."
</p><p>
The Lord has always looked upon sin with emphatic disfavor and has exclaimed, "For I the
Lord cannot look upon sin with the least degree of allowance;" then he continues:
"Nevertheless, he that repents and does the commandments of the Lord shall be forgiven"
(<span class="citation" id="14071"><a href="javascript:void(0)" onclick="sx(this, 14071)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(14071)">D&amp;C 1:31-32</a></span>).
And while the Lord regards sin with disfavor, nevertheless, he always
displays a spirit of charity and kindness for the sinner.
</p><p>
When a woman taken in sin was brought before Jesus, he faced her accusers with the
challenge: "He that is without sin among you, let him first cast a stone at her." Being smitten
by their consciences, one by one those guilty hypocrites slunk away, and when Jesus raised
his head he asked the woman: "Woman, where are those, thine accusers? hath no man
condemned thee?" She replied, "No man, Lord." And Jesus said unto her, "Neither do I
condemn thee; go, and sin no more"
(<span class="citation" id="28013"><a href="javascript:void(0)" onclick="sx(this, 28013)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(28013)">John 8:7,10-11</a></span>).
</p><p>
True forgiveness cannot be partial nor halfhearted. It must be wholehearted, genuine, and
without reservation. "The narrow soul knows not the godlike glory of forgiving," one has said.
</p><p>
I like the story of total and complete forgiveness related by the warden of a western prison. A
friend of his happened to be sitting in a railroad coach next to a young man who was
obviously depressed. Finally, the young man revealed that he was a convict returning from a
distant prison. His imprisonment had brought shame on his family, and they had neither visited
him nor written often. He hoped, however, that this was only because they were too poor to
travel, too uneducated to write. He also hoped, despite the evidence, that they had forgiven
him.
</p><p>
To make it easy for them, however, he had written them to put up a signal for him when the
train passed their little farm on the outskirts of town. If his family had forgiven him, they
were to put up a white ribbon in the big apple tree near the tracks. If they did not want him
back, they were to do nothing, and he would stay on the train, go west and probably become
a hobo. As the train neared his home his suspense became so great, he could not bear to look
out of the window. His companion changed places with him and said he would watch for the
apple tree. In a minute, he put his hand on the young convict's arm. "There it is," he
whispered, his eyes bright with sudden tears. "It's all right. The whole tree is white with
ribbons" (<i>Reader's Digest</i>, March 1961).
</p><p>
The most magnificent lesson ever taught respecting forgiveness was given by the Savior. Jesus
was subjected by his enemies to what is considered to be the cruelest and most horrible form
of death. Crucifixion is excruciatingly painful, with the victim lingering on in increasing
agony and torture for hours or even days. Yet, in spite of the humiliation and intense pain he
suffered on Calvary's cross, Jesus, with compassion, in Godlike mercy, prayed for his
tormentors, "Father, forgive them; for they know not what they do"
(<span class="citation" id="31573"><a href="javascript:void(0)" onclick="sx(this, 31573)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(31573)">Luke 23:34</a></span>).
</p><p>
Now, with the Savior's great lesson fresh in our memories, may each one of us purge from
our hearts any feeling of hatred, envy, or bitterness, so that we may with a clear conscience
and utmost confidence approach our Heavenly Father and ask for forgiveness of our
shortcomings and mistakes. For which I humbly pray, and bear you my testimony that I know
that the gospel is true, in the name of Jesus Christ. Amen.
</p>
</div>
</div>
