<?xml version="1.0" encoding="UTF-8"?>
<div class="gcera">
<div class="gchead">
<p class="gctitle">Our Work&mdash;Missionary Service to the World</p>
<p class="gcspeaker">Elder Stephen L Richards</p>
<p class="gcspkpos">Of the Council of the Twelve Apostles</p>
<p class="gcbib">Stephen L Richards, <i>Conference Report</i>, October 1945, pp. 52-57</p>
</div>
<div class="gcbody">
<p>
We stand on the threshold of a new day. We look out into the morning and see the rays of
the rising sun tint the sky with the hopes of humanity. We see some clouds also, harbingers of
storm, but the forecast is "generally fair for a season." So we go back to our work&mdash;back
where the black night of war overtook us nearly a half-dozen years ago. It has been a long
night and our work has been retarded, but good sentinels have kept the watches and
safeguarded our establishments. Now in the daylight of peace, we go back to our work.
</p><p>
THE MISSION OF THE CHURCH
</p><p>
We know our work. It is laid out for us by the Master Builder. We have full and complete
plans and specifications, and we have, in good measure, too, the tools and equipment. Perhaps
we could use a little more modern equipment, and the tools may need reconditioning and
polishing, but a sufficiency is available, and we can begin our work again.
</p><p>
The work is not new to us. We, and our predecessors, have carried it forward for more than a
hundred years. It was the first enterprise undertaken by those of sacred memory who initiated
the lofty cause to which we give our allegiance. So soon as the first revelation of the latter
days came to them, they lost no time in carrying the message to neighbors and adjacent
communities. When the Church was organized, they accepted most literally the revelation that
its mission should be to preach the gospel ". . . unto every nation, and kindred, and tongue,
and people" (<span class="citation" id="12207"><a href="javascript:void(0)" onclick="sx(this, 12207)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(12207)">D&amp;C 133:37</a></span>).
That was their work. In their poverty and weakness they
accepted it with such boldness and enthusiasm, fortitude and sacrifice, as history has seldom
recorded.
</p><p>
Their faith and confidence were marvelous. They trusted God, and they did not trust in vain.
They knew that he had said that "The weak things of the world shall come forth and break
down the mighty and strong ones" (<span class="citation" id="12069"><a href="javascript:void(0)" onclick="sx(this, 12069)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(12069)">D&amp;C 1:19</a></span>),
and that ". . . the fulness of my gospel
might be proclaimed by the weak and the simple unto the ends of the world, and before kings
and rulers" (<span class="citation" id="12070"><a href="javascript:void(0)" onclick="sx(this, 12070)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(12070)">D&amp;C 1:23</a></span>).
With this assurance our forebears went forth. They assumed their
obligation, and it superseded everything else. Families were left without a competence,
ofttimes in the care of relatives and neighbors and friends. Businesses were sacrificed. Such
accumulations as they had were expended for the cause. If I were asked to name the
outstanding, distinctive, organized accomplishment of the restored Church of Christ in the last
century I would without hesitation set forth its phenomenal missionary labors. Nothing more
truly characterizes the altruism of the gospel that it teaches; nothing more deeply signifies the
devotion and sincerity of its members.
</p><p>
The enormous cost of the service has been widely distributed, shared by nearly every family
in the Church. Many families have sent forth more than one missionary, and not infrequently
has a home kept one or more missionaries in the field continuously for ten or a dozen years,
sometimes for a quarter of a century. I know of no way of securing comparable data from
other religious bodies, but I venture the assertion that no other church at any period in history
for a century of time has ever given to missionary service such a proportion of its
membership and its available resources.
</p><p>
MISSIONARIES AND CONVERTS
</p><p>
For a hundred years there were two armies constantly on the march, an army of missionaries
outbound from Zion carrying the banner of gospel peace and liberty, and an inbound army of
free and happy people, faith and hope shining in every face, seeking the shelter, the
inspiration, and the glorious opportunities of a divinely appointed society. Contingents of
these armies have passed on almost every highway of the world&mdash;in the States&mdash;from the
Americas north and south, up and down the devious waters of the Mississippi where
missionaries of the early days like Brigham and Heber, and Willard and Parley, and Erastus
went on flatboats to embark on slow sailing vessels on their long tedious voyages to their
fields in Britain, Scandinavia, and the continent; across Europe to the land of the Arab and
the Turk and on into far-off India, over the broad expanse of the Pacific to and from Hawaii
and the distant isles of the South Seas. As the missionaries have passed the immigrants in
these great counter-marches of the century which has gone, whether in their ships at sea or as
they paused to clasp hands in their weary trek across the prairies, one can fancy their
salutations, not always spoken perhaps but ever in their hearts, the missionaries say, "We go
to carry the gospel." "Thank God we have it," the convert replies, and then adds, "we will
follow you later." So indeed they have in one heroic round; missionary to convert, then
convert to missionary. Great has been their gift; generously have they given.
</p><p>
What has been given? Why, to every man what he needed. To the poor, they who are so
many, the gospel of thrift; to the rich, who are so few, the gospel of giving; to the
intemperate, the gospel of self-control; to the indolent, the gospel of work; to the militant, the
gospel of peace; to the downcast, the gospel of hope; to the ignorant, freedom from
superstition; to the cynical and the wavering, a satisfying philosophy; to the sinner, the gospel
of repentance; and to all&mdash;faith, security, idealism, happiness, and exaltation.
</p><p>
Is it difficult then to discover the urge which has made possible this remarkable missionary
achievement? I think it is not. Such gifts, such faith, such vital endowments are highly
esteemed by man. They enrich his life. They enlarge his heart and fill him with gratitude. He
thanks God and seeks to express his gratitude in terms of devotion and service. He sees no
service comparable to that of giving to others the boon that he enjoys. So he goes forth, not
grudgingly, not merely out of a painful sense of duty, but cheerfully, eagerly to requite the
supreme blessing of his life and derive new and surpassing joy in the sharing of his joy.
</p><p>
BLESSINGS IN MISSIONARY SERVICE
</p><p>
I thank the Lord that the ardor for the service has not dulled with the passing of time. On
every hand I see evidences which convince me that the members of the Church love to
proclaim the gospel. Men who cannot go themselves send their sons and daughters. Widows
toil and scrimp to keep a missionary. Girls work to provide the necessary expense for their
brothers, young husbands, and for themselves. Quorums, wards, and societies contribute, and
occasionally a rich man opens up a generous heart and maintains a half dozen in the mission
field.
</p><p>
Those who go are blessed, and the homes and communities which send them also. Crude
country boys from the farm and the range have been exposed to the education and culture of
extensive travel and metropolitan life in great cities. Young men from the cities have been
subjected to the rigors of the most primitive, rural life. The knowledge, the tolerance, the
adventure, the polish, and the experience which worldwide travel brings have been, during the
whole history of the Church, the product of our missionary system. I feel sure that in no other
communities on the earth is the percentage of those who have "seen the world" so large as in
the villages, towns, and cities of the Latter-day Saints.
</p><p>
Such benefits, however, while important, are but incidental. The more vital results are deeper
than enlarged information and polish. The fundamental character of our manhood and
womanhood has been improved. Sacrifice has taught self-control. Giving has made for
generosity as it always does. Teaching the virtues has brought them into application, and high
spirituality has ingrained testimony and soul development. The general uplift in all standards
of living which the Church has brought to its adherents is in no small measure directly
attributable to its missionary system. How it has blessed the home! Fathers who have paid and
prayed; mothers, wives, sisters, and sweethearts who have been anxious and worried and true.
Little tots whose first lisped prayers have been, "Please, God, keep our missionary," have
made the home a sanctuary, indeed, the foundation of our religious life.
</p><p>
SIMPLICITY OF THE GOSPEL MESSAGE
</p><p>
This remarkable missionary work has been accomplished by humble men and women. Their
equipment in the main has not been the training of schools. It has been the influence and
discipline of good homes, Church organization, and individual testimony. Their testimonies
and their lives have been more potential than their preaching. The only eloquence they have
required to deliver their message is the eloquence of the message itself portrayed in the
devotion and purity of their lives. They have never had to rant and yell, nor chant and sigh, to
make a convert. They have carried the natural simple joyous message of the Christ in a
natural, cheerful way. Was that not the Savior's way? Did he not ever suit the lesson to the
people in their language and understanding? Have we any evidence that he employed rituals,
ministerial garb, and sonorous phrases to make it impressive? I think we have not, and I
advance as a worthy argument for the divine authenticity of the gospel we bear, the manner of
its presentation by the missionaries of the Church.
</p><p>
What these ambassadors of the Lord have done for individuals, families, communities, and
nations would fill books. A million hearts swell today in gratitude for their blessed service. A
man contemplates his home, the loving family which surrounds him, his prosperous business,
the esteem of his fellow men, the fraternity of his brethren in the priesthood, his faith, his
contentment, his glorious hopes and from the depths of his soul he cries, "God bless the
missionary who brought me this."
</p><p>
HOPEFUL OUTLOOK FOR THE SPREAD OF TRUTH
</p><p>
So this is our work&mdash;to spread the restored gospel of righteousness and peace throughout the
world. I think I do not need to make a case for the need of it. It seems to me that experiences
of the last few years and of the present hour are sufficient to convince every observant,
thoughtful person of that need.
</p><p>
Here then, in the respite from the ravages of war, is a new day for the proclamation of the
word of God. New and more extended opportunities are forthcoming. New methods of
transportation and communication are available, and I can but think that hundreds and
thousands of our gallant boys who have contributed so much to the liberation of the oppressed
peoples of the world will find a kindlier reception than our missionaries have ever heretofore
enjoyed.
</p><p>
Will you, my brethren and sisters in the Church of Christ, accept the challenge of this new
day? Will you set your houses in order, temporally and spiritually, and send forth
ambassadors of truth, good will, and peace to a destitute world, whose need for bread is great
but whose need for the "bread of life" (<span class="citation" id="27188"><a href="javascript:void(0)" onclick="sx(this, 27188)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(27188)">John 6:35</a></span>)
is greater? I believe you will. I believe that large
numbers of our young men returning from the armed services will wish to fill missions before
entering upon post-war employment. When they and other men who hold the priesthood can
be relieved from the exactions which the war has placed upon them, they will want to go into
the mission field. Then many of our sisters can go with them and render service under the
protection and direction of the priesthood of God. I believe, too, that thousands of our
families who have been blessed with comparative affluence in these times will wish to devote
a portion of their means to this great altruistic endeavor.
</p><p>
What a blessing it will be to our members and establishments in distant lands to welcome the
missionaries back again, and what a boon it will be to all people everywhere to hear the pure
word of God spoken by his appointed servants. "How beautiful upon the mountains are the
feet of those that bring glad tidings of good things"
(<span class="citation" id="12202"><a href="javascript:void(0)" onclick="sx(this, 12202)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(12202)">D&amp;C 128:19</a></span>).
</p><p>
God bless us, my people, that we may take up our work again with resolution to give
generously as we have received generously, I humbly pray, in the name of our Lord, Jesus
Christ. Amen.
</p>
</div>
</div>
