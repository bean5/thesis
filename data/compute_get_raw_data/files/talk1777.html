<?xml version="1.0" encoding="UTF-8"?>
<div class="gcera">
<div class="gchead">
<p class="gctitle">Beneath the Hearth</p>
<p class="gcspeaker">Elder Marion D. Hanks</p>
<p class="gcspkpos">Assistant to the Council of the Twelve</p>
<p class="gcbib">Marion D. Hanks, <i>Conference Report</i>, April 1970, pp. 132-136</p>
</div>
<div class="gcbody">
<p>
It is a very pleasant and humbling and uplifting experience to look at your faces in this
congregation and to remember with gratitude and affection the gracious kindness with which
you accept our humble efforts in your stakes and missions as we go on assignment there. I know
that many of the problems you deal with, many of the most difficult ones, involve homes and
families, and it is of this that I would like to speak this afternoon. Few other subjects seem to me
so urgently important in our time or to have such eternal relevance.
</p><p>
<b>The home and family</b>
</p><p>
I speak to those who have children at home, and to those who have influence in homes where
there are children, as well as to the great generation, represented by this marvelous [Logan
Institute of Religion] chorus, who are making decisions now that will effectively influence their
future homes and families.
</p><p>
In offering my witness about the home and family, I renew my expression of deep respect for
children who wisely choose the better way, often in improvement upon their parents, and my
deep compassion for good parents who strive earnestly to bring up their children in the way they
should go, only to have those children use their individuality and agency to follow other ways.
The Lord has forcefully taught us that in his eyes "the son shall not bear the iniquity of the
father, neither shall the father bear the iniquity of the son." (<span class="citation" id="23142"><a href="javascript:void(0)" onclick="sx(this, 23142)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(23142)">Ezek. 18:20</a></span>) Each accountable
person must ultimately answer for his own decisions.
</p><p>
It is our individual responsibility, parent or child or parent-to-be, to make decisions that will
improve upon the quality of our homes and our relationships within them, and each of us should
be anxious and honest in his efforts to do that&mdash;each of us.
</p><p>
It has been written: "As are families, so is society. If well ordered, well instructed, and well
governed, they are the springs from which go forth the streams of national greatness and
prosperity&mdash;of civil order and public happiness." (Thayer.)
</p><p>
<b>"Set in order your houses"</b>
</p><p>
In the early days of the restoration, the leaders of the Church were instructed to "set in order your
houses." (<span class="citation" id="15614"><a href="javascript:void(0)" onclick="sx(this, 15614)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(15614)">D&amp;C 90:18</a></span>)The Lord gave clear and explicit instructions to the brethren and certainly to all the
members of the Church that they be "more diligent and concerned at home, and pray always"
(<span class="citation" id="15621"><a href="javascript:void(0)" onclick="sx(this, 15621)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(15621)">D&amp;C 93:50</a></span>).
</p><p>
The wise men of the world have added their witness to the importance of doing this. Let me
quote one, Martin Buber: 
</p><p>
"If we had power over the ends of the earth it would not give us that fulfillment of existence
which a quiet, devoted relationship to nearby life can give us. If we knew the secrets of the upper
worlds, they would not allow us so much actual participation in true existence as we can achieve
by performing with holy intent a task belonging to our daily duties. Our treasure is hidden
beneath the hearth of our own home."
</p><p>
It is on this strong affirmation, which I believe with all my heart, that I offer five specific
suggestions as to how we may find and multiply the treasures hidden beneath the hearth of our
own home.
</p><p>
<b>Family associations</b>
</p><p>
First let me mention <i>family associations</i>.
</p><p>
What other families does your family know well? What other fathers and mothers do they see in
action? Do your children ever sit at the table or in family home evening, or kneel in prayer with
another family?
</p><p>
Parents should be deeply concerned to build friendships with other families who have
wholesome ideals, whose family life is constructive and strong. Children can greatly profit
through exposure to other homes, parents, and families where there is good disposition, pleasant
attitude, good fun, good humor, good literature, respect and discipline, and cleanliness and
prayer; where there is devotion to serving the Lord; where the gospel is lived.
</p><p>
With children, as all of us know, life is often a matter of following the leader, and wise parents
will want their children to enjoy the influence of other families whose convictions and example
will offer them strong incentives to build happy relationships in their own homes.
</p><p>
<b>Wonderful neighborhoods</b>
</p><p>
As parents we have been very grateful for the wonderful neighborhoods in which we have been
privileged to live, and for the strong families in whose homes our children have visited as friends
or baby tenders. Many religions and viewpoints are represented among our neighbors, and our
children have profited greatly and have been greatly strengthened in their gratitude for their own
home and faith from seeing the quality of the homes and families of the good people among
whom we are privileged to live.
</p><p>
Across the street, for instance, is a wonderful Latter-day Saint family into whose home I have
always been grateful to have my youngsters go. The mother is a warm, gracious friend and
homemaker whose surroundings reflect her own character. Her husband is a special kind of man
who has inspired our children and others in the neighborhood with his creative efforts to
encourage patriotism and learning and appreciation of our historical heritage. There have been
contests and essays and quizzes, serious celebrations along with the parties and fun on special
holidays.
</p><p>
<b>Family traditions</b>
</p><p>
That leads me to the second suggestion. Families thrive on <i>traditions</i> and the <i>special rituals</i> of
family life. Celebrating special days and seasons in special ways, working together, enjoying
family home evenings and family councils and conversations, deciding upon and preparing for
and enjoying holidays together, family meals and prayers&mdash;there are so many significant ways to
build family traditions that will be remembered.
</p><p>
With all else that is sacred about Christmas, for instance, it can mean a beloved white star on the
chimney that symbolizes the season. It may also mean that special time together on Christmas
Eve, carols sung at each home in the neighborhood, up and down the block, fun and music, and
the involvement of others from outside the home. Everyone participates, but especially the
guests who share the experience, who take part, who read and contribute some special thought of
Christmas. The Bible teaches us that we must not be forgetful to entertain strangers, for in so
doing many have entertained angels unaware. (<span class="citation" id="24404"><a href="javascript:void(0)" onclick="sx(this, 24404)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(24404)">Heb. 13:2</a></span>) The custom of having honored guests with us in
our home has given us that experience every year for many years at Christmas and other times.
</p><p>
Let me be personal enough to mention that the choicest memories of recent years, as we talk of
ritual or celebration at our home, are the times we prepared as a family to bid a precious child
farewell on her way to school. We celebrated the sad/happy event and joined our hearts together
as the head of the home gave her a father's blessing and invoked the Spirit of the Lord upon her.
Twice we have had that glorious privilege, and pray God that we may enjoy it with each child.
</p><p>
It is of such simple but significant things that family traditions are built, and unified families with
them.
</p><p>
All of us turn reflectively to the sweet memories of our childhood at home, and each of us, now
blessed with families or looking forward to that privilege, should he thinking about the
memories we will provide for their future.
</p><p>
<b>Family values</b>
</p><p>
Third, let me mention <i>family values</i>. What gets major attention in our homes? What do we really
care about, take time for? What is worthy of our consideration, our attention, our money, our
efforts? What of books and reading them? What of thoughtful acts of kindness, of sharing,
involving the whole family within and without the home? What of prayer, conversation genuine
concern with each other?
</p><p>
In 1926 <i>The Improvement Era</i> carried a memorable statement by a college senior concerning
thoughts of home and relationships there. Let me read what he wrote about his good home: 
</p><p>
"1. I wish I could remember one Fourth of July, or one circus day, or one canyon trip, in which
my father had joined us boys, instead of giving us the money and equipment to go, while he and
mother stayed home, and made us feel guilty by working while we played.
</p><p>
"2. I wish I could remember one evening when he had joined us in singing, or reading, or
tussling, instead of always sitting so quietly with his newspaper by the reading lamp.
</p><p>
"3. I wish I could remember one month, or week, or day even, when he had made purposeful
work out of drudgery by planning the farm work with us, instead of merely announcing each
morning what that day's work would be.
</p><p>
"4. I wish I could remember one Sunday when he had bundled us all into the buggy and taken all
to church together, instead of staying home while we went in the morning, and leaving us home
while he and mother went in the afternoon.
</p><p>
"5. I wish that I could remember just one talk in which we had discussed together the problems
and facts that trouble every growing boy, on which his clear and vigorous viewpoint might have
shed such light and comfort, instead of leaving me to pick up the facts haphazardly as I might
and to solve the problems as best I could.
</p><p>
"And yet, my conscience would cry shame were I to blame him, for no man could ever be more
devoted to his family, more anxious for their welfare, I more proud of their successes. His
example has been a beacon to us. He just didn't know&mdash;and there is the pity of it to me&mdash;he just
didn't know that we needed <i>him</i>. He didn't know that ye would rather have his companionship
than the land he could leave us&mdash;that some day, maybe we might make money for ourselves, but
that never can we make for ourselves the memories that might have enriched and mellowed and
molded our lives. I can't see a Fathers and Sons' outing without a lump in my throat." (<i>Era</i>,
December 1926, p. 145.)
</p><p>
<b>Discipline in the home</b>
</p><p>
Fourth, I speak of <i>discipline</i>&mdash;discipline in the home; and of course I am not talking about harsh
punishments but of fair rules, understood and enforced, with sanctions consistently imposed
when they are broken. I am thinking of realities, of facts to be faced, of a future of attitudes
toward law and rules and personal responsibility being learned. Samuel Johnson, the great
British literary genius, said that he would never permit his children to "deny him"&mdash;that is, to
deny to callers that he was at home when he was, busy as he was. He said, "If I teach my children
to lie for me, I may be sure that they will soon conceive the notion of lying to me."
</p><p>
Discipline involves adult solutions to the problems that arise in living together. Wise parents do
not subject each other or their children to emotional poisoning. Disagreements are handled
maturely and constructively and not destructively.
</p><p>
Discipline begins with concern and commitment and example, like that other word that comes
from the same root: disciple.
</p><p>
Children need standards, need guidelines of behavior, and limits. They need models who care,
who are firm and fair and sensitive and consistent. Wholesome discipline can he gentle and
sensitive, but often it isn't.
</p><p>
<b>Interest of wonderful mother</b>
</p><p>
A daughter and I were recently discussing her return home at an hour that seemed questionable
to me. I shared with her an experience with my wonderful mother. I had spent some years away
at schools and missions and wars, and the two of us were now alone at home. I returned from an
appointment one evening at midnight to find the light still on in Mother's little bedroom. As I
had always done, I reported in to Mom, sat on her bed, and kidded with her a little. I asked her
why she was still awake. "I am waiting for you," she said.
</p><p>
I said, "Did you wait for me while I was on a mission, Mom, or at sea, or in battle?"
</p><p>
Her answer was calm and sweet. She gave me that little pat on the knee that reflects the mature
compassion of the wise for the ignorant, and said: "No, that would have been foolish. I just knelt
down here by my bed and talked to the Lord about my boy. I told him what kind of man I
believed you to be and wanted you to be, and prayed for his watchful care of you, and then left
you in his hands and went to sleep. But now you are home," she said, "and you can count on it
that I will be interested in you as long as I live."
</p><p>
She is gone now, and it is remarkable how often I get the feeling that she is interested still, and
forever will be.
</p><p>
<b>Family love</b>
</p><p>
Finally I mention <i>family love</i>, expressed in so many wonderful ways. Someone once said&mdash;it's
been often quoted&mdash;that the best thing a father can do for his children is to love their mother. I
believe this, and that the strongest and surest base for loving others is to love the Lord and to
bring the binding and blessing balm of that love into all relationships of the home.
</p><p>
Children have the right to learn that love is the foundation of a good family, and that love cannot
exist apart from such qualities as respect, consideration, responsibility, and loyalty. Love is not
self-centered and is not self-serving, but is concerned with the well-being and happiness of
others. It is providing for our loved ones an atmosphere of warmth and kindness that accepts and
preserves the uniqueness of each as an individual person while building the unity of the home.
</p><p>
Love means friendship and companionship and partnership and unity. It expresses itself in
modesty, in generosity, in sensitivity, in courtesy, in counsel, in appropriate compromise. It
inspires affection and confidence and trust and self-control. Love, mature love, provides a
climate of wholesome, repentant, forgiving consideration. It listens. It hears and senses the needs
of another. It can never be separated from character, from unselfishness, from good humor, and
from every tender virtue.
</p><p>
<b>Make the effort</b>
</p><p>
It must be strongly said of each of these avenues to family felicity that it does not just
<i>happen</i>&mdash;it must be brought about by people who think and care and make the effort.
</p><p>
God help us to be more concerned with a high standard of life than with a high standard of
living. God help us, while there is time, to take time to do everything we can to bring about now,
or in the family we will one day have, by making wise choices now, the unity and strength and
sweetness that a home is meant to have. I believe we can do that, or materially move toward
bringing it about, through thoughtful family associations, memorable traditions, correct values,
wise discipline and great love.
</p><p>
What will we give our children to remember?
</p><p>
It is likely that what they will remember best is the treasure we unearth from beneath the hearth
of our own home.
</p><p>
I know the gospel is true, and that it has been restored, and that it centers in the home. God bless
us to strengthen the home, in Jesus' name. Amen.
</p>
</div>
</div>
