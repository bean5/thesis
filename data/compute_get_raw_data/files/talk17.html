<?xml version="1.0" encoding="UTF-8"?>
<div class="gcera">
<div class="gchead">
<p class="gctitle">"Whom Say Ye That I Am?"</p>
<p class="gcspeaker">Elder Albert E. Bowen</p>
<p class="gcspkpos">Of the Council of the Twelve Apostles</p>
<p class="gcbib">Albert E. Bowen, <i>Conference Report</i>, pp. 57-61</p>
</div>
<div class="gcbody">
<p>
Our meeting here this morning seems hopelessly discordant in its purpose with current,
all-enveloping happenings. We gather to worship the God of love in the name of His Son, the
Prince of Peace. And even as we speak, the whole world is ablaze with the devouring flames
of war. At this instant, in far-away places men are locked in a death grapple.
</p><p>
Both in its scope and portent the present conflict dwarfs what we heretofore, out of tribute to
its magnitude, have styled the World War, as that eclipsed the wars which had gone before.
Scarcely is there a land some of whose citizens have not forfeited their lives. In all the earth,
as it was in Ramah, there is heard the voice of lamentation: "Rachel weeping for her children
refused to be comforted for her children, because they were not"
(<span class="citation" id="33789"><a href="javascript:void(0)" onclick="sx(this, 33789)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(33789)">Matt. 2:18</a></span>).
</p><p>
But we are not met here for mourning, nor to commiserate with ourselves. We are not even
convened to call down fire from heaven to consume the adversaries of our country. We are
come together, both within these walls and beyond them so far as the spreading ether waves
reach out and bring us into communion, to refresh ourselves in the faith that God lives and
directs the destinies of this world and of men and of nations; to remind ourselves again and to
draw sustaining power from the assurance that in this world there are such simple elementary
principles as right and wrong and that in their unending struggle for supremacy right will
always triumph.
</p><p>
We should accordingly be composed in our feelings. Though none of us can penetrate the
gloom and see what lies beyond, we know that this war, like others that have gone before,
will come to an end. Then we shall want our farms and our businesses and our trades the
same as before. We should hold on to them, and, so far as may be under the limitations and
restrictions imposed by the times, keep everything productive. We cannot give way to despair.
</p><p>
We must likewise sustain our country to the full measure of the requirements of loyalty and
patriotic devotion. The nation is now at war. We dare not lose that war, for its loss would
mean the end of liberty as we have come to esteem it. It could mean loss of the right to meet
and worship as we are doing now. We may have a major task to preserve freedom as we have
known it even with the war won. With the war lost we should have no chance at all. We of
this Church have a particular regard for freedom under the protection of law. With us it is a
religious tenet. We have vivid and unhappy memories of the misery and the suffering that
follow when men in blind fury defy the restraints of law and act on their own caprice. We
recognize the right of men in the exercise of their freedom of choice to reject the very
commandments of God. Only a free soul is fit to enter His kingdom. Men must learn here to
live as free men and to apply the restraints which true freedom imposes to be fit for the
heavenly realm. Hence we are unalterably opposed to the attempt of any nation or man or
group of men, foreign or domestic, to take away or destroy or abrogate the freedoms
guaranteed under the law of our land.
</p><p>
Seated behind and around me are the men who hold the principal offices in this Church. I
doubt if there is one of them who has not now in the armed forces sons or grandsons or
brothers or other near kindred. Some of these have already made the supreme sacrifice. They,
with the membership at large, sustain the government, purchase its bonds, contribute to it their
substance and give it their fealty.
</p><p>
We abhor war with all its savagery, its human wastage and its moral degradation. But war is
here and since the principles of liberty are at stake, challenging the very purposes of God, my
faith is that they will be rescued, though at what cost of blood and treasure I know not.
</p><p>
Abraham Lincoln fully believed that the Civil War was the price this nation had to pay for
the sin of human slavery. We had proclaimed to the world as a foundation principle of our
political faith the inalienable rights of all men to be free, but we practiced human bondage.
That was a base denial of our loud-toned profession. We refused to repent. War came. In a
dark day of reverses Lincoln expressed concern lest it prove to be God's will that the
chastisement of the nation might continue "until all the wealth piled up by the bondsman's
two hundred and fifty years of unrequited toil shall sink, and until every drop of blood drawn
by the lash shall be paid by another drawn by the sword."
</p><p>
What may now be our national sins and what may be exacted in expiation I leave to your
conclusions. Among them I do not hesitate to name arrogance, godlessness and the decay of a
living Christian faith.
</p><p>
During the troubled years of his presidency, Lincoln many times by proclamation set apart
days for prayer and supplication for divine favor. He never omitted from those proclamations
the admonition to pray for forgiveness of our national sins as a condition to the reception of
God's help.
</p><p>
There are numerous ways besides those I have already mentioned in which we may contribute
strength to the nation. But I am persuaded that the service the Church can best render &mdash; and it
is a transcendently important service &mdash; is to keep alive as the foundation of our country's
future the true spirit of religion, which involves the establishment and preservation of a living
faith in a living God. Experience should have taught that "except God build the house, they
labor in vain who build it!" (<span class="citation" id="41720"><a href="javascript:void(0)" onclick="sx(this, 41720)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(41720)">Ps. 127:1</a></span>).
</p><p>
You recall an occasion when Jesus asked his disciples, "Whom do men say that I am?" They
gave him the various conflicting conjectures they had heard expressed, whereupon he put it to
them direct, "But whom say ye that I am?" The reply came from Peter with equal directness:
"Thou art the Christ, the Son of the living God"
(<span class="citation" id="33803"><a href="javascript:void(0)" onclick="sx(this, 33803)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(33803)">Matt. 16:13-19</a></span>).
Christ approved that answer and declared
that the basis of the knowledge implicit in it was the rock upon which he would build His
Church. He said more than that. He said that being so foundationed the gates of hell should
not prevail against it. That is a very important assurance. It promises solidity and perpetuity.
That is the essence of the message which His disciples bore to the world. As Paul phrased it,
they determined to know nothing save Jesus Christ and Him crucified
(<span class="citation" id="88"><a href="javascript:void(0)" onclick="sx(this, 88)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(88)">1 Cor. 2:2</a></span>).
</p><p>
I want here to quote some very penetrating observations of an eminent editorial writer,
Thomas F. Woodlock. After some remarks about the lack of real substance in the lip service
we pay to Christian precepts he continues,
</p><p>
How many of us would with a whole heart and clear conviction echo Peter's confession of
faith when challenged by his Master? And what are we teaching in our schools, grammar and
high, and colleges and universities concerning that confession?
</p><p>
Now the answer to the question put to Peter is of the all-or-none order. The Christianity
which built the western civilization was built upon Peter's answer. It was that Christianity
which brought democracy into the world because it was the first to bring to man the
revelation of human personality, and that is the rock upon which the democracy in which we
profess . . . a faith rests <i>and alone can rest</i>. It was that Christianity upon which the declared
principles of our civil order rest, and there is no other resting place for them. A belief in
democracy without a belief in that Christianity is no better than a code deprived of its creed
or a flower cut from its parent stem: it must ultimately wither and die. When it dies freedom
dies, even if democratic forms survive. Hitler rules today under the "forms" of the Weimar
constitution and Stalin under the "forms" of a constitution as "democratic" sounding as anyone
could wish! The same thing could happen here under our own "forms" if we, too, should lose
faith in the soul that alone can give them life.
</p><p>
I am not predicting dire catastrophe for our country. But I do say that the warning sounded is
no idle one. The arraignment made by Mr. Woodlock is, I am forced reluctantly to admit,
justified by the facts. Since sometime before the war started in Europe thoughtful men, there
and here, scholars, scientists, publicists, statesmen, religionists, have been calling for a
spiritual and religious recovery; they have solemnly warned that our nation cannot endure
"except upon a solid religious foundation," but I very much doubt if any of them would give
the answer Peter gave to the same question as was addressed to him. Men profess a deep
attachment to what they call the ethical quality of Christ's teachings, but they deny Him. The
nineteenth century is described as the one in which man substituted belief in himself for belief
in God. "Glory to man in the highest" was Swinburne's impious exultation. Now the things of
which man thought himself master have turned on him with a terrible vengeance. We have
seen the decline of religious faith followed by the rise of tyranny. I believe it is a safe
generalization that despotism is always at enmity with the Christian religion. They rest upon
inherently and irreconcilably antagonistic conceptions about man, his worth and dignity and
destiny and place in the order of things; the one debases him, the other exalts; the one denies
God, the other acknowledges His supreme power and bows before His majesty. The teaching
of the Christian religion irritates the despot because it is a constant denial of his assumed
supremacy and a rebuke to his tyrannies. Hence the despot always seeks to put religion down.
The rise of Hitler in Germany heralded assaults upon the church. His Minister of Religion
said, "Adolph Hitler is the true Holy Ghost," and the Minister of Culture declared, "We must
proclaim a German Christ, not a lamb of God." In Russia the line was the same, "What is
worrying us is not that Christianity is dying in Russia, but that it is still surviving," said the
Commissioner of Justice. "The natural transition," said another, "is to bring about the death of
all religion."
</p><p>
Apparently they have found it impossible to root out of their people their ingrained instincts
for religious worship. And so the effort is now to divert them to a new religion. Dr. Alfred
Rosenberg has come forward with the blue print for a "new national church." It does not
require that citizens adhere to it but it outlaws all other churches, confiscates their property,
forbids any of the teachings and practices of Christianity, banishes the Bible and substitutes
for it <i>Mein Kampf</i> which is never to be added to nor taken from and the exposition of which
by state designated orators is to be the substance of all religious service. As one reads the
prescriptions one wonders if it is of today or whether by some magic he has been shuttled
back into primitive paganism.
</p><p>
But you may say what has all that to do with us. Just this. Germany is not the only land in
which there is an ambition to set up a new order, nor to recast religion to fit into that order.
Incredible as it may seem there is at work in our own country today a body of men and
women, highly intellectual, trained and lettered, apparently earnest and sincere who have
issued a manifesto which they call "A Declaration on World Democracy." They propose the
creation of a World State of which the United States of America is to be the hub and its
framework of government to furnish the pattern, of course with plenty of circumscriptions and
modernizations. This model state is to have the modest name, "City of Man," and the
indwellers are to have their ideas of freedom redefined so as to bring them within very certain
limitations. But we pass all that to observe that this world state is to have provided for it a
religion which is to be a "religion of democracy." A committee of experts is to examine all
the various existing religions and determine what there is in them "of greater or lesser value
for the preservation and growth of the democratic principle," what "elements in them are more
apt to cooperate with the democratic community and consequently more deserving of
protection by it." Our notions of religious freedom are to be re-examined for we "must know
what limits are set by the religion of freedom, which is democracy to freedom of worship."
The implication is clear that it will be just too bad for any religion which the committee of
experts finds not to be in the best interests of democracy for "the universal religion of
democracy shall underlie each and all of them." Perhaps I ought to say that the authors
expressly disclaim the intention of setting up a state religion though they have provided all the
framework for it including a body of inquisitors. It has always been our assumption that
democracy was born of the teachings of religion, but now democracy is to determine the uses
and value and content of religion. 
</p><p>
What place, I ask you, is there for God in that "religion for democracy" set up by a
committee of experts? How would the projectors of the scheme answer the question which
was directed to Peter? And if God is excluded how can you have a religion at all? Where are
we getting to in our cry for the recovery of religion if God has no place in it except to supply
a convenient name which people are accustomed to associate with worship? Why do the
authors desire or think it important to have in the model state a religion at all? What they
would provide is a sham, a hollow shell wholly devoid of the spirit that gives life. The
proposal is near blasphemy. It dethrones God and deifies man, which is one of the principal
reasons for our present confusion and turmoil and impotence. I don't mean to imply that these
men are of a kind with Hitler at all, but I still ask what essential difference there is in
principle between their "religion for democracy" and Hitler's "new order" or Rosenberg's
"new religion."
</p><p>
The advocates of the new religion are powerful writers, capable of expressing their ideas with
force. They command wide attention. They present America with the issue, clearly drawn,
whether religion is a plan and a way of life for mortals emanating from Deity or whether
religion shall be taken over by the intellectuals, formulated on their design and made the mere
creature and servant of the political state.
</p><p>
For the future safety of the world, for the welfare of the souls of men, for the preservation
and salvation of our beloved country we can never make that surrender.
</p><p>
In that matchless prayer, in which he pleaded with the Father for the disciples whom He was
about to leave, just before He crossed over the brook into the Garden, Jesus used these words:
</p><p>
And this is life eternal, that they might know thee the only true God, and Jesus Christ whom
thou hast sent (<span class="citation" id="27088"><a href="javascript:void(0)" onclick="sx(this, 27088)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(27088)">John 17:3</a></span>).
</p><p>
That is our belief and our message. That is the message which won the western world to
acceptance of the Christian faith. Never did men more need the sustaining power of that firm
conviction than in this confused, bewildering, and muddled time. Never were those words
freighted with a deeper meaning for the needs of the hour. They are simple, direct, and clear
as distinguished from the tangled skein of mystifying phrases which men are driven to use
when they try to expound a God of philosophy who is not the Lord God omnipotent.
</p><p>
God grant us the power to stand true to our trust, I pray, in the name of Jesus, Amen.
</p>
</div>
</div>
