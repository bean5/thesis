<?xml version="1.0" encoding="UTF-8"?>
<div class="gcera">
<div class="gchead">
<p class="gctitle">"We Believe in God"</p>
<p class="gcspeaker">Elder Sterling W. Sill</p>
<p class="gcspkpos">Assistant to the Council of the Twelve Apostles</p>
<p class="gcbib">Sterling W. Sill, <i>Conference Report</i>, April 1955, pp. 116-119</p>
</div>
<div class="gcbody">
<p>
In the early part of the year 1842, John Wentworth, editor of the Chicago Democrat, went to
Nauvoo and obtained an interview with the Prophet Joseph Smith.  He requested, among other
things, that the Prophet write out a statement of the things in which the Church believed, and
the Prophet wrote the Thirteen Articles of Faith.  Later these were accepted by the vote of the
people and became a part of the doctrine of the Church.  They are now included in the Pearl
of Great Price and form a part of that great volume of latter-day scripture.
</p><p>
This afternoon, and on this anniversary of the birth of the Savior of the world, I would like to
offer for your consideration the first four words of the Prophet's statement, from the point of
view of its being the greatest success formula in the world.  Victor Hugo said, "There is
nothing in the world as powerful as an idea whose time has come," and if we can learn
anything from the signs of the times, we know that the time has fully come when great faith
in God should take a firmer hold upon our minds.
</p><p>
It has been a hundred thirty-five years since God the Father and his Son, Jesus Christ,
reappeared upon the earth to reestablish among men a belief in the God of Genesis and to
usher in the greatest and final dispensation.  And so as the very foundation of our faith, the
Prophet said, "We believe in God"
(<span class="citation" id="6685"><a href="javascript:void(0)" onclick="sx(this, 6685)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(6685)">A of F 1:1</a></span>).
</p><p>
If the meaning of this phrase were limited to the idea that we believe that God exists, it
would still be one of the great statements of the world.  That is, there is great strength in the
knowledge that we were not created by, nor are we at the mercy of, the forces of a blind and
capricious chance.  But when we say "We believe in God," we mean much more than merely
that God exists.  We mean that we understand something about the kind of being he is, that
he is literally the Father of our spirits, and, according to the great law of the universe, the
offspring may sometime become like the parent.
</p><p>
But the most thrilling and motivating part of this idea is what the words themselves indicate,
that "We believe in God." We trust him.  We believe that he knows his business, that
regardless of chance or the errors of men, his purposes will prevail.  We believe that our
interests are his interests, that he meant what he said in that wonderful declaration that "This
is my work and my glory to bring to pass the immortality and eternal life of man"
(<span class="citation" id="39069"><a href="javascript:void(0)" onclick="sx(this, 39069)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(39069)">Moses 1:39</a></span>). We
believe that God does not desire that his children be dull, or unattractive, or unhappy, or
unsuccessful.
</p><p>
There are many things that we do not understand.  We don't understand our own birth or life
or growth or death. We don't understand light or darkness.  No one in mortality has ever seen
his own spirit.  We didn't discover the circulation of our own blood until just a little over
three hundred years ago.  It must be obvious, therefore, why a wise Heavenly Father would
give us detailed instructions, setting forth objectives and the best methods for attaining them. 
It must be equally obvious that there are tremendous advantages in a complete acceptance of,
and an unwavering faith in, the gospel; for as an earthly father is powerless to confer the
maximum benefit upon a son who has no confidence in the motives or abilities of the father,
so God is powerless to confer the greatest blessings upon men who do not believe in him.  A
great power attaches to a definite objective held by a strong faith.  Jesus said, "If thou canst
believe, all things are possible to him that believe"
(<span class="citation" id="33087"><a href="javascript:void(0)" onclick="sx(this, 33087)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(33087)">Mark 9:23</a></span>).
</p><p>
Sometime ago I read about the great woman swimming champion, Florence Chadwick.  In
1950 she swam the English Channel, and then on July 4, 1952, she attempted to swim the
twenty-one miles of water lying between Catalina Island and the southern California coast. 
The temperature of the water was forty-eight degrees, and a heavy fog lay over the sea. 
When she was only half mile or so from her objective, she became discouraged and decided
to quit. Her father who was in the boat nearby tried to encourage her by pointing through the
fog and telling her that land and success were near at hand. But she was discouraged, and a
discouraged person is always a weak person.
</p><p>
The next day Miss Chadwick was interviewed by some newspapermen. They knew that she
had swum greater distances on previous occasions, and they wanted to know the reason for
her present failure.  In answering their questions, Miss Chadwick said, no, it wasn't the cold
water and it wasn't the distance.  She said, "I was licked by the fog."
</p><p>
And then she recalled that on the occasion when she swam the English Channel, she had had
a similar experience.  When only a short way from shore she had given up, and this time also,
her father had pointed ahead, and she had raised herself out of the water just long enough to
get the picture of her objective firmly fixed in her mind. This gave her a great new surge of
strength, and she never stopped again until she felt under her feet the firm earth of victory.
</p><p>
I thought of this recently when a stranger called me on the telephone and asked if he and his
wife might come and discuss with me a great tragedy that had recently occurred in their
family.  He explained that a speeding automobile had taken the life of their only daughter,
and they asked me to try and help them understand something about the purpose of life and
the meaning of death and what their relationship ought to be with each other, and where God
fit into the picture, and whether or not there was any use for them to try to live on. This great
tragedy weighed upon them so oppressively that they almost seemed to be suffocating, and for
three and a half hours I tried as hard as I could to help them with their problem.  But there
wasn't much of a foundation on which to build, and I discovered that it can be a devastating
thing all of a sudden to need great faith in God and not be able to find it.  It wasn't that they
were rebellious or that they disbelieved in God.  Their skepticism went deeper; they hadn't
given him a thought one way or the other.  It wasn't that they disbelieved in immortality; up
to this point, they hadn't cared.  Then death a stepped across their threshold and taken the
best-loved personality there. And then all of a sudden, they needed great faith in God and
were not able to find it.
</p><p>
You can't merely snap your fingers and get great faith in God, any more than you can snap
your fingers and get great musical ability.  Faith takes hold of us only when we take hold of
it. The great psychologist, William James, said, "That which holds our attention determines
our action," and one of the unfortunate things in life is that we sometimes focus our attention
on the wrong things.
</p><p>
I have been disturbed a little, as I have gone around and become more conscious of the great
variety of temptations that we wrestle with and succumb to.  When we enumerate all of the
temptations, we find that we often fall before some very small ones, merely because we have
continued to entertain them.  We talk until we are weary about the "temptations down," not so
much about the "temptations up."
</p><p>
The dictionary says that to tempt is "to arouse a desire for," and so I assume that I am correct
in thinking that temptation can go in either direction, although it is the easiest thing in the
world to allow our minds to become loaded with the temptations downward&mdash;the temptations
of lethargy, the temptations of sloth, the temptations of ignorance, the temptations of sin.
</p><p>
But every thought tends to reproduce itself in an act.  Rags, tatters, and dirt are always in the
mind before they appear on the body.  One of the greatest handicaps to spiritual growth, or
any other kind of growth, is to have a negative mind, and I suppose that one of the functions
of a great faith is to lift our thoughts upward, to houseclean our minds, to sweep out our
"temptations down," and fill our minds with the "temptations up."
</p><p>
And so I would like to offer you the thought of some of the thrilling temptations
upward&mdash;the temptations of culture, the temptations of service, the temptations of great industry, the
temptations to focus our minds on great spirituality, the temptation to believe in God.
</p><p>
I am certain that the greatest waste there is in the world is not the devastation that goes with
war; nor is it the cost that accompanies crime; nor is it both of these put together. The
greatest waste in the world is that human beings, you and I, live so far below the level of our
possibilities.
</p><p>
Henry Ward Beecher was once asked whether or not he believed that Christianity had failed,
and he said that so far as he  knew, it had never been tried. Compared with what we might
be, we are only half awake.  We have great concern that our lives may someday come to an
end, but the real tragedy is that so many lives never really have a beginning. The fires in our
souls need rekindling.  In speaking of education, Francis Bacon said, "If you want a tree to
produce, don't worry so much about the boughs; fertilize the roots." Then suppose we give in
to that temptation to stimulate those great God-given powers within ourselves which can lift
us toward heaven.
</p><p>
The brute creation goes down on all fours, which tends to throw its gaze upon the ground. 
But man stands upright in the image of his Maker that his vision may reach to the stars.
</p><p>
The mission of Jesus was up.  Even in Gethsemane with the awful weight of our sins upon
his soul, his face looked up to God.  But whatever may be the attitude of the body, the spirit
should be on its toes. When Jesus was teaching us to pray, he inserted that wonderful phrase
which says, "Thy will be done." But even when we repeat these inspiring words, intended to
lift us up, we usually surround it with a spirit of martyred resignation.  When we say, "Thy
will, not mine, be done" (see (<span class="citation" id="31417"><a href="javascript:void(0)" onclick="sx(this, 31417)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(31417)">Luke 22:42</a></span>),
we may be hoping for the best but we are usually expecting the worst.
</p><p>
We fill our hearts with too many doubts and fears and negative thoughts. But try to imagine
what the great Creator would have us do if we did his will.  Can you conceive of any limits
he would place upon our progress? What would God have us "arouse a desire for"? Certainly
not for weakness, or failure, or sin!  Certainly he does not want us to fill our minds with the
temptations down.  He is not leased when we become the problem children of God.  His will
is for us to become beautiful and glorious like him.
</p><p>
But the great truths of life become known only to those who are prepared to accept them.  So
I would like to present for your consideration the thrilling temptations of the gospel, the
temptations to live worthily of the celestial kingdom, to attain a celestial body, a celestial
mind, a celestial personality, to live with a celestial family and celestial friends on a celestial
earth. The gospel offers us the temptation to accept the challenge of Jesus when he said, "Be
ye therefore perfect, even as your Father which is in heaven is perfect"
(<span class="citation" id="34687"><a href="javascript:void(0)" onclick="sx(this, 34687)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(34687)">Matt. 5:48</a></span>).
</p><p>
"Thy will be done," means to become like God. Now try to  imagine what the mind of the
Creator is like.  If you should lose all of your material possessions, you might have reason to
be greatly depressed.  But how poor you would be if you lost your faith in God!
</p><p>
My brothers and sisters, we have lived successfully through the long ages of a pre-existence. 
Now we live in mortality which is very short.  And we are very near the end of the race. 
How unfortunate are they who relax their efforts when on the very verge of success, like the
great Roman general, Cato, who committed suicide on the very eve of his triumph.  If you
sometimes feel that the water is a little cold and the way is a little foggy, then is the time to
look up and have faith, for there is land ahead.
</p><p>
"All things are possible to him that believeth"
(<span class="citation" id="33088"><a href="javascript:void(0)" onclick="sx(this, 33088)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(33088)">Mark 9:23</a></span>),
and so in our daily devotions we hold ever
closer the very foundation of our faith, God's formula for success, "We believe in God"
(<span class="citation" id="6686"><a href="javascript:void(0)" onclick="sx(this, 6686)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(6686)">A of F 1:1</a></span>).
</p><p>
May God bless our faith, I pray in the name of Jesus Christ.  Amen.
</p>
</div>
</div>
