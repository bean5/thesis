<?xml version="1.0" encoding="UTF-8"?>
<div class="gcera">
<div class="gchead">
<p class="gctitle">Why a Church?</p>
<p class="gcspeaker">Elder Albert E. Bowen</p>
<p class="gcspkpos">Of the Council of the Twelve Apostles</p>
<p class="gcbib">Albert E. Bowen, <i>Conference Report</i>, October 1950, pp. 69-75</p>
</div>
<div class="gcbody">
<p>
One often hears the question: Why a church? I should like briefly to consider it. I shall
hope to suggest to your minds that the question is tantamount to asking: Why religion?
</p><p>
WASHINGTON'S FAREWELL MESSAGE
</p><p>
It was one hundred fifty-four years ago this very month, his second term of office as
President of the United States drawing to a close, that George Washington announced to the
country his determination to retire, and requested that he not be considered available for
re-election to the office he was about to lay down. He made it the occasion for a farewell
message which partook almost of the nature of a last testament, bequeathing to his
countrymen the fruitage of his rich and varied experiences.
</p><p>
As a participant in the long and oftentimes acrimonious disputes which eventuated in the
political severance of the American colonies from the mother country, as Commander-in-Chief
of the untrained, poorly-disciplined, ill-equipped, scantily-clad, under-provisioned, and
ofttimes unpaid Continental Army, as witness to the bickerings and jealousies and petty greeds
which, following the war, so threatened the wreckage of the infant nation that he often
wondered whether the winning of the conflict with Britain would prove to be a blessing or a
curse, as president of the convention which fashioned the Constitution of the United States of
America and as it first president, he had seen human nature at its best and almost its worst.
Under stresses and strains, sacrifice and suffering, he had seen men rise to noble heights of
patriotic devotion. Likewise, he had seen them usurp and abuse power, quarrel and bicker,
resort to petty scheming for advantage, exhibit mean little greeds, and stoop, under the spur of
selfish ambition, to ignoble deeds.
</p><p>
Drawing upon this ripe knowledge of human behavior with all its foibles and inconstancy, he
so packed into that testamentary legacy perennial wisdom that it never grows old, but is valid
for all peoples and all times.
</p><p>
Among the nuggets of pure gold tucked away in that admonitory address are Washington's
observations about religion and morality. Here is what he said: 
</p><p>
Of all the dispositions and habits which lead to political prosperity, religion and
morality are indispensable supports. In vain would that man claim the tribute of
patriotism who should labor to subvert these great pillars of human happiness&mdash;these
firmest props of the duties of men and citizens. The mere politician, equally with the
pious man, ought to respect and cherish them. A volume could not trace all their
connections with private and public felicity... And let us with caution indulge the
supposition that morality can be maintained without religion. Whatever may be
conceded to the influence of refined education on minds of peculiar structure, reason
and experience both forbid us to expect that national morality can prevail in exclusion
of religious principle. (Sept. 17, 1796.)
</p><p>
THE CHRISTIAN CHURCH
</p><p>
Without doubt, when Washington spoke about religion, he had in mind the Christian religion.
By and large that is the only religion Western man knows. When I speak, in what I shall say
today, about religion or the church, I shall have in mind the Christian
religion and the Christian church, which encompasses the moral and religious teachings of the
Old Testament as well as the new.
</p><p>
The church has had many ups and downs since Washington's day as well as before. It has had
periods of strength as well as periods of questionings and doubts. Agnostics and atheists have
ever been with it. It has taught that man, the individual as well as the race, is of very great
consequence. As a child of God endowed with divine attributes, he is capable of infinite
advancement in the scale of being, even to ultimate perfection. He must have faith in himself
and his high destiny. Thus far, the Christian is a humanist, and the church is humanistic. But
when man loses his humility and arrogates to himself a self-sufficiency which denies God or
any other power higher than himself, then the church must part company with the humanistic
creed or compromise its principles.
</p><p>
INFLUENCE WEAKENED
</p><p>
Under the impact of agnosticism, atheism, and the extreme humanism which denies God and
makes man the source of all meaning, the Christian church as a body has compromised its
basic doctrines to make its teachings more harmonious with the current of popular opinion.
And where has it got itself? It has lost its saving faith, weakened its influence, and almost
forfeited its moral leadership. In consequence, men are floundering about in confusion, not
knowing what they ought to do, but well-assured that the fair promises of irreligion and
unbelief and human sufficiency have failed them, and they are casting about for anchorage.
That is the sorry plight of man in this age.
</p><p>
REVIVAL OF FAITH NEEDED
</p><p>
Men of distinction in the world of letters, scientists, men of wide learning in almost every
realm of scholarly research are asserting with great earnestness that the only thing that can
save our civilization is a revival of religious faith. In one of his notable addresses, Robert
Gordon Sproul, president of the University of California, said: 
</p><p>
There is a great need for some directive force to rally the recuperative powers of
mankind and win the race with catastrophe. Education cannot provide such a force,
important as it is, because it is not the minds, but the souls of men that must be
regenerated if catastrophe is not surely to come... Our American heritage cannot long
endure without a firmly grounded religious faith.
</p><p>
Only day before yesterday, General Marshall said that military force alone cannot defeat the
enemies of the United States. It must be buttressed by the weight of moral force.
</p><p>
These utterances are but typical of the warnings that are repeatedly being sounded by
thoughtful people who are concerned about the state of men and women in this modern world.
Thus is the wisdom of Washington's reminder that religion and morality are Indispensable
supports to political prosperity and that morality cannot be maintained without religion
vindicated by the compelling logic of events in this disordered topsy-turvy world. One of the
most frequently urged indictments against the Soviet system of government as directed by the
polit-bureau is that it seeks to destroy all religion and forbids freedom of religious practices to
its people.
</p><p>
UNIFIED ACTION REQUIRED
</p><p>
If, then, it can be conceded, as is so vigorously asserted, that a sound religious faith is
essential to the saving of our blighted and withering civilization, the question demanding
concrete and immediate answer is: How is a religious faith equal to this supreme task to be
regenerated? I do not assert or mean to say that the average run of our people is irreligious or
anti-Christian. Christian standards of morality have too long been bred in their bones for that.
The teachings of Christ still furnish the best standards by which to measure values that the
world knows, and the people of this land out of long habit, instinctively turn to them. At least
we pay lip service to them. But clearly that is not enough to furnish the crusading fervor
essential to rousing the people of the Christian nations to that mighty endeavor. It is not a
matter for individual, uncoordinated confession of faith. It requires action, unified action. That
means an organized agency or instrumentality to give the movement direction and solid
purpose. The only such organization at hand is the church. That is its office. But there are too
many people who profess religion and would probably be insulted if charged with being
irreligious or non-Christian, who at the same time refuse to unite with their fellows in the
effective practice of religion. They tell us that they do not believe in organizational religion.
</p><p>
RELIGIOUS ISOLATIONISM
</p><p>
Who has not heard amiable, good men say: "I have my own religion and do not need to be
bolstered up by church affiliation to have a good life? Even if that were so, it may still be
that others need the bolstering up their superior strength would afford, and after all, they owe
some obligation to those who need their help. But apart from that, if this sinking, trouble-torn
world-order is to be saved through a resurgence of religious fervor, then it is incumbent on
every believer to throw in with his might. We hear much said these days about isolationism
and isolationists. The least excusable form of isolationism and the most reprehensible of
isolationists is that one who holds himself aloof and refuses to lend his
strength with fellow-believers to the supreme job of saving civilization and the world.
</p><p>
The gospel taught by Jesus is a gospel of action. It does not consist in a passive profession of
faith. Of himself, Jesus said that he came to do the Father's will
(<span class="citation" id="27408"><a href="javascript:void(0)" onclick="sx(this, 27408)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(27408)">John 5:30</a></span>), not to talk about or profess
it. He made a parable about the man who heard his sayings and did them not, likening him to
a foolish man who built his house upon the sand, and when the rains descended and the
floods came and the winds blew and beat upon that house, it fell because it was built upon the
sand. That man who heard his sayings and did them he likened to a wise man who built his
house upon the rock, and it withstood the fury of rain and flood and tempest
(<span class="citation" id="34352"><a href="javascript:void(0)" onclick="sx(this, 34352)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(34352)">Matt. 7:24-27</a></span>).
</p><p>
A GREAT BROTHERHOOD
</p><p>
The Christian church was not established by isolationists who separated themselves from each
other or the body of believers. They were formed into worshipping bodies who collectively
fought their way to victory against dire persecutions, torture, and death. They constituted
themselves a great brotherhood cemented together for the fulfillment of a purpose in which
they believed. Let him who in placid aloofness luxuriates in the freedom and comfort and
security and ease which Christianity has brought to the nations, contemplate what his status
might have been if there had been no Christian church.
</p><p>
Organization is but another name for order and stability. Its opposite is turmoil and confusion
and weakness and ultimate disintegration. If no political body in the world has ever been able
to exist without orderly coordinated authoritative organization, how can it be presumed that
religion can carry on its high commission to resuscitate a sagging world without the church
which is the organizational instrumentality through which it carries out its great work? Here is
reason enough for a church.
</p><p>
EFFECT ON FAMILY LIFE
</p><p>
There is one other vital consideration, namely, the effect on family life and succeeding
generations of the neglect of participation in organized church practices. A few years ago I
recited from this pulpit the story of a disturbed woman's perplexities. She had just visited a
dear friend of her college days who by then had a well-grown daughter and a son. She was
both embarrassed and shocked by the behavior of these children. The boy came and went as
he pleased, and no questions asked or answered. The mother's admonitions and protests
against the indelicate indiscretions of the daughter in her behavior with young men were met
with jeers at the mother's prudery and lack of sophistication. The last night of her visit, she
was awakened by a disturbance in the house. The girl had come home from
a late party thoroughly intoxicated and was leading her escort in like condition to her room
when they were intercepted by the aroused parents. A noisy scene ensued before the boy was
finally sent off home and the girl put to bed. So the embarrassed visitor went home to clear
her head and do some thinking. She remembered the home environment in which she was
reared.
</p><p>
The religious note was strong in that home. The Bible was read and believed in. Daily the
family on their knees talked to God who was revered and was a reality. They were
church-going people and set apart one day a week as a holy day on which to do reverence to
the Author of life. They sang majestic hymns which carried messages to their expanding
souls. They heard the simple, direct words of the gospels whose grandeur somehow carried
over into their hearts and furnished their ideals for living. These ideals, through practice, were
silently woven into the pattern of their lives, and they came out with established characters
and stable guides to conduct which made them secure against the waves of laxity which
washed about them with the passage of time. Her home and family experience were typical of
those of the youth of her time including the friend she had just visited. That friend, along
with herself, in the days of their girlhood association had spontaneously as a matter of habit
and acceptance observed the conventions and proprieties.
</p><p>
She explained that she and her friend and their associates had in their college years given up
the simple faith of their youth, had ceased to give credence to the beliefs which had sustained
them had given up their Bible reading and their church-going and their Sabbath observance
and their prayers. They could live the good life without these "artificial props." They didn't
need the church. They said they had their own religion, but really it had shriveled up to a
mere code of ethics now cut loose from its roots and no longer nourished from the parent
stem. Then with an incredible lack of recognition of the relation of cause and effect, she
professed amazement at the moral bankruptcy of her friend's children. The truth was that
these children by the neglect of their parents had been cut off from the very character-forming
influences upon which her own character, and her friend's character and the character of their
generation had depended for formation and growth.
</p><p>
RELIGION A STABILIZER
</p><p>
While the instance I have cited may in some aspects be extreme, it nevertheless illustrates a
result naturally to be expected. The moral foundations established through active participation
in the activities of the church may carry through for one generation but scarcely go beyond
that. When parents detach themselves from active church affiliations and
leave their children free to neglect it too, they have no right to be surprised when their
children fall below their own standards. Religion is a powerful stabilizer, and the church is
the medium through which it is made effective.
</p><p>
I have but merely mentioned some of the reasons why there must be a church if religion is to
be a force in the world or wield any influence or power. Many other cogent reasons will
occur to you.
</p><p>
The church, however, is but a dry and barren mechanism unless energized by the burning
faith of a vital religion. That is the spark that gives it life.
</p><p>
It would seem to be the part of wisdom that all professing the same creed, the rich and the
poor, the mighty and the humble, the laborer and the professional man, the unlearned and the
scholar should rally together and with united strength exert a power in the land.
</p><p>
SOLID CONVICTION REQUIRED
</p><p>
To merit the name, religion must rest on solid conviction. It must stand for something. It
cannot temporize or compromise. The Christian church rests on the premise that Jesus is the
Son of God, the resurrected Lord, the author of eternal life for man. So long as it stood
unyielding on that base, it was a force in the world. When the guardians of the faith, in their
several denominations, wavered and watered the doctrines down till the virtue was gone out
of them, they ceased to be the prop and support to morality and political prosperity which
Washington said was indispensable. So long as that is the case, the world will totter and reel.
We seem to be trying now to rear a government whose proponents and sponsors cannot even
invoke divine blessing upon their deliberations or its destiny. What chance do you think it has
to heal the wounds of the world?
</p><p>
If religion is a necessary prop to the political government, so likewise does the religious body,
the Church, need for the protection of its guaranteed freedom a righteously administered civil
government, which depends upon an intelligent and jealously guarded use of the franchise.
That is the citizen's protection against abuse and usurpation.
</p><p>
So far as Latter-day Saints are concerned, I pray that as President Smith admonished at the
beginning of this conference, they may have the wisdom and honesty to put their religion
above their partisan politics and unite together as a solid phalanx to weed out prostitution of
power and debauchery and subversion of the God ordained freedom guaranteed by the
glorious Constitution of this land, by voting into office without regard to party affiliation
those who will preserve it unpolluted and uncorrupted, the protector and guarantor of
individual liberty.
</p>
</div>
</div>
