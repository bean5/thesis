<?xml version="1.0" encoding="UTF-8"?>
<div class="gcera">
<div class="gchead">
<p class="gctitle">"Come, Follow Me"</p>
<p class="gcspeaker">Elder Thomas S. Monson</p>
<p class="gcspkpos">Of the Council of the Twelve</p>
<p class="gcbib">Thomas S. Monson, <i>Conference Report</i>, April 1967, pp. 55-58</p>
</div>
<div class="gcbody">
<p>
To the east and a little south from where we are now meeting, marking the entrance to the
valley of the Great Salt Lake, standing as a sentinel pointing the way, is located "This Is the
Place" monument. Here we see featured Brigham Young, his back turned to the privations,
hardships, and struggles of the long desert way, his outstretched arm pointing to the valley of
precious promise.
</p><p>
<b>Expectations of promises to be fulfilled brought pioneers to this land</b>
</p><p>
Miles that once took months are now traveled in minutes. The many hundreds of thousands of
visitors who each year pause at the monument tingle with the spirit of pioneer tradition. Such
tradition reaches its high point on Pioneer Day, July 24 of each year. A grateful posterity sets
aside the busy cares of our fast-moving world and reflects for a moment on the everlasting
principles that helped to guide those noble pioneers to their promised land.
</p><p>
That first trek of 1847, organized and led by Brigham Young, is described by historians as
one of the great epics of United States history. Mormon pioneers by the hundreds suffered
and died from disease, exposure, or starvation. There were some who, lacking wagons and
teams, literally walked the 1,300 miles across the plains and through the mountains, pushing
and pulling handcarts. In these groups, one in six perished.
</p><p>
For many the journey didn't begin at Nauvoo, Kirtland, Far West, or New York but rather in
distant England, Scotland, Scandinavia and Germany. Tiny children could not fully
comprehend nor understand the dynamic faith that motivated their parents to leave behind
family, friends, comfort, and security. A little one might inquiringly ask,
"Mommy, why are we leaving home? Where are we going?"
</p><p>
"Come along, precious one; we're going to Zion, the city of our God."
</p><p>
Between the safety of home and the promise of Zion stood the angry and treacherous waters
of the mighty Atlantic. Who can recount the fear that gripped the human heart during those
perilous crossings? Prompted by the silent whisperings of the Spirit, sustained by a simple, yet
abiding faith, they trusted in their God and set sail on their journey. Europe was behind,
America ahead.
</p><p>
On board one of those overcrowded and creaking vessels of yesteryear were my great
grandparents, their tiny family, and a few meager possessions. The waves were so high, the
voyage so long, the quarters so cramped. Tiny Mary had always been frail, but now, with the
passage of each day, her anxious mother knew the little one was becoming especially weak.
She had taken seriously ill. No neighborhood drugstore. No family doctor. No modern
hospital. Just the steady roll of the tired, old ship. Day after day worried parents peered for
land, but there was no land. Now Mary could not stand. Lips that were too weak to speak just
trembled with silent but eloquently expressed wonderment. The end drew near. Little Mary
peacefully passed beyond this veil of tears.
</p><p>
As the family and friends gathered on the open deck, the ship's captain directed the service,
and that precious, ever-so-small body, placed tenderly in a tear-stained canvas, was committed
to the angry sea. Strong father, in emotion-choked tones, comforted grieving mother,
repeating, "The Lord gave, and the Lord hath taken away; blessed be the name of the Lord
(<span class="citation" id="26727"><a href="javascript:void(0)" onclick="sx(this, 26727)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(26727)">Job 1:21</a></span>).
We'll see our Mary again!"
</p><p>
Such scenes were not uncommon. Tombstones of sage and rock marked tiny graves the entire
route from Nauvoo to Salt Lake City. Such was the price some pioneers paid. Their bodies
are buried in peace, but their names live on evermore.
</p><p>
Tired oxen lumbered, wagon wheels creaked, brave men toiled, war drums sounded, and
coyotes howled. Our faith-inspired and storm-driven ancestors pressed on. They, too, had their
cloud by day and pillar of fire by night.
</p><p>
Often they sang: 
</p><p>
"Come, come ye Saints, no toil nor labor fear; <br />
         But with joy wend your way.<br />
         Though hard to you this journey may appear,<br />
         Grace shall be as your day . . .<br />
         "All is well. All is well."<br /><br />
&nbsp; &nbsp; (<i>Hymns</i>, 13.)
</p><p>
These pioneers remembered the words of the Lord: "My people must be tried in all things,
that they may be prepared to receive the glory that I have for them, even the glory of Zion"
(<span class="citation" id="15080"><a href="javascript:void(0)" onclick="sx(this, 15080)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(15080)">D&amp;C 136:31</a></span>).
</p><p>
As the long, painful struggle approached its welcomed end, a jubilant spirit filled each heart.
Tired feet and weary bodies somehow found new strength.
</p><p>
<b>Fulfillment did not come at first</b>
</p><p>
Time-marked pages of a dusty pioneer journal speak movingly to us: "We bowed ourselves
down in humble prayer to Almighty God with hearts full of thanksgiving to Him, and
dedicated this land unto Him for the dwelling place of His people."
</p><p>
All struggle had not ceased; privation and hardship had not disappeared. Mrs. Rebecca Riter
describes Christmas Day in 1847 in the valley of the Great Salt Lake: "The winter was cold.
Christmas came and the children were hungry. I had brought a peck of wheat across the plains
and hid it under a pile of wood. I thought I would cook a handful of wheat for the baby.
Then I thought how we would need wheat for seed in the spring, so I left it alone."
</p><p>
The crude homes were described by a small boy in these terms: "There was no window of
any kind whatever in our house. Neither was there a door. My mother hung up an old quilt,
which served as a door for the first winter. This was our bedroom, our parlor, our sitting
room, our kitchen, our sleeping room, everything in this room of about 12 x
16 feet. How in the world we all got along in it I do not know. I recollect that my dear old
mother stated that no queen who ever entered her palace was ever more happy or proud of
shelter and the blessings of the Lord than was she when she entered that completed dugout."
</p><p>
Such were the trials, the hardships, struggles, and heartaches of a former day. They were met
with resolute courage and an abiding faith in a living God. The words of their Prophet leader
provided their pledge: "And this shall be our covenant&mdash;that we will walk in all the
ordinances of the Lord" (<span class="citation" id="15079"><a href="javascript:void(0)" onclick="sx(this, 15079)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(15079)">D&amp;C 136:4</a></span>).
</p><p>
<b>But can come today</b>
</p><p>
The passage of time dims our memories and diminishes our appreciation for those who
walked the path of pain, leaving behind a tear-marked trail of nameless graves. But what of
today's challenge? Are there no rocky roads to travel, no rugged mountains to climb, chasms
to cross, trails to blaze, or rivers to ford? Or is there a very present need for that pioneer
spirit to guide us away from the dangers that threaten to engulf us and lead us rather to a
Zion of safety?
</p><p>
In the two decades since the end of World War II standards of morality have lowered and
lowered. Today we have more people in jail, in reformatories, on probation, and in trouble
than ever before. From the padded expense account to grand larceny, from petty crimes to
crimes of passion, the figures are higher than ever and going higher. Crime spirals upward!
Decency careens downward! Many are on a giant roller coaster of disaster, seeking the thrills
of the moment while sacrificing the joys of eternity. We conquer space but cannot control
self. Thus we forfeit peace.
</p><p>
Can we somehow muster the courage, that steadfastness of purpose, that characterized the
pioneers of a former generation? Can you and I, in actual fact, be pioneers today? The
dictionary defines a pioneer as "<i>one who goes before, showing others the way to follow</i>." Oh,
how the world needs such pioneers today!
</p><p>
We forget how the Greeks and Romans prevailed magnificently in a barbaric world and how
that triumph ended, how a slackness and softness finally came over them to their ruin. In the
end, more than they wanted freedom, they wanted security, a comfortable life; and they lost
all&mdash;security and comfort and freedom. From the confusion of our modern world, sincere
persons searchingly ask themselves: "To whom shall we listen? Whom shall we follow?
Whom shall we serve?"
</p><p>
Today, chronic strife even permeates the personal province of the Prince of Peace. Contention
thrives where taught he who declared, ". . . contention is not of me, but is of the devil"
(<span class="citation" id="5704"><a href="javascript:void(0)" onclick="sx(this, 5704)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(5704)">3 Ne. 11:29</a></span>).
However, when we have ears that truly hear, we will be mindful of the echo from
Capernaum's past. Here multitudes crowded around Jesus, bringing the sick to be healed; a
palsied man picked up his bed and walked
(<span class="citation" id="33200"><a href="javascript:void(0)" onclick="sx(this, 33200)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(33200)">Mark 2:3-12</a></span>), and a Roman centurion's faith restored his
servant's health (<span class="citation" id="35713"><a href="javascript:void(0)" onclick="sx(this, 35713)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(35713)">Matt. 8:5-13</a></span>). Many turn away from our Elder Brother, who said, "I am the way, the truth
and the life" (<span class="citation" id="28348"><a href="javascript:void(0)" onclick="sx(this, 28348)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(28348)">John 14:6</a></span>), and rather follow blindly after that Pied Piper of sin who would lead us down
the slippery slopes to our own destruction. He cunningly calls to troubled youth in truly
tempting tones: "Just this once won't matter." "Everyone is doing it." "Don't be
old-fashioned."
</p><p>
Thank God for the spirit expressed by one youth who saw through Satan's deceit and boldly
declared: "I am old-fashioned enough to believe in God, to believe in the dignity and potential
of his creature, man; and I am realistic, not idealistic, enough to know that I am not alone in
these feelings." (<i>Look</i>, January 12, 1965.)
</p><p>
President David O. McKay paid faithful youth the highest compliment when he recently said:
"There has never been a time when we had greater reason to be proud of our young people
than at present."
</p><p>
The unsatisfied yearnings of the soul will not be met by a never-ending quest for joy midst
the thrills of sensation and vice. Vice never leads to virtue. Hate never points to love.
Cowardice never reflects courage. Doubt never inspires faith.
</p><p>
It is not difficult to withstand the mockings and unsavory remarks of foolish ones who would
ridicule chastity, honesty, and obedience to God's commands. The world has ever belittled
adherence to principle. Times change. Practices persist. When Noah was instructed to build an
ark, the foolish populace looked at the cloudless sky, then scoffed and jeered&mdash;until the rain
came.
</p><p>
In the Western Hemisphere, those long centuries ago, people doubted, disputed, and disobeyed
until the fire consumed Zarahemla, the earth covered Moronihah (<span class="citation" id="5701"><a href="javascript:void(0)" onclick="sx(this, 5701)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(5701)">3 Ne. 8:8-10</a></span>), and water engulfed the land
of Moroni. Jeering, mocking, ribaldry, and sin were no more. They had been replaced by
sullen silence, dense darkness. The patience of God had expired, his timetable fulfilled.
</p><p>
Must we learn such costly lessons over and over again? When we fail to profit from the
experiences of the past, we are doomed to repeat them with all their heartache, suffering, and
anguish. Haven't we the wisdom to obey him who designed the plan of salvation&mdash;rather
than that serpent who despised its beauty?
</p><p>
In the words of the poet, "Wouldst thou be gathered to Christ's chosen flock, shun the broad
way too easily explored, And let thy path be hewn out of the rock, The living rock of God's
eternal word." (William Wordsworth.)
</p><p>
Can we not follow the Prince of Peace (<span class="citation" id="25326"><a href="javascript:void(0)" onclick="sx(this, 25326)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(25326)">Isa. 9:6</a></span>), that pioneer who literally showed the way for others
to follow? His divine plan can save us from the Babylons of sin, complacency, and error. His
example points the way. When faced with temptation, he shunned it. When offered the world,
he declined it. When asked for his life, he gave it!
</p><p>
"'Come, follow me'(<span class="citation" id="31717"><a href="javascript:void(0)" onclick="sx(this, 31717)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(31717)">Luke 18:22</a></span>), the Savior said,<br />
         Then let us in his footsteps tread,<br />
         For thus alone can we be one<br />
         With God's own loved, begotten Son.<br />
         "For thrones, dominions, kingdoms, powers,<br />
         And glory great and bliss are ours<br />
         If we, throughout eternity,<br />
         Obey his words, 'Come, follow me.'"
</p><p>
&nbsp; &nbsp; (<i>Hymns</i>, 14.)
</p><p>
Now is the time. This is the place. May we follow him, I pray in the name of Jesus Christ.
Amen.
</p>
</div>
</div>
