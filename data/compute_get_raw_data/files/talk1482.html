<?xml version="1.0" encoding="UTF-8"?>
<div class="gcera">
<div class="gchead">
<p class="gctitle">How One Can Know That God Lives</p>
<p class="gcspeaker">Elder Paul H. Dunn</p>
<p class="gcspkpos">Of the First Council of the Seventy</p>
<p class="gcbib">Paul H. Dunn, <i>Conference Report</i>, April 1966, pp. 121-124</p>
</div>
<div class="gcbody">
<p>
My beloved brothers and sisters, and I include all who listen in that salutation, I have been
moved by the testimonies and the witnesses that have borne evidence again these past days
and this morning to me of the truthfulness of the gospel. I am grateful for this opportunity to
declare to you the inner feelings of my heart. It was just a few days ago that my little
six-year-old daughter, Kellie, came rushing up to me and, throwing herself into my arms with
all the exuberance that only youth can display, she said, "Daddy did you know it is only three
more days until Easter?" I assured her that I did. Then she, with an anxious look, wanted to
know just what it all meant, and so we took a moment to visit. We talked about the eternal
things that have been so prominent in this conference. As we discussed the eternal verities of
the gospel of Jesus Christ on a six-year-old plane, I commenced to think about the real
meaning of Easter, as we do on these occasions. It reminded me of the delightful poem that
Grace Daniels has recorded for us. Let me share it with you: 
</p><p>
&nbsp; &nbsp; &nbsp; &nbsp; "EASTER IS COMING"
</p><p>
"'Easter is coming,'" I said to a boy,<br />
     A wee, little lad, by the way; <br />
     His eyes were bright and he smiled with delight<br />
     As he quickly looked up from his play.<br />
     'Oh, yes, I know Easter, for that is the time<br />
     When the bunny brings eggs red and blue,<br />
     And inside they're just like what old chickie lays,<br />
     But some are real candy too.'
</p><p>
"'Easter is coming,' I said to a maid,<br />
     With brown eyes and shining brown hair.<br />
     I looked in her eyes and not with surprise<br />
     Saw the dreamlight of youth resting there.<br />
     'Yes, I know it is coming,' she shyly replied,<br />
     'And if you never will tell,<br />
     There's a wedding that day and I'm going away<br />
     To dear little home on the hill.'
</p><p>
"'Easter is coming,' I said to a man,<br />
     To whom middle-age brought no reprieve.<br />
     His silvering hair told of worry and care,<br />
     And his voice held a note of peeve.<br />
     'Don't talk about Easter, that's all I can hear,<br />
     Easter hats, Easter gowns, Easter shoes,<br />
     And for ruffles and frills, old Dad pays the bills,<br />
     Do you wonder I'm down with the blues?'
</p><p>
"'Easter is coming,' I said to a man<br />
     With bent form and beard white as snow.<br />
     His dim eyes grew bright with a wonder light<br />
     And his withered old face was aglow.<br />
     'Ah, friend, 'tis a message I fain would proclaim<br />
     To striving humanity.<br />
     To me it means life, resurrection of youth,<br />
     To endure through eternity.'
</p><p>
"I pondered their answers for many a day,<br />
     For each with its meaning was fraught,<br />
     And each one so different, yet, right in its way,<br />
     But what was the answer I sought?<br />
     Must pleasure come foremost, whatever the cost,<br />
     While life, youth, and love have their day?<br />
     And must the true meaning of Easter be lost<br />
     Till we come to the end of the way?
</p><p>
"As springtime approaches with beckoning hands<br />
     And the promise of things 'born anew,'<br />
     And Easter draws near with its myriad of plans,<br />
     Just what does it mean to you?"
</p><p>
I am sure to many of us it means new clothes, perhaps a vacation from school, spring at last,
or the beginning of baseball season. These are all quite wonderful and vital to us, but they are
not the real reasons we celebrate Easter.
</p><p>
Just a few weeks ago I stood beside the casket of a very close friend who had been taken in
the prime of his life, leaving a young widow and four tiny children. And as we stood in that
sacred room while the family said their last goodbyes, my, it tugged at my heartstrings to
watch a little four-year-old boy slip his hand into that of his mother and, wistfully looking up
at her, ask the question, "Mama, will we ever see Daddy again?" I am sure this scene has
been repeated many times throughout all the world, because death brings us face to face with
the question of the ages. To quote Job as we frequently do at this time of the year, "If a man
die, shall he live again?" (<span class="citation" id="26721"><a href="javascript:void(0)" onclick="sx(this, 26721)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(26721)">Job 14:14</a></span>).
</p><p>
<b>Hope for the future</b>
</p><p>
It was just 21 years ago on another Easter morning when a great armada of ships assembled
in the bay off the island of Okinawa. And on that Easter morn as I looked upon the faces of
those who were to take the beach, one of the great, great questions of all the ages seemed
again to be registered by those men. "What hope is there in the future?"
</p><p>
The answer came to me, I believe, in the midst of one of my darkest hours. As I pushed
ashore with my buddies, I crawled a few feet into the sand, and there I found a young soldier
in the last moment of this life. I didn't know his name, nor could I tell you to which faith he
belonged. As I tried to give him a little bit of comfort, his last words were these: "Out of this
filth, death, destruction, will come a new world and a new way of life." In the face of what
seemed to be his defeat he saw the real victory. And almost in a providential way, just a few
yards from where he lay was a patch of Easter lilies, signifying to those who would observe
the new birth and the new way of life. It was later I discovered that Okinawa was the Easter
lily capital of the Orient.
</p><p>
It is when we encounter experiences such as these that questions are often raised that one
wants to know, and rightly so: How can we know the reality of the resurrection? Is it true?
One of the great educators of our Church, Dr. Lowell Bennion, has listed for us four ways by
which we can come to know truth or reality. First, he says, by accepting it on the authority of
someone else, second, by thinking; third, by experiencing; and fourth, by feeling, which we in
this Church would call inspiration or revelation.
</p><p>
Let me just discuss for a moment each of these channels by which we come to know.
</p><p>
<b>Authority</b>
</p><p>
First, <i>authority</i>. There was a time when a prodigious mind, such as that of Aristotle and
Herbert Spencer, could survey the entire field of human knowledge and draw conclusions. But
with the great accumulation of knowledge that has been derived through specialization, no
single person can grasp all of the learning that is now available to mankind. For this reason
man is compelled to rely upon the experience or authority of others for some of his
information. Each of us turns to the doctor, the dentist, the lawyer, the teacher, the mechanic,
the spiritual leaders, and many other persons for guidance in particular problems. The student
of chemistry, for example, does not begin from scratch to rely upon his own experience. He
uses the efforts of the teacher, the text, the reference book, and other sources of authority. To
bypass such a vast accumulation of knowledge would be folly indeed.
</p><p>
Likewise, in religion we have preserved for us the sayings and teachings and testimonies of
Moses, of Amos, of Paul, of Alma in the Book of Mormon; of Joseph Smith in his life and
teachings; and of course, of the Christ. These were not persons who were eccentric, but
individuals who were significant in stature, living in real-life situations, claiming wisdom
from God, and bearing personal testimony that these things that are recorded in our scriptures
are indeed true. They too deserve an honest hearing.
</p><p>
<b>Reason</b>
</p><p>
Second, <i>reason</i> or <i>thinking</i>. In man's search for truth, the mind plays a leading role. Man, as
a child of God, was created in the image of his Heavenly Father, the glory of whose
intelligence is reflected in the beauty and orderliness of the universe. Why should man, God's
child living in his world, not trust his own mind and use it earnestly as one avenue by which
he can come to know the truth about reality&mdash;in this case, the resurrection?
</p><p>
The mind has the ability to weave the separate experiences of life into larger and more unified
views. Each day the mind is bombarded with numerous ideas, impressions, perceptions, and
feelings from without and from within. These enter the mind in a disorganized and
miscellaneous fashion, but the human mind has the ability to bring a measure of order out of
chaos by establishing meaningful relationships among phenomena that it experiences.
</p><p>
However, reason alone is not a sufficient guide to truth. For, as Goethe has said, "Human life
divided by reason leaves a remainder." Through reason alone one cannot choose a mate, find
God, or determine all things of greatest value.
</p><p>
<b>Experience</b>
</p><p>
Third, experience. One of the most trustworthy avenues to truth lies along the path of
experience. Each of us has a rich amount of it, for it is common to all. In the affairs of
everyday life, we learn to trust experience. We learn sweetness by taste, softness by touch,
colors by sight, and joy and sorrow, love and hate directly in life situations. There is no
substitute for experience, and without it we cannot know the truth.
</p><p>
Two types of experience
have been described: that which is based on science and its discoveries and that which is
common to all of us in everyday life. The latter, which is a nonscientific experience, is just as
real and may also be a valid source of knowledge, but it is often either more general or more
unique and, therefore, somewhat more difficult to communicate to others.
</p><p>
Experiences of this type play an important role in religion. Many religious principles can be
practiced and experienced in everyday life. The validity of religion does not rest on faith
alone. We feel and observe the effects of selfishness, greed, lust, and hate. We also observe
and feel the opposite effects of unselfishness, generosity, purity of heart, and love. Faith and
repentance and forgiveness are not abstract principles but are real parts of life. Prayer and
worship are religious experiences for those who participate in them with faith.
</p><p>
<b>Revelation</b>
</p><p>
And finally, <i>revelation</i>. Despite the great emphasis on reason and the experience of science,
inspiration (or intuition, as some have preferred to define it) also plays a very important role
in discovering truth. Scientists have testified that some of their most profound insights have
come to them, not in the labored process of logical thought, but as unexpected, unpremeditated
hunches, possibly as flashes from the imagination, the subconscious mind, or even from God.
They, too, recognize inspiration as a source of knowledge.
</p><p>
Revelation is communication from God to man. It is another avenue to truth, to a correct
knowledge of reality. Revelation includes all the other avenues.
</p><p>
A prophet is not without experience in human life, for he lives among men and with himself.
He is not insensitive to good and evil, right and wrong, joy and sorrow, life and death.
Questions and problems come to his mind. He thinks, he reflects, and he searches for the
answers; and then&mdash;and this step is distinctive in the life of a prophet&mdash;he turns to God in
humility and faith. When the answer comes, it is usually not in an audible tone, although it
can be and often has been in both former and latter days, but more often it comes through the
"still, small voice" (<span class="citation" id="1234"><a href="javascript:void(0)" onclick="sx(this, 1234)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(1234)">1 Kgs. 19:12</a></span>) of
the Comforter. This Comforter, the Spirit of Truth
(<span class="citation" id="28288"><a href="javascript:void(0)" onclick="sx(this, 28288)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(28288)">John 15:26</a></span>), clarifies the mind of
the prophet and causes his bosom to burn within him so that he knows the will of God. Then
he declares it to man. The testimony or reality of these things can be the personal experience
of every honest, seeking individual in the world. T through these channels man has come to
know the reality of Christ's life, divine mission, death, and eventual resurrection.
</p><p>
<b>"If a man die, shall he live again?"</b>
</p><p>
In answer to the questions: "If a man die, shall he live again?" and "What hope is there for
the future?" I summarize the words of our prophet seer, and revelator, President David O.
McKay, who spoke yesterday: 
</p><p>
To sincere believers in Christianity, to all who accept Christ as their Savior, his resurrection is
not a symbol but a reality. As Christ lived after death so shall all men, each taking his place
in the next world for which he has best fitted himself. With this assurance, obedience to
eternal law should be a joy, not a burden, for compliance with the principles of the gospel
brings happiness and peace. "He is not here," said a witness many years ago, "but is risen"
(<span class="citation" id="31693"><a href="javascript:void(0)" onclick="sx(this, 31693)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(31693)">Luke 24:6</a></span>).
Because Christ does live, so shall we. And then President McKay bore his sacred
witness to that effect.
</p><p>
And I would like to declare to President McKay and to all of you this day that I too know
that my Redeemer lives, and I give you that witness in the name of Jesus Christ. Amen.
</p>
</div>
</div>
