<?xml version="1.0" encoding="UTF-8"?>
<div class="gcera">
<div class="gchead">
<p class="gctitle">The Test of Propriety in Conduct</p>
<p class="gcspeaker">Elder Albert E. Bowen</p>
<p class="gcspkpos">Of the Council of the Twelve Apostles</p>
<p class="gcbib">Albert E. Bowen, <i>Conference Report</i>, October 1944, pp. 152-158</p>
</div>
<div class="gcbody">
<p>
Confusion seems admittedly to be the dominant characteristic of our times. There is confusion
of procedures, a baffling contrariety as to what ought to be done and how to do it. There
seems to be agreement about only one thing, namely, that the world's affairs are terribly
messed up. But there is no agreement about the causes for the sorry condition, nor the
remedy. Men confess, almost it seems with pride, that they don't know what they ought to
believe. They are bewildered and overwhelmed with a sense of futility.
</p><p>
TESTING PRESENT CONDITIONS BY PRINCIPLE OF RIGHT OR WRONG
</p><p>
One thing seems clear: our perplexity grows out of a failure of vision&mdash;of penetrating insight.
We get ourselves all tangled up in a maze of superficialities and mistake consequences for
causes. We tell ourselves over and over again that life in this day has become very complex;
that it is not simple and elementary any more as it once was; and that our outlook and
approaches to the problems of the day must take on the same complexities as the intricate web
of mechanisms we have woven about ourselves.
</p><p>
It may be granted that with our great increase in population, our shifting over from simple
rural life to concentrations of great numbers in industrial centers, the consequent change from
self direction to supervised direction, the increasing degree in which the free practice of
individual convenience impinges upon the comfort and convenience of others; the impact
upon our lives of changed conditions resulting from inventions, transportation and
communication facilities&mdash;it may be granted, I say, that all these conspire to introduce an
apparent complexity into our organized lives. But I wonder if, after all, the differences are not
largely superficial and mechanistic rather than fundamental.
</p><p>
Are there not, in reality, underlying, universal principles with reference to which all issues
must be resolved whether the society be simple or complex in its mechanical organization? It
seems to me we could relieve ourselves of most of the bewilderment which so unsettles and
distracts us by subjecting each situation to the simple test of right and wrong. Right and
wrong as moral principles do not change. They are applicable and reliable determinants
whether the situations with which we deal are simple or complicated. There is always a right
and a wrong to every question which requires our solution. We might be saved a lot of misery
and discontent and disputation in this world if we just stopped to apply the simple test, "what
is the right of this thing" before we moved into action concerning it. By thus getting down to
the root of the matter we should have reduced the problem to its simplest terms and it would
not matter very much whether it was crusted over with a simple or a complex layer of
incidental elements. They would all have to yield to the basic law of right.
</p><p>
AN ILLUSTRATION FROM PAUL'S MISSIONARY EXPERIENCE
</p><p>
I think I can illustrate how basic issues are buried under a cover of superficialities by
reference to an experience in the life of Paul. In the course of his missionary journeys, he
came to Ephesus where he found certain poorly instructed believers. He taught in the
synagogue for three months when, because of opposition, he separated his disciples, and they
went their ways teaching for a period of two years with such effect that the record says: "All
they which dwelt in Asia heard the word of the Lord Jesus Christ"
(<span class="citation" id="7527"><a href="javascript:void(0)" onclick="sx(this, 7527)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(7527)">Acts 19:10</a></span>). This brought the Christian
message squarely up against the idolatry of the Ephesians with the result that there was a
great conversion from idolatry. Says the account in Acts 19:23-29:
</p><p>
And the same time there arose no small stir about that way. For a certain man named
Demetrius, a silversmith, which made silver shrines for Diana, brought no small gain unto the
craftsmen; whom he called together with the workmen of like occupation, and said, Sirs, ye
know that by this craft we have our wealth. Moreover ye see and hear, that not alone at
Ephesus, but almost throughout all Asia, this Paul hath persuaded and turned away much
people, saying that they be no gods, which are made with hands: So that not only this our
craft is in danger to be set at nought; but also that the temple of the great goddess Diana
should be despised, and her magnificence should be destroyed, whom all Asia and the world
worshippeth. And when they heard these sayings they were full of wrath. . . . And the whole
city was filled with confusion
(<span class="citation" id="7528"><a href="javascript:void(0)" onclick="sx(this, 7528)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(7528)">Acts 19:23-29</a></span>).
</p><p>
Now, the fundamental issue, and the only issue, was between the teachings of Jesus and the
pagan religion of the Ephesians. But that issue was completely buried under the furore
engendered by a purely incidental consequence. Paul was teaching the way of life, a thing of
transcendent importance to all the race of men, the future of the world. With the purely
collateral consequence to the business of a few silversmiths and art craftsmen he had no
concern.
</p><p>
But the incident was not decided on the merits of the respective doctrines concerning the
souls and destiny of men. So far as immediate results were concerned a superficial
materialism completely smothered and took out of the reckoning the fundamental moral and
spiritual issue involved. For Paul's companions were taken into custody, and when he would
have gone publicly to their defense, he was restrained by friends but for which restraint his
life likely would have been taken
(<span class="citation" id="7529"><a href="javascript:void(0)" onclick="sx(this, 7529)">&nbsp;</a><a href="javascript:void(0)" onclick="gs(7529)">Acts 19:29-31</a></span>).
</p><p>
APPLICATION TO PRESENT-DAY CONDITIONS
</p><p>
In one way or another the process illustrated in this incident has been repeating itself
throughout history. Every would-be world conqueror from Alexander on down and almost
every empire builder, too, for that matter, has pursued his course in total disregard of the
question of what is right. They find it easy to obscure the moral issue by burying it deep
under an overburden of casuistries. It is easy for the powerful aggressor to say that his
country is denied access to raw materials; that it is overcrowded and must have <i>Lebensraum</i>,
an outlet for its over-congested population; free and open lanes of commerce, and to give
these and a thousand other specious reasons for his course. These are made to justify the
ruthless overpowering and destruction of weak neighboring states if perchance they fail to
bow to the conqueror's will or permit themselves to be absorbed into his ambitious design. A
color of righteousness may be given the whole monstrous scheme by pointing out the virtue
of the ultimate objective&mdash;to bring a larger good to his people and perchance also alleged
benefits to his conquered and plundered neighbors, just as Demetrius could justify his inciting
the mob against Paul by pointing to the threat of his teachings to their accustomed means of
making a livelihood.
</p><p>
However impressive the array of justifying reasons may be, when they are brushed away the
simple question left is whether the powerful have a right to crush the weak even to bring
added benefits to them. To this the conscience of humanity must answer with a resounding
"no!" If aggressors were willing to let the right of the thing be the final determiner there
would be no wars.
</p><p>
EMPLOYMENT OF EVIL MEANS INCONSISTENT
</p><p>
There is likewise a companion evil to the one just spoken of, just as reprehensible, though
perhaps not quite so clearly recognized. It is the case of a powerful state, apprehending attack
from another one, casting about for defensive means. It conceives that its security would be
best promoted if it possessed a strategic point owned by another state. In the interest of its
security it takes by force what it wants from its unwilling but powerless neighbor though the
latter has to be mercilessly crushed in the process. The conqueror justifies itself and is
justified by its apologists upon the plea of its own necessity. The basic immorality of the
matter is conveniently ignored. It is as if a man about to be killed by a thug perceives that by
liquidating his defenseless but innocent neighbor he can save his own skin. So far as the
morality of the thing is concerned, he would be as fully justified as would the overpowering
state.
</p><p>
In defense of such courses it is sometimes argued that where the objective is good, the end to
be achieved worthy, the means employed to attain it are justified, however bad in themselves
they may be. The idea is crystallized in the saying: "The end justifies the means." It is a
monstrously false doctrine. If this is a moral universe, as I believe it to be, no methods for
effecting change, however desirable the end sought, can properly be resorted to which are not
in themselves consistent with that end. To employ violence, oppressive coercion, cruelty,
injustice for the accomplishment of desired ends is to set loose forces of evil which must
inevitably weaken and, to a degree at least, nullify those ends. In the process of achievement
they lose their moral power. We see this truth exemplified almost every day. The employment
of evil means to achieve allegedly worthy ends threatens to destroy the efforts for lasting
peace which are so much in the public notice today.
</p><p>
A PLAN TO BRING PEACE
</p><p>
As nations indulge in such immoral practices as we have been talking about, so do individuals
and groups and organized bodies in their relations between and among themselves, and with
consequent disorders. If individuals in their dealings with each other subjected them to the test
of what is right and abided the result, there would be little opportunity for ill will or strife. If
groups or organizations seeking advantage to themselves against other groups or organizations
would sit down together each willing to subordinate self-interest in the search for the right,
and be controlled by it when found, there would be no warfare between or among them. Men
submit their differences to the judgment and decision of a court merely because they are too
childish and immature to sit down together and agree on what is right. They are in far better
position to arrive at the right than any court is because they know all the facts, whereas the
court never can have that complete knowledge. The judge is limited by the information which
a trial brings to him. If men earnestly wanted their differences settled on the moral basis of
right, there would be little work for courts.
</p><p>
Submission of differences, however, to the judgment of some disinterested body, such as a
court, is, of course, a long advance over the stage when men settled their private differences
by fighting it out&mdash;a resort to physical force. Any semblance of orderly society could not exist
on the basis of private redress of grievances. The state accordingly long ago took that over so
that if a dispute arises one doesn't kill the other party to the disagreement but calls upon the
machinery of the state to settle the matter. That marked a long step forward. But nations still
fight it out, which is a barbarous way of settling differences. It is not far removed, however,
from some of the means resorted to now for the settlement of class or group differences. In
many ways we are retrograding to the primitive status where disputants take settlement into
their own hands. We cannot well lay claim to being a grown-up, mature, civilized people until
we have come to the point where morality is the determinant, and we ask simply what is, in
good conscience, right. The conclusion seems inescapable that the confusion and distraction
and conflicts and antagonisms and uncertainties and bewilderment which plague the world
today present mankind with what is at bottom a purely moral issue&mdash;the issue between right
and wrong. That, then, should be the final test of the propriety of all courses of action.
</p><p>
But there are difficulties thrown in the way of getting that simple test adopted. One is that
there is current in the world today a school of thought which asserts that there is no such
thing as universal principles of right as opposed to wrong. They say that for the individual,
growth is a continuing "ongoing process without direction. That is, that we are continually
changing, growing but not toward any ultimate purpose. There are accordingly no fixed
principles by reference to which we may determine what we ought to do. If confronted with a
situation, all we can do is to experiment&mdash;try out the course we want to take, and if it works
out to the advantage of the experimenter, then for him it is right. Each one finds out for
himself according to his own interest. Of course this must inevitably result in confusion, and
ultimate chaos.
</p><p>
This is a deadly paralyzing notion to plant in the minds of people and particularly the
youthful and immature. It strikes down belief that man is a moral being with a purpose and a
destiny and commensurate responsibilities. It releases one who accepts it from all restraints of
conscience. It provides him with an allegedly scientific but basely false assurance that he is in
no wise responsible for his actions however vile they may be since they are after all but in the
course Of nature. Let such a notion as that gain general currency and you have dealt a
devastating blow to all organized society. A free government could no longer exist, for its
perpetuity must depend upon the moral integrity of its citizens. Only an absolute, iron-bound
despotism could deal with a situation like that.
</p><p>
THE PRESENT RELIGIOUS TREND
</p><p>
One of the most deep-seated issues of this world in our day is the issue between the concept
of man as a son of God possessed of an immortal soul with a God-given destiny and a
guiding purpose in life and the concept of man dispossessed of individual rights which must
lie universally respected, reduced to the status of a mere tool of an omnipotent state, the end
in itself to which man's life is subordinated.
</p><p>
The first of these is the foundation principle upon which our nation is founded. It is our
heritage from the fathers. It derives out of the teachings of the Master; it is an integral part of
our religious faith.
</p><p>
But it is fashionable to decry the teachings of religion upon the supposed ground that it is
authoritarian and by its pronouncements presumes to lay down for man rules of conduct and
observances which he should follow. Not believing in the omnipotence and infinite wisdom
and power of God, the objector views the directives of religion as an attempt on the part of
some man to settle forever all truths with which man is concerned and to deprive him of the
freedom of his own judgment. This, of course, entirely misconceives the claims and mission
and purposes of organized religion. If it is meant to assert that man out of his own finite
limitations is able, unguided by the voice of authoritative wisdom, to create for himself an
adequate guide for living, then the answer is that experience, the history of the race, does not
support the assumption.
</p><p>
It is not my purpose to conduct an argument about the contentions of the opponents of
authoritative religion or of the pragmatists. It is sufficient to say that wherever religion has
been discarded confusion and moral anarchy have followed. And that is one of the reasons for
the confusion in the political world today. Mr. C.E.M. Joad, an eminent English philosopher,
an atheist driven by events to reconsider his opinions, writes:
</p><p>
Where there is a large measure of general agreement in regard to ultimate ends, political
doctrines can be represented as means to their realization. Where, however, there are no
common ends to which the generality of men subscribe, political programs assume the status
of ends in themselves. In the nineteenth century there was a general agreement among
thinking people as to the nature and end of the individual. His nature was that of an immortal
soul; his end was to achieve eternal salvation. Thus, when men differed about politics&mdash;even
when they differed about ethics&mdash;their differences related to the best method of realizing the
individual's nature and achieving the individual's end. Moreover, there was, broadly speaking,
a general agreement, at least in the western democracies, as to the kind of society which it
was desirable to establish. Owing to the decline of traditional religion these agreements no
longer obtain, precisely because there is today no general acceptance of the view of the
individual as an immortal soul and no general reliance upon the hope of eternal salvation.
Consequently, political doctrines such as Fascism and Communism assume for the twentieth
century the status which religious doctrines possessed in the nineteenth; they are not, that is to
say, doctrines in regard to means to an agreed end, but doctrines in regard to ends about
which there is no agreement.
</p><p>
Thus is clearly brought into focus the danger of shifting away from old moorings. When
foundation principles are discarded, then shifting, vagrant, opportunistic substitutes for
principles take control and precisely because they are opportunistic they must shift with the
vagaries of changing popular moods. Stability&mdash;a steady march forward toward a fixed
goal&mdash;no longer is found.
</p><p>
It is for us to stand by the tried and proved principles of religion and the tried and proved
governmental principles which have so blessed our land.
</p><p>
That we may have the discerning wisdom and vision to do it and, at least among ourselves,
resolve all our differences on the basis of right, I pray, in the name of Jesus. Amen.
</p>
</div>
</div>
