//globals
{
	var fs = require('fs');
	var common = require('/lib/node_utils/common.js');

	var testing = process.env.testing === 'true' ? true : false;
	var derive_numeric_from_filename = process.env.derive_numeric_from_filename === 'true' ? true : false;

	var outputLocation = '/output/';
	var number_of_recommendations = parseInt(process.env.number_of_recommendations, 10);

	// Paths to files
	var paths = {
		// e.g. tf-idf and lda JSON
		input: [process.env.recommendation_set_1, process.env.recommendation_set_2],
		todo: outputLocation + 'todo.txt',
		// status: outputLocation + 'status.txt', // not really needed since this runs so fast
		settings: outputLocation + 'settings.json',
		debug: outputLocation + 'debug.txt',
		// e.g. tf-idf.csv or lda.csv
		out: [outputLocation + process.env.out_name1, outputLocation + process.env.out_name2],
	};

	var streams = {
		todo: fs.createWriteStream(paths.todo),
		// status: fs.createWriteStream(paths.status), // not really needed since this runs so fast
		settings: fs.createWriteStream(paths.settings),
		debug: fs.createWriteStream(paths.debug),
		out: [fs.createWriteStream(paths.out[0]), fs.createWriteStream(paths.out[1])],
	};
	streams.settings.write(JSON.stringify({testing: testing, number_of_recommendations: number_of_recommendations, paths: paths}, null, '\t'));

	if(testing) console.warn('Warning: running in test mode');
	if(testing) fs.appendFileSync(paths.todo, 'TODO: Do not run in test mode!');
}
main();

/*
	Functions
*/
function main() {
	var sets = [];

	for(var i = 0; i < 2; i++) {
		set = fs.readFileSync(paths.input[0], 'utf8', function (err, data) {
			if (err) common.halt(err);
		});
		set = JSON.parse(set);
		convert_recs_to_csv(set, streams.out[i], number_of_recommendations);
	}

	close_streams(streams);
}

function close_streams(streams) {
	common.each(streams, function(name, stream) {
		if(Array.isArray(stream)) {
			close_streams(stream);
		}
		else {
			stream.end();
		}
	});
}

function convert_recs_to_csv(set, stream, number_of_recommendations) {
	write_header(stream, number_of_recommendations);
	common.each(set, function(doc_name, recommendations) {
		if(derive_numeric_from_filename)
			doc_name = common.obtain_numerics_from_filename(doc_name);
		stream.write(doc_name);

		common.each(recommendations, function(doc_name, value) {
			if(derive_numeric_from_filename)
				doc_name = common.obtain_numerics_from_filename(doc_name);
			stream.write(',' + doc_name);
		});
		stream.write("\n");
	});
}

function write_header(stream, number_of_recommendations) {
	stream.write('ID');
	for(var i = 1; i <= number_of_recommendations; i++) {
		stream.write(','+i);
	}
	stream.write("\n");
}
