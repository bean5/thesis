default:
	time make all -j

thesis:
	time make all -j

all: data/make-sentinels/compute_compare_recommendations data/make-sentinels/convert_recs_to_csv
	@echo ''
	@echo 'Done! (If that was too fast, consider clearing out data/* folders, particularly sentinels.)'

data/make-sentinels/convert_recs_to_csv: data/make-sentinels/compute_lda_recommendations data/make-sentinels/compute_tf_idf_recommendations
		make dbr dcTarget=convert_recs_to_csv

data/make-sentinels/compute_compare_recommendations: data/make-sentinels/compute_lda_recommendations data/make-sentinels/compute_tf_idf_recommendations
	make dbr dcTarget=compute_compare_recommendations

data/make-sentinels/compute_lda_recommendations: data/make-sentinels/compute_lda
	make dbr dcTarget=compute_lda_recommendations

data/make-sentinels/compute_lda: data/make-sentinels/compute_mallet_index
	make dbr dcTarget=compute_lda

data/make-sentinels/compute_mallet_index:
	make data/make-sentinels/compute_clean_and_normalize
	make data/make-sentinels/build_mallet
	make dbr dcTarget=compute_mallet_index

data/make-sentinels/compute_tf_idf_recommendations:
	make data/make-sentinels/compute_clean_and_normalize
	make dbr dcTarget=compute_tf_idf_recommendations

# Not really necessary to have sentinels for this, but might as well keep it for consistency and simplicity of code
data/make-sentinels/build_mallet: lib/mallet.git/lib/mallet.jar

lib/mallet.git/lib/mallet.jar:
	make dbr dcTarget=build_mallet

data/make-sentinels/compute_clean_and_normalize:
	make dbr dcTarget=compute_clean_and_normalize

# Helper: builds, runs, and leaves an indicator that it was run.
dbr:
	docker-compose build $(dcTarget)
	time docker-compose run $(dcTarget)
	touch data/make-sentinels/$(dcTarget)

reset:
	rm -rf data/make-sentinels/*

# Helpful when testing/debugging. You can force a module to always run by placing FORCE as one of its dependenciess
FORCE:
