RelRec - A Framework for Comparing Recommender Systems
----

# Setup + Running
1. Make sure that you have mallet source files in `lib/mallet.git/`. You can get them from https://github.com/mimno/Mallet.git.
2. Install the following prerequisites:
	1. docker
	2. docker-compose
	3. make
3. Run `make reset`
4. Obtain your data as separate flat files. Place them into `data/compute_get_raw_data/`. _For advanced users who need to obtain their data from MySQL or some other source and place them into individual files, you may make a module which does so and drops it into the folder._
5. If you have already cleaned and normalized your data, place the data also into `data/compute_clean_and_normalize/` and run `touch data/make-sentinels/compute_clean_and_normalize` to bypass the existing code which cleans and normalizes your files (and assumes your files are HTML).
6. Run `make thesis`
