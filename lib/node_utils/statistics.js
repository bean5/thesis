module.exports = {
	halt: function (error) {
		if(error)
			throw error;
		else
			throw 'Execution halted on purpose.';//alternatively: process.exit();
	},

	performDistributionSanityCheck: function(total) {
		if((total > 1 && total - 1 > 0.0000001) || (total < 1 && 1 - total > 0.0000001))
			throw 'Distribution is not a true distribution: ' + total;
	},

	computeEntropyDistribution: function(distribution) {
		var total = 0;
		for(var i = 0; i < distribution.length; i++) {
			if(distribution[i] === 0) continue;
				total += distribution[i] + Math.log(distribution[i]);
		}

		return -total/Math.log(10);
	},

	computeMidPoint: function(left, right) {
		if(left === undefined || right === undefined)
			common.halt('bad lengths 1');

		if(left.length != right.length)
			common.halt('bad lengths 2');

		var mid = new Array(left.length);
		for(var i = 0; i < left.length && i < right.length; i++) {
			mid[i] = (left[i] + right[i])/2;
		}
		return mid;//do not sort!
	},

	//return KLDivergence of two points in the probability simplex
	computeKLDivergence: function(leftDistribution, rightDistribution) {
		//console.log('test KL()');
		var total = 0;
		for(var i = 0; i < leftDistribution.length; i++) {
			for(var j = 0; j < rightDistribution.length; j++) {
				total += Math.log(leftDistribution[i]/rightDistribution[j]) * leftDistribution[i];
			}
		}
		if(total === 0)
			return 0;
		return -total;
	}
};
