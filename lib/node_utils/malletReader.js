var fs = require('fs');
var common = require('./common.js');
var newFormatDetected = false;

module.exports = {
	readInDistributions: function (filePath, maxDocs, cacheJSONVersion) {
		//If it is already JSON, calling this with a different value for process.env.allowed_file_name_extension may cause unintended consequences

		var documents = [];

		//TODO: Perform the line parsing in parallel OR make and check for cache and use that if meta matches.
		var input = fs.readFileSync(filePath, 'utf8', function (err, data) {
			if (err) common.halt(err);
		});
		if(filePath.indexOf('-cached.json') < 0) {//read in as if for the first time
			//console.log(input);

			//split on new lines
			input = input.split('\n');
			//var size = input.length;
			//console.log('# of topic document distributions: ' + size - 1);
			//console.log('input: ' + input[1]);

			//detect older versions and if it is older, skip first line
			var first_line = input[0];
			is_old_version = !!first_line.match(/doc name topic proportion/g);
			start_line = is_old_version ? 1 : 0;
			if(is_old_version) {
				console.warn("detected an old doc-topics.out header format. Going to have to skip first line.");
			} else {
				console.log("detected new doc-topics.out format. Not going to skip first line.");
			}

			//for each line:
			for(var i = start_line; i < input.length && (!maxDocs || i < maxDocs || maxDocs === 0); i++) {
				var line = input[i];
				if(line === "") break;//do nothing on last line, which might be empty

				var value = {
					mallet_id: parseInt(line.match(/^(\d+)/i)[0]),
					fileName: line.match(/file:(.*?)\t/g)[0].replace(/^file:/, '').trim(),
					topicDistribution: [],
					ignore: false
				};

				value.ignore = value.fileName.indexOf(process.env.allowed_file_name_extension) <= 0;

				//narrow line down to distribution
				line = line.replace(/.*?file:.*?\t/gi, '');
				// console.log(line);

				//instead of splitting, perhaps just traverse and collect
				var pairs = line.split('\t');

				// Floats and strings don't have the .isInteger() method. Integers have them, but casting it to an integer would make it be an integer (tautology), so it wouldn't be helpful to call at that point.
				var val = pairs[0];
				var is_int = ""+parseInt(val) == val+"";

				if(is_int) {
					if(newFormatDetected) {
						console.warn("Both old and new mallet format detected. Either an error exists in this parsing script or the doc-topics.out is inconsistently formatted.");
						process.exi();
					}
					for(var j = 0; j < pairs.length; j += 2) {//last iteration is space
						// console.log('pair a:' + pairs[j]);
						// console.log('pair b:' + pairs[j+1]);
						pairs[j] = {
							topicID: pairs[j],
							probability: pairs[j+1]
						};
						// console.log('');
						pairs[j].probability *= 1;//convert string to a float
						value.topicDistribution[pairs[j].topicID] = pairs[j].probability;
					}
					// console.log(pairs[0].topicID);
					// console.log(pairs[0].probability);
					// console.log(pairs[0]);
				} else {
					newFormatDetected = true;
					var distributions = {};
					for(var j = 0; j < pairs.length; j++) {//last iteration is space
						// console.log('pair a:' + pairs[j]);
						// console.log('pair b:' + pairs[j+1]);
						distributions[j] = {
							topicID: j,
							probability: pairs[j]
						};
						// console.log('');
						distributions[j].probability *= 1;//convert string to a float
						value.topicDistribution[distributions[j].topicID] = distributions[j].probability;
					}
					// console.log(distributions[0].topicID);
					// console.log(distributions[0].probability);
					// console.log(distributions[0]);
				}

				//get documentID
				value.documentID = (process.env.obtain_id_from_filename && !value.ignore) ? parseInt(value.fileName.match(/(\d+)(\.html|\.txt)/i)[1]) : 0;
				// console.log('documentID: %d', value.documentID);halt();
				// console.log('mallet_id: %d', value.mallet_id);halt();
				// console.log('fileName: %s', value.fileName);halt();
				// if(value.mallet_id != i)
				// 	console.log('mallet_id != i: %d %d', value.mallet_id, i);
				// console.log(value);
				documents.push(value);
			}
			if(newFormatDetected) {
				console.log("detected new doc-topics.out proportion list format");
			} else {
				console.warn("detected old doc-topics.out proportion list format");
			}

			if(cacheJSONVersion) {//store parsed version
				var convertedFilePath = common.replaceFileExtension(filePath, /\..{4}$/, '-cached.json');
				fs.writeFile(convertedFilePath, JSON.stringify(documents), function(err) {
					if(err) {
						console.log(err);
					}
				});
			}
		} else {
			//eval version (since we trust our data to be just data, eval is more efficient)
			documents = eval(input);
		}

		//console.log('# of topic document distributions: ' + input.length - 1);
		return documents;
	},

	getTopicWithKeywords: function(filePath, topics) {
		var topicMetaData = fs.readFileSync(filePath, 'utf8', function (err, data) {
			if (err) common.halt(err);
		});
		var lines = topicMetaData.split('\n');

		var topicKeyWords = [];
		for(var i = 0; i < lines.length; i++) {
			if(lines[i].trim() === '') break;
			if(topics[i] === undefined) common.halt('should be defined'); //Deprecating: Having this is only sometimes helpful.
				topicKeyWords.push(lines[i].split('\t')[2].split(' '));
		}

		//console.log(topicKeyWords);

		return topicKeyWords;
	}
};
