var fs = require('fs');
var common = require('./common.js');
module.exports = {
    getDocumentMetaData: function(filePath, cacheJSONVersion) {
        var documentMetaData = fs.readFileSync(filePath, 'utf8', function(err, data) {
            if (err) common.halt(err);
        });

        if (filePath.indexOf('-cached.json') > 0) {
            documentMetaData = JSON.parse(documentMetaData);
        } else {
            documentMetaData = eval(documentMetaData.replace(/\|/g, ','));
            if (cacheJSONVersion) {
                var convertedFilePath = common.replaceFileExtension(filePath, /\..{4}$/, '-cached.json');
                fs.writeFile(convertedFilePath, JSON.stringify(documentMetaData), function(err) {
                    if (err) common.halt(err);
                });
            }
        }

        return documentMetaData;
    },

    augmentWithDocSize: function(distributions, filePath, skip) {
        var docSizes = fs.readFileSync(filePath, 'utf8', function(err, data) {
            if (err) {
                halt(err);
            }
        });

        var lines = docSizes.split('\n');

        for (var i = 0; i < lines.length; i++) {
            //Legacy: had to skip some indices
            if (distributions[i] === undefined && !!skip) continue;
            distributions[i].docLength = lines[i].split('\t')[1] * 1;
        }

        return distributions;
    }
};
