module.exports = {
	halt: function (error) {
		if(error)
			throw error;
		else
			// alternatively: process.exit();
			throw 'Execution halted on purpose.';
	},

	obtain_numerics_from_filename: function(fileName) {
		return fileName.replace(/\D/g, '');
	},

	convertTitle: function(title) {
		return title.replace(/&.*?;/g,'');
	},

	replaceFileExtension: function(fileName, regex, replace) {
		return fileName.replace(regex, replace);
	},

	//Discards anything beyond the 'Best' k
	keepBestK: function(similaritiesForCurrent, k, reverse) {
		var reverseMap = {};
		var scores = [];
		this.each(similaritiesForCurrent, function(fileName, score) {
			// score is key to filename
			reverseMap[score] = fileName;
			scores.push(score);
		});

		//sort
		scores = scores.sort();
		if(reverse) scores = scores.reverse();

		var trimmedMap = {};
		for(var i = 0; i < k && i < scores.length; i++) {
			// keep only top k
			trimmedMap[reverseMap[scores[i]]] = scores[i];
		}

		return trimmedMap;
	},

	// Similary to jQuery.each(), but slimmed down--just what is needed
	each: function(table, func) {
		var keys = Object.keys(table);

		for(var i = 0; i < keys.length; i++) {
			if(!table.hasOwnProperty(keys[i])) continue;
			func(keys[i], table[keys[i]]);
		}
	}
};
