//globals
{
    var fs = require('fs');

    var settings = {
        testing: process.env.testing === 'true' ? true : false, //enable to loop over fewer documents and fewer words
        input: '/input/', //Must be local when used in current virtualbox environment, otherwise it appears to not exist even when it does.
        output: '/output/',
        // due to memory constraints, use no more than 100 or this will likely crash.
        number_of_recommendations: parseInt(process.env.number_of_recommendations, 10),
        // allowing documents to recommend themselves is a sanity check (assert that the recommend themselves as being most related)
        allow_self_recommendation: process.env.allow_self_recommendation === 'true' ? true : false
    };

    var common = require('/lib/node_utils/common.js');

    // Paths to files
    var paths = {
        status: settings.output + 'status.txt',
        settings: settings.output + 'settings.json',
        debug: settings.output + 'debug.txt',
        recommendations: settings.output + 'recommendations.json',
        TFIDFMatrix: settings.output + 'tf-idf-matrix.json',
    };

    // Streams to basically all files except status since that is written syncronously
    var streams = {
        settings: fs.createWriteStream(paths.settings),
        debug: fs.createWriteStream(paths.debug),
        // recommendations: fs.createWriteStream(paths.recommendations),
        TFIDFMatrix: fs.createWriteStream(paths.TFIDFMatrix),
    };
    streams.settings.write(JSON.stringify({
        settings: settings,
        paths: paths
    }, null, '\t'));

    // These are to be computed
    var documents = [];
    var tf_idf_matrix = {};
    var recommendations = {};

    if (settings.testing) fs.appendFileSync(streams.todo, 'TODO: Do not run in test mode!');
}

//main
{
    fs.writeFileSync(paths.status, '\nComputing: Read in documents');
    readInDocuments(settings.input);

    fs.appendFileSync(paths.status, '\nComputing: TF-IDF matrix');
    computeTFIDFMatrix();
    fs.appendFileSync(paths.status, '\nComputing: write TF-IDF matrix to file');

	// On large corpora, this fails
	try {
	    streams.TFIDFMatrix.write(JSON.stringify({
	        tf_idf_matrix: tf_idf_matrix
	    }, null, '\t'));
	} catch(e) {
		console.warn("tf-idf matrix was too large to write to file. skipping")
	}

    fs.appendFileSync(paths.status, '\nComputing: recommendations');
    computeRecommendations(settings.number_of_recommendations);

    fs.appendFileSync(paths.status, '\nComputing: write recommendations to file');
    streams.recommendations.write(JSON.stringify(recommendations, null, '\t'));
    // This version will crash since it converts to string inefficiently
    // streams.recommendations.write(recommendations);

    fs.appendFileSync(paths.status, '\nComputing: close streams');

    common.each(streams, function(name, stream) {
        stream.end();
    });

    fs.appendFileSync(paths.status, '\nComputing: done!');
    return;
}

function readInDocuments(location) {
    var files = fs.readdirSync(location);

    var tokens = [];
    for (var i = 0; i < files.length && (!settings.testing || i < 100); i++) {
        tokens = readIn(files[i]).trim().split(' ');
        if (settings.testing) tokens = tokens.slice(0, 250);

        documents.push({
            fileName: location + files[i],
            uniqueWords: {},
            tokens: tokens,
            tokenCount: tokens.length
        });
    }
}

function computeTFIDFMatrix() {
    tf_idf_matrix = {
        D: documents.length, //count of docs
        words: {}, //contains each word, count of documents it appears in, and occurrences per document
        N: 0, //count of tokens
    };

    for (var i = 0; i < tf_idf_matrix.D; i++) {
        var fileName = documents[i].fileName;
        var tokens = documents[i].tokens;
        tf_idf_matrix.N += tokens.length;

        //for each word in doc
        for (var j = 0; j < tokens.length; j++) {
            var token = tokens[j];
            if (typeof tf_idf_matrix.words[token] === 'undefined') {
                tf_idf_matrix.words[token] = {
                    documents: {},
                    idf: 0, //must be computed last of all (probably next loop)
                };
            }
            if (typeof tf_idf_matrix.words[token].documents[fileName] === 'undefined') {
                tf_idf_matrix.words[token].documents[fileName] = {
                    count: 0,
                    tf: 0,
                    tf_idf: 0, //must be computed in next loop last of all
                };

                documents[i].uniqueWords[token] = 0;

                //(re)cache number of documents in which the word exists
                tf_idf_matrix.words[token].count = Object.keys(tf_idf_matrix.words[token].documents).length;

                tf_idf_matrix.words[token].idf = Math.log(tf_idf_matrix.N / (tf_idf_matrix.words[token].count + 1));
            }
            documents[i].uniqueWords[token]++;

            tf_idf_matrix.words[token].documents[fileName].count++;

            //recompute tf
            tf_idf_matrix.words[token].documents[fileName].tf = tf_idf_matrix.words[token].documents[fileName].count / tokens.length;
        }
        delete documents[i].tokens;
    }
    fs.appendFileSync(paths.status, '\nfirst portion computed');

    //No need to make empty cells (we'll just check for existence later instead)
    var firstDoc = true;
    common.each(tf_idf_matrix.words, function(wordTypeKey, wordInfo) {
        common.each(wordInfo.documents, function(fileName, wordInfoInDoc) {
            tf_idf_matrix.words[wordTypeKey].documents[fileName].tf_idf = tf_idf_matrix.words[wordTypeKey].idf * tf_idf_matrix.words[wordTypeKey].documents[fileName].tf;

            if (firstDoc && settings.testing) {
                streams.debug.write('\n\tdoc: ' + fileName);
                streams.debug.write('\n\tdocWordInfo: ' + wordInfoInDoc.count);
                streams.debug.write('\n\tdocWordInfo: ' + (wordInfoInDoc.count / wordInfoInDoc.tf));
                streams.debug.write('\n\tdocWordInfo: ' + wordInfoInDoc.tf);
                streams.debug.write('\n\tdocWordInfo: ' + wordInfoInDoc.tf_idf);
                streams.debug.write('\n\tcorpusWordInfo: ' + tf_idf_matrix.words[wordTypeKey].idf);
            }
        });
        firstDoc = false;
    });
    tf_idf_matrix.documents = documents;
    fs.appendFileSync(paths.status, '\nsecond portion computed');

    // Avoid concatentation with the long stringified--causes crash
    fs.writeFileSync(paths.debug, '\n');
    streams.debug.write(JSON.stringify(tf_idf_matrix.documents, null, '\t'));

    // Avoid concatentation with the long stringified--causes crash
    // fs.writeFileSync(paths.debug, '\n');
    // streams.debug.write(JSON.stringify(tf_idf_matrix.words, null, '\t'));
    streams.debug.write('\n' + tf_idf_matrix.N, null, '\t');
}

/*
 * http://en.wikipedia.org/wiki/Vector_space_model
 *     The vector space model has the following advantages over the Standard Boolean model:
 *
 *    Simple model based on linear algebra
 *    Term weights not binary
 *    Allows computing a continuous degree of similarity between queries and documents
 *    Allows ranking documents according to their possible relevance
 *    Allows partial matching
 *
 *    Important note: The formula online implies that every document contain at least 1 word. Although that seems obvious, at times a document ends up blank simply because data is noisy (or because the dataset is incomplete). In such cases, any document that is empty is considered to be orthogal to all other documents, except those that are also empty, in which case they are considered to be equivalent, with a similarity of 1.
 * This might address it a bit: http://www.p-value.info/2013/02/when-tfidf-and-cosine-similarity-fail.html
*/
function computeRecommendations(k) {
    recommendations = {};

	// rather than call common.each() so many times (especially for inner loop), we'll collect the keys one time and use them.
    var keys = Object.keys(documents);
    for(var i = 0; i < keys.length; i++) {
        if(!documents.hasOwnProperty(keys[i])) continue;
        document1 = documents[keys[i]];

        if (i % 50 === 0)
            fs.appendFileSync(paths.status, '\nNow at i-position ' + i + ' of ' + documents.length);

        var similaritiesForCurrent = {};

        for(var j = 0; j < keys.length; j++) {
            if(!documents.hasOwnProperty(keys[j])) continue;

            if (i == j && !settings.allow_self_recommendation) continue;
            document2 = documents[keys[j]];

            if (i % 50 === 0 && j % 100 === 0)
                fs.appendFileSync(paths.status, '\n\tNow at j-position ' + j + ' of ' + documents.length);

            if (recommendations[document1.fileName] === undefined)
                recommendations[document1.fileName] = {};

            var similarity = 0;
            if (recommendations[document2.fileName] !== undefined && recommendations[document2.fileName][document1.fileName] !== undefined) {
                //use cached value that was apparently among those that were not discarded for document2
                similarity = recommendations[document2.fileName][document1.fileName];
            } else {
                similarity = computeCosineSimilarity(document1, document2);
            }
            similaritiesForCurrent[document2.fileName] = similarity;
        }

        //Keep only the top k, making sure that low means high (reverse set to true)
        recommendations[document1.fileName] = common.keepBestK(similaritiesForCurrent, k, true); //make sure to pass conistent number when testing
    }
}

// Computes cosine similarity
// There is a decent article on it at https://janav.wordpress.com/2013/10/27/tf-idf-and-cosine-similarity/
// Wikipedia probably has a good article as well
function computeCosineSimilarity(document1, document2) {
    //for each word in document1 that is also in document2:
    var score = dotProduct(document1, document2);

    //avoid div 0 issue
    if (score === 0) return 0;

    var norm1 = norm(document1);
    var norm2 = norm(document2);

    return score / (norm1 * norm2);
}

function dotProduct(document1, document2) {
    var dotProduct = 0;

    //for each word that is in both documents
    common.each(document1.uniqueWords, function(word, count) {
        if (document2.uniqueWords[word] === undefined) return;

        dotProduct += tf_idf_matrix.words[word].documents[document1.fileName].tf_idf * tf_idf_matrix.words[word].documents[document2.fileName].tf_idf;
    });
    return dotProduct;
}

function norm(document) {
    if (typeof document.norm !== 'undefined') {
        // return value--it was already computed once.
        return document.norm;
    }

    var sum = 0;
    common.each(document.uniqueWords, function(word, count) {
        sum += tf_idf_matrix.words[word].documents[document.fileName].tf_idf;
    });
    var norm = Math.sqrt(sum);

    document.norm = norm;
    return norm;
}

function readIn(fileName) {
    return fs.readFileSync(settings.input + fileName, 'utf8');
}
